<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Header Char";
	margin:0cm;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
	{margin:0cm;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-link:Header;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:36.0pt 36.0pt 36.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=EN-IN link=blue vlink=purple style='word-wrap:break-word'>

<div class=WordSection1>

<p class=MsoHeader align=right style='text-align:right'><img width=106
height=107 src="https://kswcrc.com/images/image001.jpg" align=left hspace=12><b><span
lang=EN-US style='font-size:28.0pt'>First Affiliated Insurance</span></b><span
lang=EN-US><br>
</span><span lang=EN-US style='font-size:12.0pt'>1350 E College Ave, Suite 2,�
State College, PA� 16801-6811</span></p>

<p class=MsoHeader align=right style='text-align:right'><span lang=EN-US>(814)
867-2095������ �������������������������������<a
href="mailto:${user}@firstaffiliated.com">${user}@firstaffiliated.com</a></span></p>

<p class=MsoHeader align=right style='text-align:right'><span lang=EN-US
style='color:#002060'>_________________________________________________________________________</span></p>

<p class=MsoHeader><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNoSpacing align=center style='text-align:center'><b><span
lang=EN-US style='font-size:20.0pt'>${WC_EMPLOYER}</span></b></p>

<p class=MsoNoSpacing align=center style='text-align:center'><i><span
lang=EN-US style='font-size:12.0pt'>${WC_STREET}</span></i></p>

<p class=MsoNoSpacing align=center style='text-align:center'><i><span
lang=EN-US style='font-size:12.0pt'>${WC_CITY}, PA �${WC_ZIP}</span></i></p>

<p class=MsoNoSpacing align=center style='text-align:center'><i><span
lang=EN-US style='font-size:14.0pt'>&nbsp;</span></i></p>

<p class=MsoNoSpacing><i><span lang=EN-US style='font-size:12.0pt'>Public
workers� compensation data<sup>1</sup> shows that your base rate of <span
style='background:yellow'>$<b>${WC_RATE}</b></span> is <b><span style='background:
yellow'>${WC_C1PCT}%</span></b> higher than our carrier�s base rate of <span
style='background:yellow'>$<b>${WC_CMP1}</b></span>.� With your experience rating of
${WC_MM}%, there�s a good chance we can save you money!</span></i></p>

<table style="margin-left: auto; margin-right: auto;">
<tr>
	<td>Your Current WC Insurer:</td>
	<td>${WC_CARRIER}</td>
</tr>
<tr>
	<td>Your Class Code:</td>
	<td>${WC_GCC} (${WC_DESC})</td>
</tr>
<tr>
	<td>Your Renewal Date:</td>
	<td>${WC_XDATE}</td>
</tr>
<tr>
	<td><b>Your Insurer�s Base Rate:</b></td>
	<td><b>${WC_RATE} </b>(per $100 payroll)</td>
</tr>
<tr>
	<td><b>Our Insurer�s Base Rate<sup>2</sup></b>:</td>
	<td><b>${WC_CMP1} </b>(a ${WC_C1PCT}% savings!)</td>
</tr>
<tr>
	<td><b>Our Insurer�s Preferred Rate<sup>2</sup></b>:</td>
	<td><b>${WC_CMP2} </b>(a ${WC_C2PCT}% savings!)</td>
</tr>
</table>

</br></br>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%'>Give us a call for a quote <u>on ALL
of your commercial insurance</u><sup>3</sup></span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><u><span
lang=EN-US style='font-size:1.0pt;line-height:115%'><span style='text-decoration:
 none'>&nbsp;</span></span></u></b></p>

<p class=MsoNormal align=center style='text-align:center'><img width=43
height=43 src="https://kswcrc.com/images/image002.png" align=left hspace=12><b><span
lang=EN-US style='font-size:10.0pt;line-height:115%'>** IMPORTANT TIP **�� </span></b><span
lang=EN-US style='font-size:8.0pt;line-height:115%'>You can SAVE MONEY even
without getting a quote.�� Requesting a �5-Year Loss Report� from your current
agent will <u>guarantee</u> that you get the best renewal premium possible if
your agent thinks you�re shopping around.� Then come to us to compare!</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%'><br>
<br>
</span></p>

<p class=MsoNormal><b><i><u><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#C00000;background:lightgrey'>FIRST</span></u></i></b><b><i><u><span
lang=EN-US style='font-size:12.0pt;line-height:115%;background:lightgrey'>
AFFILIATED</span></u></i></b><b><i><span lang=EN-US style='font-size:15.0pt;
line-height:115%;background:lightgrey'> </span></i></b><b><i><span lang=EN-US
style='font-size:10.0pt;line-height:115%;background:lightgrey'>is your <u><span
style='color:#C00000'>FIRST</span></u><span style='color:#C00000'> </span>choice
for commercial insurance in Central PA!</span></i></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><i><span
lang=EN-US style='font-size:10.0pt;line-height:115%;background:lightgrey'>Our
primary insurer<sup>4</sup> is the <span style='color:#C00000'>#1 commercial
insurer</span> in Pennsylvania according to A.M. Best.</span></i></b></p>

<p class=MsoNormal><i><sup><span lang=EN-US style='font-size:10.0pt;line-height:
115%'><br>
</span></sup></i><i><sup><span lang=EN-US style='font-size:6.0pt;line-height:
115%'>1</span></sup></i><span lang=EN-US style='font-size:6.0pt;line-height:
115%'> Worker�s Compensation data is public information, accessible on
Pennsylvania�s website and on PCRB�s (Pennsylvania Compensation Rating Bureau)
website.��� &quot;http://www.dli.pa.gov&quot;�� and��
&quot;http://www.pcrb.com&quot;</span></p>

<p class=MsoNormal><i><sup><span lang=EN-US style='font-size:6.0pt;line-height:
115%'>2</span></sup></i><span lang=EN-US style='font-size:6.0pt;line-height:
115%'> This letter is not an offer of coverage.� Your business must qualify for
our insurance carrier�s base or preferred rates, which depends on your loss
history and other underwriting factors.� A 5-year loss report for all
commercial policies, available from your current carrier(s), is typically
required. </span></p>

<p class=MsoNormal><i><sup><span lang=EN-US style='font-size:6.0pt;line-height:
115%'>3</span></sup></i><span lang=EN-US style='font-size:6.0pt;line-height:
115%'> Our primary insurer will not write only the Workers Comp policy on a
business.� They require at least one of the underlying business policies.</span></p>

<p class=MsoNormal><i><sup><span lang=EN-US style='font-size:6.0pt;line-height:
115%'>4 </span></sup></i><span lang=EN-US style='font-size:6.0pt;line-height:
115%'>Our primary insurer discourages the use of their name in conjunction with
explicit rates in advertising material � even though it�s public data!</span></p>

<p class=MsoNormal><span lang=EN-US style='font-size:8.0pt;line-height:115%'>&nbsp;</span></p>

</div>

</body>

</html>
<div style="page-break-after: always;"></div>

