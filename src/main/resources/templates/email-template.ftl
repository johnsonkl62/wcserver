<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Solicit Email</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- use the font -->
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            font-size: 12px;
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">

<p>
Did you know that in ${county} county

<#if zips??>
in zip codes ${zips}  
</#if>

there are ${total} employers in desirable class codes with a
filed manual rate greater than Erie Insurance Exchange?
</p>
<p>
Every commercial producer uses PCRB and DLI to pre-qualify businesses, but how long would it take you to find the 10 
employers in ${county} county with the highest percentage of potential savings versus Erie Insurance Exchange rates?</p>
<p>You would need to get a list of all employers, and then look up the carrier for each one on DLI, then 
cross-reference that carrier's filed rate for that employer's class code obtained from PCRB to Erie's filed rates 
for the same class code.  Calculate the percentage difference for each employer and sort in descending order!
That's a lot of time.</p>

<br/>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" style="border-collapse: collapse;">
		<tr>
			<th align="left">Code</th>
			<th align="left">Description</th>
			<th align="left">Count</th>
		</tr>        
		<#list solicitBeans as value>
		<tr>
				<td>${value.code}</td>
				<td>${value.text}</td>
				<td>${value.count}</td>
		</tr>
		</#list>
    </table>
<br/>
<br/>

<p>Our competitors provide great search functions, but don't eliminate the most time-consuming aspect -- prioritizing and targeting
the right businesses based on potential savings.  With KSWCRC, you don't even have to log in!  We send you a weekly
email of desirable target employers with prime class codes renewing eight weeks in the future WITH a pre-generated solicitation letter in a format of your choice.
</p>
<p>
These are perfect candidates for commercial solicitation!<br/>
Get automatic weekly solicitation letters for employers renewing eight weeks out, <br/>
that are prime class codes and that you know Erie is competitive.<br/>
Talk to your DSM or Branch Manager about using market share dollars for purchase or subscription.
</p>
<br/>

Visit <a href="www.kswcrc.com">www.kswcrc.com</a> or email kjohnson@synesis-inc.com or call me at (570) 295-0095.

<br/>
<br/>

=======
Approved for Market Share by several Branches Managers.  Ask your DSM for verification.  
</p>
<p>Visit us at <a href="www.kswcrc.com">www.kswcrc.com</a> or email kjohnson@synesis-inc.com or call me at (570) 295-0095 for more information or a personal demo.
</p>
<br/>
Kevin L Johnson
<br/>
<br/> 
To unsubscribe, click <a href='https://kswcrc.com/unSubscribe?token=${unsubscribe}'>"here"</a>
<br/>
<br/>

</body>
</html>