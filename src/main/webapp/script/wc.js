$(document).ready(function(e){
	$("#carriers").change(function(e){
    	$('#selectCarrier').empty();
    	$("select#carriers option:selected").each(function(){
        	$("#selectCarrier").append("<option value='" + $(this).val() + "'>" + $(this).text() + "</option>");
    	});
	});
});

function toggle() {
  var checkboxes = document.getElementsByClassName('selector');
  var mastercontrol = document.getElementById('master');
  for (var i in checkboxes) {
	checkboxes[i].checked = mastercontrol.checked;	  
  }
}

function changPasswordFunction(formName) {
    var pass1 = document.getElementById("newPassword").value;
    var pass2 = document.getElementById("reTypeNewPassword").value;
    
    if (pass1 != pass2) {
        //alert("Passwords Do not match");
        document.getElementById("newPassword").style.borderColor = "#E34234";
        document.getElementById("reTypeNewPassword").style.borderColor = "#E34234";
        alert("Passwords DO NOT Match!!!");
        return false;
    }
    
    document.getElementById(formName).submit();
    
    return true;
}

function resetPasswordFunction(formName) {
    var pass1 = document.getElementById("pwd").value;
    var pass2 = document.getElementById("confirm_password").value;
    
    if (pass1 != pass2) {
        //alert("Passwords Do not match");
        document.getElementById("pwd").style.borderColor = "#E34234";
        document.getElementById("confirm_password").style.borderColor = "#E34234";
        alert("Passwords DO NOT Match!!!");
        return false;
    }
    
    document.getElementById(formName).submit();
    
    return true;
}

function myFunction(formName) {
    var pass1 = document.getElementById("pwd").value;
    var pass2 = document.getElementById("confirm_password").value;
    var carriers = document.getElementById("carriers");
    var selectCarrier = document.getElementById("selectCarrier");
    if (pass1 != pass2) {
        //alert("Passwords Do not match");
        document.getElementById("pwd").style.borderColor = "#E34234";
        document.getElementById("confirm_password").style.borderColor = "#E34234";
        alert("Passwords DO NOT Match!!!");
    } /*else if(getSelectValues(carriers).length > 3) {
    	alert("Please select 3 carriers only!");
    }*/ else if (getSelectValues(carriers).length < 1) {
    	alert("Please select at least one carrier.");
    } else {
    	selectAllValues();
    	document.getElementById(formName).submit();
    }
    
    return true;
}

function selectAllValues() {
	
	 $("#selectCarrier").find('option').attr('selected',true);
}

function getSelectValues(select) {
	  var result = [];
	  var options = select && select.options;
	  var opt;

	  for (var i=0, iLen=options.length; i<iLen; i++) {
	    opt = options[i];

	    if (opt.selected) {
	      result.push(opt.value || opt.text);
	    }
	  }
	  return result;
	}

function submitForm(fmt) {
	var opt = document.getElementById('format');
	opt.value = fmt;
	document.getElementById('wcform').action= "downloadFile";
	document.getElementById('wcform').submit();
}

function checkCounties() {
	console.log('Check counties...');

	// Get the value of the input field being submitted
	var countyList = document.getElementById("counties").value;
	console.log('countyList = ' + countyList);
	var counties = countyList.split(',');
	
	var misspelled = '';
	for ( i = 0; i < counties.length; i++ ) {
		console.log('   county: ' + counties[i]);
		if ( !validCounty(counties[i].trim()) ) {
			console.log('   invalid: ' + counties[i]);
			if ( misspelled.length > 0 ) {
				misspelled += ',';
			}
			misspelled += counties[i];
		}
	}
	console.log('Invalid: ' + misspelled);

	if (misspelled != '') {
	    document.getElementById('errorField').style.color = 'red';
	    document.getElementById('errorField').innerHTML = 'Check spelling of counties: ' + misspelled;
	    document.getElementById("counties").style.color='red';
	} else {
	    document.getElementById('errorField').style.color = 'black';
	    document.getElementById('errorField').innerHTML  = '**Use commas to separate multiple values';
	    document.getElementById("counties").style.color='black';
	}
	return true;
}

function checkCodes() {
	console.log('Check codes...');

	// Get the value of the input field being submitted
	var codeList = document.getElementById("gccs").value;
	console.log('codeList = ' + countyList);
	var codes = codeList.split(',');
	
	var nonnumeric = '';
	for ( i = 0; i < codes.length; i++ ) {
		console.log('   code: ' + codes[i]);
		if ( /^[0-9]{1,4}$/.test(codes[i]) ) {
			console.log('   invalid: ' + codes[i]);
			if ( nonnumeric.length > 0 ) {
				nonnumeric += ',';
			}
			nonnumeric += codes[i];
		}
	}
	console.log('Invalid: ' + nonnumeric);

	if (nonnumeric != '') {
	    document.getElementById('errorField').style.color = 'red';
	    document.getElementById('errorField').innerHTML = 'Check codes: ' + nonnumeric;
	    document.getElementById("gccs").style.color='red';
	} else {
	    document.getElementById('errorField').style.color = 'black';
	    document.getElementById('errorField').innerHTML  = '**Use commas to separate multiple values';
	    document.getElementById("gccs").style.color='black';
	}
	return true;
}

function validCounty(county) {
	var county = county.toUpperCase();
	return ( county == 'ADAMS' || county == 'ALLEGHENY' || county == 'ARMSTRONG' || county == 'BEAVER' ||
			county == 'BEDFORD' || county == 'BERKS' || county == 'BLAIR' || county == 'BRADFORD' ||
			county == 'BUCKS' || county == 'BUTLER' || county == 'CAMBRIA' || county == 'CAMERON' ||
			county == 'CARBON' || county == 'CENTRE' || county == 'CHESTER' || county == 'CLARION' ||
			county == 'CLEARFIELD' || county == 'CLINTON' || county == 'COLUMBIA' || county == 'CRAWFORD' ||
			county == 'CUMBERLAND' || county == 'DAUPHIN' || county == 'DELAWARE' || county == 'ELK' ||
			county == 'ERIE' || county == 'FAYETTE' || county == 'FOREST' || county == 'FRANKLIN' ||
			county == 'FULTON' || county == 'GREENE' || county == 'HUNTINGDON' || county == 'INDIANA' ||
			county == 'JEFFERSON' || county == 'JUNIATA' || county == 'LACKAWANNA' || county == 'LANCASTER' ||
			county == 'LAWRENCE' || county == 'LEBANON' || county == 'LEHIGH' || county == 'LUZERNE' ||
			county == 'LYCOMING' || county == 'MCKEAN' || county == 'MERCER' || county == 'MIFFLIN' ||
			county == 'MONROE' || county == 'MONTGOMERY' || county == 'MONTOUR' || county == 'NORTHAMPTON' ||
			county == 'NORTHUMBERLAND' || county == 'PERRY' || county == 'PHILADELPHIA' || county == 'PIKE' ||
			county == 'POTTER' || county == 'SCHUYLKILL' || county == 'SNYDER' || county == 'SOMERSET' ||
			county == 'SULLIVAN' || county == 'SUSQUEHANNA' || county == 'TIOGA' || county == 'UNION' ||
			county == 'VENANGO' || county == 'WARREN' || county == 'WASHINGTON' || county == 'WAYNE' ||
			county == 'WESTMORELAND' || county == 'WYOMING' || county == 'YORK' || county == 'ALL' );
}


function labelCheck(county) {
	document.getElementById(county).checked = !document.getElementById(county).checked;
	return true;
}


function setCounties() {

	var cValue = "";

	var countyCbxs = document.getElementsByClassName('COUNTY');
	for ( i=0; i<countyCbxs.length; i++ ) {
		if ( countyCbxs[i].checked ) {
			if ( cValue != "" ) {
				cValue += ",";
			}
			cValue += countyCbxs[i].id;
		}
	}
	document.getElementById('counties').value = cValue;
}

function selectAll() {
	var countyCbxs = document.getElementsByClassName('COUNTY');
	for ( i=0; i<countyCbxs.length; i++ ) {
		if ( countyCbxs[i].value != 'NONE') {
			countyCbxs[i].checked = TRUE;
		}
	}
	document.getElementById('counties').value = "";
}

function clearAll() {
	var countyCbxs = document.getElementsByClassName('COUNTY');
	for ( i=0; i<countyCbxs.length; i++ ) {
		countyCbxs[i].checked = FALSE;
	}
	document.getElementById('counties').value = "";
}



function checkCounties() {
	console.log('Check counties...');

	// Get the value of the input field being submitted
	var countyList = document.getElementById("counties").value;
	console.log('countyList = ' + countyList);
	var counties = countyList.split(',');
	
	var misspelled = '';
	for ( i = 0; i < counties.length; i++ ) {
		console.log('   county: ' + counties[i]);
		if ( !validCounty(counties[i].trim()) ) {
			console.log('   invalid: ' + counties[i]);
			if ( misspelled.length > 0 ) {
				misspelled += ',';
			}
			misspelled += counties[i];
		}
	}
	console.log('Invalid: ' + misspelled);

	if (misspelled != '') {
	    document.getElementById('errorField').style.color = 'red';
	    document.getElementById('errorField').innerHTML = 'Check spelling of counties: ' + misspelled;
	    document.getElementById("counties").style.color='red';
	} else {
	    document.getElementById('errorField').style.color = 'black';
	    document.getElementById('errorField').innerHTML  = '**Use commas to separate multiple values';
	    document.getElementById("counties").style.color='black';
	}
	return true;
}

function checkCodes() {
	console.log('Check codes...');

	// Get the value of the input field being submitted
	var codeList = document.getElementById("gccs").value;
	console.log('codeList = ' + countyList);
	var codes = codeList.split(',');
	
	var nonnumeric = '';
	for ( i = 0; i < codes.length; i++ ) {
		console.log('   code: ' + codes[i]);
		if ( /^[0-9]{1,4}$/.test(codes[i]) ) {
			console.log('   invalid: ' + codes[i]);
			if ( nonnumeric.length > 0 ) {
				nonnumeric += ',';
			}
			nonnumeric += codes[i];
		}
	}
	console.log('Invalid: ' + nonnumeric);

	if (nonnumeric != '') {
	    document.getElementById('errorField').style.color = 'red';
	    document.getElementById('errorField').innerHTML = 'Check codes: ' + nonnumeric;
	    document.getElementById("gccs").style.color='red';
	} else {
	    document.getElementById('errorField').style.color = 'black';
	    document.getElementById('errorField').innerHTML  = '**Use commas to separate multiple values';
	    document.getElementById("gccs").style.color='black';
	}
	return true;
}

function validCounty(county) {
	var county = county.toUpperCase();
	return ( county == 'ADAMS' || county == 'ALLEGHENY' || county == 'ARMSTRONG' || county == 'BEAVER' ||
			county == 'BEDFORD' || county == 'BERKS' || county == 'BLAIR' || county == 'BRADFORD' ||
			county == 'BUCKS' || county == 'BUTLER' || county == 'CAMBRIA' || county == 'CAMERON' ||
			county == 'CARBON' || county == 'CENTRE' || county == 'CHESTER' || county == 'CLARION' ||
			county == 'CLEARFIELD' || county == 'CLINTON' || county == 'COLUMBIA' || county == 'CRAWFORD' ||
			county == 'CUMBERLAND' || county == 'DAUPHIN' || county == 'DELAWARE' || county == 'ELK' ||
			county == 'ERIE' || county == 'FAYETTE' || county == 'FOREST' || county == 'FRANKLIN' ||
			county == 'FULTON' || county == 'GREENE' || county == 'HUNTINGDON' || county == 'INDIANA' ||
			county == 'JEFFERSON' || county == 'JUNIATA' || county == 'LACKAWANNA' || county == 'LANCASTER' ||
			county == 'LAWRENCE' || county == 'LEBANON' || county == 'LEHIGH' || county == 'LUZERNE' ||
			county == 'LYCOMING' || county == 'MCKEAN' || county == 'MERCER' || county == 'MIFFLIN' ||
			county == 'MONROE' || county == 'MONTGOMERY' || county == 'MONTOUR' || county == 'NORTHAMPTON' ||
			county == 'NORTHUMBERLAND' || county == 'PERRY' || county == 'PHILADELPHIA' || county == 'PIKE' ||
			county == 'POTTER' || county == 'SCHUYLKILL' || county == 'SNYDER' || county == 'SOMERSET' ||
			county == 'SULLIVAN' || county == 'SUSQUEHANNA' || county == 'TIOGA' || county == 'UNION' ||
			county == 'VENANGO' || county == 'WARREN' || county == 'WASHINGTON' || county == 'WAYNE' ||
			county == 'WESTMORELAND' || county == 'WYOMING' || county == 'YORK' );
}


function labelCheck(county) {
	document.getElementById(county).checked = !document.getElementById(county).checked;
	return true;
}


function setCounties() {

	var cValue = "";

	var countyCbxs = document.getElementsByClassName('COUNTY');
	for ( i=0; i<countyCbxs.length; i++ ) {
		if ( countyCbxs[i].checked ) {
			if ( cValue != "" ) {
				cValue += ",";
			}
			cValue += countyCbxs[i].id;
		}
	}
	document.getElementById('counties').value = cValue;
}

function selectAll() {
	var countyCbxs = document.getElementsByClassName('COUNTY');
	for ( i=0; i<countyCbxs.length; i++ ) {
		if ( countyCbxs[i].value != 'NONE') {
			countyCbxs[i].checked = TRUE;
		}
	}
	document.getElementById('counties').value = "";
}

function clearAll() {
	var countyCbxs = document.getElementsByClassName('COUNTY');
	for ( i=0; i<countyCbxs.length; i++ ) {
		countyCbxs[i].checked = FALSE;
	}
	document.getElementById('counties').value = "";
}

function setPct(value) {
	if ( value=='All' ) {
		var rows = document.getElementsByClassName("countyRows");
		for ( i=0; i<rows.length; i++) {
			var cells = rows[i].getElementsByClassName("totals");
			var pctCell = null;
			var pctValue = 0;
			for ( j=0; j<cells.length; j++ ) {
				var codes = cells[j].getAttribute('id').split('-');
				var county = codes[1];
				if ( codes[0].startsWith('pct') ) {
					pctCell = cells[j];
				} else if ( codes[0].startsWith('tot') ) {
					var fields = cells[j].innerHTML.split('<br>');
					pctValue = (fields[0] / fields[1] ) * 100;
					pctValue = pctValue.toFixed(2);
				}
			}
			if ( pctCell != null ) {
				pctCell.innerHTML= "(" + pctValue + "%)";
			}
		}
		document.getElementById("pctRes").innerHTML="TOTAL";

	} else {

		var rows = document.getElementsByClassName("countyRows");
		for ( i=0; i<rows.length; i++) {
			var cells = rows[i].getElementsByClassName("statsTd");
			var pctCell = null;
			var pctValue = 0;
			for ( j=0; j<cells.length; j++ ) {
				var codes = cells[j].getAttribute('id').split('-');
				var county = codes[1];
				if ( codes[0].startsWith('pct') ) {
					pctCell = cells[j];
				} else if ( codes[2]==value ) {
					var fields = cells[j].getElementsByTagName('span');
					pctValue = (fields[0].innerHTML / fields[2].innerHTML) * 100;
					pctValue = pctValue.toFixed(2);
				}
			}
			if (pctCell != null) {
				pctCell.innerHTML= "(" + pctValue + "%)";
			}
		}
		document.getElementById("pctRes").innerHTML=document.getElementById("monthHeader-" + value).innerHTML;
	}
}

function getBidData(area,month) {
/*	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	    	var values = this.responseText.split(",");
		    document.getElementById("coi").innerHTML = values[0];
	        document.getElementById("doi").innerHTML = values[1];
	        document.getElementById("bid").innerHTML = values[2];
	        document.getElementById("cbid").innerHTML = values[3];
	        document.getElementById("aDate").innerHTML = values[4];
	        document.getElementById("bidInfo").style.display = '';
	    }
	};
	xhttp.open("GET", "WCDetail?area="+area+"&month="+month, true);
	xhttp.send();
*/
	document.getElementById("dCounty").value=area;
	document.getElementById("dMonth").value=month;
	document.getElementById("details").submit();
}

function getBidDataWithZip(area,month) {
	
		document.getElementById("zCounty").value=area;
		document.getElementById("zMonth").value=month;
		document.getElementById("detailsWithZip").submit();
}

function submitBuyWithZipForm() {
	document.getElementById("zipForm").submit();
}

function selectAllNone() {
	var cbxs = document.getElementsByClassName("COUNTY");
	var cbx = document.getElementById("ALL");

	for ( i=0; i<cbxs.length; i++ ) {
		cbxs[i].checked = cbx.checked;
	}

	document.getElementById("allLabel").innerHTML = (cbx.checked ? "NONE" : "ALL");
	document.getElementById('counties').value = (cbx.checked ? "ALL" : "");
}

function checkUser() {
	var username = document.getElementById("userId").value;
	if ( username != "" ) {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		      document.getElementById("validUser").innerHTML = this.responseText;
		      document.getElementById("validUser").style.display = '';
		      document.getElementById("validUser").style.color = 'red';
		    }
		};
		xhttp.open("GET", "checkusername?username="+username, true);
		xhttp.send();
	}
}

function validatePhone() {
	var phone = document.getElementById('phoneId').value;
	phone = phone.replace("(","");
	phone = phone.replace(")","");
	phone = phone.replace("-","");
	phone = phone.replace("-","");
	phone = phone.replace(".","");
	phone = phone.replace(".","");
	phone = phone.replace(" ","");
	phone = phone.replace(" ","");
	var regex = /[0-9]{10}/;
	if (! regex.test(phone) || phone.length > 10 ) {
		document.getElementById('validPhone').innerHTML = "nnn-nnn-nnnn";
		document.getElementById('validPhone').style.display='';
		document.getElementById('validEmail').style.color='red';
 	} else {
		document.getElementById('validPhone').style.display='none';
	}
}

function validateEmail() {
	var email = document.getElementById('emailId').value;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (! re.test(email) ) {
		document.getElementById('validEmail').innerHTML = "Check email";
		document.getElementById('validEmail').style.display='';
		document.getElementById('validEmail').style.color='red';
	} else {
		document.getElementById('validEmail').style.display='none';
	}
}

function validateDate() {
	var startDate = document.getElementById('minDate').value;
	var endDate = document.getElementById('maxDate').value;
	var regex = /([0-9]{4}[-](0[1-9]|1[0-2])[-]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[-](0[1-9]|1[0-2])[-][0-9]{4})/;

	document.getElementById('minDate').style.color=( regex.test(startDate) ? '#000000' : '#ff4444');
	document.getElementById('maxDate').style.color=( regex.test(endDate) ? '#000000' : '#ff4444');
}



function selectAll() {
	var mcbx = $('#masterCbx');
    $("#masterCbx").attr('checked', true);
    $('.selector').each(function() {
        $(this).prop('checked',true);
        $(this).closest('tr').addClass("active");
    });
}

function selectNone() {
	var mcbx = $('#masterCbx');
    $("#masterCbx").attr('checked', false);
	mcbx.checked = false;
    $('.selector').each(function() {
        $(this).prop('checked',false);
        $(this).closest('tr').removeClass("active");
    });
}


function reorderByMonth(month) {
	//  Rearrange the table rows in descending order of the selected month.
	
}
