<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>FAQs</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
       
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
    </head>
    <body>
        <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container">
    <br />
    <br />
    <br />

    <div class="panel-group" id="accordion">
        <div class="faqHeader">General questions</div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Where do you get the data?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    The information presented is collected from various third-party sites. 
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">How current is the data?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    The list of employers is updated at least twice a year, while the current insurer for any particular employer is updated at least once a year.  Mid-term
cancellations and switches may not be reflected immediately.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoOne">Wow!  This is expensive!  Why so much?</a>
                </h4>
            </div>
            <div id="collapseTwoOne" class="panel-collapse collapse">
                <div class="panel-body">
                    Pricing is based primarily 
on the cost of collecting the data, but also on the known success rate of using this information.  For a moderately sized county, a single sale should net the WC policy 
along with the BOP/CPP and a Commercial Auto as well, and would more than pay for the cost of the service.  Renewals for at least a few years makes the cost of obtaining 
the lead through this service insignificant.  How valuable is your time?  When compared to other lead generation services, this site offers the best opportunity for 
success based on hard evidence and gives you many more hours to pursue sales.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Are all employers in the state listed?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    No.  There are several reasons to not include a registered employer as detailed below:
                    <ul>
                    <li>Obsolete.  The total number of registered employers is almost 500,000, but over half are obsolete with the most recent expiration or cancellation date more than three months
in the past.  A three month window is allowed to provide time for an employer to reestablish WC coverage just in case the lapse was accidental, rather than assuming the company is permanently 
discontinuing WC coverage.</li>
	<li>Code 972.  This code for "Attendant Care Services" is not found in any carrier's rates, and is therefore excluded from comparison.</li>
	<li>Not PA.  An employer with a registered address outside of Pennsylvania cannot be assigned to a PA county, and is excluded.</li>
</ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Are employers always assigned to the correct county?</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    No.  Many zip codes cross county lines, but zip code databases always assign it to only one primary county, presumably the one with the most population or area.
 This is the reason that the zip codes assigned to each of the counties is available and shown when you click on the county name on the statistics page.  There is the possibility of 
 more precise allocation by county if a high-quality webservice can be located that first maps addresses to latitude/longitude, and then lat/lon to county, but the quality of the WC
 registrations are dependent on employers and insurance agents submitting perfectly accurate information, which doesn't happen.  Manually scrubbing the addresses of 220,000 employers 
 and then keeping them accurate in the future is probably not feasible.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">How many employers ARE available?</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    As of this writing, there were 220,194 employers.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Why should I pay for publicly available data?</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                    Like everything in business, it's a decision based on cost/benefit analysis.  The complaint I heard frequently from agents within our office was that a free day 
to sell consisted mostly of data analysis, generating letters or flyers, and preparing to sell, with little time left to actually sell.  With our success rate from a year of using 
this website internally, the cost is far exceeded by the additional premium of actual sales.  The benefit far exceeds the cost.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Will you make improvements to increase the success rate and the cost/benefit curve?</a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
                <div class="panel-body">
                    Absolutely.  Please use the "Make Suggestions" link to improve the site.  If it's a feasible suggestion, we'll do our best to add the capability.
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .faqHeader {
        font-size: 27px;
        margin: 20px;
    }

    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'Glyphicons Halflings';
        content: "\e072"; /* "play" icon */
        float: right;
        color: #F58723;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }

    .panel-body {
        text-align: left;
        font-size: 14px;
        line-height: 20px;
    }
</style>
     </div>
        </body>
</html>