<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <%
           // Set to expire far in the past.
      response.setDateHeader("Expires", 0);
  // Set standard HTTP/1.1 no-cache headers.
     response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
  // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
     response.addHeader("Cache-Control", "post-check=0, pre-check=0");
  // Set standard HTTP/1.0 no-cache header.
      response.setHeader("Pragma", "no-cache");
         
        %>   
        <title>PA Employer Collection Statistics</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
    </head>
    <body>
       <%@include file="Header.jsp" %>
<%		session.setMaxInactiveInterval(15*60); %>       
<form name="details" id="details" action="WCDetail">
<input type="hidden" id="dCounty" name="county"/>
<input type="hidden" id="dMonth" name="month"/>
</form>
<form name="detailsWithZip" id="detailsWithZip" action="WCDetailWithZip">
<input type="hidden" id="zCounty" name="county"/>
<input type="hidden" id="zMonth" name="month"/>
</form>
      <div class='container-fluid'>
          <div class="container search-page content">
             
              <div class="select-multi">
                  <form id="DetailForm" action="WCZips" method="post" class="form-inline">
             
                    <div class="head"><h2 class="wch2">PA Employer Collection Statistics</h2></div>
                      
                       <div class="state-table">
                           <table class="statstable" align="center">
		  <tbody><tr class="search tbheader">
			<th>COUNTY</th>
			<th>PCT <span id="pctRes">TOTAL</span></th>
			<th><a href="#" onclick="setPct('All');">TOTAL</a></th>

	    	<c:forEach items="${monthHeaders}" var="monthName" varStatus="loop">
			    <c:if test="${loop.index<7}">
					<th><a href="#" onclick="setPct('${loop.index}');" id="monthHeader-${loop.index}">${monthName}</a></th>
				</c:if>
		    </c:forEach>
		  </tr>
			
<c:forEach items="${statistics.allStats}" var="vstats" varStatus="outerLoop">
<!--  allStats returns Map<String,CountyStats> (vstats) -->

<c:set var="zipBased" value="${((vstats.value.done)/12 - (vstats.value.comps)/12)}"/>

			<tr id="row-${vstats.key}" class="countyRows">

 	            <td class="countyTd" id="cty-${vstats.key}">
 	            	<a href="WCZips?county=${vstats.key}">${vstats.key}</a>  <br>

					<c:choose>
						<c:when test="${vstats.value.reservedFull}">
							Unavailable
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${zipBased > 100}">
				 	            	<a href="SubscribeByZip?county=${vstats.key}"><img src="images/SubscribeByZip.jpg"/></a>
				 	            	<c:if test="${vstats.value.reservedPartial}">
				 	            	<br/>Some zips unavailable
 	            	</c:if>
			 	            	</c:when>
			 	            	<c:otherwise>
                           <c:choose>
                              <c:when test="${vstats.value.reservedPartial}">
                                 <a href="SubscribeByZip?county=${vstats.key}"><img src="images/SubscribeByZip.jpg"/></a>
                                    <br/>Some zips unavailable
			      </c:when>
			      <c:otherwise>
                                 <a href="Subscribe?county=${vstats.key}"><img src="images/Subscribe.jpg"/></a>
			      </c:otherwise>
			   </c:choose>
			 	            	</c:otherwise>
		 	            	</c:choose>
						</c:otherwise>
					</c:choose>

 	            </td>
	            <td class="countyTd statsTd totals" id="pct-${vstats.key}">(<fmt:formatNumber type="number" maxFractionDigits="2" value="${(vstats.value.done*100)/vstats.value.total}" />%)</td>
	            <td class="countyTd statsTd totals" id="tot-${vstats.key}">${vstats.value.done}<br/>${vstats.value.total}</td>

	            <c:forEach items="${vstats.value.monthStats}" var="counts" varStatus="otherLoop">
<!--  vstats.value is a CountyStats object-->
<!--  vstats.value.monthStats is a TreeMap<MonthEnum,MonthStats> object -->
 	<c:choose>
	<c:when test="${counts.available || vstats.value.reservedFull}">
		<c:set var="cellFmt" value="countyTd"/>
	</c:when>
	<c:otherwise>
		<c:set var="cellFmt" value="reservedTd"/>
	</c:otherwise>
	</c:choose>

			    <c:if test="${otherLoop.index < 7}">
		            <td class="${cellFmt} statsTd" id="cm-${vstats.key}-${otherLoop.index}">
		            <table border="0" cellspacing="0" cellpadding="0" align="center">
		            <c:if test="${(counts.isPartial)== 'true' }">
		            	<div class="soldline"><i>Some Zips Sold</i></div>
		            </c:if>
			            <tr>
			            	<td><span>${counts.done}</span></td>
			            	<td>(<span>${counts.comps}</span>)</td>
			            </tr> 
	<c:choose>
	<c:when test="${counts.available}">
		<c:choose>
						<c:when test="${zipBased > 100}">
				            <tr>
				            	<td><span>$${(counts.done - counts.comps)*leadcost}</span></td> 
					<td><img src="images/BuyByZip.jpg" onclick="getBidDataWithZip('${vstats.key}', ${otherLoop.index})"/></td>
			            </tr>
	 	            	</c:when>
	 	            	<c:otherwise>
<tr>
								<c:choose>
									<c:when test="${!vstats.value.reservedFull}">
						            	<td><span>$${(counts.done - counts.comps)*leadcost}</span></td> 
										<td>
											<img src="images/Buy.bmp" onclick="getBidData('${vstats.key}', ${otherLoop.index})"/>
										</td>
									</c:when>
									<c:otherwise>
										<td colspan="2">
											Subscribed
										</td>
										
									</c:otherwise>
									</c:choose>
</tr>
			</c:otherwise>
		</c:choose>
			             
	</c:when>
	<c:when test="${not empty counts.reserved}">
			         <tr>
			         	<td><span>$${(counts.done - counts.comps)*leadcost}</span></td> 
			            <td><img src="images/Reserved.bmp"/></td> 
	                 </tr>
	</c:when>
	<c:when test="${vstats.value.reservedFull}">
										<td colspan="2">
											Subscribed
										</td>
    </c:when>
	<c:otherwise>
				     <tr>
				     	<td><span>$${(counts.done - counts.comps)*leadcost}</span></td> 
			            <td><img src="images/Sold.bmp"/></td> 
			            </tr>
	</c:otherwise>
	</c:choose>

					</table>
		            </td>
		            </c:if>
	            </c:forEach>
	        </tr>
	    </c:forEach>
			 </table>
	    
	    
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
                           <p>&nbsp;</p>
                       </div>  
                  </form>
                   
             </div>
           </div>
     </div>
        </body>
</html>
