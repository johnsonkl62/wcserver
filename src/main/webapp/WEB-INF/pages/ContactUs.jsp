<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Contact Us</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
    </head>
    <body>
        <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container menu-content">
            <div class="form-class">
              <form action="sendContactUs" method="post">
                <div class="form-group">
                  <label for="userId">Username(Optional):</label>
                  <input type="text" name="username"  placeholder="Username(Optional)" class="form-control" id="userId" > 
                </div>
                 <div class="form-group">
                  <label for="emailId">Email:</label>
                  <input type="email" name="emailId"  placeholder="john.smith@gmail.com" required="required" class="form-control" id="emailId" > 
                </div>
                <div class="form-group">
                  <label for="comment">COMMENTS:</label>
                  <textarea cols="5" rows="10"  name="comment"  class="form-control" id="comment" required="required"></textarea>
                </div>
                 
                <button type="submit" class="btn btn-default">Send Your Message</button>
                
                
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
                
              </form>
            </div>
           
           </div>
     </div>
        </body>
</html>
