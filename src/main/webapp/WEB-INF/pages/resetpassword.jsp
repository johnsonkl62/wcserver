<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Reset Password</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
         <script>

var password = document.getElementById("password")
, confirm_password = document.getElementById("confirm_password");

function validatePassword(){
if(password.value != confirm_password.value) {
  confirm_password.setCustomValidity("Passwords Don't Match");
} else {
  confirm_password.setCustomValidity('');
}
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
    </head>
    <body onload='document.resetPasswordForm.name.focus();'>
        <header>
          <nav class="navbar">
            <div class="container-fluid">
              <div class="navbar-header">
                  <a class="" href="#"><img src="images/logo.jpg" alt="" /></a>
              </div>
              <ul class="nav navbar-nav">
                  <p>&nbsp;</p>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="WCRegister"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
              </ul>
            </div>
          </nav>
        </header>
      <div class='container-fluid'>
          <div class="container menu-content">
            <div class="form-class">
              <form  name="resetPasswordForm" id="resetPasswordForm" action="updatePassword" method="post">
                <div class="form-group">
                  <label for="emailAddress">Password:</label>
                  <input type="password" class="form-control" name="password" id="pwd" required>
                </div>
                 <div class="form-group">
                  <label for="emailAddress">Confirm Password:</label>
                  <input type="password" class="form-control" name="confirmPassword" id="confirm_password" required>
                </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
<input type="hidden" name="emailAddress"
				value="<%= request.getParameter("emailAddress") %>" />
                <button type="button" onclick="resetPasswordFunction('resetPasswordForm');" class="btn btn-default">SUBMIT</button>
              </form>
        
              
    
            </div>
           
           </div>
     </div>
        </body>
</html>
