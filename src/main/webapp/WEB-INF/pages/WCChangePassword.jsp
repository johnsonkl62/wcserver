<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Change Password</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
    </head>
    <body>
        <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container menu-content">
            <div class="form-class">
              <form action="changePassword" id="changePasswordForm" method="post">
                <div class="form-group">
                  <label for="oldPassword">Old Password:</label>
                  <input type="password" name="oldPassword" class="form-control" id="oldPassword" > 
                </div>
                
                <div class="form-group">
                  <label for="newPassword">New Password:</label>
                  <input type="password" name="newPassword" class="form-control" id="newPassword" > 
                </div>
                
                <div class="form-group">
                  <label for="reTypeNewPassword">Re-Type New Password:</label>
                  <input type="password" name="reTypeNewPassword" class="form-control" id="reTypeNewPassword" > 
                </div>
                 
                <button type="button" onclick="changPasswordFunction('changePasswordForm');" class="btn btn-default">CHANGE PASSWORD</button>
                
                
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
                
              </form>
            </div>
           
           </div>
     </div>
        </body>
</html>
