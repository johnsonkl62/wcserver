<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Keystone Workers' Compensation Rate Comparison</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
          <nav class="navbar">
            <div class="container-fluid">
              <div class="navbar-header">
                  <a class="" href="index.jsp"><img src="images/logo.jpg" alt="" /></a>
              </div>
              <ul class="nav navbar-nav">
                  <p>&nbsp;</p>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="WCRegister"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
              </ul>
            </div>
          </nav>
        </header>
      <div class='container-fluid'>
          <div class="container content">
            <div class="row">
              <h2>Keystone Workers' Compensation Rate Comparison</h2>
                <p>The KSWCRC website allows insurance professionals to quickly identify and target prospective clients 
                who have the highest potential savings based on publicly available insurer rates.  The typical method
                for commercial agents to solicit is to identify businesses, then qualify them by looking up their public 
                WC information, check their experience mod or merit, and then manually compare that rate against the 
                rates of the insurers that they represent -- a process that can take dozens of hours.  This site allows 
                the agent to look at ALL employers in his/her area instantly and automatically compare the employer's 
                current rate versus the rates of three insurers that he/she represents.  The data is presented in tabular 
                format, sortable by any column heading, with the ability to automatically generate solicitation
                letters or flyers.</p>
				<p>This website was originally created by an insurance agent seeking efficiency in the commercial
				solicitation process only for his own agency's use.  It was expanded and made available at the
				request of others who saw it.  Now you can spend time selling, rather than collecting, collating, 
				and analyzing employer data.  Your time is valuable and the cost of eliminating those wasted hours is 
				far more than made up by the increased sales.</p>
                <p>If you have tried commercial sales lead services like we did, you know that you pay for low-premium, 
                low-probability, and high-risk leads whether you write them or not.  That's why this site was developed. 
                Approaching a prospect with hard evidence from public sources makes the solicitation process 
                trivial, and most business owners are well aware that this data is publicly available.  Very few
                are offended by showing them that public information, although quite a few have been misled by their
                current agents who want to suppress shopping around.</p>
                <p>You should have a basic understanding of this data to know its limitations.  By its nature,
                it cannot be perfect or instantaneously up-to-date.  But you don't need absolutely perfect data 
                to double or triple your commercial sales with the dozens of extra hours each month you'll 
                have.</p>
            </div>
            <div class="row button">
              <a href="login">LOGIN</a>
              <a href="WCRegister">REGISTER</a>
            </div>
           </div>
     </div>
        </body>
</html>
