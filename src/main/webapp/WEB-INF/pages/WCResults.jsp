<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>PA Employer Search Results</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
         <script src="script/wc.js"></script>
         <script>
         $(document).ready(
        		    function() { 
        			$('.myblue tr').click(function(event) {
        			    if ( !$(this).parent().is('thead') ) {
        		                if ($(this).hasClass('active')) {
        		                    $(':checkbox', this).unbind('click');
        		                    $(this).removeClass('active'); 
        		                    $("#employerId").val('');
        		                } // else {
        		                    if (event.target.type !== 'checkbox') {
        		                        $(':checkbox', this).trigger('click');
        		                        var idValue = $(this).attr('id');
        								$("#employerId").val(idValue);
        		                    }
        		                //}
        		            }
        		        });
        			$('.myTable tr').click(function(event) {
        			    if ( !$(this).parent().is('thead') ) {
        		                if ($(this).hasClass('active')) {
        		                    $(this).removeClass('active'); 
        		                } 
        		            }
        		        });
        			
        		        $(".selector").change(function (e) {
        		            //  Single row checkbox clicked
        		            if ($(this).is(":checked")) {
        		                $(this).closest('tr').addClass("active");
        		            } else {
        		                $(this).closest('tr').removeClass("active");
        		            }
        		        });
        		        
        		        $("#masterCbx").change(function (e) {
        		            var mcbx = this;
        		            $('.selector').each(function() {
        		                if ($(mcbx).is(":checked")) {
        		                    $(this).prop('checked',true);
        		                    $(this).closest('tr').addClass("active");
        		                } else {
        		                    $(this).prop('checked',false);
        		                    $(this).closest('tr').removeClass("active");
        		                }
        		            });
        		        });
        		        $("#selectAll").click(function (e) {
        		        	var mcbx = $("#masterCbx");
        		            $("#masterCbx").prop("checked",true);
        		            $('.selector').each(function() {
        		                if ($(mcbx).is(":checked")) {
        		                    $(this).prop('checked',true);
        		                    $(this).closest('tr').addClass("active");
        		                } else {
        		                    $(this).prop('checked',false);
        		                    $(this).closest('tr').removeClass("active");
        		                }
        		            });
        		        });
        		        $("#selectNone").click(function (e) {
        		        	var mcbx = $("#masterCbx");
        		            $("#masterCbx").prop("checked",false);
        		            $('.selector').each(function() {
        		                if ($(mcbx).is(":checked")) {
        		                    $(this).prop('checked',true);
        		                    $(this).closest('tr').addClass("active");
        		                } else {
        		                    $(this).prop('checked',false);
        		                    $(this).closest('tr').removeClass("active");
        		                }
        		            });
        		        });
        		    });
             
             ;(function($) {
            	   $.fn.fixMe = function() {
            	      return this.each(function() {
            	         var $this = $(this),
            	            $t_fixed;
            	         function init() {
            	            $this.wrap('<div class="container" />');
            	            $t_fixed = $this.clone();
            	            $t_fixed.find("tbody").remove().end().addClass("fixed").insertBefore($this);
            	            resizeFixed();
            	         }
            	         function resizeFixed() {
            	            $t_fixed.find("th").each(function(index) {
            	               //$(this).css("width",$this.find("th").eq(index).outerWidth()+"px");
            	            });
            	         }
            	         function scrollFixed() {
            	            var offset = $(this).scrollTop(),
            	            tableOffsetTop = $this.offset().top,
            	            tableOffsetBottom = tableOffsetTop + $this.height() - $this.find("thead").height();
            	            if(offset < tableOffsetTop || offset > tableOffsetBottom)
            	               $t_fixed.hide();
            	            else if(offset >= tableOffsetTop && offset <= tableOffsetBottom && $t_fixed.is(":hidden"))
            	               $t_fixed.show();
            	         }
            	         $(window).resize(resizeFixed);
            	         $(window).scroll(scrollFixed);
            	         init();
            	      });
            	   };
            	})(jQuery);

            	$(document).ready(function(){
            	   $('[data-toggle="tooltip"]').tooltip();
            	   $("table").fixMe();
            	   $(".up").click(function() {
            	      $('html, body').animate({
            	      scrollTop: 0
            	   }, 2000);
            	 });
            	});
             
        
         </script>
        <script type="text/javascript">
			 $(document).ready(function () {
						/* $('#myTable').on('click', '.clickable-row', function(event) {
						  $(this).addClass('active').siblings().removeClass('active');
						});*/
						$('.myblue').on('click', '.clickable-row', function(event) {
							  if($(this).hasClass('active')){
								$(this).removeClass('active'); 
								$("#code").attr('value', '');
							  } else {
								$(this).addClass('active').siblings().removeClass('active');
								var idValue = $(this).attr('id');
								$("#code").attr('value', idValue);
							  }
							});
						
						$('.myTable').on('click', '.clickable-row', function(event) {
							  if($(this).hasClass('active')){
								$(this).removeClass('active'); 
								$("#code").attr('value', '');
							  } else {
								$(this).addClass('active').siblings().removeClass('active');
								var idValue = $(this).attr('id');
								$("#code").attr('value', idValue);
							  }
							});
						
						$('#myModal').on('shown.bs.modal', function () {
						    $('#search-dinosaurs').focus();
						})
						
//			    $("#code-list li").css(); 
					/* highlight matches text */
				    	var highlight = function (string) {
						$("#code-list tr.match").each(function () {
							var matchStart = $(this).text().toLowerCase().indexOf("" + string.toLowerCase() + "");
							var matchEnd = matchStart + string.length - 1;
							var beforeMatch = $(this).text().slice(0, matchStart);
							var matchText = $(this).text().slice(matchStart, matchEnd + 1);
							var afterMatch = $(this).text().slice(matchEnd + 1);
							$(this).html("<td>" + beforeMatch + "<span class='matchtxt'>" + matchText + "</span>" + afterMatch + "</td>");
							
						});
					};
					/* filter products */
					$("#search-dinosaurs").on("keyup click input", function () {
						if (this.value.length > 0) {
							$("#code-list tr").removeClass("match").hide().filter(function () {
								return $(this).text().toLowerCase().indexOf($("#search-dinosaurs").val().toLowerCase()) != -1;
							}).addClass("match").show();
							highlight(this.value);
							$("#code-list").show();
						}
						else {
							$("#code-list, #code-list tr").removeClass("match").show();
							$("#code-list tr td span").removeClass("matchtxt").show();
						}
					});
					
				});
				
			 function callAjax() {
					
					var employerId = document.getElementById("employerId").value;
					var code = document.getElementById("code").value;
					var csrfname = document.getElementById("csrf").name;
					var csrfvalue = document.getElementById("csrf").value;
					
			        $.ajax({
			        	url: "WCUpdateCode?employerId="+employerId+"&code="+code+"&"+csrfname+"="+csrfvalue,
			        	type:"POST", 
			        	dataType:"json",
			        	success: function(result){
			        		
			        		$('#'+result.key.fn+'_description').html(result.description);
			        		$('#'+result.key.fn+'_code').html(result.data.code);
			        		$('#'+result.key.fn+'_comp1').html('$'+result.data.comp1.toFixed(2));
			        		$('#'+result.key.fn+'_pct1').html((result.data.pct1*100).toFixed(2)+'%');
			        		$('#'+result.key.fn+'_comp2').html('$'+result.data.comp2.toFixed(2));
			        		$('#'+result.key.fn+'_pct2').html((result.data.pct2*100).toFixed(2)+'%');
			        		$('#'+result.key.fn+'_comp3').html('$'+result.data.comp3.toFixed(2));
			        		$('#'+result.key.fn+'_pct3').html((result.data.pct3*100).toFixed(2)+'%');
			        }});
			    };
			 
		  </script>
    </head>
    <body>
    
   <form action="WCUpdateCode" method="post" id="wcupdate">
	<input type="hidden" name="employerId" id="employerId" />
	<input type="hidden" name="code" id="code"  />
	 <input type="hidden" id="csrf" 
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
	</form>
    
      <!-- Search model start here-->
	   <div class="container" style="width: 100%;">
			  <!-- Trigger the modal with a button -->
			  <!-- Modal -->
			  <div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">
				
				  <!-- Modal content-->
				  <div class="modal-content mymodal-content">
					<div class="modal-header mymodal-header">
					   <p>SEARCH</p>
					   <input type="text" class="searchlist" id="search-dinosaurs" />
					</div>
					<div class="modal-body mymodal-body">
					  <table class="table table-bordered myTable" id="code-list">
					  
					   <c:forEach items="${codes}" var="code">
					   	
						  <tr id="${code.key}" class="clickable-row">
							<td>${code.key} ${code.value}</td>
						  </tr>
						   
					   </c:forEach>
						</table>
					</div>
					<div class="modal-footer align_buttons myfooter">
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#warning">Submit</button>					 
					 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				  </div> 
				</div>
			  </div>
		</div>
		  <!-- Search model end here-->
		  
		   <!-- Warning dailog start here-->
		   
		   	<div class="container">
			  <!-- Modal -->
			  <div class="modal fade" id="warning" role="dialog">
				<div class="modal-dialog">
				
				  <!-- Modal content-->
				  <div class="modal-content warningcontent">
					<div class="modal-header warningModel">
					   <p>*** WARNING! ***</p>
					</div>
					<div class="modal-body warningbody">
					 <p>Are you sure?</br>
					  This action cannot be undone!</p>
					</div>
					<div class="modal-footer align_buttons myfooter">
                      <button type="button" id="ok" class="btn btn-default">OK</button>					 
					 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				  </div> 
				</div>
			  </div>
		</div>
		   <script>
		   $(document).ready(function(){
		      $('#ok').click(function(){
				 $('#warning').hide();
				 $('#myModal').hide();
				 $('.modal-backdrop').hide();
				 /* $( "#wcupdate" ).submit(); */
				  callAjax() ; 
				 
			  })
		   })
		   </script>
		   <!-- Warning dailog start here-->
 
         <%@include file="Header.jsp" %>
        <div class='container-fluid'>
          <nav class="navbar">
            <ul class="nav navbar-nav navbar-left">
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">TOTAL RESULTS: ${eCount}<br/>${cr2n} (${cr2}): ${cr2Count}</a>
                </li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">${cr1n} (${cr1}): ${cr1Count}<br/>${cr3n} (${cr3}): ${cr3Count}</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Select
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#" id="selectAll">All</a></li>
                    <li><a href="#" id="selectNone">None</a></li>
                  </ul>
                </li>
                  <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Download
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#" onclick="submitForm('csv')">To CSV</a></li>
                    <li><a href="#" onclick="submitForm('excel')">To Excel</a></li>
                    <li><a href="#" onclick="submitForm('combine')">Letters (docx)</a></li>
                    <li><a href="#" onclick="submitForm('labels')">Mailing Labels</a></li>
                    <li><a href="#" onclick="submitForm('envelopes')">Envelopes</a></li>
                  </ul>
                </li>
              </ul>
              </nav>
            <div class="content-list">
            
            <form action="WCData" method="post" id="wcform">
	<input type="hidden" name="option" id="option" value="download"/>
	<input type="hidden" name="format" id="format"  value=""/>

<table class="table-bordered myblue table-hover" style="width: 1175px;">
<thead>
	  <tr>
	   <th style="width: 36px;">Select<input type="checkbox" id="masterCbx" ></th>
       <th style="width: 152px;"><a href="WCOrder?sort=emp">Employer</a></th>
       <th style="width: 138px;"><a href="WCOrder?sort=desc">Description</a></th>
       <th style="width: 50px;"><a href="WCOrder?sort=mm">Mod/Merit</a></th>
       <th style="width: 40px;"><a href="WCOrder?sort=xdate">Ex Date</a></th>
       <th style="width: 90px;"><a href="WCOrder?sort=carrier">Carrier</a></th>
       <th style="width: 43px;"><a href="WCOrder?sort=rate">Current Rate</a></th>
       <th style="width: 50px;"><a href="WCOrder?sort=comp1" data-toggle="tooltip" title="${cr1n}">Comp 1 ${cr1}</a></th>
       <th style="width: 46px;"><a href="WCOrder?sort=pct1"> ${cr1} %</a></th>
       <th style="width: 49px;"><a href="WCOrder?sort=comp2" data-toggle="tooltip" title="${cr2n}">Comp 2 ${cr2}</a></th>
       <th style="width: 46px;"><a href="WCOrder?sort=pct2">${cr2} %</a></th>
       <th style="width: 30px;"><a href="WCOrder?sort=comp3" data-toggle="tooltip" title="${cr3n}">Comp 3 ${cr3}</a></th>
       <th style="width: 46px;"><a href="WCOrder?sort=pct3">${cr3} %</a></th>
       <th style="width: 75px;"><a href="WCOrder?sort=county">County</a></th>
       <th style="width: 89px;"><a href="WCOrder?sort=city">City</a></th>
       <th style="width: 82px;"><a href="WCOrder?sort=address">Address</a></th>
       <th style="width: 33px;"><a href="WCOrder?sort=zip">Zip</a></th>
       <th style="width: 25px;"><a href="WCOrder?sort=gcc">GCC</a></th>
       <th><a href="WCOrder?sort=fn">File #</a></th>
	  </tr>
</thead>
<tbody>
 
     <c:forEach items="${employers}" var="employer">
 		<c:set var="tdClass" value="normal"/>
		<c:set var="trClass" value="normalRow"/>

	<c:choose>
	    <c:when test="${employer.data.code == '-1'}">
	            <tr class="${trClass}" id="${employer.key.fn}">
	    </c:when>
	    <c:otherwise>
		    <tr class="${trClass}">
		</c:otherwise>
	</c:choose>
	
            <td class="${tdClass}"><input type="checkbox" name="n${employer.key.fn}" value="${employer.key.fn}" class="selector"/></td>
            <td class="${tdClass}" style="text-align:left"><a   target="_blank"  href="http://www.google.com/search?q=${fn:replace(employer.key.name,'&','%26')}+${employer.base.city}"> ${employer.key.name} </a></td>
            
            
            
            <c:choose>
			    <c:when test="${employer.data.code == '-1'}">
			            <td class="${tdClass} hover-item" data-toggle="modal" data-target="#myModal" style="text-align:left" id="${employer.key.fn}_description">${employer.description}</td>
			    </c:when>
			    <c:otherwise>
				    <td class="${tdClass}" style="text-align:left" id="${employer.key.fn}_description">${employer.description}</td>
				</c:otherwise>
			</c:choose>
            
            <td class="${tdClass}">${employer.data.mm}</td>
            <td class="${tdClass}"><fmt:formatDate value="${employer.data.xdate}" pattern="yyyy-MM-dd"/></td>
            <td class="${tdClass}" style="text-align:left">${employer.carrier}</td>
            <td class="${tdClass}" id="${employer.key.fn}_rate">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.rate}" /></td>

<c:choose>
    <c:when test="${ (employer.data.rate > employer.data.comp1) }">
            <td class="lower" id="${employer.key.fn}_comp1">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.comp1}" /></td>
            <td class="${tdClass} lower" id="${employer.key.fn}_pct1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.pct1*100}" />%</td>
    </c:when>
    <c:otherwise>
	    <c:choose>
		    <c:when test="${ (employer.data.rate < employer.data.comp1) }">
	            <td class="higher" id="${employer.key.fn}_comp1">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.comp1}" /></td>
	            <td class="${tdClass} higher" id="${employer.key.fn}_pct1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.pct1*100}" />%</td>
		    </c:when>
		    <c:otherwise>
		            <td class="${tdClass}" id="${employer.key.fn}_comp1">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.comp1}" /></td>
			        <td class="${tdClass}" id="${employer.key.fn}_pct1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.pct1*100}" />%</td>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${ (employer.data.rate > employer.data.comp2) }">
            <td class="${tdClass} lower" id="${employer.key.fn}_comp2">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.comp2}" /></td>
            <td class="${tdClass} lower" id="${employer.key.fn}_pct2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.pct2*100}" />%</td>
    </c:when>
    <c:otherwise>
    	<c:choose>
		    <c:when test="${ (employer.data.rate < employer.data.comp2) }">
	            <td class="${tdClass} higher" id="${employer.key.fn}_comp2">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.comp2}" /></td>
	           <td class="${tdClass} higher" id="${employer.key.fn}_pct2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.pct2*100}" />%</td>
		    </c:when>
		    <c:otherwise>
	            <td class="${tdClass}" id="${employer.key.fn}_comp2">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.comp2}" /></td>
			        <td class="${tdClass}" id="${employer.key.fn}_pct2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.pct2*100}" />%</td>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${ (employer.data.rate > employer.data.comp3) }">
            <td class="${tdClass} lower" id="${employer.key.fn}_comp3">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.comp3}" /></td>
            <td class="${tdClass} lower" id="${employer.key.fn}_pct3"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.pct3*100}" />%</td>
    </c:when>
    <c:otherwise>
    	<c:choose>
		    <c:when test="${ (employer.data.rate < employer.data.comp3) }">
	           <td class="${tdClass} higher" id="${employer.key.fn}_comp3">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.comp3}" /></td>
	            <td class="${tdClass} higher" id="${employer.key.fn}_pct3"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.pct3*100}" />%</td>
		    </c:when>
		    <c:otherwise>
			    <td class="${tdClass}" id="${employer.key.fn}_comp3">$<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.comp3}" /></td>
	            <td class="${tdClass}" id="${employer.key.fn}_pct3"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employer.data.pct3*100}" />%</td>
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>
<%-- <c:choose>
    <c:when test="${ (employer.data.rate > employer.data.comp2) }">
            <td class="${tdClass} ok">$${employer.data.rate}</td>

            <td class="higher">$${employer.data.comp1}</td>
            <td class="${tdClass} higher"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct1*100}" />%</td>
            <td class="${tdClass} lower">$${employer.data.comp2}</td>
            <td class="${tdClass} lower"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct2*100}" />%</td>
            <td class="${tdClass} lower">$${employer.data.comp3}</td>
            <td class="${tdClass} lower"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct3*100}" />%</td>
    </c:when>
    <c:when test="${ (employer.data.rate > employer.data.comp3) }">
            <td class="${tdClass}">$${employer.data.rate}</td>
            <td class="higher">$${employer.data.comp1}</td>
            <td class="${tdClass} higher"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct1*100}" />%</td>
            <td class="${tdClass} higher">$${employer.data.comp2}</td>
            <td class="${tdClass} higher"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct2*100}" />%</td>
            <td class="${tdClass} lower">$${employer.data.comp3}</td>
            <td class="${tdClass} lower"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct3*100}" />%</td>
    </c:when>
    <c:when test="${ (employer.data.rate <= employer.data.comp3) }">
            <td class="bad">$${employer.data.rate}</td>
            <td class="${tdClass} higher">$${employer.data.comp1}</td>
            <td class="${tdClass} higher"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct1*100}" />%</td>
            <td class="${tdClass} higher">$${employer.data.comp2}</td>
            <td class="${tdClass} higher"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct2*100}" />%</td>
            <td class="${tdClass} higher">$${employer.data.comp3}</td>
            <td class="${tdClass} higher"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct3*100}" />%</td>
    </c:when>            
	<c:otherwise>
	        <td class="${tdClass}">$${employer.data.rate}</td>
            <td class="${tdClass}">$${employer.data.comp1}</td>
            <td class="${tdClass}"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct1*100}" />%</td>
            <td class="${tdClass}">$${employer.data.comp2}</td>
            <td class="${tdClass}"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct2*100}" />%</td>
            <td class="${tdClass}">$${employer.data.comp3}</td>
            <td class="${tdClass}"><fmt:formatNumber type="number" maxFractionDigits="2" value="${employer.data.pct3*100}" />%</td>
	</c:otherwise>
</c:choose> --%>

            <td  class="${tdClass}">${employer.key.county}</td>
            <td class="${tdClass}">${employer.base.city}</td>
            <td class="${tdClass}">${employer.base.address}</td>
            <td class="${tdClass}">${employer.base.zip}</td>
            <td class="${tdClass}" id="${employer.key.fn}_code">${employer.data.code}</td>
            <td class="${tdClass}">${employer.key.fn}</td>
            
        </tr>
    </c:forEach>   		
	
    </tbody></table>
    
       
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
    
                </form>
            </div>
        </div>
    </body>
</html>