<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
        <title>PA Employer Search - Message</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

<body>
 <%@include file="Header.jsp" %>
<div class='container-fluid'>
          <div class="container menu-content">
            <div class="form-class">
              <form action="WCStats" >
                <div>
                  <h1>${title }</h1>
                </div>
                
	<p>${message }</p>
	
	<br/>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

               
              </form>
        
              
    
            </div>
           
           </div>
     </div>        
        

</body>
</html>