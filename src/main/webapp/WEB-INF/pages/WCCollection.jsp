<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <%
  // Set to expire far in the past.
      response.setDateHeader("Expires", 0);
  // Set standard HTTP/1.1 no-cache headers.
     response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
  // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
     response.addHeader("Cache-Control", "post-check=0, pre-check=0");
  // Set standard HTTP/1.0 no-cache header.
      response.setHeader("Pragma", "no-cache");
         
        %>   
        <title>PA Employer Collection Statistics</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/jquery.sortElements.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
         
    </head>
    <body>
       <%@include file="Header.jsp" %>
       
      <div class='container-fluid'>
          <div class="container search-page content">
             
              <div class="select-multi">
                    <div class="head"><h2 class="wch2">PA Employer Collection Statistics</h2></div>
                       <div class="state-table">

                           <table class="statstable" align="center">
		  <tbody id="statsTBody"><tr class="search tbheader">
			<th>COUNTY</th>

	    	<c:forEach items="${monthHeaders}" var="monthName" varStatus="loop">
				<th><a  id="${monthName}">${monthName}</a></th>
		    </c:forEach>
		  </tr>
		  
			<tr id="row-total" class="countyRows">
 	            <td class="countyTd">&nbsp;</td>
	            <c:forEach items="${collectionTotals}" var="monthTotal" varStatus="totalLoop">
		            <td class="countyTd">${monthTotal.value.total - monthTotal.value.done}</td>
	            </c:forEach>
	        </tr>

<c:forEach items="${collection}" var="countyStats" varStatus="outerLoop">
			<tr id="row-${countyStats.key}" class="countyRows">
 	            <td class="countyTd" id="cty-${countyStats.key}">${countyStats.key}</td>
	            <c:forEach items="${countyStats.value}" var="monthStats" varStatus="otherLoop">
		            <td class="${monthStats.key}">${monthStats.value.total - monthStats.value.done}</td>
	            </c:forEach>
	        </tr>
	    </c:forEach>
			 </table>
	    
                           <p>&nbsp;</p>
                       </div>  
             </div>
           </div>
     </div>
        </body>
        
        <script>
            var th = jQuery('th'),
                li = jQuery('li'),
                inverse = false;
            
            th.click(function(){
                
                var header = $(this),
                    index = header.index();
                    
                header
                    .closest('table')
                    .find('td')
                    .filter(function(){
                        return $(this).index() === index;
                    })
                    .sortElements(function(a, b){
                        
                        a = $(a).text();
                        b = $(b).text();
                        
                        return (
                            isNaN(a) || isNaN(b) ?
                                a > b : +a > +b
                            ) ?
                                inverse ? -1 : 1 :
                                inverse ? 1 : -1;
                            
                    }, function(){
                        return this.parentNode;
                    });
                
                inverse = !inverse;
                
            });
            
            $('button').click(function(){
                li.sortElements(function(a, b){
                    return $(a).text() > $(b).text() ? 1 : -1;
                });
            });
        </script>
        
</html>
