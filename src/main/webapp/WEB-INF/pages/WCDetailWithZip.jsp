<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>PA Employer County Details</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script src="script/wc.js"></script>
         
    </head>
    <body>
    
           
<%
String s = (String)request.getAttribute("Zips");
String[] array = s.split(String.format("(?<=\\G.{%1$d})", 48));
String zipStr = "";
if(array!=null) {
	for(String t : array) {
		zipStr = zipStr+t+"<br/>";
	}
}
%>        
    
    
        <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container search-page content">
             
              <div class="select-multi">
               <div class="head"><h2 class="wch2">PA Employer County Zip Details</h2></div>
               

               <div class="zip-table">
                   <table align="center">
		  <thead>
                
                      <tr class="search tbzip">
			<th>County</th>
			<th>${County}</th>
		  </tr>
                  </thead>
                  <tbody class="zip-rows">
			<tr>
 	            <td>Month</td>
 	            <td>${Month}</td>

 	        </tr>
		    <tr>
 	            <td>Zips</td>
 	            <td><%=zipStr %></td>
 	        </tr>
			<tr>
 	            <td>Price</td>
 	            <td>$<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${Price}" /></td>
 	        </tr> 
	   		 <tr>
 	            <td>PA Sales Tax</td>
 	            <td>$<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${Price*0.06}" /></td>
 	        </tr>
	   		 <tr>
 	            <td>Total</td>
 	            <td>$<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${Price*1.06}" /></td>
 	        </tr>
	    </tbody></table>
	    
<form name="_xclick" action="${paypalUrl}" method="post" id="paypalForm" class="form-inline" target="_top">
<input type="hidden" name="business" value="${paypalBusiness}">
<input type="hidden" name="return" value="${paypalReturn}">
<input type="hidden" name="notify_url" value="${paypalNotifyUrl}">
	 
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="item_name" value="${County} - ${Month} (${Zips})">
<input type="hidden" name="item_name1" value="${Zips}">
<input type="hidden" name="amount" value="${Price}">
<input type="hidden" name="tax_rate" value="6">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="custom" value="${username}">

<br/><p><b>PayPal account is not required.</b><br/>On PayPal login screen, scroll down to "Pay with Debit or Credit Card" button.</p><br/><br/>

<!-- <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1"> -->


    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>


	    
                   <div class=""><h2 class="align-center">
                   		
                   		<button type="submit" name="method_premium" value="PayPal - The safer, easier way to pay online!">
						  <img src="images/buybtn.png" alt="PayPal - The safer, easier way to pay online!" />
						</button>
                   		
                   		<!-- 	<a href="#" onclick="submitPaypal1();"><img src="images/buybtn.png"  name="submit" alt="PayPal - The safer, easier way to pay online!" class="mywidth" alt="" /></a> -->
                   	</h2></div>
                   <div class=""><h2 class="align-center"><img src="images/card.png" class="mywidth-two" alt="" /></h2></div>
                  </form>
               </div>
             </div>
           </div>
     </div>
     
        </body>
 
        
        
</html>
