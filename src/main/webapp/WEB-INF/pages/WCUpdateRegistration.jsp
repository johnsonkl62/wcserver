<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>PA Employer Search</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
    </head>
    <body>
        <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container menu-content">
            <div class="form-class">
              <form action="updateProfile"  enctype="multipart/form-data" method="post">
                <div class="form-group">
                  <label for="userId">Username(no special characters):</label>
                  <input type="text" name="username" value="${profileBean.username }" class="form-control" id="userId" readonly="readonly" > 
                </div>
                 
                <div class="form-group">
                  <label for="last">Last Name:</label>
                  <input type="text" name="last" value="${profileBean.lastName }" class="form-control" id="last">
                </div>
                <div class="form-group">
                  <label for="text">First name/middle initial:</label>
                   <div class="col-lg-8 zero-padding-left">
                       <input type="text" name="first" id="first" value="${profileBean.firstName }" class="form-control">
                  </div>
                    <div class="col-lg-4 zero-padding-right">
                       <input type="text" name="mi" value="${profileBean.middleInitial }" class="form-control">
                  </div>
                  
                 <div class="form-group">
                  <label for="emailId">Email:</label>
                  <input type="text" class="form-control" id="emailId" name="email" value="${profileBean.email }"  disabled="disabled"   size="40" >
                </div> 
                </div>
                <div class="form-group">
                  <label for="text">Suffix:</label>
                  <input type="text" class="form-control" value="${profileBean.suffix }" name="sfx">
                </div>
                <div class="form-group">
                  <label for="text">Agency:</label>
                  <input type="text" class="form-control" value="${profileBean.agency }" name="agency">
                </div>
              
               <div class="form-group">
                  <label for="text">Phone:</label>
                  <input type="text" class="form-control" id="phoneId" value="${profileBean.phone }" name="phone" size="40" onblur="validatePhone()"/><span id="validPhone" style="display:none">&nbsp;</span>
                </div>
               
                <div class="form-group">
                  <label for="text">Selected Carriers:</label>
                  <select multiple class="form-control" id="carriers" name="carriers" size="40" style="height:70pt" disabled="disabled" >
						<option value="${profileBean.carrier1}">${profileBean.carriersBean1.name}</option>
						<option value="${profileBean.carrier2}">${profileBean.carriersBean2.name}</option>
						<option value="${profileBean.carrier3}">${profileBean.carriersBean3.name}</option>
				</select>
                </div>
                
                <div class="form-group">
					<label  for="file">Upload template</label> <span><a href="<c:url value='/download-template?username=${profileBean.username }' />" class="btn custom-width">Download Template</a></span>
					<input type="file"  id="template" name="template"  class="form-control">
				</div>
                
                <button type="submit" class="btn btn-default">UPDATE PROFILE</button>
                
                
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
                
              </form>
            </div>
           
           </div>
     </div>
        </body>
</html>
