<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>PA Employer Zip Code to County Xref</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<script type="text/javascript" src="script/wc.js"></script>

<script>
         $(document).ready(
        		    function() { 
        			$('.mytable tr').click(function(event) {
        			    if ( !$(this).parent().is('thead') ) {
        		                if ($(this).hasClass('active')) {
        		                    $(':checkbox', this).unbind('click');
        		                    $(this).removeClass('active'); 
        		                } // else {
        		                    if (event.target.type !== 'checkbox') {
        		                        $(':checkbox', this).trigger('click');
        		                    }
        		                //}
        		            }
        			    calculate();
        		        });
        			
        		        $(".selector").change(function (e) {
        		            //  Single row checkbox clicked
        		            if ($(this).is(":checked")) {
        		                $(this).closest('tr').addClass("active");
        		            } else {
        		                $(this).closest('tr').removeClass("active");
        		            }
        		            calculate();
        		        });
        		        
        		        $("#masterCbx").change(function (e) {
        		            var mcbx = this;
        		            $('.selector').each(function() {
        		                if ($(mcbx).is(":checked")) {
        		                    $(this).prop('checked',true);
        		                    $(this).closest('tr').addClass("active");
        		                } else {
        		                    $(this).prop('checked',false);
        		                    $(this).closest('tr').removeClass("active");
        		                }
        		            });
        		            calculate();
        		        });
        		        $("#selectAll").click(function (e) {
        		        	var mcbx = $("#masterCbx");
        		            $("#masterCbx").prop("checked",true);
        		            $('.selector').each(function() {
        		                if ($(mcbx).is(":checked")) {
        		                    $(this).prop('checked',true);
        		                    $(this).closest('tr').addClass("active");
        		                } else {
        		                    $(this).prop('checked',false);
        		                    $(this).closest('tr').removeClass("active");
        		                }
        		            });
        		            calculate();
        		        });
        		        $("#selectNone").click(function (e) {
        		        	var mcbx = $("#masterCbx");
        		            $("#masterCbx").prop("checked",false);
        		            $('.selector').each(function() {
        		                if ($(mcbx).is(":checked")) {
        		                    $(this).prop('checked',true);
        		                    $(this).closest('tr').addClass("active");
        		                } else {
        		                    $(this).prop('checked',false);
        		                    $(this).closest('tr').removeClass("active");
        		                }
        		            });
        		            calculate();
        		        });
        		        
        		        
        		        function calculate() {
        		        	var iMac = document.zipForm.nZip; // array of inputs
        		        	var totalOrder = 0;                // list all checked ids
        		        	var totalCost = 0.0;               // add values to calculate cost
        		        	var eEmployers = 0;
        		        	var zips = '';

        		        	for (var i = 0; i < iMac.length; i++){
        		        	    if (iMac[i].checked) {
        		        	        zips += iMac[i].id + ",";       // add ids separated by spaces
        		        	        totalOrder++;
        		        	        totalCost += parseFloat(iMac[i].value); // add value attributes, assuming they are integers

									var tdi = parseInt(iMac[i].parentNode.id.substring(3));
        							var za = document.getElementById("za-"+tdi).innerHTML;
        							var zi = document.getElementById("zi-"+tdi).innerHTML;
        							eEmployers += (za - zi);
        		        	    }
        		        	}
        		        	document.getElementById("totalCost").innerHTML = totalCost;
        		        	document.getElementById("eligibleEmployers").innerHTML = eEmployers;
        		        	document.getElementById("zipCodes").innerHTML = totalOrder;
        		        	document.getElementById("total").value = totalCost;
        		        	document.getElementById("zips").value = zips;
        		        }
        		        
        		    });
             
             ;(function($) {
            	   $.fn.fixMe = function() {
            	      return this.each(function() {
            	         var $this = $(this),
            	            $t_fixed;
            	         function init() {
            	            $this.wrap('<div class="container" />');
            	            $t_fixed = $this.clone();
            	            $t_fixed.find("tbody").remove().end().addClass("fixed").insertBefore($this);
            	            resizeFixed();
            	         }
            	         function resizeFixed() {
            	            $t_fixed.find("th").each(function(index) {
            	               $(this).css("width",$this.find("th").eq(index).outerWidth()+"px");
            	            });
            	         }
            	         function scrollFixed() {
            	            var offset = $(this).scrollTop(),
            	            tableOffsetTop = $this.offset().top,
            	            tableOffsetBottom = tableOffsetTop + $this.height() - $this.find("thead").height();
            	            if(offset < tableOffsetTop || offset > tableOffsetBottom)
            	               $t_fixed.hide();
            	            else if(offset >= tableOffsetTop && offset <= tableOffsetBottom && $t_fixed.is(":hidden"))
            	               $t_fixed.show();
            	         }
            	         $(window).resize(resizeFixed);
            	         $(window).scroll(scrollFixed);
            	         init();
            	      });
            	   };
            	})(jQuery);

            	$(document).ready(function(){
            	   $('[data-toggle="tooltip"]').tooltip();
            	   $("table").fixMe();
            	   $(".up").click(function() {
            	      $('html, body').animate({
            	      scrollTop: 0
            	   }, 2000);
            	 });
            	});
             
        
         </script>

</head>
<body>
	<%@include file="Header.jsp"%>
	<%String MONTH = request.getParameter("month"); %>
	<div class='container-fluid'>
		<!-- <div class="container search-page content">


			<div class="select-multi"> -->

				<span class="navbar-center"><h2 class="wch2"><b>${county} COUNTY - ${month}</b></h2>
			<!-- 	<nav class="navbar">
			
					<ul class="nav navbar-nav navbar-right" style="float: right;">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">Select <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#" id="selectAll">All</a></li>
								<li><a href="#" id="selectNone">None</a></li>
							</ul></li>
					</ul>
					</span>
					</nav> -->

					<div class="content-list">
					
			<form action="WCPaymentDetailsWithZip" name="zipForm" id="zipForm" method="POST" class="form-inline">
					
					<input type="hidden" name="county" value="${county}"/>
					<input type="hidden" name="month" value="${month}"/>
					<input type="hidden" name="total" id="total" />
					<input type="hidden" name="zips" id="zips" />
					
					<nav class="navbar">
            <ul class="nav navbar-nav navbar-left">
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Selected Zip Codes : <span id="zipCodes">0</span></a>
                </li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Eligible Employers: <span id="eligibleEmployers">0</span>
                  </a>
                </li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Total Cost : $<span id="totalCost">0</span>
                  </a>
                </li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  	<img src="images/Buy.bmp" onclick="submitBuyWithZipForm()" />
                  </a>
                </li>
            </ul>
              </nav>
					
						<table align="center"
							class="table-bordered blue mytable table-hover">
							<thead>
								
								<tr>
								 	<th>Select<br/><input type="checkbox" id="masterCbx"></th>
									<th><a href="WCDetailWithZip?county=${county}&month=<%=MONTH%>&sort=zip">Zip</a></th>
									<th><a href="WCDetailWithZip?county=${county}&month=<%=MONTH%>&sort=type">Type</a></th>
									<th><a href="WCDetailWithZip?county=${county}&month=<%=MONTH%>&sort=city">City</a></th>
									<th><a href="WCDetailWithZip?county=${county}&month=<%=MONTH%>&sort=total">Total<br/>Employers</a></th>
									<th><a href="WCDetailWithZip?county=${county}&month=<%=MONTH%>&sort=available">Available</a></th>
									<th><a href="WCDetailWithZip?county=${county}&month=<%=MONTH%>&sort=ineligible">INELIGIBLE</a></th>
									<th><a href="WCDetailWithZip?county=${county}&month=<%=MONTH%>&sort=cost">COST</a></th>
								</tr>
							</thead>
							<tbody>

								<c:forEach items="${ratesByZip}" var="zip" varStatus="zipCount">
									<tr class="normalRow">
									    <c:choose>
											<c:when test="${zip.purchased || zip.subscribed}">
												<c:set var="total" value="0"/>
											</c:when>
											<c:otherwise>
												<c:set var="total" value="${(zip.available - zip.ineligible)*leadcost}"/>
											</c:otherwise>
									    </c:choose>
										
										<c:choose>
											<c:when test="${zip.purchased || zip.subscribed}">
												<td class="${tdClass}" id="zc-${zipCount.index}"></td>
											</c:when>
											<c:otherwise>
												<td class="${tdClass}" id="zc-${zipCount.index}"><input type="checkbox" name="nZip" id="${zip.zip}" value="${total}" class="selector"/></td>
											</c:otherwise>
										</c:choose>

										<td class="normal" id="zz-${zipCount.index}">${zip.zip}</td>
										<td class="normal">${zip.type}</td>
										<td class="normal">${zip.city}</td>
										<td class="normal" id="zt-${zipCount.index}">${zip.total}</td>
										<td class="normal" id="za-${zipCount.index}">${zip.available}</td>
										<td class="normal" id="zi-${zipCount.index}">${zip.ineligible}</td>

										<c:choose>
											<c:when test="${zip.purchased}">
												<td class="normal">Purchased</td>
											</c:when>
											<c:when test="${zip.subscribed}">
												<td class="normal">Subscribed</td>
											</c:when>
											<c:otherwise>
												<td class="normal">$${total}</td>
											</c:otherwise>
										</c:choose>
										
									</tr>
								</c:forEach>


							</tbody>
						</table>
	
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
						
						</form>
						
					</div>
			<!-- </div>
		</div> -->
	</div>
</body>
</html>
