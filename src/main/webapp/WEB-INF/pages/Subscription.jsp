<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <%
           // Set to expire far in the past.
      response.setDateHeader("Expires", 0);
  // Set standard HTTP/1.1 no-cache headers.
     response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
  // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
     response.addHeader("Cache-Control", "post-check=0, pre-check=0");
  // Set standard HTTP/1.0 no-cache header.
      response.setHeader("Pragma", "no-cache");
         
        %>   
        <title>PA Employer County Details</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script src="script/wc.js"></script>
         
    </head>
    <body>
        <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container search-page content">
              <div class="select-multi">
               <div class="head"><h2 class="wch2">PA Employer County Subscription</h2></div>
               <div class="zip-table">
               <table align="center">
                 <thead>
                   <tr class="search tbzip">
                     <th>County</th>
                     <th>${County}</th>
                   </tr>
                 </thead>
                 <tbody class="zip-rows">
                   <tr>    <td>Data Start Month</td>                   <td>${startMonth}</td>           </tr>
                   <tr>    <td>Average Leads Per Month</td>            <td>${avgTotalEmpCount} </td>    </tr>
                   <tr>    <td>Average Monthly Price</td>              <td>$<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${avgPrice}" /></td>  </tr>
                   <tr>    <td>Subscription Price (10% discount)</td>  <td>$<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${subscriptionPrice}" /></td>  </tr> 
                   <tr>    <td>PA Sales Tax</td>                       <td>$<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${salesTax}" /></td>   </tr>
                   <tr>    <td>Total</td>                              <td>$<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${subscriptionPrice + salesTax}" /></td>  </tr>
                 </tbody>
               </table>

<form name="_xclick" action="${paypalUrl}" method="post" id="paypalForm" class="form-inline" target="_top">
<input type="hidden" name="business" value="${paypalBusiness}">
<input type="hidden" name="return" value="${paypalReturn}">
<input type="hidden" name="notify_url" value="${paypalNotifyUrl}">

<input type="hidden" name="cmd" value="_xclick-subscriptions">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="no_shipping" value="1">
<img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">

<c:if test="${trialDays != 0}">
<input type="hidden" name="a1" value="<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${totalAmount}" />">
<input type="hidden" name="p1" value="${trialDays}">
<input type="hidden" name="t1" value="D">
</c:if>

<input type="hidden" name="a3" value="<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${totalAmount}" />">
<input type="hidden" name="p3" value="1">
<input type="hidden" name="t3" value="M">
<input type="hidden" name="src" value="1">
<input type="hidden" name="sra" value="1">
<input type="hidden" name="tax_rate" value="6">
<input type="hidden" name="custom" value="${username}">
<input type="hidden" name="item_name" value="kswcrc (${County} - starting ${startMonth})">

<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

<div class=""><h2 class="align-center">
                   		
                   		<button type="submit" name="method_premium" value="PayPal - The safer, easier way to pay online!">
						  <img src="images/Subscribe.png" alt="PayPal - The safer, easier way to pay online!" />
						</button>
                   		
                   	</h2></div>
                   <div class=""><h2 class="align-center"><img src="images/card.png" class="mywidth-two" alt="" /></h2></div>

</form>
	    
<%-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="paypalForm" class="form-inline" target="_top">
<!-- <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="paypalForm" class="form-inline" target="_top"> -->
	 
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="klj1962@gmail.com">
<input type="hidden" name="item_name" value="${County} - ${Month}">
<input type="hidden" name="amount" value="${Price}">
<input type="hidden" name="tax_rate" value="6">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="custom" value="${username}">
<input type="hidden" name="return" value="http://174.55.128.43/wcserver/WCThanks">    <!-- kswcrc.com/WCThanks -->
<input type="hidden" name="notify_url" value="http://174.55.128.43/wcserver/WCPayPal">    <!-- kswcrc.com/WCPayPal -->
<br/><p><b>PayPal account is not required.</b><br/>On PayPal login screen, scroll down to "Pay with Debit or Credit Card" button.</p><br/>
<!-- <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1"> -->

<!-- <input type="image" src="http://www.paypal.com/en_GB/i/btn/x-click-but20.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!"> -->
<!-- <input type="image" src="https://www.paypalobjects.com/en_GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!"> -->

    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>


	    
                   <div class=""><h2 class="align-center">
                   		
                   		<button type="submit" name="method_premium" value="PayPal - The safer, easier way to pay online!">
						  <img src="images/buybtn.png" alt="PayPal - The safer, easier way to pay online!" />
						</button>
                   		
                   		<!-- 	<a href="#" onclick="submitPaypal1();"><img src="images/buybtn.png"  name="submit" alt="PayPal - The safer, easier way to pay online!" class="mywidth" alt="" /></a> -->
                   	</h2></div>
                   <div class=""><h2 class="align-center"><img src="images/card.png" class="mywidth-two" alt="" /></h2></div>
                  </form> --%>
               </div>
             </div>
           </div>
     </div>
     
        </body>
</html>
