<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<header>
          <nav class="navbar">
            <div class="container-fluid">
              <div class="navbar-header">
                  <a class="" href="WCSearch"><img src="images/logo.jpg" alt="" /></a>
              </div>
              <ul class="nav navbar-nav">
                  <p>&nbsp;</p>
              </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Search
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="WCSearch">My Data</a></li>
                    <li><a href="WCStats">More Data</a></li>
                    <% request.getSession().setAttribute("isAdmin", request.isUserInRole("ROLE_ADMIN")); %>
<c:if test="${sessionScope.isAdmin}">
                    <li><a href="WCCollection">Statistics</a></li>
                    <li><a href="WCStatsAdmin">Stats (Admin)</a></li>
</c:if>
                  </ul>
                </li>
                  <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profile
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="updateProfile">Change Data</a></li>
                    <li><a href="WCChangePassword">Change Password</a></li>
                    <li><a href="WCPreferences">Preferences</a></li>
                  </ul>
                </li>
                  <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Feedback
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="makefeedback">Make Suggestion</a></li>
                  <c:if test="${sessionScope.isAdmin}">
                    <li><a href="WCAddAnnouncements">Add Announcements</a></li>
				</c:if>
                  </ul>
                </li>
                  <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Help
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="FAQs">FAQs</a></li>
                    <li><a href="contactUs">Contact</a></li>
                    <li><a href="WCTerms">Terms & Conditions</a></li>
                    <li><a href="tutorial">Tutorials</a></li>
                    <li><a href="WCAnnouncements">Announcements</a></li>
                  </ul>
                </li>
                  <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="padding-top: 6px;"><img src="images/user.png" alt="" />
                 
                  <ul class="dropdown-menu">
                    <li><a href="WCLogoff">Logout</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </nav>
        </header>
</body>
</html>