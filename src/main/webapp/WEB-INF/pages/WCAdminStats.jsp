<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <%
           // Set to expire far in the past.
      response.setDateHeader("Expires", 0);
  // Set standard HTTP/1.1 no-cache headers.
     response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
  // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
     response.addHeader("Cache-Control", "post-check=0, pre-check=0");
  // Set standard HTTP/1.0 no-cache header.
      response.setHeader("Pragma", "no-cache");
         
        %>   
        <title>PA Employer Collection Statistics</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
    </head>
    <body>
       <%@include file="Header.jsp" %>
       
      <div class='container-fluid'>
          <div class="container search-page content">
             
              <div class="select-multi">
                  <form id="DetailForm" action="WCStatsAdmin" method="post" class="form-inline">
             
                    <div class="head"><h2 class="wch2">PA Employer Collection Statistics</h2></div>
                      
                      <div class="radio_part">
							<div class="left_radio">
								<div class="radio_button">
									<label class="radio_con">
    									<input type="radio" name="radioSelect" value="leftRadio" ${leftRadio }="${leftRadio }">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="checkbox_list">
									<div class="checkbox_top">
										<label class="chk_con">Empty
	    									<input type="checkbox" name="emptyCheckbox" value="checked" ${emptyCheckbox }="${emptyCheckbox }">
  											<span class="checkmark"></span>
  										</label>
									</div>
									<div class="checkbox_top">
										<label class="chk_con">Obsolete as of
	    									<input type="checkbox" name="obsoleteCheckbox"  value="checked" ${obsoleteCheckbox }="${obsoleteCheckbox }">
  											<span class="checkmark"></span>
  										</label>
  										<div class="date_field">
  											<input type="date" name="obsoleteDate" class="form-control" value="${obsoleteDate }">
  										</div>
									</div>
								</div>
								<div class="class"></div>
							</div>
							<div class="right_radio">
								<div class="radio_button">
									<div class="radio_button">
										<label class="radio_con">
	    									<input type="radio" name="radioSelect" value="rightRadio" ${rightRadio }="${rightRadio }">
											<span class="checkmark"></span>
										</label>
									</div>
								</div>
								<div class="checkbox_list">
									
									<div class="form_date">
										<p>Collected Form</p>
										<input type="date" name="collectedFromDate" class="form-control" value="${collectedFromDate }">
									</div>
									<div class="form_date">
										<p>Collected To</p>
										<input type="date" name="collectedToDate" class="form-control" value="${collectedToDate }">
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="class"></div>
							</div>
							<div class="clearfix"></div>
						</div>
<input type="submit" name="submit" value="submit"/>
                      
                       <div class="state-table">
                           <table class="statstable" align="center">
		  <tbody><tr class="search tbheader">
			<th>COUNTY</th>
			<th>JAN</th>
			<th>FEB</th>
			<th>MAR</th>
			<th>APR</th>
			<th>MAY</th>
			<th>JUN</th>
			<th>JULY</th>
			<th>AUG</th>
			<th>SEPT</th>
			<th>OCT</th>
			<th>NOV</th>
			<th>DEC</th>
		  </tr>
			
<c:forEach items="${subscribedMap}" var="vstats" varStatus="outerLoop">
<!--  allStats returns Map<String,CountyStats> (vstats) -->


			<tr id="row-${vstats.key}" class="countyRows">

 	            <td class="countyTd" id="cty-${vstats.key}">
 	            	<a href="#">${vstats.key}</a>  <br>
 	            </td>

	            <c:forEach items="${vstats.value.monthMap}" var="counts" varStatus="otherLoop">

	            <td class="countyTd statsTd totals" >${counts.value }</td>

	            </c:forEach>
	        </tr>
	    </c:forEach>
</table>
	    

<p>&nbsp;</p>

 <table class="statstable" align="center">
		  <tbody><tr class="search tbheader">
			<th>COUNTY</th>
			<th>JAN</th>
			<th>FEB</th>
			<th>MAR</th>
			<th>APR</th>
			<th>MAY</th>
			<th>JUN</th>
			<th>JULY</th>
			<th>AUG</th>
			<th>SEPT</th>
			<th>OCT</th>
			<th>NOV</th>
			<th>DEC</th>
		  </tr>
			
<c:forEach items="${availableMap}" var="vstats" varStatus="outerLoop">
<!--  allStats returns Map<String,CountyStats> (vstats) -->


			<tr id="row-${vstats.key}" class="countyRows">

 	            <td class="countyTd" id="cty-${vstats.key}">
 	            	<a href="#">${vstats.key}</a>  <br>
 	            </td>

	            <c:forEach items="${vstats.value.monthMap}" var="counts" varStatus="otherLoop">

	            <td class="countyTd statsTd totals" >${counts.value }</td>

	            </c:forEach>
	        </tr>
	    </c:forEach>
</table>
	    
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
                           <p>&nbsp;</p>
                       </div>  
                     
                  </form>
                   
             </div>
           </div>
     </div>
        </body>
</html>
