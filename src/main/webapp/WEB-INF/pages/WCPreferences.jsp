<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <%
           // Set to expire far in the past.
      response.setDateHeader("Expires", 0);
  // Set standard HTTP/1.1 no-cache headers.
     response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
  // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
     response.addHeader("Cache-Control", "post-check=0, pre-check=0");
  // Set standard HTTP/1.0 no-cache header.
      response.setHeader("Pragma", "no-cache");
         
        %>   
        <title>PA Employer Preferences</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-duallistbox.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script src="script/jquery.bootstrap-duallistbox.js"></script>
    </head>
    <body>
       <%@include file="Header.jsp" %>
       
      <div class='container-fluid'>
          <div class="container search-page content">
             
              <div class="select-multi">
                  <form id="DetailForm" action="updatePreferences" method="post" class="form-inline">
             
                    <div class="head"><h2 class="wch2">PA Employer Preferences</h2></div>
                      
                      <div class="duallist">
							<select multiple="multiple" size="15" name="duallistbox" class="demo2">
						        <c:forEach items="${codeMap}" var="map" varStatus="outerLoop">
						        	<option value="${map.key }">${map.key } - ${map.value}</option>
						        </c:forEach>
						        <c:forEach items="${preferencesList}" var="map" varStatus="outerLoop">
						        	<option selected="selected" value="${map.key }">${map.key } - ${map.value}</option>
						        </c:forEach>
						    </select>
						</div>

                     <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
<br/>
<button type="button" class="btn btn-default"><a href="WCSearch">CANCEL</a></button>
<button type="submit" class="btn btn-default">UPDATE PREFERENCES</button>
                     
                  </form>
                   <br/>
             </div>
           </div>
     </div>
        </body>
        <script>
        var demo2 = $('.demo2').bootstrapDualListbox({
          preserveSelectionOnMove: 'moved',
          moveOnSelect: false
        });
    </script>
</html>
