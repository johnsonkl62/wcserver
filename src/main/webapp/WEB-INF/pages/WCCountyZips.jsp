<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>PA Employer Zip Code to County Xref</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
    </head>
    <body>
         <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container search-page content">
             
              <div class="select-multi">
                  <form action="" class="form-inline">
               <div class="head"><h2 class="wch2">PA Employer Zip Codes to County</h2></div>
               <h3 class="wch3">NOTE: Many zip codes cross county lines.<br>
Each zip code is assigned to ONE county, containing its largest area or population.</h3>

               <div class="zip-table">
                   <table align="center">
		  <thead>
                      <tr><td><h2 class="wch2">${countyKey}</h2></td></tr>
                      <tr class="search tbzip">
			<th>Zip</th>
			<th>Type</th>
			<th>City</th>
		  </tr>
                  </thead>
                  <tbody class="zip-rows">
	
		<c:forEach items="${countyZips}" var="zip" varStatus="zipCount">
			<tr>
 	            <td>${zip.zip}</td>
 	            <td>${zip.type}</td>
 	            <td>${zip.city}</td>
 	        </tr>
	    </c:forEach>
	    
	    
	    </tbody></table>
               </div>
                     
                  
                    
                  </form>
                   
             </div>
           </div>
     </div>
        </body>
</html>
