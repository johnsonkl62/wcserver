<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>PA Employer Zip Code to County Xref</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<script type="text/javascript" src="script/wc.js"></script>

<script>
         $(document).ready(
        		    function() { 
        			$('.mytable tr').click(function(event) {
        			    if ( !$(this).parent().is('thead') ) {
        		                if ($(this).hasClass('active')) {
        		                    $(':checkbox', this).unbind('click');
        		                    $(this).removeClass('active'); 
        		                } // else {
        		                    if (event.target.type !== 'checkbox') {
        		                        $(':checkbox', this).trigger('click');
        		                    }
        		                //}
        		            }
        			    calculate();
        		        });
        			
        		        $(".selector").change(function (e) {
        		            //  Single row checkbox clicked
        		            if ($(this).is(":checked")) {
        		                $(this).closest('tr').addClass("active");
        		            } else {
        		                $(this).closest('tr').removeClass("active");
        		            }
        		            calculate();
        		        });
        		        
        		        $("#masterCbx").change(function (e) {
        		            var mcbx = this;
        		            $('.selector').each(function() {
        		                if ($(mcbx).is(":checked")) {
        		                    $(this).prop('checked',true);
        		                    $(this).closest('tr').addClass("active");
        		                } else {
        		                    $(this).prop('checked',false);
        		                    $(this).closest('tr').removeClass("active");
        		                }
        		            });
        		            calculate();
        		        });
        		        $("#selectAll").click(function (e) {
        		        	var mcbx = $("#masterCbx");
        		            $("#masterCbx").prop("checked",true);
        		            $('.selector').each(function() {
        		                if ($(mcbx).is(":checked")) {
        		                    $(this).prop('checked',true);
        		                    $(this).closest('tr').addClass("active");
        		                } else {
        		                    $(this).prop('checked',false);
        		                    $(this).closest('tr').removeClass("active");
        		                }
        		            });
        		            calculate();
        		        });
        		        $("#selectNone").click(function (e) {
        		        	var mcbx = $("#masterCbx");
        		            $("#masterCbx").prop("checked",false);
        		            $('.selector').each(function() {
        		                if ($(mcbx).is(":checked")) {
        		                    $(this).prop('checked',true);
        		                    $(this).closest('tr').addClass("active");
        		                } else {
        		                    $(this).prop('checked',false);
        		                    $(this).closest('tr').removeClass("active");
        		                }
        		            });
        		            calculate();
        		        });
        		        

        		        var months = ["JAN","FEB","MAR", 
        		        	          "APR","MAY","JUN",
        		        	          "JUL","AUG","SEP", 
        		        	          "OCT","NOV","DEC"];

        		        function getMonthInt(p) {
        		        	for (i=0; i<months.length; i++) {
        		        		if ( months[i] === p ) return i;
        		        	}
        		        	return -1;
        		        }
        		        
        		        function setMonth(p1,p2) {
        		        	if (p1 === '') {
        		        		return p2;
        		        	} else if ( p1 === p2 ) {
        		        		return p1;
       		        		} else {
	        		        	var d = new Date();
	        		        	var cur = d.getMonth();
	        		        	var p1m = (getMonthInt(p1) + 12 - cur) % 12;
	        		        	var p2m = (getMonthInt(p2) + 12 - cur) % 12;

	        		        	return ( p1m > p2m ? p1 : p2 );
        		        	}
        		        }

        		        function calculate() {
        		        	var iMac = document.zipForm.nZip; // array of inputs
        		        	var totalOrder = 0;                // list all checked ids
        		        	var totalCost = 0;                  // add values to calculate cost
        		        	var eEmployers = 0;
        		        	var zips = '';
        		        	var firstMonth = '';

        		        	for (var i = 0; i < iMac.length; i++){
        		        	    if (iMac[i].checked) {
        		        	        zips += iMac[i].id + ",";       // add ids separated by spaces
        		        	        totalOrder++;
        		        	        totalCost += parseFloat(iMac[i].value); // add value attributes, assuming they are integers

									var tdi = parseInt(iMac[i].parentNode.id.substring(3));
        							var zt = document.getElementById("zt-"+tdi).innerHTML;
        							var zma = document.getElementById("zma-"+tdi).innerHTML;    //  Month available for this zip code

        							firstMonth = setMonth(firstMonth,zma);
        							eEmployers -= (-zt);   // double negative needed to make it integer rather than string
        		        	    }
        		        	}
        		        	//  Displayed values
        		        	document.getElementById("totalCost").innerHTML = totalCost;
        		        	document.getElementById("eligibleEmployers").innerHTML = eEmployers;
        		        	document.getElementById("zipCodes").innerHTML = totalOrder;
        		        	document.getElementById("firstMonth").innerHTML = firstMonth;
        		        	//  Form values
        		        	document.getElementById("subscrMonth").value = firstMonth;
        		        	document.getElementById("total").value = totalCost;
        		        	document.getElementById("zips").value = zips;
        		        }
        		        
        		    });
             
             ;(function($) {
            	   $.fn.fixMe = function() {
            	      return this.each(function() {
            	         var $this = $(this),
            	            $t_fixed;
            	         function init() {
            	            $this.wrap('<div class="container" />');
            	            $t_fixed = $this.clone();
            	            $t_fixed.find("tbody").remove().end().addClass("fixed").insertBefore($this);
            	            resizeFixed();
            	         }
            	         function resizeFixed() {
            	            $t_fixed.find("th").each(function(index) {
            	               $(this).css("width",$this.find("th").eq(index).outerWidth()+"px");
            	            });
            	         }
            	         function scrollFixed() {
            	            var offset = $(this).scrollTop(),
            	            tableOffsetTop = $this.offset().top,
            	            tableOffsetBottom = tableOffsetTop + $this.height() - $this.find("thead").height();
            	            if(offset < tableOffsetTop || offset > tableOffsetBottom)
            	               $t_fixed.hide();
            	            else if(offset >= tableOffsetTop && offset <= tableOffsetBottom && $t_fixed.is(":hidden"))
            	               $t_fixed.show();
            	         }
            	         $(window).resize(resizeFixed);
            	         $(window).scroll(scrollFixed);
            	         init();
            	      });
            	   };
            	})(jQuery);

            	$(document).ready(function(){
            	   $('[data-toggle="tooltip"]').tooltip();
            	   $("table").fixMe();
            	   $(".up").click(function() {
            	      $('html, body').animate({
            	      scrollTop: 0
            	   }, 2000);
            	 });
            	});
             
        
         </script>

</head>
<body>
	<%@include file="Header.jsp"%>
	<%String MONTH = request.getParameter("month"); %>
	<div class='container-fluid'>
		<!-- <div class="container search-page content">


			<div class="select-multi"> -->

				<span class="navbar-center"><h2 class="wch2"><b>SUBSCRIPTION - ${county} COUNTY</b></h2>
			<!-- 	<nav class="navbar">
			
					<ul class="nav navbar-nav navbar-right" style="float: right;">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">Select <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#" id="selectAll">All</a></li>
								<li><a href="#" id="selectNone">None</a></li>
							</ul></li>
					</ul>
					</span>
					</nav> -->

					<div class="content-list">
					
			<form action="SubscriptionPaymentDetailsWithZip" name="zipForm" id="zipForm" method="POST" class="form-inline">

					<input type="hidden" name="county" value="${county}"/>
					<input type="hidden" name="month" id="subscrMonth" value="${month}"/>
					<input type="hidden" name="total" id="total" />
					<input type="hidden" name="zips" id="zips" />

					<nav class="navbar">
            <ul class="nav navbar-nav navbar-left">
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Selected Zip Codes : <span id="zipCodes">0</span></a>
                </li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Eligible Employers: <span id="eligibleEmployers">0</span>
                  </a>
                </li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Total Cost : $<span id="totalCost">0</span>
                  </a>
                </li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">First Available Month: <span id="firstMonth">&nbsp;</span>
                  </a>
                </li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  	<img src="images/Subscribe.jpg" onclick="submitBuyWithZipForm()" />
                  </a>
                </li>
            </ul>
              </nav>

						<table align="center"
							class="table-bordered blue mytable table-hover">
							<thead>
								
								<tr>
								 	<th>Select<br/><input type="checkbox" id="masterCbx"></th>
									<th><a href="SubscribeByZip?county=${county}&sort=zip">Zip</a></th>
									<th><a href="SubscribeByZip?county=${county}&sort=type">Type</a></th>
									<th><a href="SubscribeByZip?county=${county}&sort=city">City</a></th>
									<th><a href="SubscribeByZip?county=${county}&sort=total">Average Monthly<br/>Available</a></th>
									<th><a href="SubscribeByZip?county=${county}&sort=avail">Month<br/>Available</a></th>
									<th><a href="SubscribeByZip?county=${county}&sort=cost">Average<br/>Cost</a></th>
								</tr>
							</thead>
							<tbody>

								<c:forEach items="${ratesByZip}" var="zip" varStatus="zipCount">
									<c:set var="total" value="${(zip.available - zip.ineligible)*leadcost}"/>
									<c:if test="${total>0}">
									<tr class="normalRow">
										<c:choose>
										  <c:when test="${not zip.subscribed}">
  										    <td class="${tdClass}" id="zc-${zipCount.index}"><input type="checkbox" name="nZip" id="${zip.zip}" value="${total}" class="selector"/></td>
										</c:when>
										<c:otherwise>
  										    <td>&nbsp;</td>
										</c:otherwise>
										</c:choose>
										<td class="normal" id="zz-${zipCount.index}">${zip.zip}</td>
										<td class="normal">${zip.type}</td>
										<td class="normal">${zip.city}</td>
										<td class="normal" id="zt-${zipCount.index}">${zip.available}</td>
										<td class="normal" id="zma-${zipCount.index}">${zip.monthAvailable}</td>
										<c:choose>
										  <c:when test="${not zip.subscribed}">
										<td class="normal">$${total}</td>
										</c:when>
										<c:otherwise>
										<td class="normal">Subscribed</td>
										</c:otherwise>
										</c:choose>
									</tr>
									</c:if>
								</c:forEach>


							</tbody>
						</table>
	
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
						
						</form>
						
					</div>
			<!-- </div>
		</div> -->
	</div>
</body>
</html>
