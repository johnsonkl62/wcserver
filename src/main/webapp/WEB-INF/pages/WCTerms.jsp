<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="ROBOTS" content="NOINDEX" />
<title>Terms and Conditions</title>
<link rel="stylesheet" href="css/wc.css" type="text/css" />
<script type="text/javascript" src="script/wc.js"></script>
<style>
.required{display:none;
color:red;
font-style:bold;}
</style>
<script>
function acceptAndSubmit() {
	var form = document.getElementById('aform');
	var chk = document.getElementById('tcaccept');
	if ( !chk.checked ) {
		document.getElementById("failed").style.display='block';
	} else {
		form.submit();
	}
}
</script>
</head>
<body>
<form action="WCAccept" method="POST" id="aform">

<div class="tch1">ONLINE WEB SERVICE AGREEMENT</div>
<p>Last Updated: March 26, 2017</p>

<p>Synesis, Inc.("Provider"), a Pennsylvania corporation, will provide you access to the Web Services (defined below)and related website located at kswcrc.com or such other Web addresses or uniform resource locators as may be specified by the Provider (collectively, the "Site"), specifically and solely for the purposes of requesting and receiving Data (defined below), Third Party Data (defined below), and services under the related Purchase Plans (defined below) and Support Plan (defined below).  Please read carefully the following terms and conditions (this "Agreement").  This Agreement governs your access to and use of the Site, Web Services, Data and Third Party Data, and constitutes a binding legal agreement between you (referred to herein as "You" or "Customer") and Provider.</p>

<p>YOU ACKNOWLEDGE AND AGREE THAT, BY CLICKING THE CHECKBOX OR BY ACCESSING OR USING THE SITE, WEB SERVICES, DATA OR THIRD PARTY DATA, YOU ARE INDICATING THAT YOU HAVE READ, UNDERSTAND AND AGREE TO BE BOUND BY THIS AGREEMENT.  IF YOU DO NOT AGREE TO THIS AGREEMENT, THEN YOU HAVE NO RIGHT TO ACCESS OR USE THE SITE, WEB SERVICES, DATA OR THIRD PARTY DATA.  If you accept or agree to this Agreement on behalf of a company or other legal entity, you represent and warrant that you have the authority to bind that company or other legal entity to this Agreement and, in such event; "Customer", "You" and "Your" will refer and apply to that company or other legal entity.</p>

<div class="tch2">DEFINITIONS</div>
<ul>
<li>"Web Services" consist of a set of software programs, interfaces, and webpages running on computers hosted at Provider sites or at third-party hosting facilities accessible via the Internet as described in this Agreement and the Site.</li>
<li>"Data" means the rates and other insurance information that is (i) owned by Provider and (ii) obtained by Provider from public sources, exchanges and other sources delivered to Customer via the Web Services and as described in this Agreement and on the Site.</li>
<li>"Third Party Data" means the rates and other insurance information that is (i) not owned by Provider and (ii) obtained by Provider from public sources, exchanges and other sources delivered to Customer via the Web Services and as described in this Agreement and on the Site.</li>
<li>"Data Owner" means a legal entity that holds ownership rights to some of the Data or Third Party Data (defined below) and is the original licensing source of such portion of the Data or Third Party Data when such portion of the Data or Third Party Data is not in the public domain.  In cases of Third Party Data that is not publicly available, Provider has secured redistribution agreements with the Data Owner.</li>
<li>"Distinct Software Applications" means software applications that automate substantially different business products, services, processes or functions of Customer.  Provider reserves the right, in its sole discretion, to make a reasonable determination as to whether software applications that use the Web Services or the Data or Third Party Data constitute Distinct Software Applications.</li>
<li>"Data Exchange Format" means an electronic version of the Data or Third Party Data used for sharing the Data or Third Party Data between software applications, including but not limited to any application programming interface (API), any database access (e.g., ODBC, etc.), any network transmission format (e.g., EDI, SOAP, RSS, XML, etc.) and any data file format (e.g., XLS, CSV, etc.).  Provider reserves the right, in its sole discretion, to determine if a particular electronic version of the Data or Third Party Data constitutes a Data Exchange Format.</li>
</ul>

<div class="tch2">MODIFICATION</div>

<p>Provider reserves the right to modify, discontinue or terminate the Site, Web Services, Data and Third Party Data or to modify this Agreement, at any time and without prior notice.  If Provider modifies this Agreement, Provider will post the modification on the Site or provide you with notice of the modification.  Provider will also update the "Last Updated Date" at the top of this Agreement.  By continuing to access or use the Site, Web Services, Data and Third Party Data after Provider has posted a modification on the Site or has provided you with notice of a modification, you are indicating that you agree to be bound by the modified Agreement.  If the modified Agreement is not acceptable to You, Your only recourse is to cease using the Site, Web Services, Data and Third Party Data.</p>

<div class="tch2">REGISTRATION</div>

<p>In order to access the Site, Web Services, Data and Third Party Data, You must register to create an account ("Account").  During the registration process, You will be required to provide certain information and You will establish a password.  You agree to provide accurate, current and complete information during the registration process and to update such information to keep it accurate, current and complete.  Provider reserves the right to suspend or terminate Your Account if any information provided during the registration process or thereafter proves to be inaccurate, not current or incomplete.  You are responsible for safeguarding Your password.  You agree not to disclose Your password to any third party and to take sole responsibility for any activities or actions under Your Account, whether or not You have authorized such activities or actions.  You will immediately notify Provider of any unauthorized use of Your Account.</p>

<div class="tch2">PURCHASES AND DATA</div>

<p>Web Services Purchases.  Subject to Customer's compliance with the terms and conditions of this Agreement, upon purchase of area-specific and time-specific data Provider grants to Customer a limited, non-exclusive, non-transferable, license to access and use the Web Services solely for its business purposes.  This Agreement governs Customer's access to and use of the Web Services and the Site. Customer acknowledges and agrees that but for this Agreement, Customer would have no rights or access to the Web Services and the Site.  All purchases are FINAL.</p>

<p>Proprietary Rights.  Subject to the limited rights expressly granted hereunder, Customer acknowledges that the Data, Third Party Data, Web Services, Site, and/or any developments to the Data, Third Party Data, Web Services, and Site that result from services provided hereunder ("Developments") provided to Customer that are not publicly available, are proprietary in nature and owned exclusively by Provider and/or the Data Owners.  The Data, Third Party Data, the Web Services, as well as the Developments are to be used exclusively as described herein.</p>

<div class="tch3">Restrictions on the Web Services.</div>
<ul>
<li>Customer may use the Web Services solely with any software application owned or licensed by Customer.</li>
<li>Customer may not use, adapt, modify, redistribute, sublicense, sell or otherwise make available any portion of the Web Services for use by software applications not owned or licensed by Customer.</li>
<li>Neither party will attempt to access, tamper with, or use non-public areas of the other party's website, computer systems, or the technical delivery systems of the other party's providers.</li>
<li>Neither party will attempt to probe, scan, or test the vulnerability of any of the other party's systems or networks or breach any of the other party's security or authentication measures.</li>
</ul>

<div class="tch3">Restrictions on the Data and Third Party Data.</div>
<ul>
<li>Customer may use the Data and Third Party Data solely with any software application owned or licensed by Customer.</li>
<li>Customer will not use, redistribute, sublicense, sell or otherwise make available any portion of the Data or Third Party Data in any Data Exchange Format for use in software applications not owned or licensed by Customer.</li>
<li>Customer may not display or make the Data or Third Party Data available in any Data Exchange Format to non-employee users, including but not limited to, vendors, contractors, partners and the general public (e.g., public websites, partner and vendor extranets, EDI applications, etc.).</li>
<li>Customer may cache and store the Data and Third Party Data for use within each Distinct Software Application provided such use is in compliance with restrictions imposed by the Data Owners.  The Data or Third Party Data may not be shared between Distinct Software Applications or made available in any Data Exchange Format for the purpose of sharing between Distinct Software Applications.</li>
</ul>

<p>Enforcement.  Except as otherwise provided herein, Customer is responsible for all of Customer's activities occurring through its use of the Web Services.</p>

<p>Telecommunications and Internet Services.  Customer acknowledges that the use of the Web Services by Customer is dependent upon access to telecommunications and Internet services.  Customer will be solely responsible for acquiring and maintaining all telecommunications and Internet services and other hardware and software required to access and use the Web Services, including, without limitation, any and all costs, fees, expenses, and taxes of any kind related to the foregoing.</p>


<div class="tch2">TERM AND TERMINATION</div>

<p>Term.  This Agreement will commence when You click "I accept" or "I agree" and shall continue until terminated in writing.  Thereafter, this Agreement shall renew on the date of each additional purchase of Web Services, Data or Third Party Data.  This Agreement shall continue from the date of initial registration until terminated in writing.  There is no term of service or requirement for continued use of this site.  All sales are final. </p>

<p>Right to Terminate. Either party may terminate this Agreement at any time.  After registration and acceptance, either Party may terminate this Agreement for any reason by delivering written or electronic notice of termination.  Notwithstanding the foregoing, either party may terminate this Agreement at any time in the event that the other party breaches any material term of this Agreement and fails to remedy such breach within ten (10) business days after receipt of a written notice of any such breach, or if such breach cannot be remedied within that period of time, fails to demonstrate to the satisfaction of the non-breaching party that it is taking steps reasonably necessary to remedy the breach.</p>

<p>Inability to Provide Data and Third Party Data.  If Provider ever loses the license, right or ability to provide any portion of the Data or Third Party Data, or such license or right is ever interrupted or otherwise impaired, then Provider agrees to notify Customer of such fact.  Initial access to purchased Data or Third Party Data constitutes full delivery of purchased product.  No refunds of any kind will be provided.  Customer is advised to immediately download and save all purchased data for independent and offline access in the event that Provider loses the ability to provide access.</p>

<p>Survival. The provisions of the Definitions Section and Sections that by their nature should reasonably survive, and any amendments to the provisions of the aforementioned will survive any termination or expiration of this Agreement.</p>

<p>Customer Responsibilities.  Customer will promptly report any errors in the operation of the Web Services to Provider and will not take any actions that would increase the severity of the error.  Customer will use the Web Services solely as described herein.</p>

<p>Enhancements and Modifications.  Provider will provide to Customer enhancements or modifications ("Updates") to the Web Services as they become available.</p>


<div class="tch2">WARRANTIES, INDENMITY, AND LIMITATION OF LIABILITY</div>

<p>Indemnification.  Customer agrees to defend (or settle), indemnify and hold Provider, its employees, directors and officers harmless from and against any and all liabilities, losses, damages, or expenses (including court costs and reasonable attorneys fees) in connection with any third party claim that the Customer's use of the Web Services or Data in violation of this Agreement infringes or misappropriates any Intellectual Property Rights of any third party, provided that Provider does not make any admission of Customer guilt without Customer's prior written approval and provided that Provider gives Customer (i) prompt written notification of the claim or action, (ii) sole control and authority over the defense or settlement thereof, and (iii) all reasonably available information, assistance and authority to settle and/or defend any such claim or action.</p>

<p>Warranty Disclaimers.  OTHER THAN SPECIFICALLY SET FORTH HEREIN, (i) THE WEB SERVICES, SITE, DATA, AND THIRD PARTY DATA ARE DELIVERED TO CUSTOMER ON AN "AS IS" BASIS, WITHOUT ANY WARRANTIES OR REPRESENTATIONS, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY, ACCURACY OF INFORMATION PROVIDED, NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE, QUALITY, OR PERFORMANCE, (ii) PROVIDER MAKES NO WARRANTY THAT THE SITE, DATA, OR THIRD PARTY DATA WILL MEET CUSTOMER'S SPECIFIC OBJECTIVES OR NEEDS, (iii) PROVIDER MAKES NO WARRANTY THAT THE WEB SERVICES, SITE, DATA, OR THIRD PARTY DATA WILL BE FREE FROM ERRORS OR BUGS, and (iv) PROVIDER MAKES NO WARRANTY THAT THERE WILL BE UNINTERRUPTED OPERATION OF THE WEB SERVICES, SITE, DATA, AND THIRD PARTY DATA.  CUSTOMER ACKNOWLEDGES THAT (i) ANY DATA DOWNLOADED THROUGH THE USE OF THE WEB SERVICES AND SITE IS DONE AT ITS OWN DISCRETION AND RISK, AND THAT CUSTOMER WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO ITS COMPUTER SYSTEM OR LOSS OF APPLICATIONS OR DATA THAT RESULTS FROM THE DOWNLOAD OF SUCH DATA and (ii) THE FOREGOING EXCLUSIONS AND DISCLAIMERS OF WARRANTIES ARE AN ESSENTIAL PART OF THIS AGREEMENT AND FORMED THE BASIS FOR DETERMINING THE PRICE CHARGED FOR THE PRODUCTS.  NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED FROM PROVIDER OR ELSEWHERE SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THIS AGREEMENT.  SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES IN CERTAIN CIRCUMSTANCES.  ACCORDINGLY, SOME OF THE LIMITATIONS SET FORTH ABOVE MAY NOT APPLY.</p>

<p>Accuracy of Data and Third Party Data.  PROVIDER MAKES NO WARRANTY REGARDING THE DATA, THIRD PARTY DATA OR ANY OTHER INFORMATION PURCHASED OR OBTAINED THROUGH PROVIDER'S SITE AND/OR THE WEB SERVICES, OR THE ACCURACY, TIMELINESS, TRUTHFULNESS, COMPLETENESS OR RELIABILITY OF ANY DATA, THIRD PARTY DATA OR OTHER INFORMATION OBTAINED THROUGH PROVIDER'S WEBSITE AND/OR THE WEB SERVICES.</p>

<p>Limitation of Liability.  UNLESS OTHERWISE PROVIDED HEREIN, IN NO EVENT WILL PROVIDER'S AGGREGATE LIABILITY TO CUSTOMER AND ANY THIRD PARTY IN CONNECTION WITH THIS AGREEMENT OR CUSTOMER'S ACCESS TO OR USE OF THE WEB SERVICES EXCEED FEES COLLECTED FROM CUSTOMER, REGARDLESS OF THE FORM OR THEORY OF THE CLAIM OR ACTION.  PROVIDER WILL NOT BE LIABLE FOR ANY INDIRECT, CONSEQUENTIAL, SPECIAL, PUNITIVE, EXEMPLARY OR RELIANCE DAMAGES ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, OR ANY DAMAGES RESULTING FROM ANY INTERRUPTION OR DISRUPTION IN COMMUNICATIONS OR SERVICES, UNAVAILABILITY OR INOPERABILITY OF SERVICES, TECHNICAL MALFUNCTION, LOST DATA, OR LOST PROFITS, EVEN IF PROVIDER KNEW OR SHOULD HAVE KNOWN OF THE POSSIBILTY OF OR COULD HAVE REASONABLY PREVENTED SUCH DAMAGES, AND NOTWITHSTANDING THE FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.</p>

<div class="tch2">MISCELLANEOUS PROVISIONS</div>

<p>Excusable Delays. Any delay in performance of any provision of this Agreement caused by conditions beyond the reasonable control of either party will not constitute a breach of this Agreement, provided that the delaying party has taken reasonable measures to notify the affected party of the delay in writing and uses reasonable efforts to perform in accordance with this Agreement notwithstanding such conditions.  The delayed party's time for performance will be deemed extended for a period equal to the duration of the conditions beyond its control.  Conditions beyond a party's reasonable control include, but are not limited to, natural disasters, acts of government, acts of terrorism, power failures, major network failures, fires, riots, and acts of war (collectively, "Excusable Delays").</p>

<p>Compliance with Laws and Policies.  The parties hereby agree to abide by and comply with all applicable local, state, national, and international laws and regulations (including applicable laws that pertain to the transmission of technical data, privacy, the encryption of software, the export of technology, the transmission of obscenity, or the permissible uses of intellectual property).</p>

<p>Controlling Law.  The parties agree that the laws of the Commonwealth of Pennsylvania will govern this Agreement and all matters arising out of or related to this Agreement.  The parties submit to the jurisdiction of the courts of the Commonwealth of Pennsylvania.  The parties expressly agree to venue in the state and federal courts located in State College, Pennsylvania and waive any objection based on personal jurisdiction.</p>

<p>Entire Agreement and Severability.  This Agreement, as amended from time to time according to its terms, shall constitute the entire agreement between Customer and the Provider respecting the Site, the Web Services, the Support, the Data, and the Third Party Data described herein, and shall supersede all prior agreements, arrangements, representations or promises, whether oral or written, as to its subject matter.  This Agreement may be amended only as specified herein.</p>

<p>Force Majeure.  The Provider and their respective affiliates shall not be deemed to be in default of any provision hereof or be liable for any delay, failure in performance, or interruption of service resulting directly or indirectly from acts of God, civil or military authority, civil disturbance, war, terrorism, strikes, fires, other catastrophes, power or telecommunications failure or any other cause beyond its reasonable control.</p>

<p>Waiver.  No waiver by either party of any default by the other in the performance of any provisions of this Agreement shall operate as a waiver of any continuing or future default, whether of a like or different character.</p>

<p>Assignment.  Neither party may assign this Agreement without prior written consent unless in connection with a merger or acquisition of either party.</p>

<p>Severability.  If any provision of this Agreement (or any portion thereof) shall be invalid, illegal or unenforceable, the validity, legality or enforceability of the remainder of this Agreement shall not in any way be affected or impaired thereby.</p>

<p>Relationship Between the Parties.  Nothing in this Agreement shall be construed to create a partnership, joint venture or agency relationship between the parties.  Neither party will have the power to bind the other or to incur obligations on the other's behalf without such other party's prior written consent.</p>

<p>No Third-Party Beneficiaries.  This Agreement is intended for the sole and exclusive benefit of Customer and Provider and is not intended to benefit any third party.  Only the parties to this Agreement may enforce it.</p>

<p>Notice.  The parties may give notice to each other via email, fax or certified mail.  Notices sent to Provider should be directed to info@synesis-inc.com.  Notices sent to Customer will be sent to Customer at the email address provided during registration to use the Web Services.</p>

			<br/>
			<br/>
			
			<a href="WCStats">Accept</a>

			<br/>

			 
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
			
</form>
</body>
</html>