<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
        <title>Announcements</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

<body>
  <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container menu-content">
            <div class="form-class">
              <div class="announce">
				    <div class="head">Announcement</div>
					 <div class="anouncemain">
					 <c:forEach items="${announcements}" var="announcement">
					    <!-- <div class="anounbox"> Today we are having great party. <span>New</span> <a href=""><img src="img/cross.png" alt="" /></a></div>
					   <div class="anounbox"> We have upgraded this application. Please read this carefully. <a href=""><img src="img/cross.png" alt="" /></a></div> -->
					    <div class="anounbox"> ${announcement} </div>
					 </c:forEach>
					 </div>
				</div>
            </div>
           
           </div>
     </div>


</body>
</html>