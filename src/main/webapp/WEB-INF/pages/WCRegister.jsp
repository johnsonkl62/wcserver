<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>PA Employer Search</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
        
    </head>
    <body>
        <header>
          <nav class="navbar">
            <div class="container-fluid">
              <div class="navbar-header">
                  <a class="" href="#"><img src="images/logo.jpg" alt="" /></a>
              </div>
              <ul class="nav navbar-nav">
                  <p>&nbsp;</p>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
              </ul>
            </div>
          </nav>
        </header>
      <div class='container-fluid'>
          <div class="container menu-content">
            <div class="form-class">
              <form name="regForm" id="regForm" action="register" method="post">
                <div class="form-group">
                  <label for="userId">Username(no special characters):</label>
                  <input type="text"  size="30" name="username" class="form-control" id="userId" onblur="checkUser()"> <span id="validUser" style="display:none">&nbsp;</span>
                </div>
                <div class="form-group">
                 <div class="form-inline">
                  <label for="pwd"> Password:</label>
                  <input type="password" size="10" name="pwd" class="form-control" id="pwd">
                  <label for="confirm_password">Confirm:</label>
                  <input type="password" size="10" name="confirm_password" class="form-control" id="confirm_password">
                </div>
                </div>
                <div class="form-group">
                <div class="form-inline">
                  <label for="last">Last Name:</label>
                  <input type="text" size="10" name="last" class="form-control" id="last">
                  <label for="text">First Name:</label>
                  <input type="text" size="8" name="first" id="first" class="form-control">
                  <label for="text">MI:</label>
                  <input type="text" size="1" name="mi" class="form-control">
                </div>
                </div>
                  
                 <!--  <input type="text" class="col-lg-8 zero-padding-left" name="first" id="first">&nbsp;&nbsp;<input type="text" name="mi" size="5"/> -->
                  
                <div class="form-group">
               <div class="form-inline">
                  <label for="text">Agency:</label>
                  <input type="text" class="form-control" name="agency" size="40">
                </div>
                </div>
               <div class="form-group">
               <div class="form-inline">
                  <label for="emailId">Email:</label>
                  <input type="text" class="form-control" id="emailId" name="email" size="40" onblur="validateEmail()"><span id="validEmail" style="display:none">&nbsp;</span>
                </div>
                </div>
               <div class="form-group">
               <div class="form-inline">
                  <label for="text">Phone:</label>
                  <input type="text" class="form-control" id="phoneId" name="phone" size="40" onblur="validatePhone()"/><span id="validPhone" style="display:none">&nbsp;</span>
                </div>
                </div>
                
                <div class="form-group">
                  <label for="text">Select up to three carriers that you represent for comparison against current employers' rates:</label>
                  <select multiple class="form-control" id="carriers" name="carriers" size="40" style="height:150pt">
					<c:forEach items="${carriers}" var="carrier" varStatus="loop">
						<option value="${carrier.naic}">${carrier.name}</option>
					</c:forEach>
				</select>
                </div>
                <div class="form-group">
					<label for="text">Selected:</label>
					<select multiple class="form-control" id="selectCarrier" name="selectCarrier" size="40" style="height:50pt">
					</select>
				</div>
                <button type="button" onclick="myFunction('regForm');" class="btn btn-default">REGISTER</button>

    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
                
              </form>
            </div>
           
           </div>
     </div>
        </body>
</html>
