<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Login Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
          <nav class="navbar">
            <div class="container-fluid">
              <div class="navbar-header">
                  <a class="" href="#"><img src="images/logo.jpg" alt="" /></a>
              </div>
              <ul class="nav navbar-nav">
                  <p>&nbsp;</p>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="WCRegister"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
              </ul>
            </div>
          </nav>
        </header>
      <div class='container-fluid'>
          <div class="container menu-content">
            <div class="form-class">
              <form action="login" method="post">
                <div class="form-group">
                  <label for="username">USER NAME:</label>
                  <input type="text" class="form-control" name="username" id="username">
                </div>
                <div class="form-group">
                  <label for="password">PASSWORD:</label>
                  <input type="password" class="form-control" name="password" id="password">
                </div>
                
                <!-- <div class="checkbox">
                    <label><input type="checkbox"><span style="font-weight: bold"> Remember me</span></label>
                </div> -->
     
      <input type="hidden" name="page" value="${page }"/>
                 
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <button type="submit" class="btn btn-default">LOGIN</button>
              </form>
              <br/> 
			<div class="link">
                  <label><a href="forgotpassword"><span style="font-weight: bold"> Forgot Password</span></a></label>
                </div>
<div class="link">
                  <label><a href="forgotusername"><span style="font-weight: bold"> Forgot Username</span></a></label>
                </div>
              
    
            </div>
            <!--announcement -->
			    <div class="announce">
				    <div class="head">Announcement</div>
					 <div class="anouncemain">
					 <c:forEach items="${announcements}" var="announcement">
					    <!-- <div class="anounbox"> Today we are having great party. <span>New</span> <a href=""><img src="img/cross.png" alt="" /></a></div>
					   <div class="anounbox"> We have upgraded this application. Please read this carefully. <a href=""><img src="img/cross.png" alt="" /></a></div> -->
					    <div class="anounbox"> ${announcement} </div>
					 </c:forEach>
					 </div>
				</div>
				
				
			   <!--announcement -->
           </div>
     </div>
        </body>
</html>
