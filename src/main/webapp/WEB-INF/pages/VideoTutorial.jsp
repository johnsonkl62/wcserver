<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Tutorial</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
       
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="script/wc.js"></script>
    </head>
    <body>
        <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container">
    <br />
    <br />
    <br />

    <div class="panel-group" id="accordion">
        <div class="faqHeader">Tutorials</div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a >Registration</a>
                </h4>
            </div>
            <div id="collapseOne" align="center" class="panel-collapse collapse in">
                <div align="center"  class="panel-body">
                    <video width="100%" height="85%"  controls>
					  <source src="video/KSWCRC Registration.mp4" type="video/mp4">
					</video>
                </div>
            </div>
        </div>
        <br/><br/><br/><br/><br/><br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a >Basic Search</a>
                </h4>
            </div>
            <div id="collapseOne" align="center" class="panel-collapse collapse in">
                <div align="center" class="panel-body">
                    <video  width="100%" height="85%"  controls>
					  <source src="video/KSWCRC Registration.mp4" type="video/mp4">
					</video>
                </div>
            </div>
        </div>
        <br/><br/><br/><br/><br/><br/>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a >Advanced Search</a>
                </h4>
            </div>
            <div id="collapseOne" align="center" class="panel-collapse collapse in">
                <div align="center"  class="panel-body">
                    <video width="100%" height="85%" controls>
					  <source src="video/KSWCRC Advanced Search.mp4" type="video/mp4">
					</video>
                </div>
            </div>
        </div>
        
        <br/><br/><br/><br/><br/><br/>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a >Downloads</a>
                </h4>
            </div>
            <div id="collapseOne" align="center" class="panel-collapse collapse in">
                <div align="center"  class="panel-body">
                    <video width="100%" height="85%" controls>
					  <source src="video/KSWCRC Downloads.mp4" type="video/mp4">
					</video>
                </div>
            </div>
        </div>
        
        <br/><br/><br/><br/><br/><br/>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a >Purchase Search</a>
                </h4>
            </div>
            <div id="collapseOne" align="center" class="panel-collapse collapse in">
                <div align="center"  class="panel-body">
                    <video width="100%" height="85%" controls>
					  <source src="video/KSWCRC Purchase Search.mp4" type="video/mp4">
					</video>
                </div>
            </div>
        </div>
        
        
    </div>
</div>

<style>
    .faqHeader {
        font-size: 27px;
        margin: 20px;
    }

    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'Glyphicons Halflings';
        content: "\e072"; /* "play" icon */
        float: right;
        color: #F58723;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }

    .panel-body {
        text-align: left;
        font-size: 14px;
        line-height: 20px;
    }
</style>
     </div>
        </body>
</html>