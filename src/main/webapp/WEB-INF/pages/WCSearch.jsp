<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>PA Employer Search</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="stylesheet" href="css/pickmeup.css" type="text/css" />
			<script type="text/javascript" src="script/pickmeup.min.js"></script>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/wc.css" rel="stylesheet" type="text/css"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
          <script type="text/javascript" src="script/wc.js"></script>
			
    </head>
    <body>
    
    <script type="text/javascript">
			
    $(document).ready(function () {
        
        $('.datepick').each(function(){
        	pickmeup(this, {
        		format	: 'Y-m-d',
        		hide_on_select : true,
        		default_date: false
        	});
           
        });
        
       
    });
			
			</script>
    
       <%@include file="Header.jsp" %>
      <div class='container-fluid'>
          <div class="container search-page content">
             
              <div class="select-multi">
                  <form action="WCData" method="post" class="form-inline">

               <div class="head"><h2 class="wch2">Purchased Data</h2></div>
                    <sec:authorize access="!hasRole('ROLE_ADMIN')">
                    <h3 class="wch3">Use Ctrl or Shift to select or deselect purchases</h3>
                    <c:choose>
						<c:when test="${ empty purchases }">
							<a href="WCStats">See available data</a>	
						</c:when>
						<c:otherwise>
						<select multiple name="subs">
							<c:forEach items="${purchases}" var="sub" varStatus="loop">
								<option value="${sub.id}" selected>${sub.county} - ${sub.zipCode } (${sub.month})</option>
							</c:forEach>
						</select>
						<br/>
						<a href="WCStats">See more available data</a>	
						</c:otherwise>
					</c:choose>
					</sec:authorize>
                    <div class="row">
                        <div class="form-group">
                            <label for="minDate">FROM</label>
                            	<input type="text" class="form-control datepick" name="minDate" id="minDate" size="15" onblur="validateDate();"/>
                          </div>
                          <div class="form-group">
                            <label for="maxDate">TO</label>
                            	<input type="text"  class="form-control datepick" name="maxDate" id="maxDate" size="15" onblur="validateDate();"/>
                          </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            Limit Ex Dates by date range (Use format: yyyy-mm-dd)
                          </div>
                    </div>
                    
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
					 <div class="row">
						<div class="form-group">
                            <label for="counties">Counties:</label>
                            <input type="text"  class="form-control" name="counties" id="counties" size="75" onblur="checkCounties();"/>
                    	</div>
					</div>
					</sec:authorize>
                   
                    
                <div class="head"><h2 class="wch2">Filter Results By</h2></div>
                     <div class="row margin-top">
                        <div class="form-group" style="width:40%">
                          <label for="">Employer Name:</label>
                          <input type="text" class="form-control" name="empName">
                        </div>
                         <div class="form-group" style="width:8%">
                          <label for="">Exact:</label>
                          <input type="checkbox" name="cbxEmpNameExact">
                        </div>
                     </div>

					 <div class="row">
						<div class="form-group">
                            <label for="street">Street:</label>
                            <input type="text"  class="form-control" name="street">
                    	</div>
					</div>
					 <div class="row">
						<div class="form-group">
                            <label for="zip">Zip:</label>
                            <input type="text"  class="form-control" name="zip">
                    	</div>
					</div>

                    <div class="row margin-top">
                        <div class="form-group" style="width:40%">
                          <label for="">Carrier(s)**: &nbsp; &nbsp; &nbsp; &nbsp;</label>
                          <input type="text" class="form-control" name="carriers">
                        </div>
                         <div class="form-group" style="width:8%">
                          <label for="">Exact:</label>
                          <input type="checkbox" name="cbxCarrierExact">
                        </div>
                     </div>
                   <div class="row margin-top">
                        <div class="form-group" style="width:40%">
                          <label for="">Class Code(s)**:</label>
                          <input type="text" class="form-control" name="gccs" onblur="checkCodes();"> 
                        </div>
                         <div class="form-group" style="width:8%">
                        </div>
                        
                       <div><label for="" class="mlable"> Example: 661,951,953</label></div>
                     </div>
                     <div class="row margin-top">
                        <div class="form-group" style="width:40%">
                          <label for="">Description(s)**:</label>
                          <input type="text" class="form-control" name="descs">
                        </div>
                         <div class="form-group" style="width:8%">
                        </div>
                        
                     <div><label for="" class="mlable"> Example: electric,plumbing,carpentry</label></div>
                     </div>
                    <div class="row caution">
                        <p>**Use commas to separate multiple values</p>
                    </div>
                <div class="sear"><h2 class="wch2"><button type="submit" value="">Search</button></h2></div>
                
                		
			 
    <input type="hidden"
name="${_csrf.parameterName}"
value="${_csrf.token}"/>
                
                  </form>
                   
             </div>
           </div>
     </div>
        </body>
</html>
