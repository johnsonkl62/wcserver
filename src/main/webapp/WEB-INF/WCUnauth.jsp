<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PA Employer Search - Unauthorized</title>
<link rel="stylesheet" href="css/wc.css" type="text/css" />
</head>

<body>
	<form action="WCSearch.jsp">
	<h1>Authorization failure</h1>
	<br/>
	<p>You are not authorized to view the results of the selected search.</p>
	<br/>
	<input type="submit" value="return to search page"/> 
</form>
</body>
</html>