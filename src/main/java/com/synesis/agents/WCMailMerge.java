package com.synesis.agents;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;

import com.google.common.base.Preconditions;
import com.synesis.utility.Configuration;
import com.synesis.utility.LogUtil;
//import com.synesis.utility.GeneratePdfFromTemplate;

//import org.dstadler.commons.logging.jdk.LoggerFactory;

/**
 * Simple application which performs a "mail-merge" of a Microsoft Word template
 * document which contains replacement templates in the form of ${name}, ${first-name}, ...
 * and an Microsoft Excel spreadsheet which contains a list of entries that are merged in.
 *
 * Call this application with parameters <word-template> <excel/csv-template> <output-file>
 *
 * The resulting document has all resulting documents concatenated.
 *
 * @author dominik.stadler
 *
 */
public class WCMailMerge {
//    private static final Logger log = LoggerFactory.make();
	private static final String BASE = "/tmp/tomcat/";

	
    public static void main(String[] args) throws Exception {
//        LoggerFactory.initLogging();

        if(args.length != 3) {
            throw new IllegalArgumentException("Usage: MailMerge <word-template> <excel/csv-template> <output-file>");
        }

        File wordTemplate = new File(args[0]);
        File excelFile = new File(args[1]);
        String outputFile = args[2];

        if(!wordTemplate.exists() || !wordTemplate.isFile()) {
            throw new IllegalArgumentException("Could not read Microsoft Word template " + wordTemplate);
        }
        if(!excelFile.exists() || !excelFile.isFile()) {
            throw new IllegalArgumentException("Could not read data file " + excelFile);
        }

        new WCMailMerge().mergeLabels(wordTemplate, excelFile, outputFile);
    }

    public void merge(File wordTemplate, File dataFile, String outputFile, String username) throws Exception {
        LogUtil.log("Merging data from " + wordTemplate + " and " + dataFile + " into " + outputFile);

        // read the data-rows from the CSV or XLS(X) file
        Data data = new Data();
        data.read(dataFile);

        // now open the word file and apply the changes
       try (InputStream is = new FileInputStream(wordTemplate)) {
            try (XWPFDocument doc = new XWPFDocument(is)) {
                // apply the lines and concatenate the results into the document
                applyLines(data, doc);

                Configuration.verifyBaseFolder();
                LogUtil.log("Writing overall result to " + outputFile);
                try (OutputStream out = new FileOutputStream(BASE + outputFile)) {
                    doc.write(out);

                }
            }
        }
//        GeneratePdfFromTemplate.generatePdf(BASE, outputFile, data.getKeyValueMap(), username);
    }


	/*
	 * private void generatePdf(String outputFile) throws Exception {
	 * 
	 * ByteArrayOutputStream bo = new ByteArrayOutputStream();
	 * 
	 * InputStream in = new BufferedInputStream(new
	 * FileInputStream(BASE+outputFile));
	 * 
	 * Future<Boolean> conversion = converter .convert(in).as(DocumentType.MS_WORD)
	 * .to(bo).as(DocumentType.PDF) .prioritizeWith(1000) // optional .schedule();
	 * conversion.get(); try (OutputStream outputStream = new
	 * FileOutputStream(BASE+outputFile+".pdf")) { bo.writeTo(outputStream); } catch
	 * (IOException e) { e.printStackTrace(); } in.close(); bo.close();
	 * 
	 * }
	 */

    private void applyLines(Data dataIn, XWPFDocument doc) throws XmlException, IOException {
        // small hack to not having to rework the commandline parsing just now
        String includeIndicator = System.getProperty("org.dstadler.poi.mailmerge.includeindicator");

        CTBody body = doc.getDocument().getBody();

        XmlOptions optionsOuter = new XmlOptions();
        optionsOuter.setSaveOuter();

        // read the current full Body text
        String srcString = body.xmlText();
//        LogUtil.log(srcString);
        
        // apply the replacements
        boolean first = true;
        List<String> headers = dataIn.getHeaders();
        for(List<String> data : dataIn.getData()) {
            LogUtil.log("Applying to template: " + data);

            // if the special option is set ignore lines which do not have the indicator set
            if(includeIndicator != null) {
                int indicatorPos = headers.indexOf(includeIndicator);
                Preconditions.checkState(indicatorPos >= 0,
                        "An include-indicator is set via system properties as %s, but there is no such column, had: %s",
                        includeIndicator, headers);

                if(!(data.get(indicatorPos).equals("1") ||
                                data.get(indicatorPos).equalsIgnoreCase("true"))) {
                    LogUtil.log("Skipping line " + data + " because include-indicator was not set");
                    continue;
                }
            }

            String replaced = srcString;
            for(int fieldNr = 0;fieldNr < headers.size();fieldNr++) {
                String header = headers.get(fieldNr);
                String value = data.get(fieldNr);

                // ignore columns without headers as we cannot match them
                if(header == null) {
                    continue;
                }

                // use empty string for data-cells that have no value
                if(value == null) {
                    value = "";
                }

                replaced = replaced.replaceAll(header, value);
            }

            // check for missed replacements or formatting which interferes
            if(replaced.contains("${")) {
                LogUtil.log(srcString);
                LogUtil.log("Still found template-marker after doing replacement: " +
                        StringUtils.abbreviate(StringUtils.substring(replaced, replaced.indexOf("${")), 200));
            }

            appendBody(body, replaced, first);
            XWPFParagraph paragraph = doc.createParagraph();
            paragraph.setPageBreak(true);
            first = false;
        }
    }

    private static void appendBody(CTBody src, String append, boolean first) throws XmlException {
        XmlOptions optionsOuter = new XmlOptions();
        optionsOuter.setSaveOuter();
        String srcString = src.xmlText();
        String prefix = srcString.substring(0,srcString.indexOf(">")+1);

        final String mainPart;
        // exclude template itself in first appending
        if(first) {
            mainPart = "";
        } else {
            mainPart = srcString.substring(srcString.indexOf(">")+1,srcString.lastIndexOf("<"));
        }

        String sufix = srcString.substring( srcString.lastIndexOf("<") );
        String addPart = append.substring(append.indexOf(">") + 1, append.lastIndexOf("<"));
        CTBody makeBody = CTBody.Factory.parse(prefix+mainPart+addPart+sufix);
        src.set(makeBody);
    }

	public void mergeLabels(File wordTemplate, File dataFile, String outputFile) {
        LogUtil.log("Merging data from " + wordTemplate + " and " + dataFile + " into " + outputFile);

        try {
			// read the data-rows from the CSV or XLS(X) file
			Data data = new Data();
			data.read(dataFile);

			// now open the word file and apply the changes
			try (InputStream is = new FileInputStream(wordTemplate)) {
			    try (XWPFDocument doc = new XWPFDocument(is)) {
			        // apply the lines and concatenate the results into the document
			        applyLinesToPage(data, doc);

			        Configuration.verifyBaseFolder();
			        LogUtil.log("Writing overall result to " + outputFile);
			        try (OutputStream out = new FileOutputStream(BASE + outputFile)) {
			            doc.write(out);
			        }
			    }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void applyLinesToPage(Data dataIn, XWPFDocument doc) throws XmlException, IOException {

		try {
			CTBody body = doc.getDocument().getBody();

			XmlOptions optionsOuter = new XmlOptions();
			optionsOuter.setSaveOuter();

			// // read the current full Body text
			String srcString = body.xmlText();
//			LogUtil.log(srcString);
			while ( srcString.contains("[NAME]") ) {
				LogUtil.log(srcString.substring(srcString.indexOf("[NAME]"),srcString.indexOf("[NAME]")+30) );
				srcString = srcString.replace("[NAME]", "Replace String");
			}
			// String replaced = srcString;

//			List<PackagePart> w = doc.getAllEmbedds();
//			LogUtil.log("w: " + (w == null ? w : w.size()));
//			List<XWPFPictureData> x = doc.getAllPackagePictures();
//			LogUtil.log("x: " + (x == null ? x : x.size()));
//			List<XWPFPictureData> y = doc.getAllPictures();
//			LogUtil.log("y: " + (y == null ? y : y.size()));

			List<IBodyElement> z = doc.getBodyElements();
			LogUtil.log("z: " + (z == null ? z : z.size()));
			for ( IBodyElement ibe : z ) {
				LogUtil.log("ibe body: " + ibe.getBody() );
				LogUtil.log("ibe part: " + ibe.getPart() );
				LogUtil.log("ibe str: " + ibe.toString() );
			}
			
//			List<XWPFFootnote> c = doc.getFootnotes();
//			LogUtil.log("c: " + (c == null ? c : c.size()));
//			XWPFHeaderFooterPolicy d = doc.getHeaderFooterPolicy();
//			LogUtil.log("d: " + d);

			XWPFParagraph g = doc.getLastParagraph();
			LogUtil.log("g: " + g.toString());

			OPCPackage i = doc.getPackage();
			LogUtil.log("i: " + i.toString());
			List<XWPFParagraph> j = doc.getParagraphs();
			LogUtil.log("j: " + (j == null ? j : j.size()));
			
//			CTStyles l = doc.getStyle();
//			LogUtil.log("l: " + l);

			List<XWPFTable> n = doc.getTables();
			LogUtil.log("n: " + (n == null ? n : n.size()));
			XWPFTable xt = n.get(0);
			LogUtil.log("xt: " + xt.getText());

			//
			// for( XWPFParagraph p : doc.getParagraphs()) {
			// List<XWPFRun> runs = p.getRuns();
			// if ( runs != null ) {
			// String text = r.getText(0);
			// if ( text != null && text.contains("") ) {
			//
			// }
			// }
			// }
			//

			// for(List<String> data : dataIn.getData()) {
			// LogUtil.log("Applying to template: " + data);
			// List<String> headers = dataIn.getHeaders();
			//
			// for(int fieldNr = 0;fieldNr < headers.size();fieldNr++) {
			// String header = headers.get(fieldNr);
			// String value = data.get(fieldNr);

			// if ( replaced.contains(header) ) {
			// int index = replaced.indexOf(header);
			// LogUtil.log("Body[" + index + "]: " +
			// replaced.substring(index, index+header.length()));
			// replaced = replaced.replace(header, value);
			// LogUtil.log("Body[" + index + "]: " +
			// replaced.substring(index, index+header.length()));
			// }
			// }
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
