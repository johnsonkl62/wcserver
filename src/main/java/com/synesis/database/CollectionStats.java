package com.synesis.database;

public class CollectionStats {

		private int total = -1;  // the sum of all month totals
		private int done = -1;   // the sum of all month totals

		public CollectionStats() {
		}

		public int getTotal() {
			return total;
		}
		public void setTotal(int total) {
			this.total = total;
		}
		public int getDone() {
			return done;
		}
		public void setDone(int done) {
			this.done = done;
		}
		
		public String toString() {
			return done + " / " + total;
		}
		
}
