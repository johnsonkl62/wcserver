package com.synesis.database;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TreeMap;

import com.synesis.utility.LogUtil;
import com.synesis.utility.MonthEnum;

public class CountyStats {
		private String name;

		private int total = -1;  // the sum of all month totals
		private int done = -1;   // the sum of all month totals
		private int comps = -1;   // the sum of all month totals
		private boolean reservedFull = false;
		private boolean reservedPartial = false;

		private TreeMap<MonthEnum,MonthStats> monthStats = new TreeMap<MonthEnum,MonthStats>();
		
		private static SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd"); 	

		private Calendar thisMonth = Calendar.getInstance();

		public CountyStats(String n) {
			name = n;
		}
		public String getName() {
			return name;
		}
		public int getTotal() {
			return total;
		}
		public void setTotal(int total) {
			this.total = total;
		}
		public int getDone() {
			return done;
		}
		public void setDone(int done) {
			this.done = done;
		}
		public int getComps() {
			return comps;
		}
		public void setComps(int comps) {
			this.comps = comps;
		}
		public boolean isReservedFull() {
			return reservedFull;
		}
		public void setReservedFull(boolean reservedFull) {
			this.reservedFull = reservedFull;
		}
		public boolean isReservedPartial() {
			return reservedPartial;
		}
		public void setReservedPartial(boolean reservedPartial) {
			this.reservedPartial = reservedPartial;
		}
		
		public MonthStats[] getMonthStats() {
			MonthStats[] orderedMap = new MonthStats[12];
	
			thisMonth = Calendar.getInstance();
			thisMonth.set(Calendar.DAY_OF_MONTH, 1);
	
			int index = thisMonth.get(Calendar.MONTH) % 12;
			for (int i = 0; i < 12; i++) {
				String month = MonthEnum.values()[index++].name();
	
				MonthEnum me = MonthEnum.valueOf(month);
				orderedMap[i] = monthStats.get(me);
				index %= 12;
			}
			return orderedMap;
		}

		public MonthStats getMonth(String month) {
			MonthEnum me = MonthEnum.valueOf(month);
			return monthStats.get(me);
		}

		public MonthStats getMonthByColumnId(int column) {
			int index = thisMonth.get(Calendar.MONTH) % 12;
			MonthEnum me = MonthEnum.values()[(column+index)%12];
			return monthStats.get(me);
		}

		public String getMonthNameByColumnId(int column) {
			int index = thisMonth.get(Calendar.MONTH) % 12;
			return MonthEnum.values()[(column+index)%12].name();
		}

		public void setTotal(String month, Integer count) {
			MonthStats ms = monthStats.get(MonthEnum.valueOf(month));
			if ( ms == null ) {
				ms = new MonthStats();
				monthStats.put(MonthEnum.valueOf(month), ms);
			}
			ms.setTotal(count);
		}

		public void setDone(String month, Integer count) {
			MonthStats ms = monthStats.get(MonthEnum.valueOf(month));
			if ( ms == null ) {
				ms = new MonthStats();
				monthStats.put(MonthEnum.valueOf(month), ms);
			}
			ms.setDone(count);
		}

		public void setComps(String month, Integer count) {
			MonthStats ms = monthStats.get(MonthEnum.valueOf(month));
			if ( ms == null ) {
				ms = new MonthStats();
				monthStats.put(MonthEnum.valueOf(month), ms);
			}
			ms.setComps(count);
		}

		public void setPurchase(String username, String month, java.util.Date buyDate, String zip) {
			LogUtil.log("Purchased[" + name + "]:   " + username + "    - " + month + " - " + dt.format(buyDate));
	
			if ("ALL".equalsIgnoreCase(month)) {
				for (MonthEnum me : MonthEnum.values() ) {
					MonthStats ms = monthStats.get(me);
					if (ms != null) {
						if (zip != null) {
							ms.setIsPartial("true");
						} else {
							ms.setBought(username);
						}
					}
				}
			} else {
				MonthStats ms = monthStats.get(MonthEnum.valueOf(month));
				if (ms != null) {
					if (zip != null) {
						ms.setIsPartial("true");
					} else {
						ms.setBought(username);
					}
				}
			}
			
		}

		public void setReserved(String username, java.util.Date buyDate, Integer zip) {
			LogUtil.log("Reserved[" + name + "]:   " + username + "    - " + dt.format(buyDate));
			for (MonthEnum me : MonthEnum.values() ) {
				MonthStats ms = monthStats.get(me);
				if (ms != null) {
					if (zip != null) {
						ms.setIsPartial("true");
					} else {
						ms.setReserved(username);
					}
				}
			}
			reservedFull = (zip == null);
			reservedPartial = (zip != null);
		}
}
