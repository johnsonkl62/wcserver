package com.synesis.database;

public class MonthStats {

	private int total = 0;
	private int done = 0;
	private int comps = 0;
	private String bought = null;
	private String reserved = null;
	private String isPartial;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getDone() {
		return done;
	}

	public void setDone(int done) {
		this.done = done;
	}

	public int getComps() {
		return comps;
	}

	public void setComps(int comps) {
		this.comps = comps;
	}

	public boolean isAvailable() {
		return (bought == null && reserved == null);
	}

	public String getBought() {
		return bought;
	}

	public void setBought(String bought) {
		this.bought = bought;
	}
	public String getReserved() {
		return reserved;
	}
	public void setReserved(String reserved) {
		this.reserved = reserved;
	}

	public String getIsPartial() {
		return isPartial;
	}

	public void setIsPartial(String isPartial) {
		this.isPartial = isPartial;
	}

	
}
