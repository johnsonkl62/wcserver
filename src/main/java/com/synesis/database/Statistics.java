package com.synesis.database;

import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import com.synesis.utility.MonthEnum;

public class Statistics {

	public static Calendar thisMonth = Calendar.getInstance();

	private Map<String,CountyStats> countyStats = new TreeMap<String,CountyStats>();

	public Statistics() {
		thisMonth.set(Calendar.DAY_OF_MONTH, 1);
	}

	public static String[] getMonthHeaders() {
		thisMonth = Calendar.getInstance();
		thisMonth.set(Calendar.DAY_OF_MONTH, 1);

		String[] months = new String[12];
		int index = thisMonth.get(Calendar.MONTH) % 12;
		for ( int i = 0; i < 12; i++ ) {
			months[i] = MonthEnum.values()[index++].name();
			index %= 12;
		}
		return months;
	}
	
	public Map<String,CountyStats> getAllStats() {
		return countyStats;
	}

	public void setCountyStats(Map<String, CountyStats> countyStats) {
		this.countyStats = countyStats;
	}
}
