package com.synesis.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.synesis.config.core.CustomAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("userDetailsService")
	UserDetailsService userDetailsService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
		.antMatchers("/robots.txt").permitAll()
		.antMatchers("/WCPayPal/**").permitAll()
		.antMatchers("/WCPayPalSubscribe/**").permitAll()
		.antMatchers("/wcserver/WCPayPalSubscribe/**").permitAll()
		.antMatchers("/").permitAll()
		.antMatchers("/WCSearch/**").access("hasRole('ROLE_USER')")
			.and()
		.formLogin().loginPage("/login")
			.successHandler(customAuthenticationSuccessHandler())
			.failureUrl("/login?error")
			.usernameParameter("username")
			.passwordParameter("password")
			.and()
		.logout().logoutSuccessUrl("/login?logout")
			.and()
		/*.requiresChannel().antMatchers("/login").requiresSecure()
			.and()*/
		.csrf().ignoringAntMatchers("/WCPayPal/**")
			.and()
		.csrf().ignoringAntMatchers("/WCPayPalSubscribe/**")
			.and()
		.csrf().ignoringAntMatchers("/wcserver/WCPayPalSubscribe/**")
			.and()
		.exceptionHandling().accessDeniedPage("/403");
		
		
	}
	
	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
	
	@Bean
	public CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler() {
		return new CustomAuthenticationSuccessHandler();
	}
	
	
}