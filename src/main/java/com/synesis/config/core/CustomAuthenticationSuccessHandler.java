package com.synesis.config.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.synesis.service.access.AccessService;

@Component
public class CustomAuthenticationSuccessHandler extends  SimpleUrlAuthenticationSuccessHandler {

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Autowired
	private AccessService accessService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException,
			ServletException {
		
		HttpSession session = request.getSession();
		session.setMaxInactiveInterval(15*60);
		String page = request.getParameter("page");
		
		/*Set some session variables*/
		User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();  
        session.setAttribute("username", authUser.getUsername());  
        
        String redirectPage = "/WCSearch";
        
        if(!StringUtils.isEmpty(page) && !"null".equals(page)) {
        	redirectPage = "/"+page;
        }
        
        String sid = request.getHeader("X-FORWARDED-FOR");  
		if (sid == null) {  
			sid = request.getRemoteAddr();  
			if ( sid == null ) {
				sid = request.getParameter("ip");
			}
		}
		try {
			accessService.save(String.valueOf(session.getAttribute("username")), sid);
		} catch(Exception e){e.printStackTrace();}
        
        /*Set target URL to redirect*/
        redirectStrategy.sendRedirect(request, response, redirectPage);
	}

	public RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}
	
}
