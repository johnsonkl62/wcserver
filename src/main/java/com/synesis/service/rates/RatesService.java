package com.synesis.service.rates;

import java.util.List;
import java.util.Map;

public interface RatesService {

	Double getRateFromCodeAndCarrier(String code, String name);

	Double getRateFromCodeAndNaic(String code, String naic);
	
	Map<String, Double> getRateFromCodeAndNaic(String code, String naic, String naic1, String naic2, String naic3);
	
	Map<String, Double> getAllRatesByDate( List<String> naic, String naic1, String naic2, String naic3);
	

}
