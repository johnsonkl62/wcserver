package com.synesis.service.rates;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.beans.RatesBean;
import com.synesis.dao.rates.RatesDao;
import com.synesis.model.Rates;
import com.synesis.utility.ConvertionUtility;

@Service
public class RatesServiceImpl implements RatesService {

	@Autowired
	private RatesDao ratesDao;
	
	@Override
	@Transactional(readOnly=true)
	public Double getRateFromCodeAndCarrier(String code, String name) {
		return ratesDao.getRateFromCodeAndCarrier(code, name);
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String, Double> getRateFromCodeAndNaic(String code, String naic,String naic1,String naic2,String naic3) {
		return ratesDao.getRateFromCodeAndNaic(code, naic, naic1,naic2,naic3);
	}

	@Override
	@Transactional(readOnly=true)
	public Double getRateFromCodeAndNaic(String code, String naic) {
		return ratesDao.getRateFromCodeAndNaic(code, naic);
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String, Double> getAllRatesByDate(List<String> naic, String naic1,String naic2,String naic3) {
		 List<Object[]> rates = ratesDao.getAllRatesByDate( naic, naic1,naic2, naic3);
		 
		 Map<String, Double> map = ConvertionUtility.convertToRatesMap(rates);
		 
		 return map;
	}

}
