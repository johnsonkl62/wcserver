package com.synesis.service.employers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.cancelled.CancelledDao;
import com.synesis.dao.codes.CodesDao;
import com.synesis.dao.employers.EmployerDao;
import com.synesis.dao.purchase.PurchaseDao;
import com.synesis.employer.Employer;
import com.synesis.model.Employers;
import com.synesis.model.Purchase;
import com.synesis.utility.AppUtility;
import com.synesis.utility.LogUtil;

@Service
public class EmployerService {

	@Autowired
	private PurchaseDao purchaseDao;
	
	@Autowired
	private CodesDao codesDao;
	
	@Autowired
	private CancelledDao cancelledDao;
	
	@Autowired
	private EmployerDao employerDao;
	
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@Transactional(readOnly=true)
	public List<Employer> getAllEmployers(List<Purchase> purchases, LocalDate startDate, LocalDate endDate,
			StringBuffer logBuf) {

		String startMonthAbbrev = startDate.getMonth().name().substring(0, 3);
		String endMonthAbbrev = endDate.getMonth().name().substring(0, 3);

		StringBuffer whereClause = null;
		
		//  Create end month clause (if different).
		if ( !startMonthAbbrev.equalsIgnoreCase(endMonthAbbrev) ) {
			LocalDate adjStart = LocalDate.of(startDate.getYear(), startDate.getMonth(), startDate.getDayOfMonth());
			LocalDate adjEnd = LocalDate.of(endDate.getYear(), endDate.getMonth(), endDate.getDayOfMonth());
			
			adjEnd = startDate.with(TemporalAdjusters.lastDayOfMonth());

			//  adjust dates for partial first month and partial second month
			whereClause = createClause(purchases, startMonthAbbrev, startDate, adjEnd);

			adjStart = endDate.with(TemporalAdjusters.firstDayOfMonth());
			StringBuffer secondaryClause = createClause(purchases, endMonthAbbrev, adjStart, endDate); 
			if ( secondaryClause.length() > 0 ) {
				if ( whereClause.length() > 0 ) {
					whereClause.append(" OR ");
				}
				whereClause.append(secondaryClause);
			}

		} else {
			//  No adjustment needed.  Date range does not span a month end.
			whereClause = createClause(purchases, startMonthAbbrev, startDate, endDate);
		}

		logBuf.append("SELECT fn,xdate,name,county,zip from employers WHERE " + whereClause.toString() + "\r\n");
		List<Employers> employers = employerDao.getEmployers(whereClause.toString());
		List<Employer> list = null;
		
		if(employers!=null) {
			Map<String,String> codeMap = codesDao.getAllCodesWithDescription();
			list = employers.stream().map(employer -> AppUtility.convertToEmployerData(employer,codeMap)).collect(Collectors.toList());
		}
		logBuf.append("\r\nResult Count: " + employers.size() + "\t" + list.size() + "\r\n");
		return list;
	}

	private StringBuffer createClause(List<Purchase> purchases, String monthAbbrev, LocalDate startDate, LocalDate endDate) {
		StringBuffer whereClause = new StringBuffer();
		StringBuffer zipClause = new StringBuffer();
		StringBuffer countyClause = new StringBuffer();
		StringBuffer areaClause = new StringBuffer();

		//  Create start month clause.
		for ( Purchase purchase : purchases ) {
			if ( purchase.getMonth().equalsIgnoreCase(monthAbbrev)) {
				if ( purchase.getZip() == null ) {
					if ( countyClause.length() != 0 ) {
						countyClause.append(",");
					}
					countyClause.append("'" + purchase.getCounty() + "'");
				} else {
					if ( zipClause.length() != 0 ) {
						zipClause.append(",");
					}
					zipClause.append("'" + purchase.getZip() + "'");
				}
			}
		}
		if ( zipClause.length() > 0 ) {
			zipClause = new StringBuffer(" zip in (" + zipClause.toString() + ") " );
		}
		if ( countyClause.length() > 0 ) {
			countyClause = new StringBuffer(" county in (" + countyClause.toString() + ") " );
		}

		if ( zipClause.length() > 0 && countyClause.length() == 0 ) {
			areaClause.append(zipClause);
		} else if ( zipClause.length() == 0 && countyClause.length() > 0 ) {
			areaClause.append(countyClause);
		} else if ( zipClause.length() > 0 && countyClause.length() > 0 ) {
			areaClause.append( "(" + countyClause.toString() + " OR " + zipClause.toString() + ")" );
		}

		//  If neither zips nor counties are purchased for this month, then the areaClause will be zero length at this point.
		if ( areaClause.length() > 0 ) {
			// date range clause
			whereClause.append(
					"( " + 
					areaClause + " AND (" + 
					" DATE_FORMAT(xdate,'%m-%d') >=  DATE_FORMAT('" + formatter.format(startDate) + "','%m-%d') AND " +
					" DATE_FORMAT(xdate,'%m-%d') <=  DATE_FORMAT('" + formatter.format(endDate) + "','%m-%d') )" + 
					")");
		}

		return whereClause;
	}

	@Transactional(readOnly=true)
	public List<Employer> getAllEmployers(Purchase purchase, LocalDate startDate, LocalDate endDate, StringBuffer logBuf) {

		LocalDate adjStart = LocalDate.of(startDate.getYear(), startDate.getMonth(), startDate.getDayOfMonth());
		LocalDate adjEnd = LocalDate.of(endDate.getYear(), endDate.getMonth(), endDate.getDayOfMonth());
		
		String startMonthAbbrev = startDate.getMonth().name().substring(0, 3);
//		String endMonthAbbrev = endDate.getMonth().name().substring(0, 3);

		if ( !startDate.getMonth().equals(endDate.getMonth())) {
			if ( startMonthAbbrev.equalsIgnoreCase(purchase.getMonth()) ) {
				//  set adjEnd to last day of the start month 
				adjEnd = adjStart.with(TemporalAdjusters.lastDayOfMonth());
			} else {
				//  set adjStart to the first day of the month
				adjStart = adjEnd.with(TemporalAdjusters.firstDayOfMonth());
			}
		}
		
		StringBuffer whereClause = new StringBuffer();

		// zip or county
		if ( purchase.getZip() == null ) {
			whereClause.append(" county='" + purchase.getCounty() + "' ");
		} else {
			whereClause.append(" zip='" + purchase.getZip() + "' ");
		}
		
		// date range clause
		whereClause.append(" AND " + 
				" DATE_FORMAT(xdate,'%m-%d') >=  DATE_FORMAT('" + formatter.format(adjStart) + "','%m-%d') AND " +
				" DATE_FORMAT(xdate,'%m-%d') <=  DATE_FORMAT('" + formatter.format(adjEnd) + "','%m-%d')");
		
		logBuf.append("SELECT fn,xdate,name,county,zip from employers WHERE (" + whereClause.toString() + "\r\n");
		List<Employers> employers = employerDao.getEmployers(whereClause.toString());
		List<Employer> list = null;
		
		if(employers!=null) {
			Map<String,String> codeMap = codesDao.getAllCodesWithDescription();
			list = employers.stream().map(employer -> AppUtility.convertToEmployerData(employer,codeMap)).collect(Collectors.toList());
		}
		logBuf.append("\r\nResult Count: " + employers.size() + "\t" + list.size() + "\r\n");
		return list;
	}
	
	@Transactional(readOnly=true)
	public List<Employer> getEmployers(String name, boolean nameExact,
			String carriers, boolean carrierExact, String street, String zip, String gccs, String descs, String subs[],
			String minDate, String maxDate, String username) {

		String nameClause = AppUtility.generateClause("name", name, nameExact, false);
		String carrierClause = AppUtility.generateClause("carrier", carriers, carrierExact, false);
		String addrClause = AppUtility.generateClause("addr", street, false, false);
		String zipClause = AppUtility.generateClause("zip", zip, false, false);

		String codes = "";
		if ( descs != null && descs.trim().length() > 0 ) {
			String[] texts = descs.split(",");
			for ( String text : texts ) {
				codes += codesDao.getCodesFromDescription(text.toUpperCase());
			}			
		}
		
		if ((gccs != null && gccs.trim().length() > 0) || (codes != null && codes.trim().length() > 0)) {
			if ((gccs == null || gccs.trim().length() == 0) && (codes != null && codes.trim().length() > 0)) {
				gccs = codes;
			} else if ((gccs != null && gccs.trim().length() > 0)
					&& (codes != null && codes.trim().length() > 0)) {
				gccs += "," + codes;
			}
		}

		List<Purchase> authorizedAreas = purchaseDao.getPurchasesByUsername(username);
		ArrayList<Purchase> selectedAreas = new ArrayList<Purchase>();

		if(subs!=null) {
			for (String selection : subs) {
				int selectedIndex = Integer.parseInt(selection);
				
				int index = 0;
				for (Purchase s : authorizedAreas) {
//					if (index++ == selectedIndex) {
					if (s.getId() == selectedIndex) {
						selectedAreas.add(s);
					}
				}
			}
		}

		String authClause = AppUtility.generateClause(selectedAreas, minDate, maxDate);
		String gccClause = AppUtility.generateClause("e.code", gccs, true, false);

		LogUtil.log(nameClause+ "_"+ gccClause+ "_"+ carrierClause+ "_"+ authClause);
		
		List<Employers> employers = employerDao.getEmployers(nameClause, gccClause, carrierClause, addrClause, zipClause, authClause);
		List<Employer> list = null;
		
		if(employers!=null) {
			Map<String,String> codeMap = codesDao.getAllCodesWithDescription();
			list = employers.stream().map(employer -> AppUtility.convertToEmployerData(employer,codeMap)).collect(Collectors.toList());
		}
		
		
		return list;
	}

	
	
	@Transactional(readOnly=true)
	public List<Employer> getEmployersForAdminUser(String counties, String name, boolean nameExact, String carriers, boolean carrierExact,
			String street, String zip, String gccs, String descs, String subs[], String minDate, String maxDate, String username) {
		
		String nameClause = AppUtility.generateClause("name", name, nameExact, false);
		String carrierClause = AppUtility.generateClause("carrier", carriers, carrierExact, false);
		String addrClause = AppUtility.generateClause("addr", street, false, false);
		String zipClause = AppUtility.generateClause("zip", zip, false, false);
//		String countyClause = AppUtility.generateClause("carrier", carriers, carrierExact, false);

		
		String codes = "";
		if ( descs != null && descs.trim().length() > 0 ) {
			String[] texts = descs.split(",");
			for ( String text : texts ) {
				
				codes += codesDao.getCodesFromDescription(text.toUpperCase());
			}			
		}
		
		if ((gccs != null && gccs.trim().length() > 0) || (codes != null && codes.trim().length() > 0)) {
			if ((gccs == null || gccs.trim().length() == 0) && (codes != null && codes.trim().length() > 0)) {
				gccs = codes;
			} else if ((gccs != null && gccs.trim().length() > 0)
					&& (codes != null && codes.trim().length() > 0)) {
				gccs += "," + codes;
			}
		}

		String[] countyList = counties.split(",");
		for ( int i = 0; i < countyList.length; i++ ) {
			countyList[i] = countyList[i].trim();
		}
		List<String> clist = Arrays.asList(countyList);

		String authClause = AppUtility.generateAdminClause(clist, minDate, maxDate);
		String gccClause = AppUtility.generateClause("e.code", gccs, true, false);

		LogUtil.log(nameClause+ "_"+ gccClause+ "_"+ carrierClause+ "_"+ authClause);

		List<Employers> employers = employerDao.getEmployers(nameClause, gccClause, carrierClause, addrClause, zipClause, authClause);
		List<Employer> list = null;
		
		if(employers!=null) {
			Map<String,String> codeMap = codesDao.getAllCodesWithDescription();
			list = employers.stream().map(employer -> AppUtility.convertToEmployerData(employer,codeMap)).collect(Collectors.toList());
		}
		
		
		return list;
	}



	@Transactional
	public void updateEmployer(String employerId, String code) {
		employerDao.updateEmployer(employerId,code);
	}


	@Transactional(readOnly=true)
	public Employer findOne(String employerId) {
		
		Employers employers = employerDao.findOne(employerId);
		
		if(employers!=null) {
			Map<String,String> codeMap = codesDao.getAllCodesWithDescription();
			return AppUtility.convertToEmployerData(employers,codeMap);
		}
		
		return null;
	}


	@Transactional(readOnly=false)
	public List<Employers> processCancelledEmployers() {
		List<Employers> cancelled = cancelledDao.getAllCancelled();
		int count = cancelledDao.transferCancelled();
		cancelledDao.deleteCancelled();
		LogUtil.log("Cancelled employers:  " + (cancelled == null ? "null" : cancelled.size()));
		LogUtil.log("Cancelled deletions:  " + count);
		return cancelled;
	}


}
