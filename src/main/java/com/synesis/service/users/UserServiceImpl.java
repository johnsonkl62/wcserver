package com.synesis.service.users;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.beans.ProfileBean;
import com.synesis.beans.TemplateBean;
import com.synesis.dao.users.UserDao;
import com.synesis.model.Profile;
import com.synesis.model.Template;
import com.synesis.utility.ConvertionUtility;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Override
	@Transactional
	public void save(ProfileBean profileBean) {
		userDao.saveUser(profileBean);
		
	}

	@Override
	@Transactional
	public boolean sendEmailAndUpdateToken(String url, String email) {
		return userDao.sendEmailAndUpdateToken(url, email);
	}

	@Override
	@Transactional(readOnly=true)
	public boolean resetPassword(String token, String email) {
		return userDao.resetPassword(token, email);
	}

	@Override
	@Transactional
	public boolean updatePassword(String password, String email) {
		return userDao.updatePassword(password, email);
	}

	@Override
	@Transactional
	public boolean checkUserName(String username) {
		return userDao.checkUserName(username);
	}

	@Override
	@Transactional(readOnly=true)
	public ProfileBean getUserProfile(String username) {
		
		Profile profile = userDao.getUserProfile(username);
		Template template = userDao.getUserTemplate(username);
		return ConvertionUtility.convertToProfileBean(profile,template);
	}

	@Override
	@Transactional
	public void update(ProfileBean p) {
		userDao.updateUser(p);
	}

	@Override
	@Transactional(readOnly=true)
	public boolean checkEmail(String email) {
		return userDao.checkEmail(email);
	}

	@Override
	@Transactional(readOnly=true)
	public Profile findByEmail(String email) {
		return userDao.findByEmail(email);
	}

	@Override
	@Transactional(readOnly=true)
	public TemplateBean getUserTemplate(String username) {
		
		Template template = userDao.getUserTemplate(username);
		return ConvertionUtility.convertToTemplateBean(template);
	}

	@Override
	@Transactional(readOnly=true)
	public List<ProfileBean> getAllUsers() {
		
		List<Profile> profiles = userDao.getAllUsers();
		List<ProfileBean> result  = new ArrayList<ProfileBean>();

		for ( Profile profile : profiles ) {
			Template template = userDao.getUserTemplate(profile.getUsername());
			result.add(ConvertionUtility.convertToProfileBean(profile,template));
		}
		
		return result.stream().collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly=true)
	public List<ProfileBean> getAllAdmins() {
		
		List<Profile> profiles = userDao.getAllAdmins();
		List<ProfileBean> result  = new ArrayList<ProfileBean>();

		for ( Profile profile : profiles ) {
			Template template = userDao.getUserTemplate(profile.getUsername());
			result.add(ConvertionUtility.convertToProfileBean(profile,template));
		}
		
		return result.stream().collect(Collectors.toList());
	}

	@Override
	public List<ProfileBean> getUsersByMonth(String purchaseMonth) {
		List<Profile> profiles = userDao.getUsersByMonth(purchaseMonth);
		return profiles.stream().map(p -> ConvertionUtility.convertToProfileBean(p, null)).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly=true)
	public boolean isOldPasswordMatched(String username, String oldPassword) {
		return userDao.isOldPasswordMatched(username,oldPassword);
	}

	@Override
	@Transactional
	public boolean changePassword(String newPassword, String username) {
		return userDao.changePassword(newPassword, username);
	}

	
	
	
}
