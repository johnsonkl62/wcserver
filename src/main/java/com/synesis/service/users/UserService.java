package com.synesis.service.users;

import java.util.List;

import com.synesis.beans.ProfileBean;
import com.synesis.beans.TemplateBean;
import com.synesis.model.Profile;


public interface UserService {
	
	public boolean checkUserName(String username);

	public void save(ProfileBean profileBean);
	
	public ProfileBean getUserProfile(String username);
	
	public boolean sendEmailAndUpdateToken(String url, String email);

	public boolean resetPassword(String token, String email);

	public boolean updatePassword(String password, String emailAddress);

	public void update(ProfileBean p);

	public boolean checkEmail(String email);

	public Profile findByEmail(String parameter);

	public TemplateBean getUserTemplate(String username);
	
	public List<ProfileBean> getAllUsers();

	public List<ProfileBean> getAllAdmins();

	public List<ProfileBean> getUsersByMonth(String purchaseMonth);

	public boolean isOldPasswordMatched(String string, String oldPassword);

	public boolean changePassword(String newPassword, String string);
	
}
