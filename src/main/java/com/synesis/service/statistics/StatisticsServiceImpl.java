package com.synesis.service.statistics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.beans.StatsBean;
import com.synesis.beans.ZipsBean;
import com.synesis.dao.statistics.StatisticsDao;
import com.synesis.database.CountyStats;

@Service
public class StatisticsServiceImpl implements StatisticsService {

	public static final String[] MONTH = {"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
	public Calendar thisMonth = Calendar.getInstance();
	
	@Autowired
	private StatisticsDao statisticsDao;
	
	public StatisticsServiceImpl() {
		thisMonth.set(Calendar.DAY_OF_MONTH, 1);
	}
	
	@Override
	public String[] getMonthHeaders() {
		
		String[] months = new String[12];
		int index = thisMonth.get(Calendar.MONTH);
		for ( int i = 0; i < 12; i++ ) {
			months[i] = MONTH[index++];
			index %= 12;
		}
		return months;
		
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String, CountyStats> update(String[] comps, String counties, String descs, String gccs,boolean isAdmin) {
		
		ArrayList<String> countyFilter = new ArrayList<String>();
		
		if ( counties != null && counties.trim().length() > 0 ) {
			if ( !"ALL".equalsIgnoreCase(counties) ) {
				String[] cts = counties.split(",");
				for ( String c : cts ) {
					countyFilter.add(c.trim().toUpperCase());
				}
			} else {
				countyFilter.addAll(statisticsDao.getAllDistinctCounties());
			}
		}
		
		
		Map<String, CountyStats> statistics = statisticsDao.getAllCountyStats(comps, countyFilter,isAdmin);
		
		return statistics;
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String,ZipsBean> updateUsingZip(String[] comps, String county,String month,boolean isAdmin) {
		return statisticsDao.updateUsingZip(comps, county,month,isAdmin) ;
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String,ZipsBean> fetchCountsByZip(String[] comps, String county) {
		return statisticsDao.fetchCountsByZip(comps, county);
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String, StatsBean> getAvailableAdminStats(boolean isCarrierEmpty, boolean isObsoleteCheckbox,boolean isCollectedCheckbox, String fromDate, String toDate) {
		return statisticsDao.getAvailableAdminStats(isCarrierEmpty,isObsoleteCheckbox,isCollectedCheckbox,fromDate,toDate);
	}


	@Override
	@Transactional(readOnly=true)
	public Map<String, StatsBean> getSubscribedAdminStats(boolean isCarrierEmpty, boolean isObsoleteCheckbox,boolean isCollectedCheckbox, String fromDate, String toDate) {
		return statisticsDao.getSubscribedAdminStats(isCarrierEmpty,isObsoleteCheckbox,isCollectedCheckbox,fromDate,toDate);
	}

	
	
}
