package com.synesis.service.statistics;

import java.util.Map;

import com.synesis.beans.StatsBean;
import com.synesis.beans.ZipsBean;
import com.synesis.database.CountyStats;

public interface StatisticsService {

	public String[] getMonthHeaders();
	
	public Map<String, CountyStats> update(String[] comps, String counties, String descs, String gccs,boolean isAdmin);

	public Map<String,ZipsBean> updateUsingZip(String[] comps, String county,String month,boolean isAdmin);

	public Map<String, ZipsBean> fetchCountsByZip(String[] comps, String county);

	public Map<String, StatsBean> getAvailableAdminStats(boolean isCarrierEmpty, boolean isObsoleteCheckbox,boolean isCollectedCheckbox, String fromDate, String toDate);
	
	public Map<String, StatsBean> getSubscribedAdminStats(boolean isCarrierEmpty, boolean isObsoleteCheckbox,boolean isCollectedCheckbox, String fromDate, String toDate);

}
