package com.synesis.service.auction;

import java.util.List;

import com.synesis.model.auction.Auction;

public interface AuctionService {

	List<Auction> getAllAuctions();

	Auction getBid(String area, int month);

	Auction getBid(String area, String month);
}
