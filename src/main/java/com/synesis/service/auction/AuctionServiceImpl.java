package com.synesis.service.auction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.auction.AuctionDao;
import com.synesis.model.auction.Auction;

@Service
public class AuctionServiceImpl implements AuctionService {

	@Autowired
	private AuctionDao auctionDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Auction> getAllAuctions() {
		return auctionDao.getAllAuctions();
	}

	@Override
	@Transactional(readOnly=true)
	public Auction getBid(String area, int month) {
		return auctionDao.getBid(area, month);
	}

	@Override
	@Transactional(readOnly=true)
	public Auction getBid(String area, String month) {
		return auctionDao.getBid(area, month);
	}

}
