package com.synesis.service.subscription;

import java.util.List;

import com.synesis.model.SubscriptionInfo;

public interface SubscriptionService {

	public void saveSubscription(String county, String username, String startMonth, String item_name, String subscr_id);
//	public void saveSubscriptionWithZip(String county, String startMonth, String username, String zips, String subscr_id);

	public List<SubscriptionInfo> fetchAllActiveSubscription();
	public String[] getZips(SubscriptionInfo subscriptionInfo);
	
	public void saveOrUpdatePaypalSubscription(SubscriptionInfo subscriptionInfo);
	
	public void deactivateSubscription(String subscr_id);

	public SubscriptionInfo getSubscriptionById(String subscr_id);
	public List<SubscriptionInfo> getSubscriptionsByUsername(String username);
}
