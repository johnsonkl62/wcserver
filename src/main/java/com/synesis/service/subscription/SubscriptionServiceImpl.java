package com.synesis.service.subscription;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.subscription.SubscriptionDao;
import com.synesis.model.SubscriptionInfo;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {
	
	public static final String[] MONTH = {"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};

	@Autowired
	private SubscriptionDao subscriptionDao;
	
	@Override
	@Transactional
	public void saveSubscription(String county, String username, String startMonth, String item_name, String subscr_id) {
		subscriptionDao.saveSubscription(county, username, startMonth, Calendar.getInstance().getTime(), item_name, subscr_id);
	}

	@Override
	public String[] getZips(SubscriptionInfo subscriptionInfo) {
		try {
			String item = subscriptionInfo.getItem();
			if ( item.indexOf(']') < 0 ) {
				return null;
			}
			String zips = item.substring(item.indexOf('[')+1,item.indexOf(']'));
			return zips.split(",");
		} catch ( Exception e ) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional
	public List<SubscriptionInfo> fetchAllActiveSubscription() {
		return subscriptionDao.fetchAllActiveSubscription();
	}

	@Override
	@Transactional
	public void saveOrUpdatePaypalSubscription(SubscriptionInfo subscriptionInfo) {
		subscriptionDao.saveOrUpdatePaypalSubscription(subscriptionInfo);
	}
	
	@Override
	@Transactional
	public void deactivateSubscription(String subscr_id) {
		subscriptionDao.deactivateSubscription(subscr_id);
	}

	@Override
	@Transactional
	public SubscriptionInfo getSubscriptionById(String subscr_id) {
		return subscriptionDao.getSubscriptionById(subscr_id);
	}

	@Override
	@Transactional
	public List<SubscriptionInfo> getSubscriptionsByUsername(String username) {
		return subscriptionDao.getSubscriptionsByUsername(username);
	}
}
