package com.synesis.service.codes;

import java.util.Map;

public interface CodesService {

	String getDescriptionFromCode(String code);

	String getCodesFromDescription(String description);

	Map<String, String> getAllCodesWithDescription();
}
