package com.synesis.service.codes;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.codes.CodesDao;

@Service
public class CodesServiceImpl implements CodesService {

	@Autowired
	private CodesDao codesDao;
	
	@Override
	@Transactional(readOnly=true)
	public String getDescriptionFromCode(String code) {
		return codesDao.getDescriptionFromCode(code);
	}

	@Override
	@Transactional(readOnly=true)
	public String getCodesFromDescription(String description) {
		return codesDao.getCodesFromDescription(description);
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String, String> getAllCodesWithDescription() {
		return codesDao.getAllCodesWithDescription();
	}

}
