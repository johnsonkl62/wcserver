package com.synesis.service.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.profile.ProfileDao;
import com.synesis.model.Profile;

@Service
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	private ProfileDao profileDao;
	
	@Override
	@Transactional
	public Profile saveProfile(String user, String last, String first,
			String middle, String sfx, String ag, String em, String ph,byte[] template) {
		return profileDao.saveProfile(user, last, first, middle, sfx, ag, em, ph, template);
	}

	@Override
	@Transactional(readOnly=true)
	public boolean isProfilePresent(String username) {
		return profileDao.isProfilePresent(username);
	}

	@Override
	@Transactional(readOnly=true)
	public Profile getProfileByUsername(String username) {
		return profileDao.getProfileByUsername(username);
	}

}
