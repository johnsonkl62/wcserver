package com.synesis.service.collection;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.collection.CollectionDao;
import com.synesis.database.CollectionStats;

@Service
public class CollectionServiceImpl implements CollectionService {

	@Autowired
	private CollectionDao collectionDao;
	
	@Override
	@Transactional
	public Map<String,Map<String,CollectionStats>>update() {
		return collectionDao.update();
	};
	
	@Override
	public Map<String,CollectionStats>getTotals(Map<String,Map<String,CollectionStats>> map) {
		Map<String,CollectionStats> totals = new HashMap<String,CollectionStats>();
		try {
		for ( String ctyIndex : map.keySet() ) {
			Map<String,CollectionStats> ctyStats = map.get(ctyIndex);
			
			for ( String month : ctyStats.keySet() ) {
				CollectionStats cs = ctyStats.get(month);
				
				CollectionStats tcs = totals.get(month);
				if ( tcs == null ) {
					tcs = new CollectionStats();
					totals.put(month, tcs);
				}
				tcs.setTotal(tcs.getTotal()+cs.getTotal());
				tcs.setDone(tcs.getDone()+cs.getDone());
			}
		}
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return totals;
	}

}
