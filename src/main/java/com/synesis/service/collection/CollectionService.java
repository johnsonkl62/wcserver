package com.synesis.service.collection;

import java.util.Map;

import com.synesis.database.CollectionStats;
import com.synesis.database.CountyEnum;
import com.synesis.utility.MonthEnum;


public interface CollectionService {
	
	public Map<String,Map<String,CollectionStats>>update();
	public Map<String,CollectionStats>getTotals(Map<String,Map<String,CollectionStats>> map);

}
