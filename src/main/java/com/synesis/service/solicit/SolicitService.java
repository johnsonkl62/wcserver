package com.synesis.service.solicit;

import java.util.List;

import com.synesis.beans.SolicitBean;
import com.synesis.model.solicit.Solicit;

public interface SolicitService {

	public Solicit searchByAgencyAndEmail(String agency, String email);

	public List<SolicitBean> searchEmployers(String county, List<String> codes,
			List<String> zips);

	public void unsubscribeEmail(String agency, String email);
	
	public List<Solicit> getAllSolicits();
	
}
