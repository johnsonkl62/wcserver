package com.synesis.service.solicit;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.synesis.beans.SolicitBean;
import com.synesis.dao.solicit.SolicitDao;
import com.synesis.model.solicit.Solicit;

@Service
public class SolicitServiceImpl implements SolicitService {

	@Autowired
	private SolicitDao solicitDao;

	@Override
	@Transactional(readOnly = true)
	public Solicit searchByAgencyAndEmail(String agency, String email) {
		return solicitDao.searchByAgencyAndEmail(agency, email);
	}

	@Override
	@Transactional(readOnly = true)
	public List<SolicitBean> searchEmployers(String county, List<String> codes,List<String> zips) {
		
		List<Object[]> resultList = solicitDao.searchEmployers(county, codes,zips);

		if (!CollectionUtils.isEmpty(resultList)) {
			return resultList.stream().map(object -> new SolicitBean(object))
					.collect(Collectors.toList());

		}

		return null;
	}

	@Override
	@Transactional
	public void unsubscribeEmail(String agency,String email) {
		solicitDao.unsubscribeEmail(agency, email);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Solicit> getAllSolicits() {
		return solicitDao.getAllSolicits();
	}

}
