package com.synesis.service.zip;

import java.util.List;
import java.util.Map;

import com.synesis.model.Zips;

public interface ZipService {

	List<Zips> findZipByCounty(String county);
	
	Map<String,Zips> findAllZipsByCounty(String county); 
	
}
