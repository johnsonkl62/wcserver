package com.synesis.service.zip;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.zip.ZipDao;
import com.synesis.model.Zips;

@Service
public class ZipServiceImpl implements ZipService {

	@Autowired
	private ZipDao zipDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Zips> findZipByCounty(String county) {
		return zipDao.findZipByCounty(county);
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String, Zips> findAllZipsByCounty(String county) {
		return zipDao.findAllZipsByCounty(county);
	}

}
