package com.synesis.service.purchase;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.beans.ProfileBean;
import com.synesis.dao.purchase.PurchaseDao;
import com.synesis.model.Profile;
import com.synesis.model.Purchase;

@Service
public class PurchaseServiceImpl implements PurchaseService {
	
	@Autowired
	private PurchaseDao purchaseDao;
	
	@Override
	@Transactional
	public List<Purchase> getPurchasesByUsername(String username) {
		return purchaseDao.getPurchasesByUsername(username);
	}

	@Override
	@Transactional
	public List<Purchase> getPurchasesBySubscrId(String subscr_id) {
		return purchaseDao.getPurchasesBySubscrId(subscr_id);
	}

	@Override
	@Transactional
	public List<Purchase> fetchPurchasesByZip(String zips) {
		return purchaseDao.fetchPurchasesByZip(zips);
	}

	@Override
	@Transactional
	public String savePurchase(String county, String month, String username, String zips, String subscr_id) {
		return purchaseDao.savePurchase(county, username, month, Calendar.getInstance().getTime(), zips, subscr_id);
	}

	@Override
	@Transactional
	public List<Purchase> fetchPurchasesByCountyMonth(String county, String month) {
		return purchaseDao.fetchPurchasesByCountyMonth(county, month);
	}

	@Override
	@Transactional
	public List<Purchase> fetchPurchasesByUserByMonth(String username, String month) {
		return purchaseDao.fetchPurchasesByUserByMonth(username, month);
	}

	@Override
	@Transactional
	public StringBuffer deletePurchasesByMonth(String month) {
		return purchaseDao.deletePurchasesByMonth(month);
	}

	@Override
	@Transactional
	public Purchase saveExamplePurchase(String username) {
		return purchaseDao.saveExamplePurchase(username);
	}

	@Override
	@Transactional
	public void removeExamplePurchase(String username) {
		purchaseDao.removeExamplePurchase(username);
	}

}
