package com.synesis.service.purchase;

import java.util.List;

import com.synesis.model.Purchase;

public interface PurchaseService {

	public List<Purchase> getPurchasesByUsername(String username);
	public List<Purchase> getPurchasesBySubscrId(String subscr_id);
	public List<Purchase> fetchPurchasesByZip(String zips);
//	public List<Purchase> fetchPurchasesByMonth(String month);
	public List<Purchase> fetchPurchasesByCountyMonth(String county, String month);
	public List<Purchase> fetchPurchasesByUserByMonth(String username, String month);
	public StringBuffer deletePurchasesByMonth(String month);

	public String savePurchase(String county, String month, String username, String zips, String subscr_id);

	public Purchase saveExamplePurchase(String username);
	public void removeExamplePurchase(String username);

}
