package com.synesis.service.announcement;

import java.util.List;

public interface AnnouncementService {

	void addAnnouncement(String announcementText);

	List<String> getAllAnnouncements();
	
}
