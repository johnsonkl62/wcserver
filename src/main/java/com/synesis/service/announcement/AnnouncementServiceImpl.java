package com.synesis.service.announcement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.announcement.AnnouncementDao;
import com.synesis.model.Announcements;

@Service
public class AnnouncementServiceImpl implements AnnouncementService {

	@Autowired
	private AnnouncementDao announcementDao;

	@Override
	@Transactional
	public void addAnnouncement(String announcementText) {

		announcementDao.saveAnnouncement(announcementText);

	}

	@Override
	@Transactional(readOnly = true)
	public List<String> getAllAnnouncements() {

		List<Announcements> announcements = announcementDao
				.getAllAnnouncements();

		List<String> list = new ArrayList<String>();
		
		if (announcements != null && !announcements.isEmpty()) {
			list = announcements.stream().filter(a-> a.isActive()).map(a -> a.toString())
					.collect(Collectors.toList());
		}

		return list;
	}

}
