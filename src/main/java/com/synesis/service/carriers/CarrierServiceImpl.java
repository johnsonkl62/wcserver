package com.synesis.service.carriers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.carriers.CarrierDao;
import com.synesis.model.Carriers;

@Service
public class CarrierServiceImpl implements CarrierService {

	@Autowired
	private CarrierDao carrierDao;
	
	@Override
	@Transactional(readOnly=true)
	public String getNameById(String naic) {
		return carrierDao.getNameById(naic);
	}

	@Override
	@Transactional(readOnly=true)
	public String getCarrierIdByName(String name) {
		return carrierDao.getCarrierIdByName(name);
	}

	@Override
	@Transactional(readOnly=true)
	public Carriers getCarrierModelById(String naic) {
		return carrierDao.getCarrierModelById(naic);
	}

	@Override
	@Transactional(readOnly=true)
	public Carriers getCarrierModelByName(String name) {
		return carrierDao.getCarrierModelByName(name);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Carriers> getAllCarriers() {
		return carrierDao.getAllCarriers();
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String, String> getAllCarriersByName() {
		return carrierDao.getAllCarriersByName();
	}

}
