package com.synesis.service.carriers;

import java.util.List;
import java.util.Map;

import com.synesis.model.Carriers;

public interface CarrierService {

	String getNameById(String naic);

	String getCarrierIdByName(String name);

	Carriers getCarrierModelById(String naic);

	Carriers getCarrierModelByName(String name);

	List<Carriers> getAllCarriers();

	Map<String, String> getAllCarriersByName();
	
}
