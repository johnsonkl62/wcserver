package com.synesis.service.access;

import com.synesis.model.Access;

public interface AccessService {

	Access save(String user, String ip);
	
}
