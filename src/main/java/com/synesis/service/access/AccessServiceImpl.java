package com.synesis.service.access;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.access.AccessDao;
import com.synesis.model.Access;

@Service
public class AccessServiceImpl implements AccessService {


	@Autowired
	private AccessDao accessDao;
	
	@Override
	@Transactional
	public Access save(String user, String ip) {
		return accessDao.save(user, ip);
	}

}
