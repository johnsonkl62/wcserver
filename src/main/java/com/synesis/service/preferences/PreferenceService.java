package com.synesis.service.preferences;

import java.util.Map;

public interface PreferenceService {

	public void updatePreferences(String username,String[] codes);
	
	public Map<String,String> getAllPreferencesByUsername(String username);
}
