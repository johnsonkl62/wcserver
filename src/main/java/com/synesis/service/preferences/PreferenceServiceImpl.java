package com.synesis.service.preferences;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.dao.preferences.PreferencesDao;
import com.synesis.model.Preferences;

@Service
public class PreferenceServiceImpl implements PreferenceService {

	@Autowired
	private PreferencesDao preferencesDao;
	
	@Override
	@Transactional
	public void updatePreferences(String username, String[] codes) {
		preferencesDao.updatePreferences(username, codes);
	}

	@Override
	@Transactional(readOnly=true)
	public Map<String, String> getAllPreferencesByUsername(String username) {
		Map<String,String> preferences = new TreeMap<>();
		
		List<Preferences> preferencesList =  preferencesDao.getAllPreferencesByUsername(username);
		
		if(preferencesList!=null) {
			preferencesList.forEach(e->preferences.put(e.getCodes().getCode(),e.getCodes().getText()));
		}
		
		return preferences;
	}

}
