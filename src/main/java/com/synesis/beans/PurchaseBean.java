package com.synesis.beans;

import java.util.Date;

public class PurchaseBean {

	private Long id;
	private String username;
	private String county;
	private String month;
	private Date buyDate;
	private String zipCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public PurchaseBean() {
	}

	@Override
	public String toString() {
		return "PurchaseBean [id=" + id + ", username=" + username + ", county=" + county + ", month=" + month + ", buyDate=" + buyDate + ", zipCode=" + zipCode + "]";
	}

}
