package com.synesis.beans;

import java.util.HashMap;
import java.util.Map;

public class StatsBean {

	private String county;
	private Map<Integer, Integer> monthMap = new HashMap<Integer, Integer>();

	public StatsBean() {
		for (int i = 1; i <= 12; i++) {
			monthMap.put(i, 0);
		}
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void addMonth(Integer month, Integer count) {
		monthMap.put(month, count);
	}

	public Map<Integer, Integer> getMonthMap() {
		return monthMap;
	}

	public void setMonthMap(Map<Integer, Integer> monthMap) {
		this.monthMap = monthMap;
	}
	
	
}
