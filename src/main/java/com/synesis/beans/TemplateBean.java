package com.synesis.beans;

import java.util.Arrays;

public class TemplateBean {

	private byte[] template;
	private Integer priority;

	
	
	public TemplateBean(byte[] template, Integer priority) {
		super();
		this.template = template;
		this.priority = priority;
	}

	public byte[] getTemplate() {
		return template;
	}

	public void setTemplate(byte[] template) {
		this.template = template;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "TemplateBean [template=" + Arrays.toString(template)
				+ ", priority=" + priority + "]";
	}

}
