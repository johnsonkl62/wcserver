package com.synesis.beans;

public class ProfileBean {

	private String username;
	private String lastName;
	private String firstName;
	private String middleInitial;
	private String suffix;
	private String agency;
	private String email;
	private String phone;
	private String password;
	private String carrier1; // Highest rates
	private String carrier2; // Middle rates
	private String carrier3; // Best rates
	private CarriersBean carriersBean1;
	private CarriersBean carriersBean2;
	private CarriersBean carriersBean3;

	private Double rate1;
	private Double rate2;
	private Double rate3;

	private TemplateBean templateBean;

	public ProfileBean(String user, String last, String first, String middle,
			String sfx, String ag, String em, String ph, String pwd, String c1,
			String c2, String c3) {
		username = user;
		lastName = last;
		firstName = first;
		middleInitial = middle;
		suffix = sfx;
		agency = ag;
		email = em;
		phone = ph;
		password = pwd;
		carrier1 = c1;
		carrier2 = c2;
		carrier3 = c3;
	}

	public ProfileBean(String user, String last, String first, String middle,
			String sfx, String ag, String em, String ph, String pwd) {
		username = user;
		lastName = last;
		firstName = first;
		middleInitial = middle;
		suffix = sfx;
		agency = ag;
		email = em;
		phone = ph;
		password = pwd;
	}

	public CarriersBean getCarriersBean1() {
		return carriersBean1;
	}

	public void setCarriersBean1(CarriersBean carriersBean1) {
		this.carriersBean1 = carriersBean1;
	}

	public CarriersBean getCarriersBean2() {
		return carriersBean2;
	}

	public void setCarriersBean2(CarriersBean carriersBean2) {
		this.carriersBean2 = carriersBean2;
	}

	public CarriersBean getCarriersBean3() {
		return carriersBean3;
	}

	public void setCarriersBean3(CarriersBean carriersBean3) {
		this.carriersBean3 = carriersBean3;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCarrier1() {
		return carrier1;
	}

	public void setCarrier1(String carrier1) {
		this.carrier1 = carrier1;
	}

	public String getCarrier2() {
		return carrier2;
	}

	public void setCarrier2(String carrier2) {
		this.carrier2 = carrier2;
	}

	public String getCarrier3() {
		return carrier3;
	}

	public void setCarrier3(String carrier3) {
		this.carrier3 = carrier3;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Double getRate1() {
		return rate1;
	}

	public void setRate1(Double rate1) {
		this.rate1 = rate1;
	}

	public Double getRate2() {
		return rate2;
	}

	public void setRate2(Double rate2) {
		this.rate2 = rate2;
	}

	public Double getRate3() {
		return rate3;
	}

	public void setRate3(Double rate3) {
		this.rate3 = rate3;
	}

	public TemplateBean getTemplateBean() {
		return templateBean;
	}

	public void setTemplateBean(TemplateBean templateBean) {
		this.templateBean = templateBean;
	}

	public String toString() {
		return "   Username: " + username + "\n" + "	Name:     " + lastName
				+ ", " + firstName + " " + middleInitial + " " + suffix + "\n"
				+ "   Agency:   " + agency + "\n" + "   Email:    " + email
				+ "\n" + "   Phone:    " + phone;
	}

}
