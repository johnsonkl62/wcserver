package com.synesis.beans;


public class ZipsBean {

	private String zip;
	private String type;
	private String city;
	private String county;
	private Integer total; //total employers
	private Integer available; // employers with non null carrier
	private Integer ineligible; //employers whose carrier is matching profile carrier
	private boolean isPurchased;
	private boolean isSubscribed;
	private String monthAvailable;


	public ZipsBean() {
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public Integer getIneligible() {
		return ineligible;
	}

	public void setIneligible(Integer ineligible) {
		this.ineligible = ineligible;
	}

	
	public boolean isPurchased() {
		return isPurchased;
	}

	public void setPurchased(boolean isPurchased) {
		this.isPurchased = isPurchased;
	}

	public boolean isSubscribed() {
		return isSubscribed;
	}

	public void setSubscribed(boolean isSubscribed) {
		this.isSubscribed = isSubscribed;
	}

	public String getMonthAvailable() {
		return monthAvailable;
	}

	public void setMonthAvailable(String monthAvailable) {
		this.monthAvailable = monthAvailable;
	}

	@Override
	public String toString() {
		return "ZipsBean [" + zip + ", " + county + ", " + city + "] - (" 
				+ total + " | " + available + " | " + ineligible + " | " + isPurchased + " | " + isSubscribed + " | " + monthAvailable + ")";
	}

}
