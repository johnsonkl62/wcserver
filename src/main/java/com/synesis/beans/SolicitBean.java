package com.synesis.beans;

import java.io.Serializable;
import java.math.BigInteger;

public class SolicitBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;
	private String text;
	private Integer count;

	public SolicitBean(Object[] object) {
		if (object != null && object.length == 3) {
			this.code = (String) object[0];
			this.text = (String) object[1];
			this.count = ((BigInteger) object[2]).intValue();
		}
	}

	public String getCode() {
		return code;
	}

	public String getText() {
		return text;
	}

	public Integer getCount() {
		return count;
	}

}
