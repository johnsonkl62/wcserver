package com.synesis.beans;

import java.util.ArrayList;
import java.util.List;


public class CarriersBean {

	private String naic;
	private String name;

	private List<RatesBean> ratesBeans = new ArrayList<RatesBean>();
	
	public CarriersBean() {
	}

	
	
	
	public List<RatesBean> getRatesBeans() {
		return ratesBeans;
	}




	public void setRatesBeans(List<RatesBean> ratesBeans) {
		this.ratesBeans = ratesBeans;
	}


	public void addRatesBean(RatesBean ratesBean) {
		this.ratesBeans.add(ratesBean);
	}

	public String getNaic() {
		return naic;
	}

	public void setNaic(String naic) {
		this.naic = naic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CarriersBean [naic=" + naic + ", name=" + name + "]";
	}

}
