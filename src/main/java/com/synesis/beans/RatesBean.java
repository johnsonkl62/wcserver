package com.synesis.beans;

import java.io.Serializable;
import java.util.Date;

public class RatesBean implements  Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String naic;
	private String code;
	private Double rate;
	private Date date;
	
	
	public String getNaic() {
		return naic;
	}
	public void setNaic(String naic) {
		this.naic = naic;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
