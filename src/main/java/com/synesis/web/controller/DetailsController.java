package com.synesis.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.beans.ProfileBean;
import com.synesis.beans.ZipsBean;
import com.synesis.database.CountyStats;
import com.synesis.database.MonthStats;
import com.synesis.database.Statistics;
import com.synesis.model.Zips;
import com.synesis.service.statistics.StatisticsService;
import com.synesis.service.users.UserService;
import com.synesis.service.zip.ZipService;
import com.synesis.utility.Configuration;
import com.synesis.utility.LogUtil;
import com.synesis.utility.SendMailUtility;

@Controller
public class DetailsController {

	@Autowired
	private StatisticsService statisticsService;

	@Autowired
	private UserService userService;

	@Autowired
	private ZipService zipService;

	public static final String ASC = "Ascending";
	public static final String DSC = "Descending";

	@RequestMapping(value = "/WCDetail", method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView defaultPage(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("username") == null) {
			request.getSession().setAttribute("TargetPage", "/");
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}

		boolean isAdmin = false;
		if (request.isUserInRole("ROLE_ADMIN")) {
			isAdmin = true;
	    }
		
		String county = request.getParameter("county");
		String monthColumn = request.getParameter("month");
		Integer monthColId = Integer.parseInt(monthColumn);

		ProfileBean profile = userService.getUserProfile(String.valueOf(request.getSession().getAttribute("username")));
		String[] comps = new String[3];
		comps[0] = profile.getCarriersBean1().getName();
		comps[1] = profile.getCarriersBean2().getName();
		comps[2] = profile.getCarriersBean3().getName();

		Map<String, CountyStats> map = statisticsService.update(comps, county, null, null,isAdmin);
		CountyStats cs = map.get(county);

		String monthName = cs.getMonthNameByColumnId(monthColId);
		MonthStats ms = cs.getMonthByColumnId(monthColId);
		int total = ms.getTotal();
		int done = ms.getDone();
		int comp = ms.getComps();

		LogUtil.log("County: " + county + "   Month: " + monthName);
		LogUtil.log("      Total: " + total);
		LogUtil.log("      Done:  " + done);
		LogUtil.log("      Comps: " + comp);

		// Base price
		float price = (done - comp) * Configuration.LEAD_COST;
		System.out.format("      Base Price:  %6.2f\r\n", price);
		System.out.format("      Final Price:  %6.2f\r\n", price);
		model.addObject("County", county);
		model.addObject("Month", monthName);
		model.addObject("Price", price);
		model.addObject("Total", new Integer(total));
		model.addObject("Done", new Integer(done));
		model.addObject("Comps", new Integer(comp));
		model.addObject("email", profile.getEmail());

		//  Add PayPal settings for LIVE or SANDBOX based on this server's IP
		model.addObject("paypalUrl", Configuration.getConfig().get("paypalUrl"));
		model.addObject("paypalBusiness", Configuration.getConfig().get("paypalBusiness"));
		model.addObject("paypalReturn", Configuration.getConfig().get("paypalReturn"));
		model.addObject("paypalNotifyUrl", Configuration.getConfig().get("paypalNotifyUrl"));
		
		model.setViewName("WCDetail");

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("PayPal purchase:\r\n");
			sb.append("  Username: " + session.getAttribute("username") + "\r\n");
			sb.append("  Email:    " + profile.getEmail() + "\r\n");
			sb.append("  County:   " + county + "\r\n");
			sb.append("  Month:    " + monthName + "\r\n");
			sb.append("  Total:    " + total + "\r\n");
			
			SendMailUtility.emailReport(sb.toString(),"Sending data to PayPal ...");
		} catch ( Exception e ) {
			
		}
		return model;

	}

	@RequestMapping(value = "/WCPaymentDetailsWithZip", method = {
			RequestMethod.GET, RequestMethod.POST })
	public ModelAndView showPaymentDetailsPageWithZip(
			HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("username") == null) {
			request.getSession().setAttribute("TargetPage", "/");
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}

		ProfileBean profile = userService.getUserProfile(String.valueOf(request
				.getSession().getAttribute("username")));

		String county = request.getParameter("county");
		String month = request.getParameter("month");
		String total = request.getParameter("total");
		String zips = request.getParameter("zips");

		if (zips.contains(",")) {
			zips = zips.substring(0, zips.length() - 1);
		}

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("PayPal purchase:\r\n");
			sb.append("  Username: " + session.getAttribute("username") + "\r\n");
			sb.append("  Email:    " + profile.getEmail() + "\r\n");
			sb.append("  County:   " + county + "\r\n");
			sb.append("  Month:    " + month + "\r\n");
			sb.append("  Zips:     " + zips + "\r\n");
			sb.append("  Total:    " + total + "\r\n");
			
			SendMailUtility.emailReport(sb.toString(),"Sending data to PayPal ...");
		} catch ( Exception e ) {
			
		}
		model.addObject("County", county);
		model.addObject("Month", month);
		model.addObject("Price", total);
		model.addObject("Zips", zips);
		model.addObject("email", profile.getEmail());
		
		//  Add PayPal settings for LIVE or SANDBOX based on this server's IP
		model.addObject("paypalUrl", Configuration.getConfig().get("paypalUrl"));
		model.addObject("paypalBusiness", Configuration.getConfig().get("paypalBusiness"));
		model.addObject("paypalReturn", Configuration.getConfig().get("paypalReturn"));
		model.addObject("paypalNotifyUrl", Configuration.getConfig().get("paypalNotifyUrl"));
		
		model.setViewName("WCDetailWithZip");

		return model;

	}

	@RequestMapping(value = "/WCDetailWithZip", method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView detailsWithZip(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		HttpSession session = request.getSession(false);

		boolean isAdmin = false;
		if (request.isUserInRole("ROLE_ADMIN")) {
			isAdmin = true;
	    }
		
		if (session == null || session.getAttribute("username") == null) {
			request.getSession().setAttribute("TargetPage", "/");
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}

		List<ZipsBean> zipsBeans = null;

		boolean isRetrievedFromSession = false;
		String sort = request.getParameter("sort");

		if (session.getAttribute("ratesByZip") != null && sort != null
				&& !sort.isEmpty()) {
			zipsBeans = (List<ZipsBean>) session.getAttribute("ratesByZip");
			String curOrder = (String) request.getSession().getAttribute(
					"sortedOrder");

			if ("zip".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getZip().compareTo(
							z1.getZip()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getZip().compareTo(
							z2.getZip()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			} else if ("type".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getType().compareTo(
							z1.getType()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getType().compareTo(
							z2.getType()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			else if ("city".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getCity().compareTo(
							z1.getCity()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getCity().compareTo(
							z2.getCity()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			else if ("total".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getTotal().compareTo(
							z1.getTotal()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getTotal().compareTo(
							z2.getTotal()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			else if ("available".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getAvailable().compareTo(
							z1.getAvailable()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getAvailable().compareTo(
							z2.getAvailable()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			else if ("ineligible".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getIneligible().compareTo(
							z1.getIneligible()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getIneligible().compareTo(
							z2.getIneligible()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			isRetrievedFromSession = true;
		}

		String county = request.getParameter("county");
		String month = request.getParameter("month");

		Statistics statistics = new Statistics();
		String[] headers = statistics.getMonthHeaders();

		month = headers[Integer.parseInt(month)];

		LogUtil.log("MONTH ::: " + month);

		if (!isRetrievedFromSession) {

			Map<String, Zips> zips = zipService.findAllZipsByCounty(county);
			ProfileBean profile = userService.getUserProfile(String
					.valueOf(request.getSession().getAttribute("username")));
			String[] comps = new String[3];
			comps[0] = profile.getCarriersBean1().getName();
			comps[1] = profile.getCarriersBean2().getName();
			comps[2] = profile.getCarriersBean3().getName();

			Map<String, ZipsBean> map = statisticsService.updateUsingZip(comps, county, month,isAdmin);

			zipsBeans = new ArrayList<ZipsBean>();

			for (Entry<String, ZipsBean> entry : map.entrySet()) {
				if (zips.containsKey(entry.getValue().getZip())) {
					Zips zip = zips.get(entry.getValue().getZip());
					ZipsBean bean = entry.getValue();
					bean.setType(zip.getType());
					bean.setCity(zip.getCity());
					zipsBeans.add(bean);
				}
			}

			LogUtil.log("Details using Zip : " + map);
		}

		session.setAttribute("ratesByZip", zipsBeans);
		model.addObject("ratesByZip", zipsBeans);
		model.addObject("county", county);
		model.addObject("month", month);
		model.addObject("leadcost",Configuration.LEAD_COST);

		model.setViewName("WCCountyZipDetails");

		return model;

	}

}
