package com.synesis.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.database.CollectionStats;
import com.synesis.database.Statistics;
import com.synesis.service.collection.CollectionService;
import com.synesis.utility.LogUtil;

@Controller
public class CollectionController {

	@Autowired
	private CollectionService collectionService;
	
	@RequestMapping(value = { "/WCCollection" }, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView defaultPage(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();

		String username = request.getRemoteUser();
		LogUtil.log("Username: " + username);

		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}
		
		boolean isAdmin = request.isUserInRole("ROLE_ADMIN");

		if ( isAdmin ) {
			model.addObject("monthHeaders",Statistics.getMonthHeaders());

			Map<String,Map<String,CollectionStats>> map = collectionService.update();
			Map<String,CollectionStats> totals = collectionService.getTotals(map);
//			request.getSession().setAttribute("collection",map);
			model.addObject("collectionTotals",totals);
			model.addObject("collection",map);
			model.setViewName("WCCollection");
		}
		return model;

	}

}
