package com.synesis.web.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.paypal.ipn.IPNMessage;
import com.synesis.dao.users.UserDao;
import com.synesis.model.Profile;
import com.synesis.model.Purchase;
import com.synesis.model.SubscriptionInfo;
import com.synesis.service.purchase.PurchaseService;
import com.synesis.service.subscription.SubscriptionService;
import com.synesis.utility.Configuration;
import com.synesis.utility.LogUtil;
import com.synesis.utility.MonthEnum;
import com.synesis.utility.SendMailUtility;

@Controller
public class PaypalController {

//	@Autowired
//	private UserService userService;
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private PurchaseService purchaseService;
	
	private static Semaphore semaphore = new Semaphore(1);
	@RequestMapping(value = "/WCPayPal", method = { RequestMethod.GET,  RequestMethod.POST})
	public void defaultPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,String> configurationMap =  Configuration.getConfig();
			IPNMessage 	ipnlistener = new IPNMessage(request,configurationMap);

			boolean isIpnVerified = ipnlistener.validate();
			String transactionType = ipnlistener.getTransactionType();
			Map<String,String> map = ipnlistener.getIpnMap();

			String username = request.getParameter("custom");
			if ( username != null && username.contains("@")) {
				Profile profile = userDao.findByEmail(username);
				username = profile.getUsername();
			}

			try {
				
				String item_name = request.getParameter("item_name");  // May contain zips
				String zips = request.getParameter("item_name1");

				if (isIpnVerified) {
					if(item_name!=null) {
						String[] countyMonth = item_name.split("-");
						if(countyMonth!=null && countyMonth.length==2) {
							String county = countyMonth[0].trim();
							String month = countyMonth[1].trim();
							if ( month.contains("(") ) {
								String[] zipSplit = month.split(" ");
								month = zipSplit[0].trim();
								zips = zipSplit[1].replace('(', ' ').replace(')', ' ').trim();
							}
							
							purchaseService.savePurchase(county,month,username,zips,null);
							purchaseService.removeExamplePurchase(username);
						}
					}
				}
			    sendEmailForPaypal(request, isIpnVerified, transactionType, map, "");
			} catch ( Exception e ) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);

				SendMailUtility.emailReport(sw.toString(),"[/WCPayPal] - Exception in IPN subscription processing.");
			}
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);

			SendMailUtility.emailReport(sw.toString(),"[/WCPayPal] - Exception in IPN processing.");
		}
	}


	@RequestMapping(value = {"/WCPayPalSubscribe", "/wcserver/WCPayPalSubscribe"}, method = { RequestMethod.GET,  RequestMethod.POST})
	public void processSubscription(HttpServletRequest request, HttpServletResponse response) {

		request.getSession();
		StringBuffer sb = new StringBuffer("/WCPayPalSubscribe called ... \r\n");
		LogUtil.log("/WCPayPalSubscribe called ... \r\n");
		try {
			semaphore.acquire();
			Map<String,String> configurationMap =  Configuration.getConfig();
			IPNMessage 	ipnlistener = new IPNMessage(request,configurationMap);

			boolean isIpnVerified = ipnlistener.validate();
			String transactionType = ipnlistener.getTransactionType();
			Map<String,String> map = ipnlistener.getIpnMap();

//			String username = map.get("custom");
//			if ( username != null && username.contains("@")) {
//				try {
//					Profile profile = userDao.findByEmail(username);
//					username = profile.getUsername();
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//			

			try {
				sb.append("   IPN Map ... \r\n");
				for (String key : map.keySet()) {
					String value = map.get(key);
					sb.append("      \"" + key + "\" :\t \"" + value + "\"\r\n");
				}
			} catch (Exception e) {
				sb.append("     IPN Map EXCEPTION - " + e.getMessage());
			}

			String subscr_id = map.get("subscr_id");
			SubscriptionInfo subscription = subscriptionService.getSubscriptionById(subscr_id);
			String username = null; 
			if ( subscription == null ) {
				username = map.get("custom");
			} else {
				username = subscription.getUsername();				
			}

			sb.append("  isIpnVerified: " + isIpnVerified + "\r\n" +
					  "  transactionType: " + transactionType + "\r\n");
			LogUtil.log("  isIpnVerified: " + isIpnVerified);
			LogUtil.log("  transactionType: " + transactionType);
			try {
				
				String item_name = request.getParameter("item_name");  // May contain zips

				sb.append("  item_name: " + item_name + "\r\n");
				LogUtil.log("  item_name: " + item_name);
				
				if ( item_name == null ) {
					item_name = request.getParameter("product_name");
					sb.append("  using product_name: " + item_name + "\r\n");
					LogUtil.log("  using product_name: " + item_name);
				}
				
				if(/*isPresent &&*/ isIpnVerified) {

					//  Original verbose method
					//  FULL   "kswcrc.com subscription (BUTLER county - starting JAN)"
					//  BY ZIP "kswcrc.com subscription (ALLEGHENY county [15222,15219,15108,15090] - starting OCT)"

					//  Revised terse method
					//  FULL   "kswcrc (BUTLER - starting JAN)"
					//  BY ZIP "kswcrc (ALLEGHENY [15222,15219,15108,15090] - starting OCT)"
					int endIndex = item_name.indexOf(" -");
					if ( item_name.contains("[") ) {
						endIndex = item_name.indexOf(" [");
					}
					String county     = null;
					String startMonth = null;
					
					try {
						county = item_name.substring(item_name.indexOf("(") + 1, endIndex).replaceAll("county", "").trim();
						startMonth = item_name.substring(item_name.indexOf("starting ") + "starting ".length(), item_name.indexOf(")"));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
					if(item_name!=null) {
						if ("subscr_cancel".equals(transactionType) || "recurring_payment_suspended".equals(transactionType)) {
							subscriptionService.deactivateSubscription(subscr_id);
						}
						else if ("subscr_signup".equals(transactionType)) {
							//  Save subscription information, but no purchase until subscr_payment
							List<Purchase> purchases = purchaseService.getPurchasesBySubscrId(subscr_id);
							
							sb.append("  purchases: " + (purchases == null ? "null" : purchases.size() ) + "\r\n");
							LogUtil.log("  purchases: " + (purchases == null ? "null" : purchases.size() ));
							
							if ( purchases != null && purchases.size() > 0 ) {
								//  Purchase arrived prior to the signup.  SaveSubscription will use the lastPaidMonth prior to the supplied startMonth,
								//  but if the purchase arrived, then we need to advance startMonth to account for the payment.
								startMonth = MonthEnum.valueOf(startMonth).next().name();
							
								sb.append( "  adjusted: " + startMonth + "\r\n");
								LogUtil.log( "  adjusted: " + startMonth);
							}

							sb.append( "  Save subscription(" + county + ", " + username + ", " + startMonth + ", " + item_name + ", " + subscr_id + ")\r\n");
							LogUtil.log("  Save subscription(" + county + ", " + username + ", " + startMonth + ", " + item_name + ", " + subscr_id + ")");
							
							subscriptionService.saveSubscription(county, username, startMonth, item_name, subscr_id);
							
							sb.append( "  Remove example purchase\r\n");
							LogUtil.log("  Remove example purchase");
							
							purchaseService.removeExamplePurchase(username);
							
							sb.append( "  Completed\r\n");
							LogUtil.log("  Completed");

						} else if ("subscr_modify".equals(transactionType)) {
							//  This should not occur
							
						} else if ( "subscr_payment".equals(transactionType)) {
							sb.append("  Processing payment ...\r\n");
							LogUtil.log("  Processing payment ...");

							//  Process a subscription payment, update the purchase table to allow data access
							//  Retrieve the last month paid based on the subscr_id 
							String zips = null;
							try {
								zips = item_name.substring(item_name.indexOf('[')+1,item_name.indexOf(']'));
							} catch ( Exception e ) {
								zips = null;
							}
							sb.append("  zips: " + zips + "\r\n");
							LogUtil.log("  zips: " + zips);

//							SubscriptionInfo subscription = subscriptionService.getSubscriptionById(subscr_id);
							if ( subscription != null ) {
								LogUtil.log("  subscription: " + subscription);							
								sb.append("  Subscription: " + subscription.toString() + "\r\n");
								String paidMonth = MonthEnum.valueOf(subscription.getLastPaidMonth()).next().name();
									
								sb.append("  paidMonth (from): " + subscription.getLastPaidMonth() + "\r\n");
								sb.append("  paidMonth (to): " + paidMonth + "\r\n");
								LogUtil.log("  paidMonth (from): " + subscription.getLastPaidMonth());
								LogUtil.log("  paidMonth (to): " + paidMonth);

								sb.append("  SAVING PURCHASE:\r\n" +
										"username: " + username + "\r\n" + 
										"county: " + county + "\r\n" + 
										"paidMonth: " + paidMonth + "\r\n" +
										"zips: " + zips + "\r\n" +
										"subscr_id: " + subscr_id + "\r\n");
								String purId = purchaseService.savePurchase(county, paidMonth, username, zips, subscr_id);

								sb.append("  purId: " + purId + "\r\n");
								LogUtil.log("  purId: " + purId);
		
								subscription.setLastPaidMonth(paidMonth);
								subscriptionService.saveOrUpdatePaypalSubscription(subscription);
								
								sb.append("  Updated subscription ..." + "\r\n");
								LogUtil.log("  Updated subscription ...");
							} else {
								sb.append("  Subscription:  null\r\n");
								LogUtil.log("  Subscription:  null");
								
								//  Subscription payment arrived before subscr_signup email.  Must record the purchase.
								String purId = purchaseService.savePurchase(county, startMonth, username, zips, subscr_id);
								
								sb.append("  purId: " + purId + "\r\n");
								LogUtil.log("  purId: " + purId);
							}
						} else {
							sb.append("  No processing performed for transaction ... \r\n");
						}
					}
				}
			    sendEmailForPaypal(request, isIpnVerified, transactionType, map, sb.toString());
			} catch ( Exception e ) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);

				SendMailUtility.emailReport(sb.toString() + "\r\n\r\n\r\n" + sw.toString(),"[/WCPayPalSubscribe] - Exception in IPN subscription processing.");
			}
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);

			SendMailUtility.emailReport(sb.toString() + "\r\n\r\n\r\n" + sw.toString(),"[/WCPayPalSubscribe] - Exception in IPN processing.");
		} finally {
			try {
				semaphore.release();
			} catch ( Exception e ) {
			}
		}
		
	}
	
	private void sendEmailForPaypal(HttpServletRequest request,
			boolean isIpnVerified,
			String transactionType,
			Map<String,String> map, 
			String info) {

       //  Compose the message  
		try
		{  
			StringBuffer sb = new StringBuffer();
			sb.append("Paypal transaction notification\r\n" + info + "\r\n");
			Enumeration<String> paramNames = request.getParameterNames();
			while ( paramNames.hasMoreElements() ) {
				String param = paramNames.nextElement();
				sb.append("   Param: " + param + "     Value: " + request.getParameter(param) + "\r\n");
			}

			for ( String mapKey : map.keySet() ) {
				sb.append("   Map(" + mapKey + "): " + map.get(mapKey) + "\r\n");
			}

			String subject = "Paypal transaction (" + (isIpnVerified ? "Verified" : "NOT Verified")+ ") - " + transactionType;
			SendMailUtility.emailReport(sb.toString(), subject);
   
		} catch (Exception mex) {
		  mex.printStackTrace();
		}
	}
	

	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public static void main(String[] args) {

		String[] testValues = {"2017-08-15","2017-08-15","2017-08-15","2017-10-01","2017-12-31","2017-11-26","2018-02-15","2018-02-02","2018-03-15"};
		String[] subscStart = {"NOV",       "JAN",       "FEB",       "MAR",       "APR",       "JUL",       "JUN",       "JUL",       "AUG",       };

		for ( int index = 0; index < testValues.length; index++ ) {
			String testValue = testValues[index];
			String startMonth = subscStart[index];

			LocalDate subscDate = LocalDate.parse(testValue, formatter);
			LocalDate dataDate = subscDate.with(TemporalAdjusters.firstDayOfMonth());

			//  First subscribed month is the trial purchase, charged immediately
			//  Advance dataDate until the month is correct, then back off 3 for the next charge date
			String findMonth = "";
			while ( ! findMonth.equalsIgnoreCase(startMonth) ) {
				dataDate = dataDate.plusMonths(1);
				int intMonth = dataDate.getMonthValue();
				int adjMonth = (intMonth - 1) % 12;
				findMonth = MonthEnum.values()[adjMonth].toString();
			}
			LocalDate chargeDate = dataDate.minusMonths(2);

			long trialDays = java.time.temporal.ChronoUnit.DAYS.between(subscDate,chargeDate) - 1;
			LocalDate checkDate = subscDate.plusDays(trialDays);
			LogUtil.log("Buy Date: " + subscDate.format(formatter) + "      First Data Month: " + startMonth + "      Next Charge: " + chargeDate.format(formatter) + "    Days: " + trialDays + "   Check: " + checkDate.format(formatter));
		}
	}
	
	
}
