package com.synesis.web.controller;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.service.codes.CodesService;
import com.synesis.service.preferences.PreferenceService;

@Controller
public class PreferencesController {

	
	@Autowired
	private CodesService codesService;
	
	@Autowired
	private PreferenceService preferenceService;
	
	@RequestMapping(value = { "/WCPreferences" }, method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView defaultPage(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		

		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}
		
		Map<String,String> codeMap = codesService.getAllCodesWithDescription();
		
		Map<String, String> preferencesList = preferenceService.getAllPreferencesByUsername(session.getAttribute("username").toString());
		
		if(preferencesList!=null && !preferencesList.isEmpty()) {
			for(Entry<String, String> entry : preferencesList.entrySet()) {
				if(codeMap.containsKey(entry.getKey())) {
					codeMap.remove(entry.getKey());
				}
			}
		}
		
		model.addObject("codeMap", codeMap);
		model.addObject("preferencesList", preferencesList);
		model.setViewName("WCPreferences");
		return model;

	}
	
	@RequestMapping(value = { "/updatePreferences" }, method = {RequestMethod.POST})
	public ModelAndView updatePreferences(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		

		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}
		
		String[] selectedCodes = request.getParameterValues("duallistbox");
		
		preferenceService.updatePreferences(session.getAttribute("username").toString(), selectedCodes);
		
		model.addObject("title", "Preferences are Updated");
		model.addObject("message", "Your preferences will be used in generating Letters");
		model.setViewName("securemessage");
		return model;

	}
	
	
	
}
