package com.synesis.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.beans.ProfileBean;
import com.synesis.beans.StatsBean;
import com.synesis.database.CountyStats;
import com.synesis.database.Statistics;
import com.synesis.service.statistics.StatisticsService;
import com.synesis.service.users.UserService;
import com.synesis.utility.Configuration;
import com.synesis.utility.LogUtil;

@Controller
public class StatsController {

	@Autowired
	private StatisticsService statisticsService;
	
	@Autowired
	private UserService userService;
	
	
	@RequestMapping(value = { "/WCStatsAdmin" }, method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView displayAdminStats(HttpServletRequest request) {
		
		
		ModelAndView model = new ModelAndView();

		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}

		boolean isCarrierEmpty = false;
		boolean isObsoleteCheckbox = false;
		boolean isCollectedCheckbox = false;
		String fromDate = "" , toDate = "";
		
		String radioSelect = request.getParameter("radioSelect");
		if("leftRadio".equals(radioSelect)) {
			model.addObject("leftRadio", "checked");
			if("checked".equals(request.getParameter("emptyCheckbox"))) {
				isCarrierEmpty = true;
				model.addObject("emptyCheckbox", "checked");
			}
			
			if("checked".equals(request.getParameter("obsoleteCheckbox"))) {
				model.addObject("obsoleteCheckbox", "checked");
				String obsoleteDate = request.getParameter("obsoleteDate");
				if(!StringUtils.isEmpty(obsoleteDate)) {
					isObsoleteCheckbox = true;
					toDate = obsoleteDate;
					model.addObject("obsoleteDate", obsoleteDate);
				}
			}
			
		} else if("rightRadio".equals(radioSelect)) {
			model.addObject("rightRadio", "checked");
			String collectedFromDate = request.getParameter("collectedFromDate");
			String collectedToDate = request.getParameter("collectedToDate");
			isCollectedCheckbox = true;
			if(!StringUtils.isEmpty(collectedFromDate)) {
				fromDate = collectedFromDate;
				model.addObject("collectedFromDate", collectedFromDate);
			}
			if(!StringUtils.isEmpty(collectedToDate)) {
				toDate = collectedToDate;
				model.addObject("collectedToDate", collectedToDate);
			}
		}
		
		
		Map<String, StatsBean> availableMap = statisticsService.getAvailableAdminStats(isCarrierEmpty,isObsoleteCheckbox,isCollectedCheckbox,fromDate,toDate);
		Map<String, StatsBean> subscribedMap = statisticsService.getSubscribedAdminStats(isCarrierEmpty,isObsoleteCheckbox,isCollectedCheckbox,fromDate,toDate);
		
		model.addObject("availableMap", availableMap);
		model.addObject("subscribedMap", subscribedMap);
		
		
		model.setViewName("WCAdminStats");
		return model;
	}
	
	@RequestMapping(value = { "/WCStats" }, method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView defaultPage(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();

		String username = request.getRemoteUser();
		String county = request.getParameter("county");
		LogUtil.log("Username: " + username);


		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}
		
		if (county != null) {
			// display zip codes assigned to this county
			// request.getRequestDispatcher("/WEB-INF/WCZips.jsp").forward(request,
			// response);
			model.setViewName("WCZips");

		} else {

			String counties = request.getParameter("counties");
			String gccs = request.getParameter("gccs");
			String descs = request.getParameter("descs");

			boolean isAdmin = false;
			if (request.isUserInRole("ROLE_ADMIN")) {
				isAdmin = true;
		    }
			
			
			if (counties == null && gccs == null && descs == null) {
				// request.getRequestDispatcher("/WEB-INF/WCStatSel.jsp").forward(request,
				// response);
				model.setViewName("WCStatSel");
			} else {
				ProfileBean profile = userService.getUserProfile(String.valueOf(request.getSession().getAttribute("username")));

				String[] comps = new String[3];
				comps[0] = profile.getCarriersBean1().getName();
				comps[1] = profile.getCarriersBean2().getName();
				comps[2] = profile.getCarriersBean3().getName();

				Map<String, CountyStats> map = statisticsService.update(comps, counties, descs, gccs,isAdmin);
				
				Statistics s = new Statistics();
				s.setCountyStats(map);
				
				model.addObject("monthHeaders",s.getMonthHeaders());
				model.addObject("statistics", s);
				model.addObject("leadcost",Configuration.LEAD_COST);

//				request.getSession().setAttribute("monthHeaders", s.getMonthHeaders());
//				request.getSession().setAttribute("statistics", s);
				
				// request.getRequestDispatcher("/WEB-INF/WCStats.jsp").forward(request,
				// response);
				model.setViewName("WCStats");
			}
		}
		return model;

	}

}
