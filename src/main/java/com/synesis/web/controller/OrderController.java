package com.synesis.web.controller;

import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.employer.Employer;

@Controller
public class OrderController {

	public static final String ASC = "Ascending";
	public static final String DSC = "Descending";
	
	@RequestMapping(value = "/WCOrder", method = { RequestMethod.GET,  RequestMethod.POST})
	public ModelAndView defaultPage(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		
		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}
		
		@SuppressWarnings("unchecked")
		ArrayList<Employer> result = (ArrayList<Employer>)request.getSession().getAttribute("employers");
		String curSort = (String)request.getSession().getAttribute("sortedBy");
		String curOrder = (String)request.getSession().getAttribute("sortedOrder");
		String sortColumn = request.getParameter("sort");

		if ( sortColumn != null ) {
			if ( "emp".equals(sortColumn) ) {
				if ( "emp".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getKey().getName().compareTo(b.getKey().getName()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "fn".equals(sortColumn) ) {
				if ( "fn".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getKey().getFn().compareTo(b.getKey().getFn()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "carrier".equals(sortColumn) ) {
				if ( "carrier".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getCarrier().compareTo(b.getCarrier()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "desc".equals(sortColumn) ) {
				if ( "desc".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getDescription().compareTo(b.getDescription()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "address".equals(sortColumn) ) {
				if ( "address".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getBase().getAddress().compareTo(b.getBase().getAddress()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "city".equals(sortColumn) ) {
				if ( "city".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getBase().getCity().compareTo(b.getBase().getCity()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "zip".equals(sortColumn) ) {
				if ( "zip".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getBase().getZip().compareTo(b.getBase().getZip()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "gcc".equals(sortColumn) ) {
				if ( "gcc".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getCode().compareTo(b.getData().getCode()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "mm".equals(sortColumn) ) {
				if ( "mm".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getMm().compareTo(b.getData().getMm()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "xdate".equals(sortColumn) ) {
				if ( "xdate".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getXdate().compareTo(b.getData().getXdate()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "rate".equals(sortColumn) ) {
				if ( "rate".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getRate().compareTo(b.getData().getRate()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "comp1".equals(sortColumn) ) {
				if ( "comp1".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getCompRate(0).compareTo(b.getData().getCompRate(0)));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "pct1".equals(sortColumn) ) {
				if ( "pct1".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getPct(0).compareTo(b.getData().getPct(0)));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer b, Employer a) -> a.getData().getPct(0).compareTo(b.getData().getPct(0)));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "comp2".equals(sortColumn) ) {
				if ( "comp2".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getCompRate(1).compareTo(b.getData().getCompRate(1)));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "pct2".equals(sortColumn) ) {
				if ( "pct2".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getPct(1).compareTo(b.getData().getPct(1)));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer b, Employer a) -> a.getData().getPct(1).compareTo(b.getData().getPct(1)));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "comp3".equals(sortColumn) ) {
				if ( "comp3".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getCompRate(2).compareTo(b.getData().getCompRate(2)));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "pct3".equals(sortColumn) ) {
				if ( "pct3".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.sort(result, (Employer a, Employer b) -> a.getData().getPct(2).compareTo(b.getData().getPct(2)));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer b, Employer a) -> a.getData().getPct(2).compareTo(b.getData().getPct(2)));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			if ( "county".equals(sortColumn) ) {
				if ( "county".equals(curSort) && ASC.equals(curOrder) ) {
					Collections.reverse(result);
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					Collections.sort(result, (Employer a, Employer b) -> a.getKey().getCounty().compareTo(b.getKey().getCounty()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
				request.getSession().setAttribute("employers", result);
				request.getSession().setAttribute("eCount", new Integer(result.size()));
			}
			request.getSession().setAttribute("sortedBy", sortColumn);
		}
		model.setViewName("WCResults");
		
		return model;

	}
	
}
