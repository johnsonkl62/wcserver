package com.synesis.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.database.CollectionStats;

@Controller
public class SortController {

	
	private static ArrayList<String> chosen = new ArrayList<String>();
	static {
		chosen.add("ALLEGHENY");
		chosen.add("CHESTER");
		chosen.add("MONTGOMERY");
		chosen.add("PHILADELPHIA");
		chosen.add("DELAWARE");
	}
	
	public static final String ASC = "Ascending";
	public static final String DSC = "Descending";
	
	@RequestMapping(value = "/WCSort", method = { RequestMethod.GET,  RequestMethod.POST})
	public ModelAndView defaultPage(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}
		
		@SuppressWarnings("unchecked")
		Map<String,Map<String,CollectionStats>> map = 
				(Map<String,Map<String,CollectionStats>>)request.getSession().getAttribute("collection");
		String sortColumn = request.getParameter("column");

		Map<String,Map<String,CollectionStats>> sortedMap = new LinkedHashMap<String,Map<String,CollectionStats>>();
		ArrayList<String> countyKeys = new ArrayList<String>();

		if ( sortColumn != null ) {
			if ( "COUNTY".equals(sortColumn) ) {
				countyKeys = new ArrayList<String>(map.keySet());
				Collections.sort(countyKeys);
			} else {

				
				for ( String unused : chosen ) {
					int max = 0;
					String next = "";
					for (String county : chosen ) {
						if ( !countyKeys.contains(county) ) {
							CollectionStats data = map.get(county).get(sortColumn);
							int undone = (data.getTotal() - data.getDone()); 
							if ( undone > max ) {
								max = undone;
								next = county;
							}
						}
					}
					countyKeys.add(next);
				}
				for (String county : countyKeys ) {
					sortedMap.put(county, map.get(county) );
				}
				map = null;
				
			}			
			request.getSession().setAttribute("collection", sortedMap);
		}
		model.setViewName("WCCollection");
		
		return model;

	}
	
}
