package com.synesis.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.beans.ZipsBean;
import com.synesis.service.zip.ZipService;

@Controller
public class ZipController {

	
	@Autowired
	private ZipService zipService;
	
	@RequestMapping(value = { "/WCZips" }, method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView defaultPage(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		

		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}
		

		String county = request.getParameter("county");
		
		if(county!=null) {
			List<com.synesis.model.Zips> zips = zipService.findZipByCounty(county);
			List<ZipsBean> zipsBeans = zips.stream().map(zip -> convertZipToBean(zip))
					.sorted((zip1, zip2) -> zip2.getZip().compareTo(zip1.getZip()))
					.collect(Collectors.toList());
			model.addObject("countyKey", county);
			model.addObject("countyZips", zipsBeans);
//			request.getRequestDispatcher("/WEB-INF/WCCountyZips.jsp").forward(request, response);
			model.setViewName("WCCountyZips");
		}
		
		return model;

	}
	
	
	private ZipsBean convertZipToBean(com.synesis.model.Zips zips) {
		if(zips!=null) {
			ZipsBean bean = new ZipsBean();
			bean.setZip(zips.getZip());
			bean.setCity(zips.getCity());
			bean.setCounty(zips.getCounty());
			bean.setType(zips.getType());
			return bean;
		} 
		
		return null;
	}
	
}
