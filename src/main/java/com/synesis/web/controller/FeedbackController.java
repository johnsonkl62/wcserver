package com.synesis.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.beans.ProfileBean;
import com.synesis.service.users.UserService;
import com.synesis.utility.SendMailUtility;

@Controller
public class FeedbackController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/makefeedback", method = RequestMethod.GET)
	public ModelAndView defaultPage(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "makefeedback");
			model.setViewName("WCLogin");
			return model;
		}
		
		 ProfileBean profile = userService.getUserProfile(String.valueOf(session.getAttribute("username")));
		
		model.addObject("profileBean",profile);
		model.setViewName("makeSuggestion");
		
		return model;

	}
	

	@RequestMapping(value = "/sendFeedback", method = RequestMethod.POST)
	public ModelAndView sendFeedback(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "makefeedback");
			model.setViewName("WCLogin");
			return model;
		}
		
		String feedback = request.getParameter("feedback");
		String contactMe = request.getParameter("contactMe");
		
		SendMailUtility.emailReport(prepareMail(feedback, String.valueOf(session.getAttribute("username")), contactMe), "Feedback Report");
		
		model.addObject("title", "Feedback/Suggestion Sent");
		model.addObject("message", "Thank you for your trust in us. We will consider your Feedback/Suggestion and get back to you if needed");
		model.setViewName("securemessage");
		
		return model;
	}
	
	
	private String prepareMail(String feedback,String username, String contactMe) {

		StringBuilder text = new StringBuilder("KSWCRC Feedback\r\n\r\n");

		text.append("User : " + username + "\r\n\r\n");
		text.append("Contact Me : " + (contactMe != null && "on".equals(contactMe) ? "Yes":"No") + "\r\n");
		text.append("Feedback : " + feedback + "\r\n");

		return text.toString();
	}

}
