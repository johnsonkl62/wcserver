package com.synesis.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.service.announcement.AnnouncementService;
import com.synesis.utility.SendMailUtility;

@Controller
public class AnnouncementsController {

	@Autowired
	private AnnouncementService announcementService;
	
	@RequestMapping(value = { "/WCAddAnnouncements" }, method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView defaultPage(HttpServletRequest request) {
		

		ModelAndView model = new ModelAndView();
		model.setViewName("addAnnouncements");
		return model;
	}
	

	@RequestMapping(value = { "/WCAnnouncements" }, method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView getAllAnnouncements(HttpServletRequest request) {
		

		ModelAndView model = new ModelAndView();
		List<String> announcements = announcementService.getAllAnnouncements();
		
		model.addObject("announcements", announcements);
		model.setViewName("WCAnnouncements");
		return model;
	}
	
	@RequestMapping(value = "/addAnnouncement", method = RequestMethod.POST)
	public ModelAndView sendContactUs(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "WCAddAnnouncements");
			model.setViewName("WCLogin");
			return model;
		}
		
		String announcementText = request.getParameter("announcementText");
		
		announcementService.addAnnouncement(announcementText);
		
		List<String> announcements = announcementService.getAllAnnouncements();
		
		model.addObject("announcements", announcements);
		
//		model.addObject("title", "Announcement Added in DB");
//		model.addObject("message", "Thank you for your trust in us.");
		model.setViewName("WCAnnouncements");
		
		return model;

	}
	
}
