package com.synesis.web.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.io.ByteStreams;
import com.synesis.beans.CarriersBean;
import com.synesis.beans.ProfileBean;
import com.synesis.beans.TemplateBean;
import com.synesis.model.Profile;
import com.synesis.service.carriers.CarrierService;
import com.synesis.service.purchase.PurchaseService;
import com.synesis.service.rates.RatesService;
import com.synesis.service.users.UserService;
import com.synesis.utility.ConvertionUtility;
import com.synesis.utility.LogUtil;
import com.synesis.utility.SendMailUtility;

@Controller
public class MainController {

	@Autowired
	private RatesService ratesService;
	
	@Autowired
	private PurchaseService purchaseService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private CarrierService carrierService;
	
	@RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
	public ModelAndView defaultPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Here goes the title");
		model.addObject("message", "This is default page!");
		model.setViewName("index");
		return model;

	}

	@RequestMapping(value = "/secure**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "This is the Secure Page");
		model.addObject("message", "This page is for ROLE_USER only!");
		model.setViewName("secure");

		return model;

	}

	
	@RequestMapping(value = "/WCChangePassword", method = RequestMethod.GET)
	public ModelAndView changePasswordView(@RequestParam(value = "error", required = false) String error,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		model.setViewName("WCChangePassword");
		return model;

	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public ModelAndView changePassword(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}

		String oldPassword = request.getParameter("oldPassword"); 
		String newPassword = request.getParameter("newPassword");
		
		boolean isOldPasswordMatched = false;
		
		if(!StringUtils.isEmpty(oldPassword)) {
			isOldPasswordMatched = userService.isOldPasswordMatched(session.getAttribute("username").toString(),oldPassword);
		}
		
		if(isOldPasswordMatched && !StringUtils.isEmpty(newPassword)) {
			userService.changePassword(newPassword, session.getAttribute("username").toString());
			model.addObject("title", "Password Changed Successfully");
			model.addObject("message", "Your Password is changed Successfully!!");
			model.setViewName("securemessage");
			
		} else {
			model.addObject("title", "Error in Updating password");
			model.addObject("message", "Your Password is not updated in the system, Please try again sometime later");
			model.setViewName("securemessage");
		}
		
		return model;

	}
	

	@RequestMapping(value = "/forgotpassword", method = RequestMethod.GET)
	public ModelAndView forgotpassword(@RequestParam(value = "error", required = false) String error,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}
		Profile user = userService.findByEmail((String)request.getAttribute("email"));

		model.addObject("username", (user != null ? user.getUsername() : "No user associated with this email"));
		model.setViewName("forgotpassword");
		return model;

	}

	@RequestMapping(value = "/forgotusername", method = RequestMethod.GET)
	public ModelAndView forgotusername(@RequestParam(value = "error", required = false) String error,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		model.setViewName("forgotusername");
		return model;

	}
	
	@RequestMapping(value = "/emailLink", method = RequestMethod.POST)
	public ModelAndView emailLink(@RequestParam(value = "error", required = false) String error,
			HttpServletRequest request) {

		String type = request.getParameter("type");
		String msg = "";boolean result = false;
		
		if("username".equals(type)) {
			 Profile profile = userService.findByEmail(request.getParameter("emailAddress"));
			if(profile!=null) {
				try {
					new SendMailUtility().sendMail(profile.getEmail(), "Username of your account", 
							"Username of your account is : "+profile.getUsername());
					result = true;
					msg = "Username is emailed to your email address";			
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		} else if("password".equals(type)) {
			String url = getURLWithContextPath(request);
			result = userService.sendEmailAndUpdateToken(url, request.getParameter("emailAddress"));
			msg = "Please check your email and resetPassword";
		}
		
		ModelAndView model = new ModelAndView();
		
		if(result) {
			model.addObject("title", "Email Sent");
			model.addObject("message", msg);
			model.setViewName("message");
		} else {
			model.addObject("title", "Email sent Error");
			model.addObject("message", "Please check your email!!");
			model.setViewName("message");
		}
		
		
		return model;
	}
	
	@RequestMapping(value = "/resetpassword", method = RequestMethod.GET)
	public ModelAndView resetpassword(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "emailAddress", required = true) String emailAddress,
			@RequestParam(value = "token", required = true) String token,
			HttpServletRequest request) {

		boolean result = userService.resetPassword(token, emailAddress);
		
		ModelAndView model = new ModelAndView();
		if(result) {
			model.addObject("email", emailAddress);
			LogUtil.log("Success");
			
		} else {
			model.addObject("title", "Verification Failed");
			model.addObject("message", "Please verify again");
			model.setViewName("message");
		}
		
		return model;
	}
	
	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	public ModelAndView updatePassword(@RequestParam(value = "error", required = false) String error,
			HttpServletRequest request) {

		boolean result = userService.updatePassword(request.getParameter("password"), request.getParameter("emailAddress"));
		
		ModelAndView model = new ModelAndView();
		if(result) {
			model.addObject("msg", "Please re-login with new password");
			model.setViewName("WCLogin");
			LogUtil.log("Success");
			
		} else {
			model.addObject("title", "Password Update failed");
			model.addObject("message", "Please reset it again");
			model.setViewName("message");
		}
		
		return model;
	}
	
	
	@RequestMapping(value = "/WCRegister", method = RequestMethod.GET)
	public ModelAndView registration(@RequestParam(value = "error", required = false) String error,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		List<CarriersBean> list = carrierService.getAllCarriers().stream()
				.map(carrier -> ConvertionUtility.converToCarrierBean(carrier))
				.filter(carrier -> carrier!=null && carrier.getName()!=null && !carrier.getName().trim().isEmpty())
				.sorted((CarriersBean a, CarriersBean b) -> a.getName().compareTo(b.getName()))
				.collect(Collectors.toList());
//		Collections.sort(list, (CarriersBean a, CarriersBean b) -> a.getName().compareTo(b.getName()));
		
//		LogUtil.log("Carriers :: "+list);
		
		model.addObject("carriers", list);
		
		model.setViewName("WCRegister");

		return model;

	}
	

	@RequestMapping(value = "/updateProfile", method = RequestMethod.GET)
	public ModelAndView updateProfilePage(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "updateProfile");
			model.setViewName("WCLogin");
			return model;
		}
		ProfileBean profile = userService.getUserProfile(String.valueOf(session.getAttribute("username")));
		model.addObject("profileBean", profile);
		model.setViewName("WCUpdateRegistration");

		return model;

	}
	
	@RequestMapping(value="/download-template",method = RequestMethod.GET)
	public ResponseEntity<byte[]> downloadSampleTemplate(HttpServletRequest request, HttpServletResponse response) {
		
		String username = request.getParameter("username");
		
		TemplateBean templateBean =  userService.getUserTemplate(username);
		int byteSize = 0;
		InputStream ifs = null;
		String fileName = "Template-guest.docx";
		
		if(templateBean!=null && templateBean.getTemplate()!=null) {
			
			fileName = "Template-"+username+".docx";
			byteSize = templateBean.getTemplate().length;
			ifs = new ByteArrayInputStream(templateBean.getTemplate()); 
			
		} else {
			
			String tmplFileName = getClass().getClassLoader().getResource(fileName).getFile();
			
			File wordTemplate = new File(tmplFileName);
			
			byteSize = (int) wordTemplate.length();
			
			try {
				ifs = new FileInputStream(wordTemplate);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		
		try {
			
			
			
			String contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
			
			response.reset();
			response.setBufferSize(byteSize);
			response.setContentType(contentType);
			response.setHeader("Content-Length", String.valueOf(byteSize));
			response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"");
			IOUtils.copy(ifs, response.getOutputStream());
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(ifs!=null) {
				try {
					ifs.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	
	@RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
	public ModelAndView updateProfile(@RequestParam("template") MultipartFile file,HttpServletRequest request) {

		String username = request.getParameter("username");
		String lastName = request.getParameter("last");
		String firstName = request.getParameter("first");
		String middle = request.getParameter("mi");
		String suffix = request.getParameter("sfx");
		String agency = request.getParameter("agency");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone").replaceAll("[^0-9]", "");
		String pwd = request.getParameter("pwd");

		ProfileBean p = new ProfileBean(username, lastName, firstName, middle, suffix, agency, email, phone, pwd);
		if(file!=null)
			try {
				p.setTemplateBean(new TemplateBean(file.getBytes(), 1));
			} catch (IOException e) {
				e.printStackTrace();
			}
		userService.update( p);
		
		ModelAndView model = new ModelAndView();
		model.setViewName("WCSearch");
		return model;
	}
	
	
	@RequestMapping(value = "/checkusername", method = RequestMethod.GET)
    public @ResponseBody String checkUserName(@RequestParam(value = "username", required = true) String username) {
 

		boolean isPresent = userService.checkUserName(username);
		if(isPresent) {
			return "Username taken";
		} else {
			return "Available";
		}
    }
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView registerRedirect(HttpServletRequest request) {

		String username = request.getParameter("username");
		String lastName = request.getParameter("last");
		String firstName = request.getParameter("first");
		String middle = request.getParameter("mi");
		String suffix = request.getParameter("sfx");
		String agency = request.getParameter("agency");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String pwd = request.getParameter("pwd");

		String[] carriers = new String[3];
		String[] selected = request.getParameterValues("selectCarrier"); 
		for ( int i = 0; i < carriers.length; i++ ) {
			if ( selected.length > i ) {
				carriers[i] = selected[i];
			} else {
				carriers[i] = "27677";
			}
		}
		carriers = orderCarriers(carriers);
		
		ProfileBean p = new ProfileBean(username, lastName, firstName, middle, suffix, agency, email, phone.replaceAll("[^0-9]", ""), pwd);

		p.setCarrier1(carriers[0]);
		p.setCarrier2(carriers[1]);
		p.setCarrier3(carriers[2]);
		
		 try {
			byte[] template = ByteStreams.toByteArray(getClass().getClassLoader().getResourceAsStream("Template-guest.docx"));
			p.setTemplateBean(new TemplateBean(template, 1));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		userService.save( p);
//		Profile profile = profileService.getProfileByUsername(username);
		purchaseService.saveExamplePurchase(username);
		
		SendMailUtility.emailReport(p.toString(), "New WC registration - " + username);
		
		ModelAndView model = new ModelAndView();
		model.setViewName("TermsAndConditions");
		return model;

	}

	
	private String[] orderCarriers(String[] carriers) {
		class CompRate {
			String naic = "";
			Double rate = -1.0;
			CompRate(String naic) {
				this.naic = naic;
				rate = ratesService.getRateFromCodeAndNaic("951",naic);
			}
			public String getNaic() {
				return naic;
			}
			public Double getRate() {
				return rate;
			}
		}
		
//		if( carriers.length <  3) {
//			carriers = Arrays.copyOf(carriers, carriers.length+1);
//			carriers[carriers.length-1] = "27677";
//		}
//		
		String[] result = new String[carriers.length];
		
		ArrayList<CompRate> inputs = new ArrayList<CompRate>();
		if(carriers!=null) {
			for ( String s : carriers ) {
				inputs.add(new CompRate(s));
			}
			Collections.sort(inputs, (CompRate a, CompRate b) -> b.getRate().compareTo(a.getRate()));
			
			int index = 0;
			for ( CompRate cr : inputs ) {
				result[index++] = cr.getNaic();
			}
		}
		return result;
	}

	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "Invalid username and password!";
		}

		return error;
	}

	// for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			LogUtil.log(userDetail.toString());

			model.addObject("username", userDetail.getUsername());

		}

		model.setViewName("403");
		return model;

	}
	
	public  String getURLWithContextPath(HttpServletRequest request) {
		   return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
		}

}
