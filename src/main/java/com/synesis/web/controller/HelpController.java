package com.synesis.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.utility.SendMailUtility;

@Controller
public class HelpController {

	
	@RequestMapping(value = "/FAQs", method = RequestMethod.GET)
	public ModelAndView defaultPage(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "FAQs");
			model.setViewName("WCLogin");
			return model;
		}
		
		model.setViewName("FAQs");
		
		return model;

	}
	
	
	@RequestMapping(value = "/tutorial", method = RequestMethod.GET)
	public ModelAndView showTutorial(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "tutorial");
			model.setViewName("WCLogin");
			return model;
		}
		
		model.setViewName("VideoTutorial");
		
		return model;

	}
	
	
	@RequestMapping(value = "/contactUs", method = RequestMethod.GET)
	public ModelAndView contactUspage(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "contactUs");
			model.setViewName("WCLogin");
			return model;
		}
		
		model.setViewName("ContactUs");
		
		return model;

	}
	
	
	@RequestMapping(value = "/sendContactUs", method = RequestMethod.POST)
	public ModelAndView sendContactUs(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "contactUs");
			model.setViewName("WCLogin");
			return model;
		}
		
		String comment = request.getParameter("comment");
		String emailId = request.getParameter("emailId");
		
		SendMailUtility.emailReport(prepareMail(comment, String.valueOf(session.getAttribute("username")), emailId), "Contact Us - Report");
		
		model.addObject("title", "Comments Posted to Admin");
		model.addObject("message", "Thank you for your trust in us. We will soon get in touch with you.");
		model.setViewName("securemessage");
		
		return model;

	}
	
	
	@RequestMapping(value = "/WCTerms", method = RequestMethod.GET)
	public ModelAndView termsPage(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "TermsAndConditions");
			model.setViewName("WCLogin");
			return model;
		}
		
		model.setViewName("TermsAndConditions");
		
		return model;

	}
	
	private String prepareMail(String comments,String username, String emailId) {
		
		StringBuilder text = new StringBuilder("KSWCRC Help\r\n\r\n");
		
		text.append("User : "+username+"\r\n");
		text.append("Email : "+emailId+"\r\n\r\n");
		text.append("Comments : </b><pre>"+comments+"\r\n");
		
		return text.toString();
	}
	
	
}
