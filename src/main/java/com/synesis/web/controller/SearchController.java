package com.synesis.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.beans.PurchaseBean;
import com.synesis.model.Purchase;
import com.synesis.service.purchase.PurchaseService;
import com.synesis.utility.LogUtil;

@Controller
public class SearchController {

	@Autowired
	private PurchaseService purchaseService;

	@RequestMapping(value = { "/WCSearch" }, method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView defaultPage(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();

		String username = request.getRemoteUser();
		LogUtil.log("Username: " + username);

		HttpSession session = request.getSession(false);
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "WCSearch");
			model.setViewName("WCLogin");
			return model;
		}
		
		// Read subscriptions
		List<com.synesis.model.Purchase> purchases = purchaseService
				.getPurchasesByUsername(String.valueOf(session.getAttribute("username")));

		List<PurchaseBean> beans = purchases.stream()
				.map(subscription -> convertModelToBean(subscription))
				.filter(subscription -> subscription != null)
				.collect(Collectors.toList());

		model.addObject("purchases", beans);
		model.setViewName("WCSearch");
		return model;
	}

	private PurchaseBean convertModelToBean(Purchase purchase) {
		if (purchase != null) {
			PurchaseBean bean = new PurchaseBean();
			bean.setId(purchase.getId());
			bean.setCounty(purchase.getCounty());
			bean.setMonth(purchase.getMonth());
			bean.setBuyDate(purchase.getBuyDate());
			bean.setUsername(purchase.getUsername());
			bean.setZipCode(purchase.getZip());
			return bean;
		}

		return null;
	}

}
