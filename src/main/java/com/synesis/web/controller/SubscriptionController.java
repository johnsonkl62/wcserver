/**
 * 
 */
package com.synesis.web.controller;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.beans.ProfileBean;
import com.synesis.beans.ZipsBean;
import com.synesis.database.CountyStats;
import com.synesis.database.MonthStats;
import com.synesis.database.Statistics;
import com.synesis.model.Zips;
import com.synesis.service.statistics.StatisticsService;
import com.synesis.service.users.UserService;
import com.synesis.service.zip.ZipService;
import com.synesis.utility.Configuration;
import com.synesis.utility.LogUtil;
import com.synesis.utility.MonthEnum;
import com.synesis.utility.SendMailUtility;

/**
 * @author GovindG
 *
 */
@Controller
public class SubscriptionController {

	@Autowired
	private StatisticsService statisticsService;

	@Autowired
	private UserService userService;

	@Autowired
	private ZipService zipService;
	
	public static final String ASC = "Ascending";
	public static final String DSC = "Descending";
	
	@RequestMapping(value = "/Subscribe", method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView subscriptionPage(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("username") == null) {
			request.getSession().setAttribute("TargetPage", "/");
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}

		boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
		
		String county = request.getParameter("county");

		
		ProfileBean profile = userService.getUserProfile(String.valueOf(request.getSession().getAttribute("username")));
		String[] comps = new String[3];
		comps[0] = profile.getCarriersBean1().getName();
		comps[1] = profile.getCarriersBean2().getName();
		comps[2] = profile.getCarriersBean3().getName();

		Map<String, CountyStats> map = statisticsService.update(comps, county, null, null,isAdmin);
		CountyStats cs = map.get(county);

		String[] allMonths = Statistics.getMonthHeaders();
		MonthStats[] monthlyStats = cs.getMonthStats();

		String startMonth = allMonths[3];	//  startMonth is assumed to be index 3 (index 0 is current month, index 1 and 2 are too close)

		//  Need to automate the removal of obsolete purchases two months in the past (October 1st should move August purchases to "history" table)
		//  Start month should not check for last month or the month before.  For most cases, it could stop at index 7 which is the limit of the 
		//  displayed months.
		for ( int i = 3; i < 8; i++ ) {
			if ( !monthlyStats[i].isAvailable() ) {
				startMonth = allMonths[i+1];	//  If any upcoming month is not available, move the start month to one beyond it.  "i+1" will always be within range.
			}
		}
		
		// ERROR CONDITION:  If all months are unavailable, it gets here using index 3.

		int totalLeads = cs.getDone() - cs.getComps();
		int avgMonthlyLeads = (int)(((float)totalLeads / 12f) + 0.5);
		float avgMonthlyPrice = avgMonthlyLeads * Configuration.LEAD_COST;
		float subscriptionPrice = avgMonthlyPrice * Configuration.SUBSC_DISCOUNT;
		float salesTax = subscriptionPrice * Configuration.SALES_TAX;

		//  Must use trial period (at full price) to get subscription to start on the first of each month
		LocalDate subscribeDate = LocalDate.now();

		//  Need to get the first day of the data month that starts the subscription.
		//  This initial month will be purchased immediately using a trial period, which must extend
		//  from now until the charge date of the next available month (first day of the third month
		//  prior to the data).
		//
		//     Example #1:
		//     Sign-up on:   08/15/2017
		//     Available:    NOV		(no intermediate months are SOLD)
		//     Immediate:    NOV data purchased immediately 
		//     Trial Period: 16 days
		//     Next Charge:  09/01/2017 for DEC data
		//     3rd Charge:   10/01/2017 for JAN data
		//
		//     Example #2:
		//     Sign-up on:   08/15/2017
		//     Available:    FEB		(JAN is SOLD and unavailable)
		//     Immediate:    FEB data purchased immediately
		//     Trial Period: 107 days
		//     Next Charge:  12/01/2017 for MAR data
		//     3rd Charge:   01/01/2018 for APR data
		//
		LocalDate dataDate = subscribeDate.with(TemporalAdjusters.firstDayOfMonth());

		//  Advance dataDate until the month is correct.  This will be the first month of data,
		//  which has already been purchased through the immediate charge to set up the subscription.
		//  Back off 2 months to get the next charge date for the second month of data, which will
		//  start the regularly scheduled monthly charges.
		String findMonth = "";
		while ( ! findMonth.equalsIgnoreCase(startMonth) ) {
			dataDate = dataDate.plusMonths(1);
			int intMonth = dataDate.getMonthValue();
			int adjMonth = (intMonth - 1) % 12;
			findMonth = MonthEnum.values()[adjMonth].toString();
		}
		LocalDate chargeDate = dataDate.minusMonths(2);

		//  This could be zero if subscribed on the last day of a month (on AUG 31, the immediate purchase would be
		//  for NOV, then the next day (SEP 1), a charge should be made for DEC data.
		long trialDays = java.time.temporal.ChronoUnit.DAYS.between(subscribeDate,chargeDate);
		//   WARNING!  Trial days cannot exceed 90 ...
		if ( trialDays > 90 ) trialDays = 90;

		
		model.addObject("County", county);
		model.addObject("startMonth", startMonth);  // Statistics.MONTH[(pickMonth + curMonth) % 12]);
		model.addObject("avgTotalEmpCount", new Integer((int)avgMonthlyLeads));
		model.addObject("avgPrice", new Integer((int)avgMonthlyPrice));
		model.addObject("leadcost",Configuration.LEAD_COST);
		model.addObject("subscriptionPrice", new Float(subscriptionPrice));
		model.addObject("salesTax", new Float(salesTax));
		model.addObject("totalAmount", new Float(subscriptionPrice) + new Float(salesTax));
		model.addObject("trialDays",new Integer((int)trialDays));
		model.addObject("email", profile.getEmail());
		model.addObject("username", profile.getUsername());

		//  Add PayPal settings for LIVE or SANDBOX based on this server's IP
		model.addObject("paypalUrl", Configuration.getConfig().get("paypalUrl"));
		model.addObject("paypalBusiness", Configuration.getConfig().get("paypalBusiness"));
		model.addObject("paypalReturn", Configuration.getConfig().get("paypalReturn"));
		model.addObject("paypalNotifyUrl", Configuration.getConfig().get("paypalSubscribeNotifyUrl"));

		model.setViewName("Subscription");

		return model;
	}

	/*private static boolean isEligibleForAvgEmp(int percent, int totalEmployee,
			int collectedEmployee) {
		double percentageValue = totalEmployee * (percent/100.0); 
		return (percentageValue <= collectedEmployee);
	}*/

	@RequestMapping(value = "/SubscribeByZip", method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView detailsWithZip(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		HttpSession session = request.getSession(false);

		
		if (session == null || session.getAttribute("username") == null) {
			request.getSession().setAttribute("TargetPage", "/");
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}
		ProfileBean profile = userService.getUserProfile(String.valueOf(request.getSession().getAttribute("username")));
		List<ZipsBean> zipsBeans = null;

		boolean isRetrievedFromSession = false;
		String sort = request.getParameter("sort");

		if (session.getAttribute("ratesByZip") != null && sort != null
				&& !sort.isEmpty()) {
			zipsBeans = (List<ZipsBean>) session.getAttribute("ratesByZip");
			String curOrder = (String) request.getSession().getAttribute(
					"sortedOrder");

			if ("zip".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getZip().compareTo(
							z1.getZip()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getZip().compareTo(
							z2.getZip()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			} else if ("type".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getType().compareTo(
							z1.getType()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getType().compareTo(
							z2.getType()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			else if ("city".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getCity().compareTo(
							z1.getCity()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getCity().compareTo(
							z2.getCity()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			else if ("total".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getTotal().compareTo(
							z1.getTotal()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getTotal().compareTo(
							z2.getTotal()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			else if ("available".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getAvailable().compareTo(
							z1.getAvailable()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getAvailable().compareTo(
							z2.getAvailable()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			else if ("ineligible".equals(sort)) {
				if (ASC.equals(curOrder)) {
					zipsBeans.sort((z1, z2) -> z2.getIneligible().compareTo(
							z1.getIneligible()));
					request.getSession().setAttribute("sortedOrder", DSC);
				} else {
					zipsBeans.sort((z1, z2) -> z1.getIneligible().compareTo(
							z2.getIneligible()));
					request.getSession().setAttribute("sortedOrder", ASC);
				}
			}

			isRetrievedFromSession = true;
		}

		String county = request.getParameter("county");

		if (!isRetrievedFromSession) {

			Map<String, Zips> countyZips = zipService.findAllZipsByCounty(county);
			profile = userService.getUserProfile(String
					.valueOf(request.getSession().getAttribute("username")));
			String[] comps = new String[3];
			comps[0] = profile.getCarriersBean1().getName();
			comps[1] = profile.getCarriersBean2().getName();
			comps[2] = profile.getCarriersBean3().getName();

			Map<String, ZipsBean> map = statisticsService.fetchCountsByZip(comps, county);

			zipsBeans = new ArrayList<ZipsBean>();

			for (Entry<String, ZipsBean> entry : map.entrySet()) {
				if (countyZips.containsKey(entry.getValue().getZip())) {
					Zips zip = countyZips.get(entry.getValue().getZip());
					ZipsBean bean = entry.getValue();
					bean.setType(zip.getType());
					bean.setCity(zip.getCity());
					zipsBeans.add(bean);
				} else {
					LogUtil.log("County zips missing " + entry.getValue().getZip());
				}
			}

			LogUtil.log("Details using Zip : " + map);
		}

		session.setAttribute("ratesByZip", zipsBeans);
		model.addObject("ratesByZip", zipsBeans);
		model.addObject("county", county);
		model.addObject("email", profile.getEmail());
		model.addObject("leadcost",Configuration.LEAD_COST);
		model.setViewName("SubscriptionByZipDetails");

		return model;

	}
	


	@RequestMapping(value = "/SubscriptionPaymentDetailsWithZip", method = {
			RequestMethod.GET, RequestMethod.POST })
	public ModelAndView showPaymentDetailsPageWithZip(
			HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("username") == null) {
			request.getSession().setAttribute("TargetPage", "/");
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}

//		boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
		
		ProfileBean profile = userService.getUserProfile(String.valueOf(request
				.getSession().getAttribute("username")));

		String county = request.getParameter("county");
		String month = request.getParameter("month");
		String total = request.getParameter("total");
		String zips = request.getParameter("zips");

		if (zips.contains(",")) {
			zips = zips.substring(0, zips.length() - 1);
		}
		
		String[] comps = new String[3];
		comps[0] = profile.getCarriersBean1().getName();
		comps[1] = profile.getCarriersBean2().getName();
		comps[2] = profile.getCarriersBean3().getName();

		//  Unlike "/Subscribe", the start month is already calculated and provided in the call

		//  Must use trial period (at full price) to get subscription to start on the first of each month
		LocalDate subscribeDate = LocalDate.now();

		//  See "/Subscribe" for comments explaining the trial period
		LocalDate dataDate = subscribeDate.with(TemporalAdjusters.firstDayOfMonth());
		String findMonth = "";
		while ( ! findMonth.equalsIgnoreCase(month) ) {
			dataDate = dataDate.plusMonths(1);
			int intMonth = dataDate.getMonthValue();
			int adjMonth = (intMonth - 1) % 12;
			findMonth = MonthEnum.values()[adjMonth].toString();
		}
		LocalDate chargeDate = dataDate.minusMonths(2);

		//  This could be zero if subscribed on the last day of a month (on AUG 31, the immediate purchase would be
		//  for NOV, then the next day (SEP 1), a charge should be made for DEC data.
		long trialDays = java.time.temporal.ChronoUnit.DAYS.between(subscribeDate,chargeDate);
		//   WARNING!  Trial days cannot exceed 90 ...
		if ( trialDays > 90 ) trialDays = 90;

		float subscriptionPrice = Float.valueOf(total) * Configuration.SUBSC_DISCOUNT;
		float salesTax = Float.valueOf(total) * Configuration.SALES_TAX;
		model.addObject("startMonth", month);
		model.addObject("avgTotalEmpCount", new Integer((int)(Float.valueOf(total)/Configuration.LEAD_COST)));
		model.addObject("subscriptionPrice", new Float(subscriptionPrice));
		model.addObject("salesTax", new Float(salesTax));
		model.addObject("totalAmount", new Float(subscriptionPrice) + new Float(salesTax));
		model.addObject("leadcost",Configuration.LEAD_COST);

		model.addObject("County", county);
		model.addObject("Month", month);
		model.addObject("Price", total);

		if (zips.length() > 127 ) {
			// Truncate and depend on manual processing
			zips = zips.substring(1,127);
			try {
				SendMailUtility.sendMail("KLJ1962@GMAIL.COM", "PayPal subscription needs attention!!!", "Truncating zips to 127 characters" + "\r\n" + zips);
				SendMailUtility.sendMail("kevin@firstaffiliated.com", "PayPal subscription needs attention!!!", "Truncating zips to 127 characters" + "\r\n" + zips);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		model.addObject("Zips", zips	);
		
		model.addObject("trialDays",new Integer((int)trialDays));
		model.addObject("email", profile.getEmail());
		
		//  Add PayPal settings for LIVE or SANDBOX based on this server's IP
		model.addObject("paypalUrl", Configuration.getConfig().get("paypalUrl"));
		model.addObject("paypalBusiness", Configuration.getConfig().get("paypalBusiness"));
		model.addObject("paypalReturn", Configuration.getConfig().get("paypalReturn"));
		model.addObject("paypalNotifyUrl", Configuration.getConfig().get("paypalSubscribeNotifyUrl"));
		
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("Username: " + profile.getUsername() + "\r\n");
			sb.append("County:   " + county + "\r\n");
			sb.append("Zips:     " + zips + "\r\n");
			sb.append("Month:    " + month + "\r\n");
			sb.append("Total:    " + total + "\r\n");

			sb.append("avgTotalEmpCount:  " + Integer.toString(new Integer((int)(Float.valueOf(total)/Configuration.LEAD_COST))) + "\r\n");
			sb.append("subscriptionPrice: " + new Float(subscriptionPrice) + "\r\n");
			sb.append("salesTax:          " + new Float(salesTax) + "\r\n");
			sb.append("totalAmount:       " + new Float(subscriptionPrice) + new Float(salesTax) + "\r\n");
			sb.append("leadcost:          " + Configuration.LEAD_COST + "\r\n");

			SendMailUtility.sendMail("KLJ1962@GMAIL.COM", "PayPal subscription details", sb.toString());
			SendMailUtility.sendMail("kevin@firstaffiliated.com", "PayPal subscription details", sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		model.setViewName("SubscriptionPaymentDetailsWithZip");

		return model;

	}
	
}
