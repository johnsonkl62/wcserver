package com.synesis.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.service.solicit.SolicitService;

import sun.misc.BASE64Decoder;

@Controller
public class SolicitController {

	@Autowired
	private SolicitService solicitService;
	
	public void unSubscribeSolicitEmail() {
		
	}
	
	@RequestMapping(value = { "/unSubscribe" }, method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView unSubscribeSolicitEmail(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		boolean isSuccuss = false;
		
		String encodedAgencyAndEmail = request.getParameter("token");
		String decodedAgencyAndEmail = null;
		try {
			
			if(!StringUtils.isEmpty(encodedAgencyAndEmail)) {
				decodedAgencyAndEmail = new String(new BASE64Decoder().decodeBuffer(encodedAgencyAndEmail));
				String[] agencyEmail = decodedAgencyAndEmail.split("\\|");
				if(agencyEmail!=null && agencyEmail.length==2) {
					solicitService.unsubscribeEmail(agencyEmail[0],agencyEmail[1]);
					isSuccuss = true;
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(isSuccuss) {
			model.addObject("title", "You have unsubscribed");
			model.addObject("message", "We're sorry to see you go!");
		} else {
			model.addObject("title", "Verification Failed");
			model.addObject("message", "Something went wrong in our side during unsubscribe");
		}
		
		model.setViewName("message");
		
		return model;
	}

	
}
