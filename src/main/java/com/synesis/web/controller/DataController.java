package com.synesis.web.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlException;
import org.docx4j.model.fields.merge.DataFieldName;
import org.docx4j.model.fields.merge.MailMerger.OutputField;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.io.Files;
import com.synesis.agents.WCMailMerge;
import com.synesis.beans.ProfileBean;
import com.synesis.employer.Employer;
import com.synesis.service.carriers.CarrierService;
import com.synesis.service.codes.CodesService;
import com.synesis.service.employers.EmployerService;
import com.synesis.service.rates.RatesService;
import com.synesis.service.users.UserService;
import com.synesis.utility.AppUtility;
import com.synesis.utility.LogUtil;

@Controller
public class DataController {

	private static final String BASE = "/tmp/tomcat/";
	private static DecimalFormat df = new DecimalFormat("0.00");
	private static SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd"); 	
	public static final String ASC = "Ascending";
	public static final String DSC = "Descending";
	
	@Autowired
	private EmployerService employerService;
	
	@Autowired
	private RatesService ratesService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CarrierService carrierService;
	
	@Autowired
	private CodesService codesService;
	
	private XWPFDocument document;
	private int i =0 , j = 0;
	
	@RequestMapping(value = "/WCData", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView defaultPage(HttpServletRequest request, HttpServletResponse response) throws IOException {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			request.getSession().setAttribute("TargetPage", "/");
			model.addObject("page", "WCData");
			model.setViewName("WCLogin");
			return model;
		}
		
		
		
		String username = String.valueOf(session.getAttribute("username"));
		LogUtil.log("Username: " + username);



		String option = request.getParameter("option");
		if (option == null || option == "") {
			try {
			executeSearch(request, username);
			model.setViewName("WCResults");
			return model;
			} catch (Exception e ) {
				e.printStackTrace();
				model.setViewName("WCSearch");
				return model;
			}
		} else if ("download".equalsIgnoreCase(option)) {

			String format = request.getParameter("format");

			@SuppressWarnings("unchecked")
			ArrayList<Employer> result = (ArrayList<Employer>)request.getSession().getAttribute("employers");
			if ("excel".equals(format)) {
				String csvName = createCSVFile(request, result);
				String xlsxName = createExcelFile(csvName);

				File f = new File(BASE + csvName);
				int byteSize = (int) f.length();
				FileInputStream ifs = new FileInputStream(f);

				String contentType = "application/vnd.ms-excel";

				response.reset();
				response.setBufferSize(byteSize);
				response.setContentType(contentType);
				response.setHeader("Content-Length", String.valueOf(byteSize));
				response.setHeader("Content-Disposition", "attachment; filename=\"" + csvName + "\"");

				try {
					IOUtils.copy(ifs, response.getOutputStream());
				} finally {
					ifs.close();
					f.delete();

					File cf = new File(csvName);
					cf.delete();
				}
				
			} else if ("word".equals(format)) {
				downloadZippedLetters(request, response, username, result);
			} else if ("combine".equals(format)) {
				downloadCombinedLetters(request, response, username, result);
			} else if ("labels".equals(format) || "envelopes".equals(format)) {
				downloadLabelsInDoc(request, response, username, result, format);
			}
		}
	
		return model;

	}
	
	
	@RequestMapping(value = "/WCUpdateCode", method =  RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Employer updateCode(HttpServletRequest request, HttpServletResponse response) throws IOException {

		
		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			request.getSession().setAttribute("TargetPage", "/");
			return null;
		}
		
		
		
		String username = String.valueOf(session.getAttribute("username"));
		LogUtil.log("Username: " + username);


		if(request.getParameter("employerId")!=null && !request.getParameter("employerId").isEmpty() 
				&& request.getParameter("code")!=null && !request.getParameter("code").isEmpty()) {
			
			String code = request.getParameter("code");
			String employerId = request.getParameter("employerId");
			
			employerService.updateEmployer(employerId, code);
			
			
			Employer employer = employerService.findOne(employerId);
			Map<String,String> map  = carrierService.getAllCarriersByName();
			ProfileBean profile = userService.getUserProfile(String.valueOf(request.getSession().getAttribute("username")));
			
			List<String> naics = new ArrayList<String>();
			String carrier = (employer.getCarrier() == null ? null : employer.getCarrier().trim());
			if(map.containsKey(carrier)) {
				String naic = map.get(carrier);
				if(naic!=null && naic!="") {
					naics.add(naic);
				}
			}
			
			Map<String, Double> rates = ratesService.getAllRatesByDate(naics, profile.getCarrier1(), profile.getCarrier2(),profile.getCarrier3());
			
			if(map.containsKey(carrier)) {
				String naic = map.get(carrier);

				if(naic!=null && naic!="") {
					
					String carrier1 = profile.getCarriersBean1().getNaic();
					String carrier2 = profile.getCarriersBean2().getNaic();
					String carrier3 = profile.getCarriersBean3().getNaic();

					employer.getData().setRate(rates.get(naic+"_"+code)); 
					employer.getData().setCompRate(rates.get(carrier1 + "_" + code),0);
					employer.getData().setCompRate(rates.get(carrier2 + "_" + code),1);
					employer.getData().setCompRate(rates.get(carrier3 + "_" + code),2);
					
				}
			}
		
			
//			SendMailUtility.emailReport("Employer Code updated for Employer Id: "+employerId +" with Code as :"+code,
//					"Employer Code Updated by " + username);
			
			return employer;
		}
		
		return null;

	}
	
	
	@RequestMapping(value="/downloadFile", method=RequestMethod.POST)
	public ResponseEntity<byte[]> downloadFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		String option = request.getParameter("option");
		String username = request.getRemoteUser();
		LogUtil.log("Username1: " + username);

		String format = request.getParameter("format");

		@SuppressWarnings("unchecked")
		ArrayList<Employer> result = (ArrayList<Employer>)request.getSession().getAttribute("employers");
		if ("csv".equals(format)) {
			String csvName = createCSVFile(request, result);

			File f = new File(BASE + csvName);
			int byteSize = (int) f.length();
			FileInputStream ifs = new FileInputStream(f);

			String contentType = "application/vnd.ms-excel";

			response.reset();
			response.setBufferSize(byteSize);
			response.setContentType(contentType);
			response.setHeader("Content-Length", String.valueOf(byteSize));
			response.setHeader("Content-Disposition", "attachment; filename=\"" + csvName + "\"");

			try {
				IOUtils.copy(ifs, response.getOutputStream());
			} finally {
				ifs.close();
				f.delete();

				File cf = new File(csvName);
				cf.delete();
			}
			
			response.getOutputStream().flush();			
		} if ("excel".equals(format)) {
			String xlsxName = createExcelFile(request,result);

			File f = new File(BASE + xlsxName);
			int byteSize = (int) f.length();
			FileInputStream ifs = new FileInputStream(f);

			String contentType = "application/vnd.ms-excel";

			response.reset();
			response.setBufferSize(byteSize);
			response.setContentType(contentType);
			response.setHeader("Content-Length", String.valueOf(byteSize));
			response.setHeader("Content-Disposition", "attachment; filename=\"" + xlsxName + "\"");

			try {
				IOUtils.copy(ifs, response.getOutputStream());
			} finally {
				ifs.close();
				f.delete();

				File cf = new File(xlsxName);
				cf.delete();
			}
			
			response.getOutputStream().flush();			
		}else if ("word".equals(format)) {
			downloadZippedLetters(request, response, username, result);
		} else if ("combine".equals(format)) {
			downloadCombinedLetters(request, response, username, result);
		} else if ("labels".equals(format) || "envelopes".equals(format)) {
			downloadLabelsInDoc(request, response, username, result, format);
		}
		
	    return null;
		
	}
	
	
	private String createExcelFile(HttpServletRequest request,
			ArrayList<Employer> result) {
		
		long ms = Calendar.getInstance().getTimeInMillis();
		String xlsxFile = request.getRemoteUser() + "-" + ms + ".xlsx";		

	    try {
	        XSSFWorkbook workBook = new XSSFWorkbook();
//	        HSSFWorkbook workBook = new HSSFWorkbook();
	        XSSFSheet sheet = workBook.createSheet("sheet1");
	        int RowNum=0;
	        
	        XSSFDataFormat dataFormat = workBook.createDataFormat();
	        
	        XSSFCellStyle redStylePlusAccounting = workBook.createCellStyle();
	        XSSFFont redFont=workBook.createFont();
            redFont.setColor(HSSFFont.COLOR_RED);
            redStylePlusAccounting.setFont(redFont);
            redStylePlusAccounting.setDataFormat(dataFormat.getFormat("_($* #,##0.00_);_($* (#,##0.00);_($* \"-\"??_);_(@_)"));
	        
            XSSFCellStyle redStylePlusPercentage = workBook.createCellStyle();
            redStylePlusPercentage.setFont(redFont);
            redStylePlusPercentage.setDataFormat(dataFormat.getFormat("0.00%"));
	        
            
            XSSFCellStyle greenStylePlusAccounting = workBook.createCellStyle();
            XSSFFont greenFont=workBook.createFont();
            greenFont.setColor(HSSFColor.GREEN.index);
            greenStylePlusAccounting.setFont(greenFont);
            greenStylePlusAccounting.setDataFormat(dataFormat.getFormat("_($* #,##0.00_);_($* (#,##0.00);_($* \"-\"??_);_(@_)"));
            
            XSSFCellStyle greenStylePlusPercentage = workBook.createCellStyle();
            greenStylePlusPercentage.setFont(greenFont);
            greenStylePlusPercentage.setDataFormat(dataFormat.getFormat("0.00%"));
            
            
            XSSFCellStyle boldStyle = workBook.createCellStyle();
            XSSFFont boldFont = workBook.createFont();
            boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            boldStyle.setFont(boldFont);    
            
            CellStyle currencyStyle = workBook.createCellStyle();
            currencyStyle.setDataFormat(dataFormat.getFormat("_($* #,##0.00_);_($* (#,##0.00);_($* \"-\"??_);_(@_)"));

            
            CellStyle percentageStyle = workBook.createCellStyle();
            percentageStyle.setDataFormat(dataFormat.getFormat("0.00%"));
            
            
            XSSFCellStyle dateStyle = workBook.createCellStyle();
            dateStyle.setDataFormat(dataFormat.getFormat("yyyy-mm-dd"));
            
            Row headerRow=sheet.createRow(RowNum)
            		;
            for(int i = 0; i < FILE_HEADER.length; i++) {
            	headerRow.createCell(i).setCellValue(StringEscapeUtils.escapeXml11((String) FILE_HEADER[i]));
            	headerRow.getCell(i).setCellStyle(boldStyle);
            }
            
            RowNum++;
            
            for (Employer employer : result) {
//            	LogUtil.log(employer);
				String s = request.getParameter("n" + employer.getKey().getFn());
				if (s != null && s.equalsIgnoreCase(employer.getKey().getFn().toString())) {
					try {
						
						Row currentRow=sheet.createRow(RowNum);

						currentRow.createCell(0).setCellValue(StringEscapeUtils.escapeXml11(employer.getKey().getName()));
						currentRow.createCell(1).setCellValue(StringEscapeUtils.escapeXml11(employer.getCarrier()));
						currentRow.createCell(2).setCellValue(StringEscapeUtils.escapeXml11(employer.getBase().getCity()));
						currentRow.createCell(3).setCellValue(StringEscapeUtils.escapeXml11(employer.getDescription()));
						currentRow.createCell(4).setCellValue(StringEscapeUtils.escapeXml11(employer.getData().getMm()));
						
						currentRow.createCell(5).setCellValue(employer.getData().getXdate());
						currentRow.getCell(5).setCellStyle(dateStyle);
						
						currentRow.createCell(6).setCellValue(Double.valueOf(df.format(employer.getData().getRate())));
						
						currentRow.createCell(7).setCellValue(Double.valueOf(df.format(employer.getData().getCompRate(0))));
						
						currentRow.createCell(8).setCellValue(employer.getData().getPct(0));
						
						if(employer.getData().getRate() > employer.getData().getComp1()) {
							currentRow.getCell(7).setCellStyle(greenStylePlusAccounting);
							currentRow.getCell(8).setCellStyle(greenStylePlusPercentage);
						} else if(employer.getData().getRate() < employer.getData().getComp1()) {
							currentRow.getCell(7).setCellStyle(redStylePlusAccounting);
							currentRow.getCell(8).setCellStyle(redStylePlusPercentage);
						}
						
						currentRow.createCell(9).setCellValue(Double.valueOf(df.format(employer.getData().getCompRate(1))));
						currentRow.getCell(9).setCellStyle(currencyStyle);
						
						currentRow.createCell(10).setCellValue(employer.getData().getPct(1));
						currentRow.getCell(10).setCellStyle(percentageStyle);
						
						if(employer.getData().getRate() > employer.getData().getComp2()) {
							currentRow.getCell(9).setCellStyle(greenStylePlusAccounting);
							currentRow.getCell(10).setCellStyle(greenStylePlusPercentage);
						} else if(employer.getData().getRate() < employer.getData().getComp2()) {
							currentRow.getCell(9).setCellStyle(redStylePlusAccounting);
							currentRow.getCell(10).setCellStyle(redStylePlusPercentage);
						}
						
						currentRow.createCell(11).setCellValue(Double.valueOf(df.format(employer.getData().getCompRate(2))));
						currentRow.getCell(11).setCellStyle(currencyStyle);
						
						currentRow.createCell(12).setCellValue(employer.getData().getPct(2));
						currentRow.getCell(12).setCellStyle(percentageStyle);
						
						if(employer.getData().getRate() > employer.getData().getComp3()) {
							currentRow.getCell(11).setCellStyle(greenStylePlusAccounting);
							currentRow.getCell(12).setCellStyle(greenStylePlusPercentage);
						} else if(employer.getData().getRate() < employer.getData().getComp3()) {
							currentRow.getCell(11).setCellStyle(redStylePlusAccounting);
							currentRow.getCell(12).setCellStyle(redStylePlusPercentage);
						}
						
						currentRow.createCell(13).setCellValue(StringEscapeUtils.escapeXml11(employer.getData().getCode()));
						currentRow.createCell(14).setCellValue(StringEscapeUtils.escapeXml11(employer.getKey().getFn().toString()));
						currentRow.createCell(15).setCellValue(StringEscapeUtils.escapeXml11(employer.getKey().getCounty()));
						currentRow.createCell(16).setCellValue(StringEscapeUtils.escapeXml11(employer.getBase().getAddress()));
						currentRow.createCell(17).setCellValue(StringEscapeUtils.escapeXml11(employer.getBase().getZip()));
						
						
						RowNum++;
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
            
            for(int i = 0; i < FILE_HEADER.length; i++) {
            	sheet.autoSizeColumn(i);
            }

	        FileOutputStream fileOutputStream =  new FileOutputStream(BASE + xlsxFile);
	        workBook.write(fileOutputStream);
	        fileOutputStream.close();
	        workBook.close();
	        LogUtil.log("Done");
	    } catch (Exception ex) {
	        LogUtil.log(ex.getMessage()+"Exception in try");
	    }
	    return xlsxFile;
	}

	private void downloadLabelsInDoc(HttpServletRequest request, HttpServletResponse response, String username, ArrayList<Employer> result, String format) {
		long ms = Calendar.getInstance().getTimeInMillis();
		
		String file = "labels".equals(format)?"MailMerge.docx":"Envelopes.docx";
		int size = "labels".equals(format)?30:1;
		
		String tmplFileName = getClass().getClassLoader().getResource(file).getFile();
		String resultFileName = format+"-" + username + "-" + ms + ".docx";

		try {
			
			File wordTemplate = new File(tmplFileName);
			File f = new File(BASE + resultFileName);
			
			
			 WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(wordTemplate);
			 List<Map<DataFieldName, String>> data = new ArrayList<Map<DataFieldName, String>>();
			 if(result!=null && !result.isEmpty()) {
				 
				 for(Employer employer : result) {
					 
					 String s = request.getParameter("n" + employer.getKey().getFn());
						if (s != null && s.equalsIgnoreCase(employer.getKey().getFn().toString())) {
							Map<DataFieldName, String> map = new HashMap<DataFieldName, String>();
							map.put(new DataFieldName("First_Name"), employer.getKey().getName());
							map.put(new DataFieldName("Address_Line_1"), employer.getBase().getAddress());
							map.put(new DataFieldName("City"), employer.getBase().getCity()+",");
							map.put(new DataFieldName("State"), employer.getBase().getState());
							map.put(new DataFieldName("ZIP_Code"), employer.getBase().getZip());
							data.add(map);
						}
				 }
				
				 if(data.size()>size) {
					 
					 List<List<Map<DataFieldName, String>>> list = AppUtility.chopped(data, size); 
					 List<File> filesToMerge = new ArrayList<File>();
					 
					 int j = 0;
					 for(List<Map<DataFieldName, String>> list2 : list) {
						 
						 String dir = System.getProperty("java.io.tmpdir");
						 
						 File tmp = new File(dir+File.separator+username+j+".docx");
						 
						 wordMLPackage = WordprocessingMLPackage.load(
								 new java.io.File(tmplFileName));
						 
						 org.docx4j.model.fields.merge.MailMerger.setMERGEFIELDInOutput(OutputField.KEEP_MERGEFIELD);
						 
						 org.docx4j.model.fields.merge.MailMergerWithNext.performLabelMerge(wordMLPackage, list2);
						 
						 wordMLPackage.save(tmp);
						 filesToMerge.add(tmp);
						 j++;
					 }
					 
					 mergeDocx(response, filesToMerge, f);
					 
				 } else {
					 
					 
					 org.docx4j.model.fields.merge.MailMerger.setMERGEFIELDInOutput(OutputField.KEEP_MERGEFIELD);
					 
					 org.docx4j.model.fields.merge.MailMergerWithNext.performLabelMerge(wordMLPackage, data);
					 
					 wordMLPackage.save(f);
					 
					 
				 }
				 
			 }
			 
			processResult(response, resultFileName, f);
			
		} catch (Docx4JException e1) {
			e1.printStackTrace();
		} 
	}

	
	public void mergeDocx(HttpServletResponse response, List<File> filesToMerge, File dest){

		
	    try {
	    	document = new XWPFDocument();
	    	i =0 ; j = 0;
	    	
	    	for(File file : filesToMerge) {
	    		XWPFDocument doc1 = new XWPFDocument(new FileInputStream(file));
	    		
	    		if(j==0) {
	    			document = doc1;
	    			for(IBodyElement e : doc1.getBodyElements()){
	    				if(e instanceof XWPFTable){
	    					j++;
	    				}
	    			}
	    			continue;
	    		} 
	    		
	    		
	    		parseElement(doc1);
	    		
	    	}
	    	
//	        parseStyle(doc1,doc2);
	        OutputStream out = new FileOutputStream(dest);
	        document.write(out);
	        out.close();

	        
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }

	}
	
	
	private void parseElement( XWPFDocument doc1){

	    for(IBodyElement e : doc1.getBodyElements()){
	        if(e instanceof XWPFParagraph){
	            XWPFParagraph p = (XWPFParagraph) e;
	            if(p.getCTP().getPPr()!=null && p.getCTP().getPPr().getSectPr()!=null){
	                continue;
	            }else{
	                document.createParagraph();
	                document.setParagraph(p, i);
	                i++;
	            }
	        }else if(e instanceof XWPFTable){
	            XWPFTable t = (XWPFTable)e;
	            document.createTable();
	            document.setTable(j, t);
	            j++;
	        }

	    }

	}
	
	

	private void parseStyle(XWPFDocument doc1, XWPFDocument doc2){
	    try {
	        CTStyles c1 = doc1.getStyle();
	        CTStyles c2 =  doc2.getStyle();
	        int size1 = c1.getStyleArray().length;
	        int size2 = c2.getStyleArray().length;
	        for(int i = 0; i<size2; i++ ){
	            c1.addNewStyle();
	            c1.setStyleArray(size1+i, c2.getStyleArray(i));
	        }


	        document.createStyles().setStyles(c1);
	    } catch (XmlException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	private void processResult(HttpServletResponse response,
			String resultFileName, File f) {
		try {
			
			int byteSize = (int) f.length();
			FileInputStream ifs = new FileInputStream(f);

			response.reset();
			response.setBufferSize(byteSize);
			response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
			response.setHeader("Content-Length", String.valueOf(byteSize));
			response.setHeader("Content-Disposition", "attachment; filename=\"" + resultFileName + "\"");

			try {
				// Open streams.
				IOUtils.copy(ifs, response.getOutputStream());
			} finally {
				// Gently close streams.
				ifs.close();
				f.delete();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private void downloadLabels(HttpServletRequest request, HttpServletResponse response, String username, ArrayList<Employer> result) {
		long ms = Calendar.getInstance().getTimeInMillis();
		String tmplFileName = "Labels" + ".docx";
		String resultFileName = "Labels-" + username + "-" + ms + ".docx";

		try {
			//
			//  Generate CSV file for mail merge
			//
			String csvFileName = createLabelCSVFile(request, result);

			//
			//  Merge to template
			//
			File wordTemplate = new File(BASE + tmplFileName);
			File excelFile = new File(BASE + csvFileName);
			LogUtil.log("Merging data from " + tmplFileName + " and " + csvFileName + " into " + resultFileName);

			try {
			    WCMailMerge wcmm = new WCMailMerge();
			    wcmm.mergeLabels(wordTemplate, excelFile, resultFileName);
			} catch ( Exception e ) {
				e.printStackTrace();
			}

			
			try {
				File f = new File(BASE + resultFileName);
				int byteSize = (int) f.length();
				FileInputStream ifs = new FileInputStream(f);

				response.reset();
				response.setBufferSize(byteSize);
				response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
				response.setHeader("Content-Length", String.valueOf(byteSize));
				response.setHeader("Content-Disposition", "attachment; filename=\"" + resultFileName + "\"");

				try {
					// Open streams.
					IOUtils.copy(ifs, response.getOutputStream());
				} finally {
					// Gently close streams.
					ifs.close();
					f.delete();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void downloadCombinedLetters(HttpServletRequest request, HttpServletResponse response, String username,
			ArrayList<Employer> result) {

		long ms = Calendar.getInstance().getTimeInMillis();
		String tmplFileName = "Template-" + username + ".docx";
		String resultFileName = "Result-" + username + "-" + ms + ".docx";
		
		File wordTemplate = new File(BASE + tmplFileName);
		
		try {
			
			ProfileBean profile = userService.getUserProfile(username);
			if(profile!=null && profile.getTemplateBean()!=null 
					&& profile.getTemplateBean().getTemplate()!=null) {
				Files.write(profile.getTemplateBean().getTemplate(), wordTemplate);
			}
			
			//
			//  Generate CSV file for mail merge
			//
			String csvFileName = createCSVFile(request, result);
			
			//
			//  Merge to template
			//
//			File wordTemplate = new File(BASE + tmplFileName);
			if ( ! wordTemplate.exists() ) {
				wordTemplate = new File(getClass().getClassLoader().getResource("Template-guest.docx").getFile());
			}
			File excelFile = new File(BASE + csvFileName);
			LogUtil.log("Merging data from " + tmplFileName + " and " + csvFileName + " into " + resultFileName);

			try {
			    WCMailMerge wcmm = new WCMailMerge();
			    wcmm.merge(wordTemplate, excelFile, resultFileName, profile.getUsername());
			} catch ( Exception e ) {
				e.printStackTrace();
			}

			
			try {
				File f = new File(BASE + resultFileName);
				int byteSize = (int) f.length();
				FileInputStream ifs = new FileInputStream(f);

				response.reset();
				response.setBufferSize(byteSize);
				response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
				response.setHeader("Content-Length", String.valueOf(byteSize));
				response.setHeader("Content-Disposition", "attachment; filename=\"" + resultFileName + "\"");

				try {
					// Open streams.
					IOUtils.copy(ifs, response.getOutputStream());
				} finally {
					// Gently close streams.
					ifs.close();
					f.delete();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void downloadZippedLetters(HttpServletRequest request, HttpServletResponse response, String username,
			ArrayList<Employer> result) throws FileNotFoundException, IOException {
		ArrayList<Employer> selectList = new ArrayList<Employer>();
		for (Employer emp : result) {
			String s = request.getParameter("n" + emp.getKey().getFn());
			if (s != null && s.equalsIgnoreCase(emp.getKey().getFn().toString())) {
				selectList.add(emp);
			}
		}

		// output file

		String zipFile = "wc-" + (Calendar.getInstance().getTimeInMillis()) + ".zip";
		// ZipOutputStream zipFile = new ZipOutputStream(new
		// FileOutputStream(zipFilename));
		ArrayList<String> filenames = new ArrayList<String>();
		for (Employer emp : selectList) {
			if (emp.getData().getPct(0) > 0.0) {
				try {
					String templateName = "Template-" + username + ".docx";
					File f = new File(BASE + templateName);
					if (!f.exists()) {
						templateName = "Template-guest.docx";
					}
					FileInputStream fis = new FileInputStream(BASE + templateName);
					XWPFDocument xDoc = new XWPFDocument(OPCPackage.open(fis));

					try {
						String filename = validFilename(emp.getKey().getName()) + ".docx";

						replace(xDoc, "WC_CARRIER", emp.getCarrier());
						replace(xDoc, "WC_CITY", emp.getBase().getCity());
						replace(xDoc, "WC_DESC", emp.getDescription());
						replace(xDoc, "WC_EMPLOYER", emp.getKey().getName());
						//  Obsolete - for backward compatibility
						replace(xDoc, "WC_EXPCT", df.format(emp.getData().getPct(0) * 100.0));
						replace(xDoc, "WC_EXCH", df.format(emp.getData().getCompRate(0)));
						replace(xDoc, "WC_FLPCT", df.format(emp.getData().getPct(1) * 100.0));
						replace(xDoc, "WC_FLAG", df.format(emp.getData().getCompRate(1)));
						//  end backward compatibility
						replace(xDoc, "WC_C1PCT", df.format(emp.getData().getPct(0) * 100.0));
						replace(xDoc, "WC_CMP1", df.format(emp.getData().getCompRate(0)));
						replace(xDoc, "WC_C2PCT", df.format(emp.getData().getPct(1) * 100.0));
						replace(xDoc, "WC_CMP2", df.format(emp.getData().getCompRate(1)));
						try {
							replace(xDoc, "WC_NPCT", df.format(emp.getData().getCompRate(2)));
							replace(xDoc, "WC_NY", df.format(emp.getData().getCompRate(2)));
							replace(xDoc, "WC_C3PCT", df.format(emp.getData().getCompRate(2)));
							replace(xDoc, "WC_CMP3", df.format(emp.getData().getCompRate(2)));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						replace(xDoc, "WC_GCC", emp.getData().getCode());
						replace(xDoc, "WC_MM", emp.getData().getMm());
						replace(xDoc, "WC_RATE", df.format(emp.getData().getRate()));
						replace(xDoc, "WC_STREET", emp.getBase().getAddress());
						replace(xDoc, "WC_XDATE", dt.format(emp.getData().getXdate()));
						replace(xDoc, "WC_ZIP", emp.getBase().getZip());

						FileOutputStream fos = new FileOutputStream(new File(filename));
						xDoc.write(fos);
						fos.close();
						filenames.add(filename);

					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						xDoc.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		try {
			// create byte buffer
			byte[] buffer = new byte[1024];

			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);

			for (String filename : filenames) {

				File srcFile = new File(filename);
				FileInputStream fis = new FileInputStream(srcFile);

				// begin writing a new ZIP entry, positions the
				// stream to the start of the entry data
				zos.putNextEntry(new ZipEntry(srcFile.getName()));

				int length;
				while ((length = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, length);
				}
				zos.closeEntry();

				// close the InputStream
				fis.close();
			}
			// close the ZipOutputStream
			zos.close();

			for (String filename : filenames) {
				File srcFile = new File(filename);
				srcFile.delete();
			}

		} catch (IOException ioe) {
			LogUtil.log("Error creating zip file: " + ioe);
		}

		// Init servlet response.
		File f = new File(zipFile);
		int byteSize = (int) f.length();
		FileInputStream ifs = new FileInputStream(f);

		response.reset();
		response.setBufferSize(byteSize);
		response.setContentType("application/zip");
		response.setHeader("Content-Length", String.valueOf(byteSize));
		response.setHeader("Content-Disposition", "attachment; filename=\"" + zipFile + "\"");

		try {
			// Open streams.
			IOUtils.copy(ifs, response.getOutputStream());
		} finally {
			// Gently close streams.
			ifs.close();
			f.delete();
		}
	}

    private static final String NEW_LINE_SEPARATOR = "\n";
//  private static final Object [] FILE_HEADER = {"WC_EMPLOYER","WC_CARRIER","WC_CITY","WC_DESC","WC_MM","WC_XDATE","WC_RATE","WC_EXCH","WC_EXPCT","WC_FLAG","WC_FLPCT","WC_NY","WC_NPCT","WC_GCC","WC_FN","WC_COUNTY","WC_STREET","WC_ZIP"};
    private static final Object [] FILE_HEADER = {"WC_EMPLOYER","WC_CARRIER","WC_CITY","WC_DESC","WC_MM","WC_XDATE","WC_RATE","WC_CMP1","WC_C1PCT","WC_CMP2","WC_C2PCT","WC_CMP3","WC_C3PCT","WC_GCC","WC_FN","WC_COUNTY","WC_STREET","WC_ZIP"};
    private static final Object [] LABEL_HEADER = {"[NAME]","[ADDRESS]","[CITY]","[STATE]","[ZIP]"};
	
	private String createCSVFile(HttpServletRequest request, ArrayList<Employer> result) throws IOException {
		long ms = Calendar.getInstance().getTimeInMillis();
		
		String csvFileName = request.getRemoteUser() + "-" + ms + ".csv";

		FileWriter fileWriter = null;
		CSVPrinter csvFilePrinter = null;

		// Create the CSVFormat object with "\n" as a record delimiter
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);

		try {
			// initialize FileWriter object
			fileWriter = new FileWriter(BASE + csvFileName);

			// initialize CSVPrinter object
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

			// Create CSV file header
			csvFilePrinter.printRecord(FILE_HEADER);

			// Write a new student object list to the CSV file
			for (Employer employer : result) {
				String s = request.getParameter("n" + employer.getKey().getFn());
				if (s != null && s.equalsIgnoreCase(employer.getKey().getFn().toString())) {
					try {
						List<String> dataRecord = new ArrayList<String>();

						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getKey().getName()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getCarrier()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getCity()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getDescription()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getData().getMm()));
						dataRecord.add(StringEscapeUtils.escapeXml11(dt.format(employer.getData().getXdate().getTime())));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getRate())));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getCompRate(0))));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getPct(0)*100)));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getCompRate(1))));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getPct(1)*100)));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getCompRate(2))));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getPct(2)*100)));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getData().getCode()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getKey().getFn().toString()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getKey().getCounty()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getAddress()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getZip()));
						csvFilePrinter.printRecord(dataRecord);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			LogUtil.log("CSV file was created successfully !!!");

		} catch (Exception e) {
			LogUtil.log("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
				csvFilePrinter.close();
			} catch (IOException e) {
				LogUtil.log("Error while flushing/closing fileWriter/csvPrinter !!!");
				e.printStackTrace();
			}
		}
		return csvFileName;
	}

	private String createLabelCSVFile(HttpServletRequest request, ArrayList<Employer> result) throws IOException {
		long ms = Calendar.getInstance().getTimeInMillis();
		String csvFileName = request.getRemoteUser() + "-" + ms + ".csv";

		FileWriter fileWriter = null;

		CSVPrinter csvFilePrinter = null;

		// Create the CSVFormat object with "\n" as a record delimiter
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);

		try {
			// initialize FileWriter object
			fileWriter = new FileWriter(BASE + csvFileName);

			// initialize CSVPrinter object
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

			// Create CSV file header
			csvFilePrinter.printRecord(LABEL_HEADER);

			// Write a new student object list to the CSV file
			for (Employer employer : result) {
				String s = request.getParameter("n" + employer.getKey().getFn());
				if (s != null && s.equalsIgnoreCase(employer.getKey().getFn().toString())) {
					try {
						List<String> dataRecord = new ArrayList<String>();

						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getKey().getName()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getAddress()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getCity()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getState()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getZip()));
						csvFilePrinter.printRecord(dataRecord);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			LogUtil.log("CSV file was created successfully !!!");

		} catch (Exception e) {
			LogUtil.log("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
				csvFilePrinter.close();
			} catch (IOException e) {
				LogUtil.log("Error while flushing/closing fileWriter/csvPrinter !!!");
				e.printStackTrace();
			}
		}
		return csvFileName;
	}

	private String createExcelFile(String csvFile) throws IOException {
    	String xlsxFile = csvFile.substring(0,csvFile.length()-4)+".xlsx";
	    try {
//	        XSSFWorkbook workBook = new XSSFWorkbook();
	        HSSFWorkbook workBook = new HSSFWorkbook();
	        HSSFSheet sheet = workBook.createSheet("sheet1");
	        String currentLine=null;
	        int RowNum=0;
	        
	        HSSFCellStyle my_style = workBook.createCellStyle();
            /* Create HSSFFont object from the workbook */
            HSSFFont my_font=workBook.createFont();
            /* set the weight of the font */
            my_font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            /* Also make the font color to RED */
            my_font.setColor(HSSFFont.COLOR_RED);
            /* attach the font to the style created earlier */
            my_style.setFont(my_font);
	        
	        BufferedReader br = new BufferedReader(new FileReader(csvFile));
	        while ((currentLine = br.readLine()) != null) {
	            Object str[] = currentLine.split(",");
	            Row currentRow=sheet.createRow(RowNum);
	            RowNum++;
	            for(int i=0;i<str.length;i++){
	            	
	            	Cell cell = currentRow.createCell(i);
	                if (str[i] instanceof String) {
	                    cell.setCellValue((String) str[i]);
	                    cell.setCellStyle(my_style);
	                } else if (str[i] instanceof Integer) {
	                    cell.setCellValue((Integer) str[i]);
	                    cell.setCellStyle(my_style);
	                }
	            }
	        }
	        br.close();

	        FileOutputStream fileOutputStream =  new FileOutputStream(BASE + xlsxFile);
	        workBook.write(fileOutputStream);
	        fileOutputStream.close();
	        workBook.close();
	        LogUtil.log("Done");
	    } catch (Exception ex) {
	        LogUtil.log(ex.getMessage()+"Exception in try");
	    }
	    return xlsxFile;
	}

	private void executeSearch(HttpServletRequest request, String username)
			throws ServletException, IOException {

		String name = request.getParameter("empName");
		boolean nameExact = request.getParameter("cbxEmpNameExact") != null;
		String carriers = request.getParameter("carriers");
		boolean carrierExact = request.getParameter("cbxCarrierExact") != null;
		String gccs = request.getParameter("gccs");
		String descs = request.getParameter("descs");
		String subs[] = request.getParameterValues("subs");
		String street = request.getParameter("street");
		String zip = request.getParameter("zip");

		List<Employer> result = null;
		if ( subs!= null && subs.length>0) {
			result = employerService.getEmployers(name, nameExact, carriers, carrierExact, street, zip, gccs, 
				descs, subs, request.getParameter("minDate"), request.getParameter("maxDate"), username);
		} else if (request.isUserInRole("ROLE_ADMIN") ) {
			String counties = request.getParameter("counties");
			result = employerService.getEmployersForAdminUser(counties, name, nameExact, carriers, carrierExact, street, zip, gccs, 
					descs, subs, request.getParameter("minDate"), request.getParameter("maxDate"), username);
		} else {
			result = new ArrayList<Employer>();
		}
		
		ProfileBean profile = userService.getUserProfile(String.valueOf(request.getSession().getAttribute("username")));
		request.getSession().setAttribute("cr1", profile.getCarriersBean1().getNaic());
		request.getSession().setAttribute("cr2", profile.getCarriersBean2().getNaic());
		request.getSession().setAttribute("cr3", profile.getCarriersBean3().getNaic());
		request.getSession().setAttribute("cr1n", profile.getCarriersBean1().getName());
		request.getSession().setAttribute("cr2n", profile.getCarriersBean2().getName());
		request.getSession().setAttribute("cr3n", profile.getCarriersBean3().getName());
		
		Map<String, String> codes = codesService.getAllCodesWithDescription();
		request.getSession().setAttribute("codes", codes);
		
		Map<String,String> map  = carrierService.getAllCarriersByName();
		
		List<String> naics = new ArrayList<String>();
		for ( Employer e : result ) {
			String carrier = (e.getCarrier() == null ? null : e.getCarrier().trim());
			if(map.containsKey(carrier)) {
				String naic = map.get(carrier);
				if(naic!=null && naic!="") {
					naics.add(naic);
				}
			}
		}
		
		Map<String, Double> rates = ratesService.getAllRatesByDate(naics, profile.getCarrier1(), profile.getCarrier2(),profile.getCarrier3());

		Integer cr1Count = 0;
		Integer cr2Count = 0;
		Integer cr3Count = 0;

		int empCount = 0;
		for ( Employer e : result ) {
			String carrier = (e.getCarrier() == null ? null : e.getCarrier().trim());
			if(map.containsKey(carrier)) {
				String naic = map.get(carrier);

				if(naic!=null && naic!="") {
					String code = (e.getData() == null ? null : e.getData().getCode() == null ? null : e.getData().getCode().trim());
					if ( code == null || code.length() == 0 ) {
						e.getData().setCode("-1");
						e.setDescription("No code specified.  Using average rates.");
						code = "-1";
					}
					String carrier1 = profile.getCarriersBean1().getNaic();
					String carrier2 = profile.getCarriersBean2().getNaic();
					String carrier3 = profile.getCarriersBean3().getNaic();

					e.getData().setRate(rates.get(naic+"_"+code)); 
					e.getData().setCompRate(rates.get(carrier1 + "_" + code),0);
					e.getData().setCompRate(rates.get(carrier2 + "_" + code),1);
					e.getData().setCompRate(rates.get(carrier3 + "_" + code),2);

					if ( naic.equals(carrier1) ) {
						cr1Count++;
					}
					if ( naic.equals(carrier2) ) {
						cr2Count++;
					}
					if ( naic.equals(carrier3) ) {
						cr3Count++;
					}
				}
			}
		}

		if(result!=null) {
			try {
				result = result.stream()
					.filter(a -> (a.getData()!=null && a.getData().getPct(0)!=null))
					.sorted((Employer a, Employer b) -> a.getData().getPct(0).compareTo(b.getData().getPct(0)))
					.collect(Collectors.toList());
				Collections.reverse(result);
				request.getSession().setAttribute("sortedOrder", ASC);
				request.getSession().setAttribute("sortedBy", "pct1");
			} catch ( Exception e ) {
				e.printStackTrace();
			}
		}

		request.getSession().setAttribute("employers", result);
		request.getSession().setAttribute("eCount", new Integer(result.size()));
		request.getSession().setAttribute("cr1Count", cr1Count);
		request.getSession().setAttribute("cr2Count", cr2Count);
		request.getSession().setAttribute("cr3Count", cr3Count);
	}


	private String validFilename(String name) {
		String result = name.replaceAll("/", "_");
		result = result.replaceAll("\\\\", "_");
		result = result.replaceAll(":", "_");
		result = result.replaceAll("\\*", "_");
		result = result.replaceAll("\\?", "_");
		result = result.replaceAll("\"", "_");
		result = result.replaceAll("<", "_");
		result = result.replaceAll(">", "_");
		result = result.replaceAll("\\|", "_");
		return result;
	}

	public static void replace(XWPFDocument doc, String search, String subst) {
		for (XWPFParagraph p : doc.getParagraphs()) {
		    List<XWPFRun> runs = p.getRuns();
		    if (runs != null) {
		        for (XWPFRun r : runs) {
		            String text = r.getText(0);
		            if (text != null && text.contains(search)) {
		                text = text.replace(search, subst);
		                r.setText(text, 0);
		            }
		        }
		    }
		}
		for (XWPFTable tbl : doc.getTables()) {
		   for (XWPFTableRow row : tbl.getRows()) {
		      for (XWPFTableCell cell : row.getTableCells()) {
		         for (XWPFParagraph p : cell.getParagraphs()) {
		            for (XWPFRun r : p.getRuns()) {
		              String text = r.getText(0);
		              if (text.contains(search)) {
		                text = text.replace(search, subst);
		                r.setText(text);
		              }
		            }
		         }
		      }
		   }
		}
	}
	
}
