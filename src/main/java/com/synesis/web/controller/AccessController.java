package com.synesis.web.controller;

import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.synesis.service.access.AccessService;
import com.synesis.service.announcement.AnnouncementService;

@Controller
public class AccessController {

	@Autowired
	private AccessService accessService;
	
	@Autowired
	private AnnouncementService announcementService;
	
	
	@RequestMapping(value = "/WCAccept", method = { RequestMethod.GET,  RequestMethod.POST})
	public ModelAndView defaultPage(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		
		if ( session == null || session.getAttribute("username")==null) {
			model.addObject("page", "");
			model.setViewName("WCLogin");
			return model;
		}
		
		request.getSession().setAttribute("TermsAccepted","true");
		String target = (String)request.getSession().getAttribute("TargetPage");
		request.getSession().removeAttribute("TargetPage");
		
		String sid = request.getHeader("X-FORWARDED-FOR");  
		if (sid == null) {  
			sid = request.getRemoteAddr();  
			if ( sid == null ) {
				sid = request.getParameter("ip");
			}
		}
		
		if(session.getAttribute("username")!=null) {
			accessService.save(String.valueOf(session.getAttribute("username")), sid);
			model.setViewName(target);
		} else {
			model.setViewName("index");
		}
		
		return model;

	}
	
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			 HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		List<String> announcements = announcementService.getAllAnnouncements();
		
		model.addObject("announcements", announcements);
		model.setViewName("WCLogin");

		return model;

	}
	
	@RequestMapping(value = "/WCLogoff", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		Enumeration<String> atts = request.getSession().getAttributeNames();
		while ( atts.hasMoreElements() ) {
			String attribute = atts.nextElement();
			request.getSession().removeAttribute(attribute);
		}
		request.getSession().invalidate();
		
		model.setViewName("index");

		return model;

	}
	
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "Invalid username and password!";
		}

		return error;
	}
	
}
