package com.synesis.jobs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.io.Files;
import com.synesis.agents.WCMailMerge;
import com.synesis.beans.ProfileBean;
import com.synesis.employer.Employer;
import com.synesis.model.Purchase;
import com.synesis.service.carriers.CarrierService;
import com.synesis.service.employers.EmployerService;
import com.synesis.service.preferences.PreferenceService;
import com.synesis.service.purchase.PurchaseService;
import com.synesis.service.rates.RatesService;
import com.synesis.service.users.UserService;
import com.synesis.utility.Configuration;
import com.synesis.utility.LogUtil;
import com.synesis.utility.SendMailUtility;

@Component
public class GenerateLettersJob {

	@Autowired
	private UserService userService;

	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private EmployerService employerService;

	@Autowired
	private CarrierService carrierService;
	
	@Autowired
	private RatesService ratesService;
	
	@Autowired
	private PreferenceService preferenceService;
	
	private static final String BASE = "/tmp/tomcat/";
	private static DecimalFormat df = new DecimalFormat("0.00");
	private static DecimalFormat pf = new DecimalFormat("0.00%");
	private static SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
	private static DateTimeFormatter md = DateTimeFormatter.ofPattern("MM-dd");
	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
	private static final String NEW_LINE_SEPARATOR = "\n";
    private static final Object [] FILE_HEADER = {"WC_EMPLOYER","WC_CARRIER","WC_CITY","WC_DESC","WC_MM","WC_XDATE","WC_RATE","WC_CMP1","WC_C1PCT","WC_CMP2","WC_C2PCT","WC_CMP3","WC_C3PCT","WC_GCC","WC_FN","WC_COUNTY","WC_STREET","WC_ZIP"};
	
	@Transactional
	@Scheduled(cron = "0 53 1 * * MON")   // 2:00 AM each Monday
	public void prepLetterGeneration() {
		LogUtil.log("**********");
		LogUtil.log("Entered prepLetterGeneration ...");
		LogUtil.log("**********");
		
		StringBuffer logBuf = new StringBuffer();
		try {
			List<ProfileBean> allUsers = userService.getAllUsers();
			LogUtil.log(allUsers.toString());
		} catch (Exception e) {
			e.printStackTrace();
			logBuf.append(getStringStackTrace(e));
		}
		Configuration.verifyBaseFolder();
	}

    //  Cron expression = "s m h d wd m y"
	@Transactional
	@Scheduled(cron = "0 0 2 * * MON")   // 2:00 AM each Monday
//	@Scheduled(cron = "0 0 */3 * * *")   // Testing
	public void generateLetters() {
		int daysFromToday = 56;
		int daysInPeriod = 6;

		boolean exceptions = false;

		LogUtil.log("**********");
		LogUtil.log("Entered generateLetters ...");
		LogUtil.log("**********");
		
		Configuration.verifyBaseFolder();

		StringBuffer logBuf = new StringBuffer();
		try {
			// Get current date
			LocalDate now = LocalDate.now();
			LocalDate startDate = now.plusDays(daysFromToday);
			LocalDate endDate = startDate.plusDays(daysInPeriod);

			logBuf.append("Now:        " + LocalDateTime.now().toString() + "\r\n");
			logBuf.append("Start date: " + startDate.toString() + "\r\n");
			logBuf.append("End date:   " + endDate.toString() + "\r\n");

			String dateRangeStartMonth = startDate.getMonth().name().substring(0, 3);
			String dateRangeEndMonth = endDate.getMonth().name().substring(0, 3);

			logBuf.append("Purchase range months:  " + dateRangeStartMonth + ".." + dateRangeEndMonth + "\r\n");

			List<ProfileBean> allUsers = userService.getAllUsers();

			for (ProfileBean profileBean : allUsers) {
				List<Employer> finalList = new ArrayList<Employer>();
				List<Employer> excludedList = new ArrayList<Employer>();
				List<Employer> resultList = null;

				try {
					try {
						logBuf.append("\r\nChecking user " + profileBean.getUsername() + "(" + profileBean.getLastName()
								+ ", " + profileBean.getFirstName() + ")\r\n");

						List<Purchase> purchases = purchaseService.fetchPurchasesByUserByMonth(profileBean.getUsername(),
								dateRangeStartMonth);
						if (!dateRangeStartMonth.equalsIgnoreCase(dateRangeEndMonth)) {
							List<Purchase> endMonthPurchases = purchaseService
									.fetchPurchasesByUserByMonth(profileBean.getUsername(), dateRangeEndMonth);
							purchases.addAll(endMonthPurchases);
						}
						if (purchases == null || purchases.size() == 0) {
							logBuf.append("   No purchases found.\r\n");
							continue;
						} else {
							//  Dump purchases to log
							try {
								for (Purchase purchase : purchases) {
									logBuf.append("   " + purchase.toString() + "\r\n");
								}
							} catch (Exception e) {
								logBuf.append("  EXCEPTION DUMPING PURCHASES \r\n");
								logBuf.append(getStringStackTrace(e));
								exceptions = true;
							}
						}
						resultList = employerService.getAllEmployers(purchases, startDate, endDate, logBuf);

						//  Dump employers to log
						for (Employer e : resultList) {
							try {
								logBuf.append("   Employer:  " + e.toTabbedString() + "\r\n");
							} catch (Exception e1) {
								logBuf.append("   *** Exception ***  Employer: " + e.getKey().getFn());
								logBuf.append(getStringStackTrace(e1));
								exceptions = true;
							}
						}

					} catch (Exception e) {
						logBuf.append("   EXCEPTION GETTING ALL EMPLOYERS \r\n");
						logBuf.append(getStringStackTrace(e));
						exceptions = true;
					}

					logBuf.append("\r\n");

					//  Need carrier information to cross-reference to NAIC.  Employer uses carrier name; rates use NAIC.
					Map<String, String> carriers = carrierService.getAllCarriersByName();

					//  Collect the list of NAICs needed for rate comparisons
					List<String> naics = new ArrayList<String>();
					for (Employer e : resultList) {
						String carrier = (e.getCarrier() == null ? null : e.getCarrier().trim());
						if (carriers.containsKey(carrier)) {
							logBuf.append("   Carrier comparison:  [" + carrier + "]\r\n");
							String naic = carriers.get(carrier);
							if (naic != null && naic != "") {
								logBuf.append("   Adding naic:  [" + naic + "]\r\n");
								naics.add(naic);
							}
						} else {
							logBuf.append("*** Carrier not found in database *** -- " + carrier + "\r\n");
						}
					}

					//  Get all rates for the user's three selected carriers
					Map<String, Double> rates = ratesService.getAllRatesByDate(naics, profileBean.getCarrier1(),profileBean.getCarrier2(), profileBean.getCarrier3());
					logBuf.append("   Retrieved user's three carriers' rates...\r\n");

					Map<String, String> preferredMap = preferenceService.getAllPreferencesByUsername(profileBean.getUsername());
					List<String> allowedCodes = new ArrayList<String>();
					if (preferredMap != null) {
						allowedCodes = preferredMap.keySet().stream().map(s -> String.valueOf(s)).collect(Collectors.toList());
					}
					logBuf.append("   Retrieved user's code preferences...\r\n");

					String carrier1 = profileBean.getCarriersBean1().getNaic();
					String carrier2 = profileBean.getCarriersBean2().getNaic();
					String carrier3 = profileBean.getCarriersBean3().getNaic();

					//  Check each employer to match required preferences
					for ( Employer e : resultList ) {
						logBuf.append("   " + e.getKey().getFn() + ":  ");
						try {
								String carrier = (e.getCarrier() == null ? null : e.getCarrier().trim());
								if (carriers.containsKey(carrier)) {
									String naic = carriers.get(carrier);

									if (naic != null && naic != "") {
										String code = (e.getData().getCode() == null ? null : e.getData().getCode().trim());
										if (code == null || code.length() == 0) {
											e.getData().setCode("-1");
											e.setDescription("No code specified.  Using average rates.");
											code = "-1";
										} else {
											//  Remove leading zeroes for comparison (if any)
											while ( code.charAt(0) == '0' ) {
												code = code.substring(1);
											}
										}
										
										if (rates != null ) {
											e.getData().setRate(rates.get(naic + "_" + code));
											e.getData().setCompRate(rates.get(carrier1 + "_" + code), 0);
											e.getData().setCompRate(rates.get(carrier2 + "_" + code), 1);
											e.getData().setCompRate(rates.get(carrier3 + "_" + code), 2);

											if (rates.get(naic + "_" + code) != null
													&& rates.get(carrier1 + "_" + code) != null
													&& (rates.get(naic + "_" + code) > rates.get(carrier1 + "_" + code))) {
												// Include only specified codes or anything if there are no preferences
												if (allowedCodes == null || allowedCodes.size() == 0 || e.getData().getCode() == null ||
														(e.getData().getCode() != null && allowedCodes.contains(e.getData().getCode()))) {

													finalList.add(e);
												} else {
													excludedList.add(e);
													logBuf.append("   Allowed code check failed\r\n");
												}
											} else {
												excludedList.add(e);
												logBuf.append("   Rate too low\r\n");
											}
										} else {
											excludedList.add(e);
											logBuf.append("   Rate check failed\r\n");
										}
									} else {
										excludedList.add(e);
										logBuf.append("   Naic check failed\r\n");
									}
								} else {
									excludedList.add(e);
									logBuf.append("   Carrier check failed\r\n");
								}
						} catch (Exception e1) {
							logBuf.append("   EXCEPTION prevented processing\r\n");
							logBuf.append(getStringStackTrace(e1));
							exceptions = true;
							excludedList.add(e);
							e1.printStackTrace();
						}
					}

					logBuf.append("   Final list count   : " + finalList.size() + "\r\n");
					logBuf.append("   Excluded list count: " + excludedList.size() + "\r\n");

					if (!finalList.isEmpty()) {
						finalList = finalList.stream().filter(a -> (a.getData() != null && a.getData().getPct(0) != null))
								.sorted((Employer a, Employer b) -> a.getData().getPct(0).compareTo(b.getData().getPct(0)))
								.collect(Collectors.toList());
						Collections.reverse(finalList);
					}
					if (!excludedList.isEmpty()) {
						excludedList = excludedList.stream().filter(a -> (a.getData() != null && a.getData().getPct(0) != null))
								.sorted((Employer a, Employer b) -> a.getData().getPct(0).compareTo(b.getData().getPct(0)))
								.collect(Collectors.toList());
						Collections.reverse(excludedList);
					}
					// Generate Letter and Email
					generateLetterAndSendMail(profileBean, finalList, excludedList, startDate, endDate, exceptions);
					try { Thread.sleep(120000); } catch ( Exception e ) { }

				} catch (Exception e) {
					try {
						logBuf.append(getStringStackTrace(e));
					    SendMailUtility.sendMail("KLJ1962@GMAIL.COM", "kswcrc letter generation log (EXCEPTION)", logBuf.toString());
					} catch (Exception f) {
						f.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logBuf.append("EXCEPTION" + e.getMessage());
			logBuf.append(getStringStackTrace(e));
			exceptions = true;
		} finally {
			try {
				LogUtil.log("Letter Generation Log: " + logBuf.toString());
			    SendMailUtility.sendMail("KLJ1962@GMAIL.COM", "kswcrc letter generation log", logBuf.toString());
			} catch (Exception f) {
				f.printStackTrace();
				logBuf.append(getStringStackTrace(f));
				exceptions = true;
			}
		}
	}

	//  Cron expression = "s m h d wd m y"
	@Transactional
	@Scheduled(cron = "0 0 3 * * MON")   // 2:00 AM each Monday
	public void generateAdminLetters() {
		int daysFromToday = 56;
		int daysInPeriod = 7;

		boolean exceptions = false;
		
		StringBuffer logBuf = new StringBuffer();
		try {
			// Get current date
			LocalDate now = LocalDate.now();
			LocalDate startDate = now.plusDays(daysFromToday);
			LocalDate endDate = startDate.plusDays(daysInPeriod);

			logBuf.append("Admin Letters:\r\n");
			logBuf.append("Now:        " + now.toString() + "\r\n");
			logBuf.append("Start date: " + startDate.toString() + "\r\n");
			logBuf.append("End date:   " + endDate.toString() + "\r\n");

			List<ProfileBean> adminUsers = userService.getAllAdmins();

			for (ProfileBean profileBean : adminUsers) {
				List<Employer> finalList = new ArrayList<Employer>();
				List<Employer> excludedList = new ArrayList<Employer>();
				List<Employer> resultList = null;

				try {
					String counties = "CENTRE,CLINTON";
					if ( "melanie".equalsIgnoreCase(profileBean.getUsername()) ) {
						counties = "CENTRE,CLINTON,BLAIR";
					} else if ( "brett".contentEquals(profileBean.getUsername())) {
						counties = "CENTRE,CLINTON,LYCOMING";
					}
					resultList = employerService.getEmployersForAdminUser(counties, null, false, null, false, null, null, null, 
							null, null, startDate.toString(), endDate.toString(), profileBean.getUsername() );

					//  Dump employers to log
					for (Employer e : resultList) {
						try {
							logBuf.append("   Employer:  " + e.toTabbedString() + "\r\n");
						} catch (Exception e1) {
							logBuf.append("   *** Exception ***  Employer: " + e.getKey().getFn());
							logBuf.append(getStringStackTrace(e1));
							exceptions = true;
						}
					}

				} catch (Exception e) {
					logBuf.append("   EXCEPTION GETTING ALL EMPLOYERS \r\n");
					logBuf.append(getStringStackTrace(e));
					exceptions = true;
				}

				logBuf.append("\r\n");

				//  Need carrier information to cross-reference to NAIC.  Employer uses carrier name; rates use NAIC.
				Map<String, String> carriers = carrierService.getAllCarriersByName();

				//  Collect the list of NAICs needed for rate comparisons
				List<String> naics = new ArrayList<String>();
				for (Employer e : resultList) {
					String carrier = (e.getCarrier() == null ? null : e.getCarrier().trim());
					if (carriers.containsKey(carrier)) {
						logBuf.append("   Carrier comparison:  [" + carrier + "]\r\n");
						String naic = carriers.get(carrier);
						if (naic != null && naic != "") {
							logBuf.append("   Adding naic:  [" + naic + "]\r\n");
							naics.add(naic);
						}
					} else {
						logBuf.append("*** Carrier not found in database *** -- " + carrier + "\r\n");
					}
				}

				//  Get all rates for the user's three selected carriers
				Map<String, Double> rates = ratesService.getAllRatesByDate(naics, profileBean.getCarrier1(),profileBean.getCarrier2(), profileBean.getCarrier3());
				logBuf.append("   Retrieved user's three carriers' rates...\r\n");

				Map<String, String> preferredMap = preferenceService.getAllPreferencesByUsername(profileBean.getUsername());
				List<String> allowedCodes = new ArrayList<String>();
				if (preferredMap != null) {
					allowedCodes = preferredMap.keySet().stream().map(s -> String.valueOf(s)).collect(Collectors.toList());
				}
				logBuf.append("   Retrieved user's code preferences...\r\n");

				String carrier1 = profileBean.getCarriersBean1().getNaic();
				String carrier2 = profileBean.getCarriersBean2().getNaic();
				String carrier3 = profileBean.getCarriersBean3().getNaic();

				//  Check each employer to match required preferences
				for ( Employer e : resultList ) {
					logBuf.append("   " + e.getKey().getFn() + ":  ");
					try {
							String carrier = (e.getCarrier() == null ? null : e.getCarrier().trim());
							if (carriers.containsKey(carrier)) {
								String naic = carriers.get(carrier);

								if (naic != null && naic != "") {
									String code = (e.getData().getCode() == null ? null : e.getData().getCode().trim());
									if (code == null || code.length() == 0) {
										e.getData().setCode("-1");
										e.setDescription("No code specified.  Using average rates.");
										code = "-1";
									} else {
										//  Remove leading zeroes for comparison (if any)
										while ( code.charAt(0) == '0' ) {
											code = code.substring(1);
										}
									}
									
									if (rates != null ) {
										e.getData().setRate(rates.get(naic + "_" + code));
										e.getData().setCompRate(rates.get(carrier1 + "_" + code), 0);
										e.getData().setCompRate(rates.get(carrier2 + "_" + code), 1);
										e.getData().setCompRate(rates.get(carrier3 + "_" + code), 2);

										if (rates.get(naic + "_" + code) != null
												&& rates.get(carrier1 + "_" + code) != null
												&& (rates.get(naic + "_" + code) > rates.get(carrier1 + "_" + code))) {
											// Include only specified codes or anything if there are no preferences
											if (allowedCodes == null || allowedCodes.size() == 0 || e.getData().getCode() == null ||
													(e.getData().getCode() != null && allowedCodes.contains(e.getData().getCode()))) {

												finalList.add(e);
											} else {
												excludedList.add(e);
												logBuf.append("   Allowed code check failed\r\n");
											}
										} else {
											excludedList.add(e);
											logBuf.append("   Rate too low\r\n");
										}
									} else {
										excludedList.add(e);
										logBuf.append("   Rate check failed\r\n");
									}
								} else {
									excludedList.add(e);
									logBuf.append("   Naic check failed\r\n");
								}
							} else {
								excludedList.add(e);
								logBuf.append("   Carrier check failed\r\n");
							}
					} catch (Exception e1) {
						logBuf.append("   EXCEPTION prevented processing\r\n");
						logBuf.append(getStringStackTrace(e1));
						exceptions = true;
						excludedList.add(e);
						e1.printStackTrace();
					}
				}

				logBuf.append("   Final list count   : " + finalList.size() + "\r\n");
				logBuf.append("   Excluded list count: " + excludedList.size() + "\r\n");

				if (!finalList.isEmpty()) {
					finalList = finalList.stream().filter(a -> (a.getData() != null && a.getData().getPct(0) != null))
							.sorted((Employer a, Employer b) -> a.getData().getPct(0).compareTo(b.getData().getPct(0)))
							.collect(Collectors.toList());
					Collections.reverse(finalList);
				}
				if (!excludedList.isEmpty()) {
					excludedList = excludedList.stream().filter(a -> (a.getData() != null && a.getData().getPct(0) != null))
							.sorted((Employer a, Employer b) -> a.getData().getPct(0).compareTo(b.getData().getPct(0)))
							.collect(Collectors.toList());
					Collections.reverse(excludedList);
				}
				// Generate Letter and Email
				generateLetterAndSendMail(profileBean, finalList, excludedList, startDate, endDate, exceptions);
				try { Thread.sleep(120000); } catch ( Exception e ) { }
			}
		} catch (Exception e) {
			e.printStackTrace();
			logBuf.append("EXCEPTION" + e.getMessage());
			logBuf.append(getStringStackTrace(e));
			exceptions = true;
		} finally {
			try {
			    SendMailUtility.sendMail("KLJ1962@GMAIL.COM", "kswcrc admin letter generation log", logBuf.toString());
			} catch (Exception f) {
				f.printStackTrace();
				logBuf.append(getStringStackTrace(f));
				exceptions = true;
			}
		}
	}

	private String getStringStackTrace(Exception exception) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		exception.printStackTrace(pw);
		return pw.toString();
	}
	
	private void generateLetterAndSendMail(ProfileBean profile, List<Employer> result, List<Employer> excluded, LocalDate start, LocalDate end, boolean exceptions) {

		long ms = Calendar.getInstance().getTimeInMillis();
		String tmplFileName = "Template-" + profile.getUsername() + ".docx";
		String resultFileName = "Result-" + profile.getUsername() + "-" + ms + ".docx";
		
		File wordTemplate = new File(BASE + tmplFileName);
		
		try {
			
			if(profile!=null && profile.getTemplateBean()!=null 
					&& profile.getTemplateBean().getTemplate()!=null) {
				Files.write(profile.getTemplateBean().getTemplate(), wordTemplate);
			}
			
			//
			//  Generate CSV file for mail merge
			//
			String csvFileName = createCSVFile(result, profile.getUsername());
			StringBuffer emailBody = createEmailBody(result, excluded, start, end);

			//
			//  Generate Excel spreadsheet
			//
			List<Employer> excelList = new ArrayList<Employer>();
			String xslxFileName = null;
			try {
				excelList.addAll(result);
				excelList.addAll(excluded);
				xslxFileName = createExcelFile(excelList);
			} catch ( Exception e ) {
				e.printStackTrace();
			}

			//
			//  Merge to template
			//
//			File wordTemplate = new File(BASE + tmplFileName);
			if ( ! wordTemplate.exists() ) {
				wordTemplate = new File(getClass().getClassLoader().getResource("Template-guest.docx").getFile());
			}
			File excelFile = new File(BASE + csvFileName);
			LogUtil.log("Merging data from " + tmplFileName + " and " + csvFileName + " into " + resultFileName);

			try {
			    WCMailMerge wcmm = new WCMailMerge();
			    wcmm.merge(wordTemplate, excelFile, resultFileName, profile.getUsername());
			} catch ( Exception e ) {
				e.printStackTrace();
			    SendMailUtility.sendMail("KLJ1962@GMAIL.COM", profile.getLastName() + ", " + profile.getFirstName() + "(" + profile.getUsername() + ")" + "*** Exception in mail merge "
			    		+ "WC Prospects - expiring " + md.format(start) + " to " + md.format(end), getStringStackTrace(e));
			}

			try {
				SendMailUtility.sendMail(profile.getEmail(), "WC Prospects - expiring " + md.format(start) + " to " + md.format(end), emailBody.toString(), new String[]{BASE + resultFileName, BASE + xslxFileName} );
			    SendMailUtility.sendMail("KLJ1962@GMAIL.COM", profile.getLastName() + ", " + profile.getFirstName() + "(" + profile.getUsername() + ")" + 
			    		(exceptions ? "***EXCEPTIONS OCCURRED*** " : "") + "WC Prospects - expiring " + md.format(start) + " to " + md.format(end), emailBody.toString(), new String[]{BASE + resultFileName, BASE + xslxFileName}  );
			} catch (Exception e) {
				e.printStackTrace();
			    SendMailUtility.sendMail("KLJ1962@GMAIL.COM", profile.getLastName() + ", " + profile.getFirstName() + "(" + profile.getUsername() + ")" + "*** Exception in mail merge "
			    		+ "WC Prospects - expiring " + md.format(start) + " to " + md.format(end), getStringStackTrace(e));
			}
		} catch (Exception e) {
			e.printStackTrace();
		    try {
				SendMailUtility.sendMail("KLJ1962@GMAIL.COM", profile.getLastName() + ", " + profile.getFirstName() + "(" + profile.getUsername() + ")" + "*** Exception in mail merge "
						+ "WC Prospects - expiring " + md.format(start) + " to " + md.format(end), getStringStackTrace(e));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}


	public static StringBuffer createEmailBody(List<Employer> result, List<Employer> excluded, LocalDate start, LocalDate end) {
		StringBuffer sb = new StringBuffer();
		sb.append(LocalDateTime.now().format(dtf) + "\r\n\r\n");
		sb.append("Automatically generated letters for the week of " + md.format(start) + " to " + md.format(end) + "\r\n\r\n");
		
		sb.append("Ex Date\tMod/Mrt\tCurRate\tCmpRate\tPct\tCarrier\tDesc\tFile#\tEmpName\tCounty\tZip\r\n");
		for ( Employer e : result) {
			sb.append(
					dt.format(e.getData().getXdate()) + "\t" +
					e.getData().getMm() + "\t" + 
					(e.getCarrier()!= null ? "$" + e.getData().getRate() + "\t" : "N/A\t") + 
					(e.getCarrier()!= null ? "$" + e.getData().getComp1() + "\t" : "N/A\t")  + 
					(e.getCarrier()!= null ? pf.format(e.getData().getPct1()) + "\t" : "N/A\t")  + 
					fixedWidth(e.getCarrier(), 25) + "\t" +
					fixedWidth(e.getDescription(), 20) + "\t" +
					e.getKey().getFn() + "\t" +
					e.getKey().getName() + "\t" + 
					e.getBase().getCounty() + "\t" + 
					(e.getBase().getZip() != null ? e.getBase().getZip() : "") +
					"\r\n");
		}
		sb.append("\r\n\r\nExcluded from letter generation (usually due to rate or carrier is one of your three.)\r\n");

		sb.append("Ex Date\tMod/Mrt\tCurRate\tCmpRate\tPct\tCarrier\tDesc\tFile#\tEmpName\tAddr\tCity\tState\tZip\tCounty\r\n");
		for ( Employer e : excluded) {
			sb.append(
					dt.format(e.getData().getXdate()) + "\t" +
					e.getData().getMm() + "\t" + 
					(e.getCarrier()!= null ? "$" + e.getData().getRate() + "\t" : "N/A\t") + 
					(e.getCarrier()!= null ? "$" + e.getData().getComp1() + "\t" : "N/A\t")  + 
					(e.getCarrier()!= null ? pf.format(e.getData().getPct1()) + "\t" : "N/A\t")  + 
					fixedWidth(e.getCarrier(), 25) + "\t" +
					fixedWidth(e.getDescription(), 20) + "\t" +
					e.getKey().getFn() + "\t" +
					e.getKey().getName() + "\t" + 
					e.getBase().getAddress() + ", " + e.getBase().getCity() + "  " + e.getBase().getState() + "  " + e.getBase().getZip() + " [" + e.getBase().getCounty() + "]" +
					"\r\n");
		}
		return sb;
	}

	
	private static String fixedWidth(String s, int w) {
		String result = "";
		if (s == null) {
			while (result.length() < w ) {
				result += " ";
			} 
		} else if ( s.length() > w ) {
			result = s.substring(0, w-3) + "...";
		} else if ( s.length() == w ) {
			result = s;
		} else {
			result = s;
			while ( result.length() < w ) {
				result += " ";
			}
		}
		return result;
	}
	
	
	private static String createCSVFile(List<Employer> result,String username) throws IOException {
		long ms = Calendar.getInstance().getTimeInMillis();
		
		String csvFileName = username + "-" + ms + ".csv";

		FileWriter fileWriter = null;
		CSVPrinter csvFilePrinter = null;

		// Create the CSVFormat object with "\n" as a record delimiter
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);

		try {
			// initialize FileWriter object
			fileWriter = new FileWriter(BASE + csvFileName);

			// initialize CSVPrinter object
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

			// Create CSV file header
			csvFilePrinter.printRecord(FILE_HEADER);

			// Write a new student object list to the CSV file
			for (Employer employer : result) {
					try {
						List<String> dataRecord = new ArrayList<String>();

						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getKey().getName()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getCarrier()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getCity()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getDescription()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getData().getMm()));
						dataRecord.add(StringEscapeUtils.escapeXml11(dt.format(employer.getData().getXdate().getTime())));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getRate())));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getCompRate(0))));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getPct(0)*100)));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getCompRate(1))));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getPct(1)*100)));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getCompRate(2))));
						dataRecord.add(StringEscapeUtils.escapeXml11(df.format(employer.getData().getPct(2)*100)));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getData().getCode()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getKey().getFn().toString()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getKey().getCounty()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getAddress()));
						dataRecord.add(StringEscapeUtils.escapeXml11(employer.getBase().getZip()));
						csvFilePrinter.printRecord(dataRecord);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}

			LogUtil.log("CSV file was created successfully !!!");

		} catch (Exception e) {
			LogUtil.log("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
				csvFilePrinter.close();
			} catch (IOException e) {
				LogUtil.log("Error while flushing/closing fileWriter/csvPrinter !!!");
				e.printStackTrace();
			}
		}
		return csvFileName;
	}

	private String createExcelFile(List<Employer> fullList) {
		
		long ms = Calendar.getInstance().getTimeInMillis();
		String xlsxFile = "Results-" + ms + ".xlsx";		

	    try {
	        XSSFWorkbook workBook = new XSSFWorkbook();
//	        HSSFWorkbook workBook = new HSSFWorkbook();
	        XSSFSheet sheet = workBook.createSheet("sheet1");
	        int RowNum=0;
	        
	        XSSFDataFormat dataFormat = workBook.createDataFormat();
	        
	        XSSFCellStyle redStylePlusAccounting = workBook.createCellStyle();
	        XSSFFont redFont=workBook.createFont();
            redFont.setColor(HSSFFont.COLOR_RED);
            redStylePlusAccounting.setFont(redFont);
            redStylePlusAccounting.setDataFormat(dataFormat.getFormat("_($* #,##0.00_);_($* (#,##0.00);_($* \"-\"??_);_(@_)"));
	        
            XSSFCellStyle redStylePlusPercentage = workBook.createCellStyle();
            redStylePlusPercentage.setFont(redFont);
            redStylePlusPercentage.setDataFormat(dataFormat.getFormat("0.00%"));
	        
            
            XSSFCellStyle greenStylePlusAccounting = workBook.createCellStyle();
            XSSFFont greenFont=workBook.createFont();
            greenFont.setColor(HSSFColor.GREEN.index);
            greenStylePlusAccounting.setFont(greenFont);
            greenStylePlusAccounting.setDataFormat(dataFormat.getFormat("_($* #,##0.00_);_($* (#,##0.00);_($* \"-\"??_);_(@_)"));
            
            XSSFCellStyle greenStylePlusPercentage = workBook.createCellStyle();
            greenStylePlusPercentage.setFont(greenFont);
            greenStylePlusPercentage.setDataFormat(dataFormat.getFormat("0.00%"));
            
            
            XSSFCellStyle boldStyle = workBook.createCellStyle();
            XSSFFont boldFont = workBook.createFont();
            boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            boldStyle.setFont(boldFont);    
            
            CellStyle currencyStyle = workBook.createCellStyle();
            currencyStyle.setDataFormat(dataFormat.getFormat("_($* #,##0.00_);_($* (#,##0.00);_($* \"-\"??_);_(@_)"));

            
            CellStyle percentageStyle = workBook.createCellStyle();
            percentageStyle.setDataFormat(dataFormat.getFormat("0.00%"));
            
            
            XSSFCellStyle dateStyle = workBook.createCellStyle();
            dateStyle.setDataFormat(dataFormat.getFormat("yyyy-mm-dd"));
            
            Row headerRow=sheet.createRow(RowNum);
            for(int i = 0; i < FILE_HEADER.length; i++) {
            	headerRow.createCell(i).setCellValue(StringEscapeUtils.escapeXml11((String) FILE_HEADER[i]));
            	headerRow.getCell(i).setCellStyle(boldStyle);
            }
            
            RowNum++;
            
            for (Employer employer : fullList) {
//				String s = request.getParameter("n" + employer.getKey().getFn());
//				if (s != null && s.equalsIgnoreCase(employer.getKey().getFn().toString())) {
					try {
						
						Row currentRow=sheet.createRow(RowNum);

						currentRow.createCell(0).setCellValue(StringEscapeUtils.escapeXml11(employer.getKey().getName()));
						currentRow.createCell(1).setCellValue(StringEscapeUtils.escapeXml11(employer.getCarrier()));
						currentRow.createCell(2).setCellValue(StringEscapeUtils.escapeXml11(employer.getBase().getCity()));
						currentRow.createCell(3).setCellValue(StringEscapeUtils.escapeXml11(employer.getDescription()));
						currentRow.createCell(4).setCellValue(StringEscapeUtils.escapeXml11(employer.getData().getMm()));
						
						currentRow.createCell(5).setCellValue(employer.getData().getXdate());
						currentRow.getCell(5).setCellStyle(dateStyle);
						
						currentRow.createCell(6).setCellValue(Double.valueOf(df.format(employer.getData().getRate())));
						
						currentRow.createCell(7).setCellValue(Double.valueOf(df.format(employer.getData().getCompRate(0))));
						
						currentRow.createCell(8).setCellValue(employer.getData().getPct(0));
						
						if(employer.getData().getRate() > employer.getData().getComp1()) {
							currentRow.getCell(7).setCellStyle(greenStylePlusAccounting);
							currentRow.getCell(8).setCellStyle(greenStylePlusPercentage);
						} else if(employer.getData().getRate() < employer.getData().getComp1()) {
							currentRow.getCell(7).setCellStyle(redStylePlusAccounting);
							currentRow.getCell(8).setCellStyle(redStylePlusPercentage);
						}
						
						currentRow.createCell(9).setCellValue(Double.valueOf(df.format(employer.getData().getCompRate(1))));
						currentRow.getCell(9).setCellStyle(currencyStyle);
						
						currentRow.createCell(10).setCellValue(employer.getData().getPct(1));
						currentRow.getCell(10).setCellStyle(percentageStyle);
						
						if(employer.getData().getRate() > employer.getData().getComp2()) {
							currentRow.getCell(9).setCellStyle(greenStylePlusAccounting);
							currentRow.getCell(10).setCellStyle(greenStylePlusPercentage);
						} else if(employer.getData().getRate() < employer.getData().getComp2()) {
							currentRow.getCell(9).setCellStyle(redStylePlusAccounting);
							currentRow.getCell(10).setCellStyle(redStylePlusPercentage);
						}
						
						currentRow.createCell(11).setCellValue(Double.valueOf(df.format(employer.getData().getCompRate(2))));
						currentRow.getCell(11).setCellStyle(currencyStyle);
						
						currentRow.createCell(12).setCellValue(employer.getData().getPct(2));
						currentRow.getCell(12).setCellStyle(percentageStyle);
						
						if(employer.getData().getRate() > employer.getData().getComp3()) {
							currentRow.getCell(11).setCellStyle(greenStylePlusAccounting);
							currentRow.getCell(12).setCellStyle(greenStylePlusPercentage);
						} else if(employer.getData().getRate() < employer.getData().getComp3()) {
							currentRow.getCell(11).setCellStyle(redStylePlusAccounting);
							currentRow.getCell(12).setCellStyle(redStylePlusPercentage);
						}
						
						currentRow.createCell(13).setCellValue(StringEscapeUtils.escapeXml11(employer.getData().getCode()));
						currentRow.createCell(14).setCellValue(StringEscapeUtils.escapeXml11(employer.getKey().getFn().toString()));
						currentRow.createCell(15).setCellValue(StringEscapeUtils.escapeXml11(employer.getKey().getCounty()));
						currentRow.createCell(16).setCellValue(StringEscapeUtils.escapeXml11(employer.getBase().getAddress()));
						currentRow.createCell(17).setCellValue(StringEscapeUtils.escapeXml11(employer.getBase().getZip()));
						
						
						RowNum++;
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
//			}
            
            for(int i = 0; i < FILE_HEADER.length; i++) {
            	sheet.autoSizeColumn(i);
            }

	        FileOutputStream fileOutputStream =  new FileOutputStream(BASE + xlsxFile);
	        workBook.write(fileOutputStream);
	        fileOutputStream.close();
	        workBook.close();
	        LogUtil.log("Done");

	    } catch (Exception ex) {
	        LogUtil.log(ex.getMessage()+"Exception in try");
	    }
	    return xlsxFile;
	}

	
	
	@Transactional
//	@Scheduled(cron = "0 45 1 * * WED")   // 2:00 AM each Monday
	public void prepTestLetterGeneration() {
		LogUtil.log("**********");
		LogUtil.log("Entered prepLetterGeneration ...");
		LogUtil.log("**********");
		
		StringBuffer logBuf = new StringBuffer();
		try {
			List<ProfileBean> allUsers = userService.getAllUsers();
			LogUtil.log(allUsers.toString());
		} catch (Exception e) {
			e.printStackTrace();
			logBuf.append(getStringStackTrace(e));
		}
	}



	//  Cron expression = "s m h d wd m y"
	@Transactional
//	@Scheduled(cron = "0 0 2 * * WED")   // 2:00 AM each Monday
	public void generateTestLetters() {
		int daysFromToday = 56;
		int daysInPeriod = 7;

		boolean exceptions = false;
		
		StringBuffer logBuf = new StringBuffer();
		try {
			// Get current date
			LocalDate now = LocalDate.now();
			LocalDate startDate = now.plusDays(daysFromToday);
			LocalDate endDate = startDate.plusDays(daysInPeriod);

			logBuf.append("Now:        " + now.toString() + "\r\n");
			logBuf.append("Start date: " + startDate.toString() + "\r\n");
			logBuf.append("End date:   " + endDate.toString() + "\r\n");

			String dateRangeStartMonth = startDate.getMonth().name().substring(0, 3);
			String dateRangeEndMonth = endDate.getMonth().name().substring(0, 3);

			logBuf.append("Purchase range months:  " + dateRangeStartMonth + ".." + dateRangeEndMonth + "\r\n");

			List<ProfileBean> allUsers = userService.getAllUsers();

			for (ProfileBean profileBean : allUsers) {
				List<Employer> finalList = new ArrayList<Employer>();
				List<Employer> excludedList = new ArrayList<Employer>();
				List<Employer> resultList = null;

				try {
					logBuf.append("\r\nChecking user " + profileBean.getUsername() + "(" + profileBean.getLastName()
							+ ", " + profileBean.getFirstName() + ")\r\n");

					List<Purchase> purchases = purchaseService.fetchPurchasesByUserByMonth(profileBean.getUsername(),
							dateRangeStartMonth);
					if (!dateRangeStartMonth.equalsIgnoreCase(dateRangeEndMonth)) {
						List<Purchase> endMonthPurchases = purchaseService
								.fetchPurchasesByUserByMonth(profileBean.getUsername(), dateRangeEndMonth);
						purchases.addAll(endMonthPurchases);
					}
					if (purchases == null || purchases.size() == 0) {
						logBuf.append("   No purchases found.\r\n");
					}

					if (purchases.size() == 0) {
						continue;
					} else {
						//  Dump purchases to log
						try {
							for (Purchase purchase : purchases) {
								logBuf.append("   " + purchase.toString() + "\r\n");
							}
						} catch (Exception e) {
							logBuf.append("  EXCEPTION DUMPING PURCHASES \r\n");
							logBuf.append(getStringStackTrace(e));
							exceptions = true;
						}
					}
					resultList = employerService.getAllEmployers(purchases, startDate, endDate, logBuf);

					//  Dump employers to log
					for (Employer e : resultList) {
						try {
							logBuf.append("   Employer:  " + e.toTabbedString() + "\r\n");
						} catch (Exception e1) {
							logBuf.append("   *** Exception ***  Employer: " + e.getKey().getFn());
							logBuf.append(getStringStackTrace(e1));
							exceptions = true;
						}
					}
				} catch (Exception e) {
					logBuf.append("   EXCEPTION GETTING ALL EMPLOYERS \r\n");
					logBuf.append(getStringStackTrace(e));
					exceptions = true;
				}

				logBuf.append("\r\n");

				//  Need carrier information to cross-reference to NAIC.  Employer uses carrier name; rates use NAIC.
				Map<String, String> carriers = carrierService.getAllCarriersByName();

				//  Collect the list of NAICs needed for rate comparisons
				List<String> naics = new ArrayList<String>();
				for (Employer e : resultList) {
					String carrier = (e.getCarrier() == null ? null : e.getCarrier().trim());
					if (carriers.containsKey(carrier)) {
						logBuf.append("   Carrier comparison:  [" + carrier + "]\r\n");
						String naic = carriers.get(carrier);
						if (naic != null && naic != "") {
							logBuf.append("   Adding naic:  [" + naic + "]\r\n");
							naics.add(naic);
						}
					} else {
						logBuf.append("*** Carrier not found in database *** -- " + carrier + "\r\n");
					}
				}

				//  Get all rates for the user's three selected carriers
				Map<String, Double> rates = ratesService.getAllRatesByDate(naics, profileBean.getCarrier1(),profileBean.getCarrier2(), profileBean.getCarrier3());
				logBuf.append("   Retrieved user's three carriers' rates...\r\n");

				Map<String, String> preferredMap = preferenceService.getAllPreferencesByUsername(profileBean.getUsername());
				List<String> allowedCodes = new ArrayList<String>();
				if (preferredMap != null) {
					allowedCodes = preferredMap.keySet().stream().map(s -> String.valueOf(s)).collect(Collectors.toList());
				}
				logBuf.append("   Retrieved user's code preferences...\r\n");

				String carrier1 = profileBean.getCarriersBean1().getNaic();
				String carrier2 = profileBean.getCarriersBean2().getNaic();
				String carrier3 = profileBean.getCarriersBean3().getNaic();

				//  Check each employer to match required preferences
				for ( Employer e : resultList ) {
					logBuf.append("   " + e.getKey().getFn() + ":  ");
					try {
							String carrier = (e.getCarrier() == null ? null : e.getCarrier().trim());
							if (carriers.containsKey(carrier)) {
								String naic = carriers.get(carrier);

								if (naic != null && naic != "") {
									String code = (e.getData().getCode() == null ? null : e.getData().getCode().trim());
									if (code == null || code.length() == 0) {
										e.getData().setCode("-1");
										e.setDescription("No code specified.  Using average rates.");
										code = "-1";
									} else {
										//  Remove leading zeroes for comparison (if any)
										while ( code.charAt(0) == '0' ) {
											code = code.substring(1);
										}
									}
									
									if (rates != null ) {
										e.getData().setRate(rates.get(naic + "_" + code));
										e.getData().setCompRate(rates.get(carrier1 + "_" + code), 0);
										e.getData().setCompRate(rates.get(carrier2 + "_" + code), 1);
										e.getData().setCompRate(rates.get(carrier3 + "_" + code), 2);

										if (rates.get(naic + "_" + code) != null
												&& rates.get(carrier1 + "_" + code) != null
												&& (rates.get(naic + "_" + code) > rates.get(carrier1 + "_" + code))) {
											// Include only specified codes or anything if there are no preferences
											if (allowedCodes == null || allowedCodes.size() == 0 || e.getData().getCode() == null ||
													(e.getData().getCode() != null && allowedCodes.contains(e.getData().getCode()))) {

												finalList.add(e);
											} else {
												excludedList.add(e);
												logBuf.append("   Allowed code check failed\r\n");
											}
										} else {
											excludedList.add(e);
											logBuf.append("   Rate too low\r\n");
										}
									} else {
										excludedList.add(e);
										logBuf.append("   Rate check failed\r\n");
									}
								} else {
									excludedList.add(e);
									logBuf.append("   Naic check failed\r\n");
								}
							} else {
								excludedList.add(e);
								logBuf.append("   Carrier check failed\r\n");
							}
					} catch (Exception e1) {
						logBuf.append("   EXCEPTION prevented processing\r\n");
						logBuf.append(getStringStackTrace(e1));
						exceptions = true;
						excludedList.add(e);
						e1.printStackTrace();
					}
				}

				logBuf.append("   Final list count   : " + finalList.size() + "\r\n");
				logBuf.append("   Excluded list count: " + excludedList.size() + "\r\n");

				if (!finalList.isEmpty()) {
					finalList = finalList.stream().filter(a -> (a.getData() != null && a.getData().getPct(0) != null))
							.sorted((Employer a, Employer b) -> a.getData().getPct(0).compareTo(b.getData().getPct(0)))
							.collect(Collectors.toList());
					Collections.reverse(finalList);
				}
				if (!excludedList.isEmpty()) {
					excludedList = excludedList.stream().filter(a -> (a.getData() != null && a.getData().getPct(0) != null))
							.sorted((Employer a, Employer b) -> a.getData().getPct(0).compareTo(b.getData().getPct(0)))
							.collect(Collectors.toList());
					Collections.reverse(excludedList);
				}
				// Generate Letter and Email
				generateTestLetterAndSendMail(profileBean, finalList, excludedList, startDate, endDate, exceptions);
				try { Thread.sleep(120000); } catch ( Exception e ) { }
			}
		} catch (Exception e) {
			e.printStackTrace();
			logBuf.append("EXCEPTION" + e.getMessage());
			logBuf.append(getStringStackTrace(e));
			exceptions = true;
		} finally {
			try {
			    SendMailUtility.sendMail("KLJ1962@GMAIL.COM", "kswcrc test letter generation log", logBuf.toString());
			} catch (Exception f) {
				f.printStackTrace();
				logBuf.append(getStringStackTrace(f));
				exceptions = true;
			}
		}
	}

	private void generateTestLetterAndSendMail(ProfileBean profile, List<Employer> result, List<Employer> excluded, LocalDate start, LocalDate end, boolean exceptions) {

		long ms = Calendar.getInstance().getTimeInMillis();
		String tmplFileName = "Template-" + profile.getUsername() + ".docx";
		String resultFileName = "Result-" + profile.getUsername() + "-" + ms + ".docx";
		
		File wordTemplate = new File(BASE + tmplFileName);
		
		try {
			
			if(profile!=null && profile.getTemplateBean()!=null 
					&& profile.getTemplateBean().getTemplate()!=null) {
				Files.write(profile.getTemplateBean().getTemplate(), wordTemplate);
			}
			
			//
			//  Generate CSV file for mail merge
			//
			String csvFileName = createCSVFile(result, profile.getUsername());
			StringBuffer emailBody = createEmailBody(result, excluded, start, end);

			//
			//  Generate Excel spreadsheet
			//
			List<Employer> excelList = new ArrayList<Employer>();
			String xslxFileName = null;
			try {
				excelList.addAll(result);
				excelList.addAll(excluded);
				xslxFileName = createExcelFile(excelList);
			} catch ( Exception e ) {
				e.printStackTrace();
			}

			//
			//  Merge to template
			//
//			File wordTemplate = new File(BASE + tmplFileName);
			if ( ! wordTemplate.exists() ) {
				wordTemplate = new File(getClass().getClassLoader().getResource("Template-guest.docx").getFile());
			}
			File excelFile = new File(BASE + csvFileName);
			LogUtil.log("Merging data from " + tmplFileName + " and " + csvFileName + " into " + resultFileName);

			try {
			    WCMailMerge wcmm = new WCMailMerge();
			    wcmm.merge(wordTemplate, excelFile, resultFileName, profile.getUsername());
			} catch ( Exception e ) {
				e.printStackTrace();
			    SendMailUtility.sendMail("KLJ1962@GMAIL.COM", profile.getLastName() + ", " + profile.getFirstName() + "(" + profile.getUsername() + ")" + "*** Exception in mail merge "
			    		+ "WC Prospects - expiring " + md.format(start) + " to " + md.format(end), getStringStackTrace(e));
			}

			try {
//				SendMailUtility.sendMail(profile.getEmail(), "WC Prospects - expiring " + md.format(start) + " to " + md.format(end), emailBody.toString(), new String[]{BASE + resultFileName, BASE + xslxFileName} );
			    SendMailUtility.sendMail("KLJ1962@GMAIL.COM", profile.getLastName() + ", " + profile.getFirstName() + "(" + profile.getUsername() + ")" + 
			    		(exceptions ? "***EXCEPTIONS OCCURRED*** " : "") + "WC Prospects - expiring " + md.format(start) + " to " + md.format(end), emailBody.toString(), new String[]{BASE + resultFileName, BASE + xslxFileName}  );
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
