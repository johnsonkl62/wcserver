package com.synesis.jobs;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.service.purchase.PurchaseService;
import com.synesis.utility.SendMailUtility;

@Component
public class PurgePurchaseJob {

	@Autowired
	private PurchaseService purchaseService;
	
	//  Cron expression = "s m h d wd m y"
	@Transactional
	@Scheduled(cron = "0 0 4 1 * *")   // 2:00 AM each first of the month
	public void purgePurchases() {

		//  Get current date
		LocalDate now = LocalDate.now();
		LocalDate start = now.minusDays(45);

		String purchaseMonth = start.getMonth().name().substring(0,3);
		
		StringBuffer emailBody = purchaseService.deletePurchasesByMonth(purchaseMonth);

	    try {
			SendMailUtility.sendMail("KLJ1962@GMAIL.COM", "Purchase Purge", emailBody.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
