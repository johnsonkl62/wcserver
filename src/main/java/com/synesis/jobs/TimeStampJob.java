package com.synesis.jobs;

import org.springframework.scheduling.annotation.Scheduled;

import com.synesis.utility.LogUtil;

public class TimeStampJob {

	@Scheduled(cron = "0 */10 * * * *")   // 2:00 AM each Monday
	public void stampLogFile() {
		LogUtil.log("...Time Stamp...");
	}

}
