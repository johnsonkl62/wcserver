package com.synesis.jobs;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.synesis.service.employers.EmployerService;

@Component
public class PurgeCancelledJob {

	@Autowired
	private EmployerService employerService;
	
	//  Cron expression = "s m h d wd m y"
	@Transactional
	@Scheduled(cron = "0 0 5 1 * *")   // 2:00 AM each first of the month
	public void purgeCancelled() {

		//  Get current date
		LocalDate now = LocalDate.now();

		employerService.processCancelledEmployers();

	}
}
