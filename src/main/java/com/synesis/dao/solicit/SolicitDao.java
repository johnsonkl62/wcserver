package com.synesis.dao.solicit;

import java.util.List;

import com.synesis.model.solicit.Solicit;

public interface SolicitDao {

	public Solicit searchByAgencyAndEmail(String agency, String email);

	public List<Object[]> searchEmployers(String county, List<String> codes,
			List<String> zips);

	public void unsubscribeEmail(String agency, String email);

	public List<Solicit> getAllSolicits();
	
}
