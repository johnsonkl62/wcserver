package com.synesis.dao.solicit;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.synesis.model.solicit.Solicit;

@Repository
public class SolicitDaoImpl implements SolicitDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Solicit searchByAgencyAndEmail(String agency, String email) {
		
		Solicit solicit;
		try {
			solicit = (Solicit) sessionFactory.getCurrentSession()
					.createQuery("from Solicit s where s.solicitIdentity.agency = :agency and s.solicitIdentity.email = :email and s.isActive = :isActive")
					.setParameter("agency", agency).setParameter("email", email).setParameter("isActive", true)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

		
		return solicit;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> searchEmployers(String county, List<String> codes,
			List<String> zips) {
		
		List<Object[]> list = null;
		try {
			StringBuilder stringBuilder = new StringBuilder("select c.code,c.text, count(c.code) as total from employers e, codes c "
					+ " where e.code=c.code and e.county = :county"
					+ " and e.code in (:codes) ");
			
			if(!CollectionUtils.isEmpty(zips)) {
				stringBuilder.append(" and e.zip in (:zips) ");
			}
			

			stringBuilder.append(" and e.carrier in (select c.name from carriers c, rates r where r.naic=c.naic and r.code = e.code and ");
			stringBuilder.append(" r.rate>(select rate from rates where naic='26271' and code=r.code) ) group by c.code ");

			
			NativeQuery<Object[]> nativeQuery = sessionFactory.getCurrentSession().createNativeQuery(stringBuilder.toString())
			.setParameter("county", county).setParameter("codes", codes);
			
			if(!CollectionUtils.isEmpty(zips)) {
				nativeQuery.setParameter("zips", zips);
			}
			
			list = nativeQuery.getResultList();
			
		} catch (NoResultException e) {
		}
		
		return list;
	}

	@Override
	public void unsubscribeEmail(String agency, String email) {
		Solicit solicit = searchByAgencyAndEmail(agency, email);
		if(solicit!=null) {
			solicit.setIsActive(false);
			sessionFactory.getCurrentSession().update(solicit);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Solicit> getAllSolicits() {
		List<Solicit> solicits = null;
		try {
			 solicits = sessionFactory.getCurrentSession()
					.createQuery("from Solicit s where s.isActive = :isActive")
					.setParameter("isActive", true)
					.getResultList();

		} catch (NoResultException e) {
			return null;
		}
		return solicits;
	}

}
