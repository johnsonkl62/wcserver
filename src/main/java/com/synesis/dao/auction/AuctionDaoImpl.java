package com.synesis.dao.auction;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.auction.Auction;
import com.synesis.utility.MonthEnum;

@Repository
public class AuctionDaoImpl implements AuctionDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Auction> getAllAuctions() {
		List<Auction> auctions = new ArrayList<Auction>();

		auctions = sessionFactory.getCurrentSession()
				.createQuery("from Auction")
				.getResultList();

		return auctions;
	}

	@Override
	public Auction getBid(String area, int month) {
		
		Auction auction = (Auction) sessionFactory.getCurrentSession()
				.createQuery("from Auction where upper(area)=? and month=?")
				.setParameter(0, area.toUpperCase())
				.setParameter(1, MonthEnum.byIndex(month))
				.getSingleResult();
		
		return auction;
	}

	@Override
	public Auction getBid(String area, String month) {
		
		Auction auction = (Auction) sessionFactory.getCurrentSession()
				.createQuery("from Auction where upper(area)=? and month=?")
				.setParameter(0, area.toUpperCase())
				.setParameter(1, MonthEnum.valueOf(month))
				.getSingleResult();
		
		return auction;
	}

}
