package com.synesis.dao.carriers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Carriers;

@Repository
public class CarrierDaoImpl implements CarrierDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public String getNameById(String naic) {

		Carriers carriers = (Carriers) sessionFactory.getCurrentSession()
				.createQuery("from Carriers where naic=?")
				.setParameter(0, naic).getSingleResult();

		if (carriers != null) {
			return carriers.getName();
		}

		return null;
	}

	@Override
	public String getCarrierIdByName(String name) {

		Carriers carriers;
		try {
			carriers = (Carriers) sessionFactory.getCurrentSession()
					.createQuery("from Carriers where name=?")
					.setParameter(0, name).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

		if (carriers != null) {
			return carriers.getNaic();
		}

		return null;
	}

	@Override
	public Carriers getCarrierModelById(String naic) {

		Carriers carriers = (Carriers) sessionFactory.getCurrentSession()
				.createQuery("from Carriers where naic=?")
				.setParameter(0, naic).getSingleResult();

		return carriers;
	}

	@Override
	public Carriers getCarrierModelByName(String name) {

		try {

			Carriers carriers = (Carriers) sessionFactory.getCurrentSession()
					.createQuery("from Carriers where name=?")
					.setParameter(0, name).getSingleResult();

			return carriers;

		} catch (NoResultException ex) {
			ex.printStackTrace();
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Carriers> getAllCarriers() {
		
			List<Carriers> carriers = sessionFactory.getCurrentSession()
				.createQuery("from Carriers")
				.getResultList();

		return carriers;
	}

	@Override
	public Map<String, String> getAllCarriersByName() {
		
		Map<String, String> map = new HashMap<String, String>();
		
		List<Carriers> carriers = sessionFactory.getCurrentSession()
				.createQuery("from Carriers")
				.getResultList();
		
		if(carriers!=null && !carriers.isEmpty()) {
			for(Carriers carriers2 : carriers) {
				map.put(carriers2.getName().toUpperCase(), carriers2.getNaic());
			}
		}
		
		return map;
	}

}
