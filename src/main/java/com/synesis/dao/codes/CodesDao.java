package com.synesis.dao.codes;

import java.util.Map;

public interface CodesDao {

	String getDescriptionFromCode(String code);
	
	String getCodesFromDescription(String description);

	Map<String, String> getAllCodesWithDescription();
}
