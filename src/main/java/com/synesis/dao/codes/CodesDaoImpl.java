package com.synesis.dao.codes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Codes;

@Repository
public class CodesDaoImpl implements CodesDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public String getDescriptionFromCode(String code) {

		Codes codesModel = (Codes) sessionFactory.getCurrentSession()
				.createQuery("from Codes where code=?").setParameter(0, code)
				.getSingleResult();

		if (codesModel != null) {
			return codesModel.getText();
		}

		return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public String getCodesFromDescription(String description) {
		List<Codes> codesModel = new ArrayList<Codes>();

		codesModel = sessionFactory.getCurrentSession()
				.createQuery("from Codes where upper(text) like ?")
				.setParameter(0, "%" + description.toUpperCase() + "%")
				.getResultList();

		if (codesModel != null && !codesModel.isEmpty()) {

			
			//Comma Separated Codes
			return codesModel.stream()
					.map(code -> String.valueOf(code.getCode()))
					.collect(Collectors.joining(","));
		}

		return null;
	}

	@Override
	public Map<String, String> getAllCodesWithDescription() {
		
		List<Codes> codesList = sessionFactory.getCurrentSession()
				.createQuery("from Codes")
				.getResultList();
		
		Map<String, String> map = new TreeMap<String, String>();
		
		if(codesList!=null && !codesList.isEmpty()){
			for(Codes code : codesList) {
				map.put(code.getCode(), code.getText());
			}
		}
		
		return map;
	}
	
}
