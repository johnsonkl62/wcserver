package com.synesis.dao.zip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Zips;

@Repository
public class ZipDaoImpl implements ZipDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Zips> findAllZips() {

		List<Zips> zips = sessionFactory.getCurrentSession()
				.createQuery("from Zips").getResultList();

		return zips;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Zips> findZipByCounty(String county) {
		List<Zips> zips = new ArrayList<Zips>();

		zips = sessionFactory.getCurrentSession()
				.createQuery("from Zips where county=?")
				.setParameter(0, county).getResultList();

		return zips;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Zips> findAllZipsByCounty(String county) {
		Map<String, Zips> map = new HashMap<String, Zips>();

		List<Zips> zips = sessionFactory.getCurrentSession()
				.createQuery("from Zips where county=?")
				.setParameter(0, county).getResultList();

		if(zips!=null) {
			zips.forEach(zip -> map.put(zip.getZip(), zip));
		}
		
		return map;
	}

}
