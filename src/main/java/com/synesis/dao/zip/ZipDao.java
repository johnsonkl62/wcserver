package com.synesis.dao.zip;

import java.util.List;
import java.util.Map;

import com.synesis.model.Zips;

public interface ZipDao {

	List<Zips> findAllZips();
	
	List<Zips> findZipByCounty(String county);

	Map<String, Zips> findAllZipsByCounty(String county);
	
}
