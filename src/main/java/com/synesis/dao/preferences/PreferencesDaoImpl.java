package com.synesis.dao.preferences;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Preferences;

@Repository
public class PreferencesDaoImpl implements PreferencesDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void updatePreferences(String username, String[] codes) {
		
		sessionFactory.getCurrentSession().createQuery("delete from Preferences p where p.username=?")
		.setParameter(0, username)
		.executeUpdate();
		
		if(codes!=null && codes.length!=0) {
			
			for(String code : codes) {
				Preferences preferences = new Preferences();
				preferences.setUsername(username);
				preferences.setCode(code);
				sessionFactory.getCurrentSession().save(preferences);
			}
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Preferences> getAllPreferencesByUsername(String username) {
		
		List<Preferences> list = null;
		try {
			list = sessionFactory.getCurrentSession().createQuery("select p from Preferences p where p.username=?")
			.setParameter(0, username)
			.getResultList();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
