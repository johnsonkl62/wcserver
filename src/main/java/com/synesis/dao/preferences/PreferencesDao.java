package com.synesis.dao.preferences;

import java.util.List;

import com.synesis.model.Preferences;

public interface PreferencesDao {

	public void updatePreferences(String username,String[] codes);

	public List<Preferences> getAllPreferencesByUsername(String username);
	
}
