package com.synesis.dao.users;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.persistence.NoResultException;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import com.synesis.beans.ProfileBean;
import com.synesis.model.Profile;
import com.synesis.model.Template;
import com.synesis.model.users.PasswordResetToken;
import com.synesis.model.users.UserRole;
import com.synesis.utility.SendMailUtility;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@SuppressWarnings("unchecked")
	public Profile findByUserName(String username) {

		List<Profile> users = new ArrayList<Profile>();

		users = sessionFactory.getCurrentSession().createQuery("from Profile where username=?").setParameter(0, username)
				.getResultList();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}
	
	public void saveUser( ProfileBean profileBean) {
		
		
		if(profileBean!=null) {
			Profile profile = new Profile();
			profile.setAgency(profileBean.getAgency());
			profile.setEmail(profileBean.getEmail());
			profile.setFirstName(profileBean.getFirstName());
			profile.setLastName(profileBean.getLastName());
			profile.setMiddleName(profileBean.getMiddleInitial());
			profile.setPhoneNo(profileBean.getPhone());
			profile.setSuffix(profileBean.getSuffix());
			profile.setUsername(profileBean.getUsername());
			profile.setPassword(passwordEncoder.encode(profileBean.getPassword()));
			profile.setCarrier1(profileBean.getCarrier1());
			profile.setCarrier2(profileBean.getCarrier2());
			profile.setCarrier3(profileBean.getCarrier3());
//			profile.setTemplate(profileBean.getTemplate());
			
			sessionFactory.getCurrentSession().save(profile);
			
			if(profileBean.getTemplateBean()!=null && profileBean.getTemplateBean().getTemplate()!=null) {
				Template template = new Template();
				template.setPriority(1);
				template.setTemplate(profileBean.getTemplateBean().getTemplate());
				template.setUsername(profileBean.getUsername());
				sessionFactory.getCurrentSession().save(template);
				
			}
			
			
			UserRole userRole = new UserRole(profile, "ROLE_USER");
			sessionFactory.getCurrentSession().save(userRole);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean sendEmailAndUpdateToken(String url, String email) {
		
		List<Profile> users = sessionFactory.getCurrentSession().createQuery("from Profile where email=?").setParameter(0, email)
				.getResultList();

		if (users.size() > 0) {
			Profile user = users.get(0);
			
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			
			String token = UUID.randomUUID().toString();
			PasswordResetToken myToken = new PasswordResetToken();
			myToken.setToken(token);
			myToken.setProfile(user);
			myToken.setExpiryDate(calendar.getTime());
			
			sessionFactory.getCurrentSession().save(myToken);
			
			
			SendMailUtility sendMailUtility = new SendMailUtility();
			try {
				sendMailUtility.sendEmail(url, token, email);
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			
			return true;
		} 	else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean resetPassword(String token, String email) {
		
		List<PasswordResetToken> users = sessionFactory.getCurrentSession().createQuery("from PasswordResetToken where profile.email=? and token=?")
				.setParameter(0, email)
				.setParameter(1, token)
				.getResultList();
		
		if (users.size() > 0) {
			return true;
		} 
		
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean updatePassword(String password, String email) {
		
		
		List<Profile> users = sessionFactory.getCurrentSession().createQuery("from Profile where email=?").setParameter(0, email)
				.getResultList();

		if (users.size() > 0) {
			Profile user = users.get(0);
			user.setPassword(passwordEncoder.encode(password));
			sessionFactory.getCurrentSession().update(user);
			return true;
		}
		
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkUserName(String username) {
		
		List<Profile> users = sessionFactory.getCurrentSession().createQuery("from Profile where username=?")
				.setParameter(0, username)
				.getResultList();

		
		if(users!=null && !users.isEmpty()) {
			return true;
		}
		
		return false;
		
	}

	@Override
	public Profile getUserProfile(String username) {
		
		Profile profile = (Profile) sessionFactory.getCurrentSession().createQuery("from Profile where username=?")
				.setParameter(0, username)
				.getSingleResult();

		
		return profile;
	}

	@Override
	public void updateUser(ProfileBean profileBean) {
		
		
		Profile profile = (Profile) sessionFactory.getCurrentSession().createQuery("from Profile where username=?")
				.setParameter(0, profileBean.getUsername())
				.getSingleResult();
		
		
		if(profileBean!=null) {
			profile.setAgency(profileBean.getAgency());
			profile.setFirstName(profileBean.getFirstName());
			profile.setLastName(profileBean.getLastName());
			profile.setMiddleName(profileBean.getMiddleInitial());
			profile.setPhoneNo(profileBean.getPhone());
			profile.setSuffix(profileBean.getSuffix());
			
			Template template = getUserTemplate(profileBean.getUsername());
			
			
			if(profileBean.getTemplateBean()!=null && profileBean.getTemplateBean().getTemplate()!=null) {
				if(template==null) {
					template = new Template();
				} 
				
				template.setPriority(1);
				template.setTemplate(profileBean.getTemplateBean().getTemplate());
				template.setUsername(profileBean.getUsername());
				sessionFactory.getCurrentSession().saveOrUpdate(template);
				
			}
			
			sessionFactory.getCurrentSession().update(profile);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkEmail(String email) {
		
		List<Profile> users;
		try {
			users = sessionFactory.getCurrentSession().createQuery("from Profile where email=?")
					.setParameter(0, email)
					.getResultList();
		} catch (NoResultException e) {
			return false;
		}

		
		if(users!=null && !users.isEmpty()) {
			return true;
		}
		
		
		return false;
	}

	public Profile findByEmail(String email) {
		
		Profile profile;
		try {
			@SuppressWarnings("unchecked")
			List<Profile> profiles = sessionFactory.getCurrentSession().createQuery("from Profile where email=?")
					.setParameter(0, email)
					.getResultList();

			profile = profiles.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return profile;
	}

	@Override
	public Template getUserTemplate(String username) {
		Template template;
		try {
			template = (Template) sessionFactory.getCurrentSession().createQuery("from Template where username=?")
					.setParameter(0, username)
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		
		return template;		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Profile> getAllUsers() {
		
		List<Profile> profiles =  sessionFactory.getCurrentSession().createQuery("from Profile")
				.getResultList();
		
		return profiles;
	}


	@Override
	public List<Profile> getAllAdmins() {
		List<Profile> everyone = getAllUsers();

		List<Profile> profiles = new ArrayList<Profile>();
		for (Profile p : everyone) {
			if (p.getUserRole() != null) {
				if (p.getUserRole().size() > 0) {
					Iterator<UserRole> it = p.getUserRole().iterator();
					while (it.hasNext()) {
						UserRole ur = it.next();
						if ("ROLE_ADMIN".equalsIgnoreCase(ur.getRole())) {
							profiles.add(p);
						}
					}
				}
			}
		}
		return profiles;
	}


	@Override
	public List<Profile> getUsersByMonth(String purchaseMonth) {
		@SuppressWarnings("unchecked")
		List<Profile> profiles =  sessionFactory.getCurrentSession().createQuery("from Profile p, Purchase b where p.username=b.username and b.month=?").setParameter(0, purchaseMonth)
				.getResultList();
		
		return profiles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isOldPasswordMatched(String username, String oldPassword) {
		

		List<Profile> users  = sessionFactory.getCurrentSession().createQuery("from Profile where username=?").setParameter(0, username)
				.getResultList();

		if (users.size() > 0) {
			Profile p =  users.get(0);
			return passwordEncoder.matches(oldPassword, p.getPassword());
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean changePassword(String newPassword, String username) {
		
		List<Profile> users = sessionFactory.getCurrentSession().createQuery("from Profile where username=?")
				.setParameter(0, username)
				.getResultList();

		if (users.size() > 0) {
			Profile user = users.get(0);
			user.setPassword(passwordEncoder.encode(newPassword));
			sessionFactory.getCurrentSession().update(user);
			return true;
		}
		
		return false;
	}

}