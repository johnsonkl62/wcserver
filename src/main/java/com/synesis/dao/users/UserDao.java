package com.synesis.dao.users;

import java.util.List;

import com.synesis.beans.ProfileBean;
import com.synesis.model.Profile;
import com.synesis.model.Template;



public interface UserDao {

	Profile findByUserName(String username);

	void saveUser(ProfileBean profileBean);

	boolean sendEmailAndUpdateToken(String url, String email);

	boolean resetPassword(String token, String email);

	boolean updatePassword(String password, String email);

	boolean checkUserName(String username);

	public Profile getUserProfile(String username);

	public void updateUser(ProfileBean p);

	boolean checkEmail(String email);

	Profile findByEmail(String email);

	Template getUserTemplate(String username);

	List<Profile> getAllUsers();

	List<Profile> getAllAdmins();

	List<Profile> getUsersByMonth(String purchaseMonth);

	boolean isOldPasswordMatched(String username, String oldPassword);

	boolean changePassword(String newPassword, String username);
	
}