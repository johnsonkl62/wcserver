package com.synesis.dao.statistics;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.beans.StatsBean;
import com.synesis.beans.ZipsBean;
import com.synesis.database.CountyStats;
import com.synesis.model.Purchase;
import com.synesis.model.SubscriptionInfo;
import com.synesis.service.subscription.SubscriptionService;
import com.synesis.utility.MonthEnum;

@Repository
public class StatisticsDaoImpl implements StatisticsDao {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private SubscriptionService subscriptionService;
	
	public Calendar thisMonth = Calendar.getInstance();
	public static final String[] MONTH = {"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
	
	public StatisticsDaoImpl() {
		thisMonth.set(Calendar.DAY_OF_MONTH, 1);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Integer> getCountyTotalCounts(List<String> counties) {
		

		Map<String, Integer> map = new HashMap<String, Integer>();
		
		List<Object[]> employersCountList = sessionFactory.getCurrentSession()
				.createQuery("select county,count(*) as cnt from Employers where county is not null and county in (:county)"
						+ " group by county order by county")
				.setParameter(0, counties).getResultList();

		if(employersCountList!=null && !employersCountList.isEmpty()) {
			for(Object obj[] : employersCountList) {
				map.put(String.valueOf(obj[0]), Integer.parseInt(obj[1]+""));
			}
		}
		
		return map;
	}

	@Override
	public Map<String, Integer> getCountyCompletedCounts(
			List<String> counties) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		@SuppressWarnings("unchecked")
		List<Object[]> employersCountList = sessionFactory.getCurrentSession()
				.createQuery("select county,count(*) as cnt from Employers where county is not null and carrier is not null and carrier!='CANCELLED' and county in (:county)"
						+ " group by county order by county")
				.setParameter(0, counties).getResultList();

		if(employersCountList!=null && !employersCountList.isEmpty()) {
			for(Object obj[] : employersCountList) {
				map.put(String.valueOf(obj[0]), Integer.parseInt(obj[1]+""));
			}
		}
		
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, CountyStats> getAllCountyStats(String[] comps, List<String> counties,boolean isAdmin) {
		
		TreeMap<String,CountyStats> countyStats = new TreeMap<String,CountyStats>();

		//
		// "ADAMS" --> { (total, updated, comps), {"JAN" --> (t,u,c), "FEB" --> (t,u,c), etc... } }
		// "ALLEGHENY" ==> { (total, updated, comps), {"JAN" --> (t,u,c), "FEB" --> (t,u,c), etc... } }
		// "ARMSTRONG" --> { (total, updated, comps), {"JAN" --> (t,u,c), "FEB" --> (t,u,c), etc... } }
		// "BEAVER" --> { (total, updated, comps), {"JAN" --> (t,u,c), "FEB" --> (t,u,c), etc... } }
		// "BEDFORD" --> { (total, updated, comps), {"JAN" --> (t,u,c), "FEB" --> (t,u,c), etc... } }
		// ... 
		// "YORK" --> { (total, updated, comps), {"JAN" --> (t,u,c), "FEB" --> (t,u,c), etc... } }
		//

		if (counties==null || counties.isEmpty()) {
			return countyStats;
		}

		//  Get the total number of active employers for each county
		List<Object[]> employersCountList = sessionFactory.getCurrentSession()
				.createNativeQuery("select county,count(*) as cnt from employers where carrier!='CANCELLED' and county in (:county)"
						+ " group by county order by county")
				.setParameter("county", counties).getResultList();
		
		if (employersCountList!=null && !employersCountList.isEmpty()) {
			for(Object obj[] : employersCountList) {
				
				CountyStats cs = countyStats.get(String.valueOf(obj[0]));
				if ( cs == null ) {
					cs = new CountyStats(String.valueOf(obj[0]));
					cs.setTotal(Integer.parseInt(obj[1]+""));
					countyStats.put(String.valueOf(obj[0]), cs);
				} 
			}
		}
		
		//  Get the total number of active employers successfully retrieved for each county (carrier is non-null and not 'cancelled')
		 employersCountList = sessionFactory.getCurrentSession()
				.createQuery("select county,count(*) as cnt from Employers where county is not null and "
						+ " carrier!='CANCELLED' and county in (:county)"
						+ " group by county order by county")
				.setParameter("county", counties).getResultList();

		if (employersCountList!=null && !employersCountList.isEmpty()) {
			for(Object obj[] : employersCountList) {
				CountyStats cs = countyStats.get(String.valueOf(obj[0]));
				if ( cs != null ) {
					cs.setDone(Integer.parseInt(obj[1]+""));
					countyStats.put(String.valueOf(obj[0]), cs);
				} 
			}
		}
		 
		 //  Get count of comparisons
		 StringBuffer sb = new StringBuffer();
		 if ( comps.length > 0 ) {
			 sb.append(" and (");
			 for ( int i = 0; i < comps.length; i++ ) {
				 if ( i != 0 ) {
					 sb.append(" or ");
				 }
				 sb.append("carrier like '%" + comps[i] + "%'");
			 }
			 sb.append(")");
		 }

		 //  Get total number of active employers with one of the selected carriers
		 employersCountList = sessionFactory.getCurrentSession()
					.createQuery("select county, count(*) as c from Employers "
							+ " where county is not null " + sb.toString() + " and county in (:county) "
							+ " group by county order by county")
					.setParameter("county", counties).getResultList();

			if (employersCountList!=null && !employersCountList.isEmpty()) {
				for(Object obj[] : employersCountList) {
					CountyStats cs = countyStats.get(String.valueOf(obj[0]));
					if ( cs != null ) {
						cs.setComps(Integer.parseInt(obj[1]+""));
						countyStats.put(String.valueOf(obj[0]), cs);
					} 
				}
			}
		
		
		 //  Get total number of active employers with one of the selected carriers by month
		 employersCountList = sessionFactory.getCurrentSession()
					.createQuery("select county, month(xdate) as mn, count(*) as c from Employers "
							+ " where county is not null and county in (:county) and carrier!='CANCELLED' group by county,month(xdate) "
							+ " order by county,month(xdate)")
					.setParameter("county", counties).getResultList();
	
			if(employersCountList!=null && !employersCountList.isEmpty()) {
				for(Object obj[] : employersCountList) {
					
					String cName = String.valueOf(obj[0]);
					Integer month = Integer.parseInt(obj[1]+"");
					Integer count = Integer.parseInt(obj[2]+"");
	
					CountyStats cs = countyStats.get(cName);
					if ( cs != null ) {
						cs.setTotal(MONTH[month-1], count);
						countyStats.put(String.valueOf(obj[0]), cs);
					}
				}
			}
		
			employersCountList = sessionFactory.getCurrentSession()
					.createQuery("select county, month(xdate) as mn, count(*) as c from Employers "
							+ " where county is not null and carrier is not null and carrier!='CANCELLED' and county in (:county) group by county,month(xdate) "
							+ " order by county,month(xdate)")
					.setParameter("county", counties).getResultList();
	
			if(employersCountList!=null && !employersCountList.isEmpty()) {
				for(Object obj[] : employersCountList) {
					
					String cName = String.valueOf(obj[0]);
					Integer month = Integer.parseInt(obj[1]+"");
					Integer count = Integer.parseInt(obj[2]+"");
	
					CountyStats cs = countyStats.get(cName);
					if ( cs != null ) {
						cs.setDone(MONTH[month-1], count);
						countyStats.put(String.valueOf(obj[0]), cs);
					}
				}
			}
			
			employersCountList = sessionFactory.getCurrentSession()
					.createQuery("select county, month(xdate) as mn, count(*) as c from Employers "
							+ " where county is not null " + sb.toString() + " and county in (:county) "
							+ " and carrier!='CANCELLED'"
							+ " group by county,month(xdate) "
							+ " order by county,month(xdate)")
					.setParameter("county", counties).getResultList();
	
			if(employersCountList!=null && !employersCountList.isEmpty()) {
				for(Object obj[] : employersCountList) {
					
					String cName = String.valueOf(obj[0]);
					Integer month = Integer.parseInt(obj[1]+"");
					Integer count = Integer.parseInt(obj[2]+"");
	
					CountyStats cs = countyStats.get(cName);
					if ( cs != null ) {
						cs.setComps(MONTH[month-1], count);
						countyStats.put(String.valueOf(obj[0]), cs);
					}
				}
			}
			

			List<SubscriptionInfo> subscriptions = sessionFactory.getCurrentSession()
					.createQuery("select o from SubscriptionInfo o where county in (:county) and active='true' order by county")
					.setParameter("county", counties).getResultList();
	
			if (subscriptions !=null && !subscriptions.isEmpty()) {
				for(SubscriptionInfo subscription : subscriptions) {
					CountyStats cs = countyStats.get(subscription.getCounty());
					if ( cs != null ) {
						String[] zips = subscriptionService.getZips(subscription);
						cs.setReservedPartial( zips != null );
						cs.setReservedFull( zips == null );
					}
				}
			}

			List<Purchase> purchases = sessionFactory.getCurrentSession()
					.createQuery("select o from Purchase o where county is not null and county in (:county) ")
					.setParameter("county", counties).getResultList();
	
			if(purchases!=null && !purchases.isEmpty()) {
				for(Purchase purchase : purchases) {
//					if ( !"guest".equals(subscription.getUsername()) && (subscription.getProfile().getAgency() != null && !subscription.getProfile().getAgency().startsWith("First Aff"))) {
//						if(!isAdmin) {
						CountyStats cs = countyStats.get(purchase.getCounty());
						if ( cs != null ) {
							cs.setPurchase(purchase.getUsername(), purchase.getMonth(), purchase.getBuyDate(),purchase.getZip());
						}
//					}
				}
			}
		return countyStats;
	}

	@Override
	public List<String> getAllDistinctCounties() {

		@SuppressWarnings("unchecked")
		List<String> counties = sessionFactory.getCurrentSession()
				.createNativeQuery("select distinct a.county from employers a where county is not null and county!='EXAMPLE'")
				.getResultList();

		
		return counties;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String,ZipsBean> updateUsingZip(String[] comps, String county,String month,boolean isAdmin) {
		
		Map<String, ZipsBean> map = new HashMap<String, ZipsBean>();

		MonthEnum monthEnum = MonthEnum.valueOf(month);

		List<Object[]> employersCountList = sessionFactory.getCurrentSession()
				.createQuery(
						"select county,zip,count(*) as cnt from Employers where county is not null and county = :county"
								+ " and month(xdate)=:month and carrier!='CANCELLED' group by county,zip order by county,zip")
				.setParameter("county", county).setParameter("month", monthEnum.getIndex()).getResultList();

		if (employersCountList != null && !employersCountList.isEmpty()) {
			for (Object obj[] : employersCountList) {

				ZipsBean bean = new ZipsBean();
				bean.setCounty(county);
				bean.setZip(obj[1] + "");
				bean.setTotal(Integer.parseInt(obj[2] + ""));
				map.put(obj[1] + "", bean);
			}
		}

		employersCountList = sessionFactory.getCurrentSession()
				.createQuery("select county,zip,count(*) as cnt from Employers where county is not null and "
						+ " carrier is not null and carrier!='CANCELLED' and county  = :county"
						+ " and month(xdate)=:month group by county,zip order by county,zip")
				.setParameter("county", county).setParameter("month", monthEnum.getIndex()).getResultList();

		if (employersCountList != null && !employersCountList.isEmpty()) {
			for (Object obj[] : employersCountList) {
				ZipsBean bean = map.get(obj[1] + "");
				if (bean != null) {
					bean.setAvailable(Integer.parseInt(obj[2] + ""));
					map.put(obj[1] + "", bean);
				}
			}
		}

		//  Get count of comparisons
		StringBuffer sb = new StringBuffer();
		if (comps.length > 0) {
			sb.append(" and (");
			for (int i = 0; i < comps.length; i++) {
				if (i != 0) {
					sb.append(" or ");
				}
				sb.append("carrier like '%" + comps[i] + "%'");
			}
			sb.append(")");
		}
		employersCountList = sessionFactory.getCurrentSession()
				.createQuery("select county,zip, month(xdate) as mn, count(*) as c from Employers "
						+ " where county is not null and carrier!='CANCELLED' " + sb.toString() + " and county = :county "
						+ "  and month(xdate)=:month group by county,zip,month(xdate) "
						+ " order by county,zip,month(xdate)")
				.setParameter("county", county).setParameter("month", monthEnum.getIndex()).getResultList();

		if (employersCountList != null && !employersCountList.isEmpty()) {
			for (Object obj[] : employersCountList) {

				String zip = obj[1] + "";
				Integer count = Integer.parseInt(obj[3] + "");

				ZipsBean bean = map.get(zip);
				if (bean != null) {
					bean.setIneligible(count);
					map.put(zip, bean);
				}
			}
		}						
			
		List<Purchase> purchases = sessionFactory.getCurrentSession()
				.createQuery("select o from Purchase o where county is not null and county = :county ")
				.setParameter("county", county).getResultList();

		if (purchases != null && !purchases.isEmpty()) {

			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.MONTH, monthEnum.getIndex() - 1);

			for (Purchase purchase : purchases) {
				if ( month.equalsIgnoreCase(purchase.getMonth())) {
					ZipsBean bean = map.get(purchase.getZip());
					if (bean != null) {
						bean.setPurchased(true);
//						bean.setTotal(bean.getTotal() - 1);
//						bean.setAvailable(bean.getAvailable() - 1);
						map.put(purchase.getZip(), bean);
					} else {
						// purchase of full county
						for (String zip : map.keySet()) {
							map.get(zip).setPurchased(true);
						}
					}
				}
			}
		}

		List<SubscriptionInfo> subscriptions = sessionFactory.getCurrentSession()
				.createQuery("select o from SubscriptionInfo o where county = :county and active='true' ")
				.setParameter("county", county).getResultList();

		if(subscriptions!=null && !subscriptions.isEmpty()) {
			for(SubscriptionInfo subscription : subscriptions) {
				String[] zips = subscriptionService.getZips(subscription);
				if ( zips != null ) {
					for ( String zip : zips ) {
						ZipsBean bean = map.get(zip);
						if ( bean != null ) {
							bean.setSubscribed(true);
							map.put(zip, bean);
						}						
					}
				} else {
					for (String zip : map.keySet() ) {
						map.get(zip).setSubscribed(true);
					}
				}
			}
		}

		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String,ZipsBean> fetchCountsByZip(String[] comps, String county){

		Map<String,ZipsBean> map = new LinkedHashMap<String, ZipsBean>();
		
		StringBuffer sb = new StringBuffer();
		if ( comps.length > 0 ) {
			sb.append(" (");
			for ( int i = 0; i < comps.length; i++ ) {
				if ( i != 0 ) {
					sb.append(" or ");
				}
				sb.append("carrier like '%" + comps[i] + "%'");
			}
			sb.append(")");
		}

		List<Object[]> employersCountList = sessionFactory.getCurrentSession()
				.createNativeQuery("SELECT A.zip,A.totalEmp,B.availableEmp,C.ineligible FROM " +
								"(SELECT zip,COUNT(*) AS totalEmp FROM employers " +
								" WHERE county = :county and carrier!='CANCELLED' GROUP BY zip ORDER BY zip) AS A " +
								"INNER JOIN " +
								"(SELECT zip,COUNT(*) AS availableEmp FROM employers " +
								"  WHERE carrier IS NOT NULL and carrier!='CANCELLED' AND county = :county GROUP BY zip ORDER BY zip) AS B " +
								"ON A.zip = B.zip " +
								"INNER JOIN " +
								"(SELECT SUM(CASE WHEN (" + sb + 
								") THEN 1 ELSE 0 END) AS ineligible,zip " +
								"FROM employers WHERE county = :county and carrier!='CANCELLED' GROUP BY zip ORDER BY zip) AS C " +
								"ON B.zip = C.zip GROUP BY zip ORDER BY zip")

//						"select FirstSet.county,FirstSet.zip,FirstSet.totalEmp,SecondSet.availableEmp,ThirdSet.ineligible,FirstSet.month from "+ 
//						"("+
//						    "select county,zip,month(xdate) as month,count(*) as totalEmp from employers "+ 
//						    "where county is not null and county = :county "+ 
//							"group by county,zip,month(xdate) order by county,zip,month(xdate)"+
//						") as FirstSet "+
//						"inner join "+
//						"("+
//						    "select county,zip,month(xdate) as month,count(*) as availableEmp from employers "+ 
//						    "where carrier is not null and county = :county "+
//							"group by county,zip,month(xdate) order by county,zip,month(xdate) "+
//						") as SecondSet "+
//						"on FirstSet.county = SecondSet.county "+
//						"AND FirstSet.zip = SecondSet.zip "+
//						"AND FirstSet.month = SecondSet.month "+
//						"inner join "+ 
//						"("+
//							"select sum(case "+
//							"WHEN ("+sb+") Then 1 "+
//						    "ELSE 0 "+
//						    "END) as ineligible, "+
//						"county,zip,month(xdate) as month from employers "+ 
//						    "where county is not null  "+
//						        "and county = :county "+
//							"group by county,zip,month(xdate) order by county,zip,month(xdate) "+
//						") as ThirdSet "+
//						"on SecondSet.county = ThirdSet.county "+
//						"AND SecondSet.zip = ThirdSet.zip "+
//						"AND SecondSet.month = ThirdSet.month "+
//						"group by county,zip,month order by county,zip,month ")
				.setParameter("county", county)
				.getResultList();

		LocalDate now = LocalDate.now();
		String currentMonth = now.getMonth().name().substring(0,3);

		LocalDate subscrDefault = now.plusMonths(3);
		String defaultMonth = subscrDefault.getMonth().name().substring(0,3);

		if(employersCountList!=null && !employersCountList.isEmpty()) {
			for(Object obj[] : employersCountList) {

				String zip = obj[0]+"";
				int total = Integer.parseInt(obj[1]+"");
				int available = Integer.parseInt(obj[2]+"");
				int ineligible = Integer.parseInt(obj[3]+"");

				if ( (available - ineligible) >= 12 ) {
					ZipsBean bean = new ZipsBean();
					bean.setCounty(county);
					bean.setZip(zip);
					bean.setTotal(total/12);
					bean.setAvailable(available/12);
					bean.setIneligible(ineligible/12);
					bean.setMonthAvailable(defaultMonth);
					map.put(zip, bean);
				}
			}
		}

		List<Purchase> purchases = sessionFactory.getCurrentSession()
				.createQuery("select o from Purchase o where county is not null and county = :county ")
				.setParameter("county", county).getResultList();

		if(purchases!=null && !purchases.isEmpty()) {
			for(Purchase purchase : purchases) {
				String availMonth = MonthEnum.valueOf(purchase.getMonth()).next().name();

				ZipsBean bean = map.get(purchase.getZip());
				if ( bean != null ) {
					bean.setPurchased(true);
//					bean.setTotal(bean.getTotal()-1);
//					bean.setAvailable(bean.getAvailable()-1);
					bean.setMonthAvailable(getNewestMonth(currentMonth, availMonth, bean.getMonthAvailable()));
					map.put(purchase.getZip(), bean);
				} else {
					//  If bean is null, the whole county has been purchased
					for (String z : map.keySet()) {
						bean = map.get(z);
						bean.setPurchased(true);
						bean.setMonthAvailable(getNewestMonth(currentMonth, availMonth, bean.getMonthAvailable()));
					}
				}
			}
		}

		List<SubscriptionInfo> subscriptions = sessionFactory.getCurrentSession()
				.createQuery("select o from SubscriptionInfo o where county = :county and active='true' ")
				.setParameter("county", county).getResultList();

		if(subscriptions!=null && !subscriptions.isEmpty()) {
			for(SubscriptionInfo subscription : subscriptions) {
				String[] zips = subscriptionService.getZips(subscription);
				if ( zips != null ) {
					for ( String zip : zips ) {
						ZipsBean bean = map.get(zip);
						if ( bean != null ) {
							bean.setSubscribed(true);
							map.put(zip, bean);
						}						
					}
				} else {
					for (String zip : map.keySet()) {
						map.get(zip).setSubscribed(true);
					}
				}
			}
		}
		return map;
	}


	/**
	 * This function determines the month furthest in the future, which is the starting point of the
	 * subscription.  There is a seven-month window between the current date and the display of a
	 * month for individual sale (with the exception of special full-year sales which is no longer
	 * necessary due to the subscription implementation).  The month index cannot be used directly
	 * for obvious reasons since 1 (JAN) would be great than 12 (DEC) if the start month is NOV.
	 * 
	 * Examples:  Today is JAN, p1 = APR, p2 = FEB		return APR	(AUG to DEC are not displayed)
	 *            Today is OCT, p1 = DEC, p2 = FEB		return FEB	(MAY to SEP are not displayed)
	 * 
	 * @param cur	3-character String representation of today's month
	 * @param p1	3-character String representation of one of the months to compare
	 * @param p2	3-character String representation of the other month to compare
	 * @return		Either p1 or p2
	 */
	private String getNewestMonth(String cur, String p1, String p2) {
		int curMonth = MonthEnum.valueOf(cur).ordinal();
		int p1m = (MonthEnum.valueOf(p1).ordinal() + 12 - curMonth) % 12;
		int p2m = (MonthEnum.valueOf(p2).ordinal() + 12 - curMonth) % 12;
		return (p1m > p2m ? p1 : p2);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, StatsBean> getAvailableAdminStats(boolean isCarrierEmpty, boolean isObsoleteCheckbox,
			boolean isCollectedCheckbox,String fromDate, String toDate) {
		
		
		Map<String, StatsBean> resultMap = new TreeMap<String, StatsBean>();
		
		StringBuilder query = new StringBuilder("select county,month(xdate) as month, count(*) from employers where county not in (select distinct(county) from Purchase) ");

		if(isCarrierEmpty) {
			query.append(" and (carrier is null or carrier='') ");
		}
		
		if(isObsoleteCheckbox) {
			query.append(" and updated <  '"+toDate+"'");
		}
		
		if(isCollectedCheckbox) {
			query.append(" and updated between  '"+fromDate+"' and '"+toDate+"'");
		}
		
		query.append(" and (county is not null or county!='') group by county,month order by county ");
		
		List<Object[]> countiesCountList = sessionFactory.getCurrentSession()
				.createNativeQuery(query.toString())
				.getResultList();

		extractResultMap(resultMap, countiesCountList);
		
		
		return resultMap;
	}

	private void extractResultMap(Map<String, StatsBean> resultMap,
			List<Object[]> countiesCountList) {
		if(countiesCountList!=null && !countiesCountList.isEmpty()) {
			for(Object[] result : countiesCountList) {
				
				String county = (String) result[0];
				if(resultMap.containsKey(county)) {
					
					StatsBean bean = resultMap.get(county);
					bean.addMonth(Integer.parseInt(result[1]+""), Integer.parseInt(result[2]+""));
					resultMap.put(county, bean);
					
				} else {
					StatsBean bean = new StatsBean();
					bean.setCounty(county);
					bean.addMonth(Integer.parseInt(result[1]+""), Integer.parseInt( result[2]+""));
					resultMap.put(county, bean);
				}
				
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, StatsBean> getSubscribedAdminStats(boolean isCarrierEmpty, boolean isObsoleteCheckbox,
			boolean isCollectedCheckbox,String fromDate, String toDate) {
		
		
		Map<String, StatsBean> resultMap = new TreeMap<String, StatsBean>();
		
		StringBuilder query = new StringBuilder("select county,month(xdate) as month, count(*) from employers where county  in (select distinct(county) from Purchase) ");

		if(isCarrierEmpty) {
			query.append(" and (carrier is null or carrier='') ");
		}
		
		if(isObsoleteCheckbox) {
			query.append(" and updated <  '"+toDate+"'");
		}
		
		if(isCollectedCheckbox) {
			query.append(" and updated between  '"+fromDate+"' and '"+toDate+"'");
		}
		
		query.append("  group by county,month order by county ");
		
		List<Object[]> countiesCountList = sessionFactory.getCurrentSession()
				.createNativeQuery(query.toString())
				.getResultList();

		extractResultMap(resultMap, countiesCountList);
		
		
		return resultMap;
	}

	
	
	
}
