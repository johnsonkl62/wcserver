package com.synesis.dao.statistics;

import java.util.List;
import java.util.Map;

import com.synesis.beans.StatsBean;
import com.synesis.beans.ZipsBean;
import com.synesis.database.CountyStats;

public interface StatisticsDao {

	public Map<String, Integer> getCountyTotalCounts(List<String> counties);
	
	public Map<String, Integer> getCountyCompletedCounts(List<String> counties);
	
	public Map<String,CountyStats> getAllCountyStats(String[] comps, List<String> counties,boolean isAdmin);
	
	public List<String> getAllDistinctCounties();

	public Map<String,ZipsBean> updateUsingZip(String[] comps, String county,String month,boolean isAdmin);
	
	public Map<String,ZipsBean> fetchCountsByZip(String[] comps, String county);

	public Map<String, StatsBean> getAvailableAdminStats(boolean isCarrierEmpty, boolean isObsoleteCheckbox,boolean isCollectedCheckbox, String fromDate, String toDate);
	
	public Map<String, StatsBean> getSubscribedAdminStats(boolean isCarrierEmpty, boolean isObsoleteCheckbox,boolean isCollectedCheckbox, String fromDate, String toDate);
}
