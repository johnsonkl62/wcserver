package com.synesis.dao.announcement;

import java.util.List;

import com.synesis.model.Announcements;

public interface AnnouncementDao {

	Announcements saveAnnouncement(String text);

	List<Announcements> getAllAnnouncements();
}
