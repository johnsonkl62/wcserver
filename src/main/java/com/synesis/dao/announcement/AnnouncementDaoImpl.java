package com.synesis.dao.announcement;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Announcements;
import com.synesis.model.Profile;

@Repository
public class AnnouncementDaoImpl implements AnnouncementDao {

	@Autowired
	private SessionFactory sessionFactory;

	
	@Override
	public Announcements saveAnnouncement(String text) {
		
		Announcements announcement = new Announcements();
		announcement.setActive(true);
		announcement.setText(text);
		announcement.setPostedDate(new Timestamp(System.currentTimeMillis()));
		
		sessionFactory.getCurrentSession().save(announcement);
		
		return announcement;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Announcements> getAllAnnouncements() {
		

		List<Announcements> announcements = sessionFactory.getCurrentSession()
				.createQuery("from Announcements order by postedDate desc")
				.getResultList();

		
		return announcements;
	}

	
	
}
