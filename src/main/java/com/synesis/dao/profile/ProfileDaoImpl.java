package com.synesis.dao.profile;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Profile;
import com.synesis.model.Template;


@Repository
public class ProfileDaoImpl implements ProfileDao {

	
	@Autowired
	private SessionFactory sessionFactory;

	
	@Override
	public Profile saveProfile(String user, String last, String first,
			String middle, String sfx, String ag, String em, String ph,byte[] templateByte) {
		
		Profile profile  = new Profile();
		profile.setUsername(user);
		profile.setLastName(last);
		profile.setFirstName(first);
		profile.setMiddleName(middle);
		profile.setSuffix(sfx);
		profile.setAgency(ag);
		profile.setEmail(em);
		profile.setPhoneNo(ph);
//		profile.setTemplate(templateByte);
		
		sessionFactory.getCurrentSession().save(profile);
		
		if(templateByte!=null) {
			Template template = new Template();
			template.setPriority(1);
			template.setTemplate(templateByte);
			template.setUsername(user);
			sessionFactory.getCurrentSession().save(template);
			
		}
		
		return profile;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isProfilePresent(String username) {

		List<Profile> profiles = new ArrayList<Profile>();

		profiles = sessionFactory.getCurrentSession().createQuery("from Profile where username=?").setParameter(0, username)
				.getResultList();

		if (profiles.size() != 0) {
			return true;
		} 
		
		return false;
	}

	@Override
	public Profile getProfileByUsername(String username) {
		
		Profile profile = (Profile) sessionFactory.getCurrentSession().createQuery("from Profile where username=?")
				.setParameter(0, username).getSingleResult();

		return profile;
	}

}
