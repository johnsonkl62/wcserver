package com.synesis.dao.profile;

import com.synesis.model.Profile;

public interface ProfileDao {

	Profile saveProfile(String user, String last, String first, String middle, String sfx, String ag, String em, String ph,byte[] template);
	
	boolean isProfilePresent(String username);
	
	Profile getProfileByUsername(String username);
}
