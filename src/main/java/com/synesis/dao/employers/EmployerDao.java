//package com.synesis.dao.employers;
//
//import java.util.List;
//
//import com.synesis.model.Employers;
//
//
//public interface EmployerDao {
//
//	public List<Employers> getEmployers(String nameClause, String gccClause, String carrierClause, String authClause);
//
//	public void updateEmployer(String employerId, String code);
//
//	public Employers findOne(String employerId);
//	
//}
package com.synesis.dao.employers;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Employers;
import com.synesis.utility.AppUtility;

@Repository
public class EmployerDao {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<Employers> getEmployers(String ... clauses) {

		List<Employers> employers = sessionFactory.getCurrentSession().createQuery("from Employers e WHERE "
		+ AppUtility.combineClauses(clauses) + " ORDER BY e.name")
				.getResultList();

		return employers;
	}

	public void updateEmployer(String employerId, String code) {
		
		try {
			Employers employer = (Employers) sessionFactory.getCurrentSession().createQuery("from Employers e WHERE e.fn=:empId ")
							.setParameter("empId", employerId)
							.getSingleResult();
			
			if(employer!=null) {
				
				employer.setCode(code);
				sessionFactory.getCurrentSession().update(employer);
				sessionFactory.getCurrentSession().flush();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}

	public Employers findOne(String employerId) {
	
		try {
			Employers employer = (Employers) sessionFactory.getCurrentSession().createQuery("from Employers e WHERE e.fn=:empId ")
							.setParameter("empId", employerId)
							.getSingleResult();
			
			return employer;
			
		} catch (Exception e) {
			return null;
		}
		
	}

}
