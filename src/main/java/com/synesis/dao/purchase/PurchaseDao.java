package com.synesis.dao.purchase;

import java.util.Date;
import java.util.List;

import com.synesis.model.Profile;
import com.synesis.model.Purchase;


public interface PurchaseDao {

	List<Purchase> getPurchasesByUsername(String username);
	List<Purchase> getPurchasesBySubscrId(String subscr_id);
	List<Purchase> fetchPurchasesByZip(String zips);
	List<Purchase> fetchPurchasesByUserByMonth(String username, String month);
	List<Purchase> fetchPurchasesByCountyMonth(String county, String month);
	StringBuffer deletePurchasesByMonth(String month);
	
	Purchase saveExamplePurchase(String username);
	void removeExamplePurchase(String username);

	String savePurchase(String county, String username, String month, Date buyDate, String zips, String subscr_id);
}
