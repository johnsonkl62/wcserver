package com.synesis.dao.purchase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.dao.users.UserDao;
import com.synesis.model.Purchase;

@Repository
public class PurchaseDaoImpl implements PurchaseDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private UserDao userDao;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Purchase> getPurchasesByUsername(String username) {
		List<Purchase> purchases = new ArrayList<Purchase>();

		purchases = sessionFactory.getCurrentSession()
				.createQuery("from Purchase where username=?")
				.setParameter(0, username).getResultList();
		return purchases;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Purchase> getPurchasesBySubscrId(String subscr_id) {
		List<Purchase> purchases = new ArrayList<Purchase>();
		purchases = sessionFactory.getCurrentSession()
				.createQuery("from Purchase where subscr_id=?")
				.setParameter(0, subscr_id).getResultList();

		return purchases;
	}

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public Purchase saveExamplePurchase(String username) {
		Purchase purchase = null;
		try {
			purchase = new Purchase();
			purchase.setUsername(username);
			purchase.setCounty("EXAMPLE");
			purchase.setMonth(getExampleDataMonth());
			purchase.setBuyDate(sdf.parse(formatter.format(LocalDate.now())));
//			purchase.setProfile(profile);
			
			sessionFactory.getCurrentSession().save(purchase);
		} catch ( ParseException pe ) {
			pe.printStackTrace();
		}
		return purchase;
	}

	@Override
	public void removeExamplePurchase(String username) {
		try {
//			Profile profile = userDao.findByEmail(username);
//			if(profile!=null) {
				List<Purchase> slist = getPurchasesByUsername(username);
				for ( Purchase s : slist ) {
					if ("EXAMPLE".equals(s.getCounty()) ) {
						sessionFactory.getCurrentSession().delete(s);
					}
				}
//			}
		} catch ( Exception pe ) {
			pe.printStackTrace();
		}
	}

	public String getExampleDataMonth() {
		String month = "JAN";

		try {
			month = (String) sessionFactory.getCurrentSession()
					.createNativeQuery("select DATE_FORMAT(max(xdate),'%b') from employers where county='EXAMPLE'")
					.getSingleResult();
		} catch (HibernateException e) {
			e.printStackTrace();
		}

		return month.toUpperCase();
	}
	
	
	@Override
	public String savePurchase(String county, String username, String month, Date buyDate, String zips, String subscr_id) {
		
//		Profile profile = userDao.findByEmail(email);
		StringJoiner sj = new StringJoiner(",");

//		if ( profile != null ) {
			if ( zips != null && !zips.isEmpty() ) {
				String zipArray[] = zips.split(",");
				if ( zipArray != null ) {
					for ( String zip  :zipArray ) {
						sj.add(saveSinglePurchase(username,county,month,buyDate,zip,subscr_id).toString());
					}
				}
			} else {
				sj.add(saveSinglePurchase(username,county,month,buyDate,null,subscr_id).toString());
			}
//		}
		return sj.toString();
	}


	private Long saveSinglePurchase(String username, String county, String month, Date buyDate, String zip, String subscr_id) {
		Purchase purchase = new Purchase();
		purchase.setUsername(username);
		purchase.setCounty(county);
		purchase.setMonth(month);
		purchase.setBuyDate(buyDate);
		purchase.setZip(zip);
		purchase.setSubscrId(subscr_id);
		sessionFactory.getCurrentSession().save(purchase);
		return purchase.getId();
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<Purchase> fetchPurchasesByZip(String zips) {
		List<Purchase> purchases = new ArrayList<Purchase>();

		purchases = sessionFactory.getCurrentSession()
				.createQuery("from Purchase where zip in ("+zips+") order by zip, month")
				.getResultList();

		return purchases;
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Purchase> fetchPurchasesByCountyMonth(String county, String month) {
		List<Purchase> purchases = new ArrayList<Purchase>();

		purchases = sessionFactory.getCurrentSession()
				.createQuery("from Purchase where county=? and month=?")
				.setParameter(0, county).setParameter(1, month)
				.getResultList();

		return purchases;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Purchase> fetchPurchasesByUserByMonth(String username, String month) {
		List<Purchase> purchases = new ArrayList<Purchase>();

		purchases = sessionFactory.getCurrentSession()
				.createQuery("from Purchase where county!='EXAMPLE' and username=? and month=?")
				.setParameter(0, username)
				.setParameter(1, month)
				.getResultList();

		return purchases;
	}

	@SuppressWarnings("unchecked")
	@Override
	public StringBuffer deletePurchasesByMonth(String month) {
		StringBuffer sb = new StringBuffer("Purchase Purge - " + month + "\r\n");

		List<Purchase> dlist = sessionFactory.getCurrentSession().createQuery("from Purchase where month=?").setParameter(0, month).getResultList();

		for ( Purchase s : dlist ) {
			if (month.equals(s.getMonth()) ) {
				sb.append("INSERT INTO history (username,county,month,buyDate"
						+ (s.getZip() != null && s.getZip() != "" ? ",zip" : "")
						+ (s.getSubscrId() != null && s.getSubscrId() != "" ? ",subscr_id" : "")
						+ ") values (");
				sb.append("'" + s.getUsername() + "'," +
						"'" + s.getCounty() + "'," +
						"'" + s.getMonth() + "'," +
						"'" + dt.format(s.getBuyDate()) + "'" +
						(s.getZip() != null && s.getZip() != "" ? ",'" + s.getZip() + "'" : "") +
						(s.getSubscrId() != null && s.getSubscrId() != "" ? ",'" + s.getSubscrId() + "'" : "") 
						+ ");\r\n");
				sessionFactory.getCurrentSession().delete(s);
			}
		}
		return sb;
	}

	private static SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");

}
