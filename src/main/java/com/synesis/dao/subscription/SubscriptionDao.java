package com.synesis.dao.subscription;

import java.util.Date;
import java.util.List;

import com.synesis.model.SubscriptionInfo;


public interface SubscriptionDao {

	void saveSubscription(String county, String email, String startMonth, Date buyDate, String item_name, String subscr_id);
//	void saveSubscriptionWithZip(String county, String email, String startMonth, 
//			Date buyDate, String zips, String subscr_id);
	
	public List<SubscriptionInfo> fetchAllActiveSubscription();
	
	public void saveOrUpdatePaypalSubscription(SubscriptionInfo subscriptionInfo);

	void deactivateSubscription(String subscr_id);

	public List<SubscriptionInfo> getSubscriptionsByUsername(String username);

	public SubscriptionInfo getSubscriptionById(String subsId);
}
