package com.synesis.dao.subscription;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.dao.users.UserDao;
import com.synesis.model.SubscriptionInfo;
import com.synesis.utility.MonthEnum;

@Repository
public class SubscriptionDaoImpl implements SubscriptionDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private UserDao userDao;
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public String getExampleDataMonth() {
		String month = "JAN";

		try {
			month = (String) sessionFactory.getCurrentSession()
					.createNativeQuery("select DATE_FORMAT(max(xdate),'%b') from employers where county='EXAMPLE'")
					.getSingleResult();
		} catch (HibernateException e) {
			e.printStackTrace();
		}

		return month.toUpperCase();
	}
	
	
	@Override
	public void saveSubscription(String county, String username, String startMonth, Date buyDate, String item_name, String subscr_id) {
		
		//****   KLJ:  Enter a purchase only when paid for.  Paypal will initiate the subscription with
		//****   a charge for the "trial period" which will be now until the second available month of
		//****   data is to be charged.
		SubscriptionInfo subscriptionInfo = new SubscriptionInfo();
		subscriptionInfo.setUsername(username);
		subscriptionInfo.setCounty(county);
		subscriptionInfo.setSubscriptionDate(buyDate);
		subscriptionInfo.setActive("true");
//		subscriptionInfo.setZip(null);
		subscriptionInfo.setLastPaidMonth(MonthEnum.valueOf(startMonth).previous().name());
		subscriptionInfo.setSubscr_id(subscr_id);
		subscriptionInfo.setItem(item_name);
//		subscriptionInfo.setPurchaseIds(purId);		//  Cannot get this until payment is made.
		sessionFactory.getCurrentSession().save(subscriptionInfo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubscriptionInfo> fetchAllActiveSubscription() {


		List<SubscriptionInfo> activeSubscriptions = new ArrayList<SubscriptionInfo>();

		activeSubscriptions = sessionFactory.getCurrentSession()
				.createQuery("from SubscriptionInfo where active='true'").getResultList();

		return activeSubscriptions;
	}

	@Override
	public void saveOrUpdatePaypalSubscription(SubscriptionInfo subscriptionInfo) {
		sessionFactory.getCurrentSession().saveOrUpdate(subscriptionInfo);
	}

	@Override
	public void deactivateSubscription(String subscr_id) {
		try {
			SubscriptionInfo subscription = getSubscriptionById(subscr_id);
			if ( subscription != null ) {
				subscription.setActive("false");
				saveOrUpdatePaypalSubscription(subscription);
			} else {
			}
		} catch ( Exception pe ) {
			pe.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SubscriptionInfo> getSubscriptionsByUsername(String username) {
		List<SubscriptionInfo> subscriptionInfo = new ArrayList<SubscriptionInfo>();

		subscriptionInfo = sessionFactory.getCurrentSession()
				.createQuery("from SubscriptionInfo where username=?")
				.setParameter(0, username).getResultList();

		return subscriptionInfo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public SubscriptionInfo getSubscriptionById(String subsId) {
		SubscriptionInfo subscriptionInfo = null;
		try {
			subscriptionInfo = (SubscriptionInfo)sessionFactory.getCurrentSession()
				.createQuery("from SubscriptionInfo where subscr_id=?")
				.setParameter(0, subsId).getSingleResult();
		} catch ( Exception e ) {
			//  If payment is processed before signup message, this will be null.  No stack trace needed.
			subscriptionInfo = null;
		}
		return subscriptionInfo;
	}
}
