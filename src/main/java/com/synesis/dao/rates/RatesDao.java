package com.synesis.dao.rates;

import java.util.List;
import java.util.Map;

import com.synesis.model.Rates;

public interface RatesDao {

	Double getRateFromCodeAndCarrier(String code, String name);

	Map<String, Double> getRateFromCodeAndNaic(String code,String naic, String naic1,String naic2,String naic3);

	Double getRateFromCodeAndNaic(String code, String naic);

	List<Object[]> getAllRatesByDate( List<String> naic, String naic1,String naic2,String naic3);

}
