package com.synesis.dao.rates;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Rates;
import com.synesis.utility.LogUtil;

@Repository
public class RatesDaoImpl implements RatesDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Double getRateFromCodeAndCarrier(String code, String name) {
		
		Rates ratesModel = (Rates) sessionFactory.getCurrentSession()
				.createQuery("from Rates r,Carriers c where r.naic=c.naic and r.code=? and c.name=? and date=(select max(date) from rates where code=? and naic=(select naic from carriers where name=?))")
				.setParameter(0, code).setParameter(1, name).setParameter(2, code).setParameter(3, name)
				.getSingleResult();

		if (ratesModel != null) {
			return ratesModel.getRate();
		}
		return null;
	}

//	private static Connection mainConnection;
//
//	private static Connection getConnection() {
//		try {
//			if (mainConnection == null || mainConnection.isClosed()) {
//				// Connect to the database
//				String url = "jdbc:mysql://localhost:3306/wcdev";
//				String username = "root";
//				String password = "Feb#9th#2ol3";
//
//				mainConnection = DriverManager.getConnection(url, username, password);
//			}
//
//			try {
//				Class.forName("com.mysql.jdbc.Driver");
//				LogUtil.log("MySQL driver loaded");
//			} catch (ClassNotFoundException cnfe) {
//				throw new IllegalStateException("Cannot find JDBC driver");
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return mainConnection;
//	}
//   	
//   	private static Double getRate(Integer naic, String code) {
//		Double rate = 0.0;
//		try {
//			PreparedStatement rstmt = getConnection().prepareStatement("select rate from rates where code=? and naic=? and date=(select max(date) from rates where code=? and naic=?)");
//			rstmt.setString(1, code);
//			rstmt.setInt(2, naic);
//			rstmt.setString(3, code);
//			rstmt.setInt(4, naic);
//			ResultSet rs1 = rstmt.executeQuery();
//			if ( rs1.next() ) {
//				rate = rs1.getDouble("rate");
//			}
//		} catch ( Exception re1 ) {
//			LogUtil.log("Exception retrieving rate.  Naic=\"" + naic + "\"   Code: " + code);
//		}
//		return rate;
//	}
	
	
	@Override
	public Map<String, Double> getRateFromCodeAndNaic(String code, String naic, String naic1,String naic2,String naic3) {


		LogUtil.log("Code :"+code +" :naic:"+naic1+" :naic2:"+naic2+" naic3:"+naic3+" naic:"+naic);
		
		Map<String, Double> map = new  HashMap<String, Double>();
		
		try {
			if(naic!=null && naic!="") {
				BigDecimal rate = (BigDecimal) sessionFactory.getCurrentSession()
				.createNativeQuery("select rate from rates where naic=:naic and code=:code order by date desc limit 1")
				.setParameter("code", code)
				.setParameter("naic", naic)
				.getSingleResult();
				
				
				if(rate!=null) {
					map.put("naic", rate.doubleValue());
				}
			}
			
			if(naic1!=null && Integer.parseInt(naic1)!=0) {
				BigDecimal rate = (BigDecimal) sessionFactory.getCurrentSession()
				.createNativeQuery("select rate from rates where naic=:naic and code=:code order by date desc limit 1")
				.setParameter("code", code)
				.setParameter("naic", naic1)
				.getSingleResult();
				
				
				if(rate!=null) {
					map.put("naic1", rate.doubleValue());
				}
			}
			
			if(naic2!=null && naic2!="") {
				BigDecimal rate = (BigDecimal) sessionFactory.getCurrentSession()
				.createNativeQuery("select rate from rates where naic=:naic and code=:code order by date desc limit 1")
				.setParameter("code", code)
				.setParameter("naic", naic2)
				.getSingleResult();
				
				
				if(rate!=null) {
					map.put("naic2", rate.doubleValue());
				}
			}
			
			if(naic3!=null && naic3!="") {
				BigDecimal rate = (BigDecimal) sessionFactory.getCurrentSession()
				.createNativeQuery("select rate from rates where naic=:naic and code=:code order by date desc limit 1")
				.setParameter("code", code)
				.setParameter("naic", naic3)
				.getSingleResult();
				
				
				if(rate!=null) {
					map.put("naic3", rate.doubleValue());
				}
			}
		} catch (Exception e) {
			LogUtil.log("Error:"+e.getMessage());
		}
		
		return map;
	}

	@Override
	public Double getRateFromCodeAndNaic(String code, String naic) {
		
		
		LogUtil.log("Code :"+code +" :naic:"+naic);
		
		Rates ratesModel = null;
		try {
			ratesModel = (Rates) sessionFactory.getCurrentSession()
					.createQuery("from Rates where code.code=:code and naic = :naic order by date desc")
					.setParameter("code", code)
					.setParameter("naic", naic)
					.setMaxResults(1)
					.getSingleResult();
			
		} catch (NoResultException e) {
			return null;
		} catch (NumberFormatException e) {
			return null;
		}
		
		if (ratesModel != null) {
			return ratesModel.getRate();
		}
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getAllRatesByDate(List<String> naic, String naic1,String naic2,String naic3) {
		List<Object[]> rates =null;
		try {
		
		List<String> naics	 = new ArrayList<String>();
		
		
		if(naic1!=null && !naic1.isEmpty()) {
			naics.add(naic1);
		}
		if(naic2!=null && !naic2.isEmpty()) {
			naics.add(naic2);
		}
		if(naic3!=null && !naic3.isEmpty()) {
			naics.add(naic3);
		}
		naics.addAll(naic);
		
		String sql = "SELECT naic,code,rate FROM rates t1 WHERE  t1.naic in (:naic) and t1.date = (SELECT MAX(t2.date) FROM rates t2 WHERE "
				+ " t2.code=t1.code and t2.naic=t1.naic)";	
			
		 rates = sessionFactory.getCurrentSession()
					.createNativeQuery(sql)
					.setParameter("naic", naics)
					.getResultList();
			
		} catch (NoResultException e) {
			return null;
		} catch (NumberFormatException e) {
			return null;
		}
		
		return rates;
	}

}
