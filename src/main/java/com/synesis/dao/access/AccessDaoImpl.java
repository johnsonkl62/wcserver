package com.synesis.dao.access;

import java.sql.Timestamp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Access;

@Repository
public class AccessDaoImpl implements AccessDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Access save(String user, String ip) {
		
		Access access = new Access();
		access.setUser(user);
		access.setIp(ip);
		access.setAtime(new Timestamp(System.currentTimeMillis()));
		
		sessionFactory.getCurrentSession().save(access);
		
		return access;
	}

	
	
}
