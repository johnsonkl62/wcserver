package com.synesis.dao.access;

import com.synesis.model.Access;

public interface AccessDao {

	Access save(String user, String ip);
	
}
