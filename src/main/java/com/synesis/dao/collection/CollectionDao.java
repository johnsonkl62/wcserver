package com.synesis.dao.collection;

import java.util.Map;

import com.synesis.database.CollectionStats;

public interface CollectionDao {

	public Map<String, Map<String, CollectionStats>> update();
	
}
