package com.synesis.dao.collection;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.database.CollectionStats;
import com.synesis.database.CountyEnum;
import com.synesis.utility.LogUtil;
import com.synesis.utility.MonthEnum;

@Repository
public class CollectionDaoImpl implements CollectionDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public Calendar thisMonth = Calendar.getInstance();
	
	public CollectionDaoImpl() {
		thisMonth.set(Calendar.DAY_OF_MONTH, 1);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Map<String, CollectionStats>> update() {
		
		TreeMap<String, Map<String, CollectionStats>> allStats = new TreeMap<String,Map<String,CollectionStats>>();

		for ( CountyEnum cty : CountyEnum.values() ) {
			allStats.put(cty.name(), new LinkedHashMap<String,CollectionStats>());

			Calendar now = Calendar.getInstance();
			int month = now.get(Calendar.MONTH);	//   Zero-based month number 0=JAN
			for ( int i = 0; i < 12; i++ ) {
//			for ( MonthEnum m : MonthEnum.values() ) {
				allStats.get(cty.name()).put(MonthEnum.values()[month].name(), new CollectionStats());
				month = ((month+1)%12);
			}

//			for ( MonthEnum m : MonthEnum.values() ) {
//				allStats.get(cty).put(m, new CollectionStats());
//			}
		}
		
		List<Object[]> employersCountList = sessionFactory.getCurrentSession()
				.createQuery("select county, MONTH(xdate), count(*) as c from Employers where county is not null and county!='EXAMPLE' group by county,MONTH(xdate) order by county,MONTH(xdate)")
				.getResultList();

		if ( employersCountList != null && !employersCountList.isEmpty() ) {
			for(Object obj[] : employersCountList) {
				CountyEnum ce = CountyEnum.valueOf(String.valueOf(obj[0]));
				if ( ce != null ) {
					MonthEnum me = MonthEnum.values()[Integer.parseInt(String.valueOf(obj[1]))-1];
					if ( me != null ) {
						allStats.get(ce.name()).get(me.name()).setTotal(Integer.parseInt(obj[2]+""));
					} else {
						LogUtil.log("County[" + String.valueOf(obj[0]) + "][" + String.valueOf(obj[0]) + "] not found.");
					}
				} else {
					LogUtil.log("County[" + String.valueOf(obj[0])  + "][" + String.valueOf(obj[0]) + "] not found.");
				}
			}
		}
		
		employersCountList = sessionFactory.getCurrentSession()
				.createQuery("select county,MONTH(xdate), count(*) as c from Employers where county is not null and county!='EXAMPLE' and carrier is not null group by county,MONTH(xdate) order by county,MONTH(xdate)")
					.getResultList();

		if ( employersCountList != null && !employersCountList.isEmpty() ) {
			for(Object obj[] : employersCountList) {
				CountyEnum ce = CountyEnum.valueOf(String.valueOf(obj[0]));
				if ( ce != null ) {
					MonthEnum me = MonthEnum.values()[Integer.parseInt(String.valueOf(obj[1]))-1];
					if ( me != null ) {
						allStats.get(ce.name()).get(me.name()).setDone(Integer.parseInt(obj[2]+""));
					} else {
						LogUtil.log("County[" + String.valueOf(obj[0]) + "][" + String.valueOf(obj[0]) + "] not found.");
					}
				} else {
					LogUtil.log("County[" + String.valueOf(obj[0])  + "][" + String.valueOf(obj[0]) + "] not found.");
				}
			}
		}
		
		return allStats;
	}

}
