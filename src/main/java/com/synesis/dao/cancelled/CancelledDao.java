//package com.synesis.dao.employers;
//
//import java.util.List;
//
//import com.synesis.model.Employers;
//
//
//public interface EmployerDao {
//
//	public List<Employers> getEmployers(String nameClause, String gccClause, String carrierClause, String authClause);
//
//	public void updateEmployer(String employerId, String code);
//
//	public Employers findOne(String employerId);
//	
//}
package com.synesis.dao.cancelled;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.synesis.model.Employers;

@Repository
public class CancelledDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Employers> getAllCancelled() {
		try {
			List<Employers> employers = sessionFactory.getCurrentSession().createQuery("from Employers e WHERE e.carrier='CANCELLED'")
							.getResultList();
			return employers;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int transferCancelled() {
		try {
			int count = sessionFactory.getCurrentSession().createQuery("insert into Cancelled SELECT from Employers e WHERE e.carrier='CANCELLED'")
							.executeUpdate();
			return count;
			
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public void deleteCancelled() {
		try {
			sessionFactory.getCurrentSession().createNativeQuery("delete from Employers e WHERE e.carrier='CANCELLED'").executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
