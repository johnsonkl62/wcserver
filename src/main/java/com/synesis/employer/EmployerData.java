package com.synesis.employer;

import java.util.Date;


public class EmployerData {

	private String code;
	private String mm;
	private Date xdate;
	private Double rate = 0.0;
	private Double rates[] = new Double[3];
	private Double pcts[] = new Double[3];

	public EmployerData() {
		super();
		pcts[0] = -1.0;
		pcts[1] = -1.0;
		pcts[2] = -1.0;
		rates[0] = -1.0;
		rates[1] = -1.0;
		rates[2] = -1.0;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMm() {
		return mm;
	}
	public void setMm(String mm) {
		this.mm = mm;
	}
	public Date getXdate() {
		return xdate;
	}
	public void setXdate(Date xdate) {
		this.xdate = xdate;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = (rate==null?0d:rate);
		if ( this.rate != null && this.rate != 0.0 ) {
			for ( int i = 0; i < rates.length; i++ ) {
				if ( rates[i] != null ) {
					pcts[i] = ( rate - rates[i] ) / rate;
				}
			}
		}
	}
	public Double getComp1() {
		return rates[0];
	}
	public Double getComp2() {
		return rates[1];
	}
	public Double getComp3() {
		return rates[2];
	}
	public Double getPct1() {
		return pcts[0];
	}
	public Double getPct2() {
		return pcts[1];
	}
	public Double getPct3() {
		return pcts[2];
	}

	public Double getCompRate(int index) {
		return rates[index];
	}
	public void setCompRate(Double compRate, int index) {
		if(compRate==null) {
			compRate = 0d;
		}
		rates[index]=compRate;
		if ( rate != null && rate != 0.0 ) {
			pcts[index] = new Double(( rate - compRate ) / rate );
		}
	}

	public Double getPct(int index) {
		return pcts[index];
	}

	public String toString() {
		return "Code: " + code + 
               "  MM: " + mm + 
               "  Xdate: " + xdate + 
               "  Rate: " + rate + 
               "  Comp 1: " + rates[0] + 
               "  Comp 1%: " + pcts[0] + 
               "  Comp 2:  " + rates[1] + 
               "  Comp 2%: " + pcts[1] + 
               "  Comp 3:  " + rates[2] + 
               "  Comp 3%: " + pcts[2];
	}
}
