package com.synesis.employer;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Employer {

	private EmployerKey key;
	private String carrier;
	private String description;
	private Date update;

	private EmployerBase base = new EmployerBase();
	private EmployerData data = new EmployerData();

	private static DecimalFormat df = new DecimalFormat("0.00");
	private static SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd"); 	

	public EmployerKey getKey() {
		return key;
	}

	public void setKey(EmployerKey key) {
		this.key = key;
	}

	public EmployerBase getBase() {
		return base;
	}

	public void setBase(EmployerBase base) {
		this.base = base;
	}

	public EmployerData getData() {
		return data;
	}

	public void setData(EmployerData data) {
		this.data = data;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
		if ( this.carrier != null ) {
			this.carrier = this.carrier.toUpperCase();
		}
	}

	public Date getUpdate() {
		return update;
	}

	public void setUpdate(Date update) {
		this.update = update;
	}

	public String toString() {
		return key + "   Carrier: " + carrier + "   " + ( base != null ? base : "") + "   " + ( data != null ? data : "");
	}

	public String toTabbedString() {
		return key.getName() + "\t" + getCarrier() + "\t" + base.getCity() + "\t" + getDescription() + "\t" + data.getMm() + "\t" + data.getXdate() + "\t" + 
				data.getRate() + "\t" + data.getCompRate(0) + "\t" + data.getPct(0) + "\t" + 
				data.getCompRate(1) + "\t" + data.getPct(1) + "\t" + data.getCompRate(2) + "\t" + data.getPct(2) + "\t" + 
				data.getCode() + "\t" + key.getFn() + "\t" + key.getCounty() + "\t" + base.getAddress() + "\t" + base.getZip();
	}


//	private static DecimalFormat df = new DecimalFormat("0.00");
//	private static SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd"); 	

//	public String toCSVString() {
//		return StringEscapeUtils.escapeXml11(
//				commaSafe(key.getName()) + "," + 
//				commaSafe(getCarrier()) + "," + 
//				commaSafe(base.getCity()) + "," + 
//				commaSafe(getDescription()) + "," + 
//				commaSafe(data.getMm()) + "," + 
//				dt.format(data.getXdate()) + "," +
//				df.format(data.getRate()) + "," + 
//				df.format(data.getExch()) + "," + 
//				df.format(data.getExchPct()*100) + "," + 
//				df.format(data.getFlag()) + "," + 
//				df.format(data.getFlagPct()*100) + "," + 
//				df.format(data.getNy()) + "," + 
//				df.format(data.getNyPct()*100) + "," +
//				data.getCode() + "," + 
//				key.getFn() + "," + 
//				commaSafe(key.getCounty()) + "," + 
//				commaSafe(base.getAddress()) + "," + 
//				commaSafe(base.getZip()));
//	}

	public String commaSafe(String s) {
		if (s.contains(","))
			return "\""+s+"\"";
		return s;
	}
	
	public static String getHeaders() {
		return "Employer\tCarrier\tCity\tDescription\tMod/Merit\tEx Date\tCurrent Rate\tComp Rate 1\tCR1 %\tComp Rate 2\tCR2 %\tComp Rate 3\tCR3 %\tGCC\tFile #\tCounty\tAddress\tZip\r\n";
	}

	public static String getMailMergeHeaders() {
//		return "WC_EMPLOYER,WC_CARRIER,WC_CITY,WC_DESC,WC_MM,WC_XDATE,WC_RATE,WC_EXCH,WC_EXPCT,WC_FLAG,WC_FLPCT,WC_NY,WC_NPCT,WC_GCC,WC_FN,WC_COUNTY,WC_STREET,WC_ZIP";
		return "WC_EMPLOYER,WC_CARRIER,WC_CITY,WC_DESC,WC_MM,WC_XDATE,WC_RATE,WC_CMP1,WC_C1PCT,WC_CMP2,WC_C2PCT,WC_CMP3,WC_C3PCT,WC_GCC,WC_FN,WC_COUNTY,WC_STREET,WC_ZIP";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
