package com.synesis.employer;

public class EmployerKey {

	private String name;
	private String county;
	private String fn;
	
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getFn() {
		return fn;
	}
	
	public void setFn(String fn) {
		this.fn = fn;
	}
	
	public String toString() {
		return "Name: " + name + "   Fn: " + fn + "  County: " + county;
	}
}
