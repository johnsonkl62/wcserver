package com.synesis.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.synesis.employer.Employer;
import com.synesis.employer.EmployerBase;
import com.synesis.employer.EmployerData;
import com.synesis.employer.EmployerKey;
import com.synesis.model.Employers;
import com.synesis.model.Purchase;


public class AppUtility {

	static SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd"); 

	
	public static <T> List<List<T>> chopped(List<T> list, final int L) {
	    List<List<T>> parts = new ArrayList<List<T>>();
	    final int N = list.size();
	    for (int i = 0; i < N; i += L) {
	        parts.add(new ArrayList<T>(
	            list.subList(i, Math.min(N, i + L)))
	        );
	    }
	    return parts;
	}

	public static String generateClause(String field, String value, boolean exact, boolean number) {
		StringBuffer clause = new StringBuffer();
		if ( value == null || value.trim().length() == 0) {
			return "";
		}

		clause.append(" (");
		String values[] = value.split(",");
		int index = 0;
		for ( String next : values ) {
			if ( index++ > 0 ) {
				clause.append(" or ");
			}
			if ( exact ) {
				clause.append(field + "=" + (number?"":"'") + next.toUpperCase() + (number?"":"'") );
			} else {
				clause.append(field + " like " + "'%" + next.toUpperCase() + "%'" );
			}
		}
		clause.append(") ");
		return clause.toString();
	}

	
	public  static String generateClause(ArrayList<Purchase> selectedAreas, String minDate, String maxDate) {
		Date startFilter = null;
		Date endFilter = null;
		
		Calendar startCal = Calendar.getInstance();
		Calendar endCal = Calendar.getInstance();
		
		try {
			if ( minDate != null && minDate.trim().length() > 0 ) {
				startFilter = dt.parse(minDate);     //  Year is eliminated in DATE_FORMAT clause below
				startCal.setTime(startFilter);
			}
			if ( maxDate != null && maxDate.trim().length() > 0 ) {
				endFilter = dt.parse(maxDate);     //  Year is eliminated in DATE_FORMAT clause below
				endCal.setTime(endFilter);
			}
		} catch (ParseException e) {
			LogUtil.log("Invalid date formats entered.  Start: " + minDate + "   End: " + maxDate);
			startFilter = null;
			endFilter = null;
		}

		StringBuffer value = new StringBuffer("");
		
		if(!selectedAreas.isEmpty()) {
			value.append("((");
			for ( Purchase next : selectedAreas ) {
				if ( value.length() > 2 ) {
					value.append(") OR (");
				};

				//  Verify that the selected start and end times are within purchased data
				Calendar subStartDate = Calendar.getInstance();
				int month = MonthEnum.valueOf(next.getMonth()).ordinal();
				subStartDate.set(Calendar.MONTH, month);
				subStartDate.set(Calendar.DAY_OF_MONTH, 1);
				subStartDate.set(Calendar.YEAR, startCal.get(Calendar.YEAR));

				Calendar subEndDate = Calendar.getInstance();
				subEndDate.set(Calendar.MONTH, (month+1)%12);
				subEndDate.set(Calendar.DAY_OF_MONTH, 1);
				subEndDate.set(Calendar.YEAR, endCal.get(Calendar.YEAR));

				if ( month == 11 ) {
					subEndDate.set(Calendar.YEAR, subEndDate.get(Calendar.YEAR)+1);
				}
				subEndDate.add(Calendar.DAY_OF_MONTH, -1);
				
				if ( startFilter != null && startFilter.after(subStartDate.getTime()) ) {
					subStartDate.setTime(startFilter);
				}
				if ( endFilter != null && endFilter.before(subEndDate.getTime()) ) {
					subEndDate.setTime(endFilter);
				}

				if ( subStartDate.get(Calendar.YEAR) < subEndDate.get(Calendar.YEAR)) {
					value.append( "(" + (!"ALL".equals(next.getCounty()) ? " county='" + next.getCounty() + "' AND " : "" ) +
					" (( DATE_FORMAT(xdate,'%m-%d') >=  DATE_FORMAT('" + dt.format(subStartDate.getTime()) + "','%m-%d') AND " +
					" DATE_FORMAT(xdate,'%m-%d') <=  DATE_FORMAT('2000-12-31','%m-%d') ) OR " +  // year doesn't matter
					"( DATE_FORMAT(xdate,'%m-%d') >=  DATE_FORMAT('2000-01-01','%m-%d') AND " +  // year doesn't matter
					" DATE_FORMAT(xdate,'%m-%d') <=  DATE_FORMAT('" + dt.format(subEndDate.getTime()) + "','%m-%d') ) ) )");
				} else {
					value.append( "(" + (!"ALL".equals(next.getCounty()) ? " county='" + next.getCounty() + "' AND " : "" ) +
							" DATE_FORMAT(xdate,'%m-%d') >=  DATE_FORMAT('" + dt.format(subStartDate.getTime()) + "','%m-%d') AND " +
							" DATE_FORMAT(xdate,'%m-%d') <=  DATE_FORMAT('" + dt.format(subEndDate.getTime()) + "','%m-%d') )");
				}
				if(next.getZip()!=null) {
					value.append(" AND zip='"+next.getZip()+"' ");
				}
				
			}
			
			value.append("))");
		}
		return value.toString();
	}

	
	public  static String generateAdminClause(List<String> selectedAreas, String minDate, String maxDate) {
		Date startFilter = null;
		Date endFilter = null;
		try {
			if ( minDate != null && minDate.trim().length() > 0 ) {
				startFilter = dt.parse(minDate);
			} else {
				startFilter = dt.parse("2021-01-01");
			}
			if ( maxDate != null && maxDate.trim().length() > 0 ) {
				endFilter = dt.parse(maxDate);
			} else {
				endFilter = dt.parse("2021-12-31");
			}
		} catch (ParseException e) {
			LogUtil.log("Invalid date formats entered.  Start: " + minDate + "   End: " + maxDate);
			startFilter = null;
			endFilter = null;
		}

		StringBuffer value = new StringBuffer("");

		Calendar startDate = Calendar.getInstance();
		startDate.setTime(startFilter);
		Calendar endDate = Calendar.getInstance();
		endDate.setTime(endFilter);
		
		if(!selectedAreas.isEmpty()) {
			value.append("((");
			for ( String next : selectedAreas ) {
				if ( value.length() > 2 ) {
					value.append(") OR (");
				};

				if ( startDate.get(Calendar.YEAR) < endDate.get(Calendar.YEAR)) {
					value.append( (!"ALL".equals(next) ? " county='" + next + "' AND " : "" ) +
					" (( DATE_FORMAT(xdate,'%m-%d') >=  DATE_FORMAT('" + dt.format(startDate.getTime()) + "','%m-%d') AND " +
					" DATE_FORMAT(xdate,'%m-%d') <=  DATE_FORMAT('2000-12-31','%m-%d') ) OR " +  // year doesn't matter
					"( DATE_FORMAT(xdate,'%m-%d') >=  DATE_FORMAT('2000-01-01','%m-%d') AND " +  // year doesn't matter
					" DATE_FORMAT(xdate,'%m-%d') <=  DATE_FORMAT('" + dt.format(endDate.getTime()) + "','%m-%d') ) )");
				} else {
					value.append( (!"ALL".equals(next) ? " county='" + next + "' AND " : "" ) +
							" DATE_FORMAT(xdate,'%m-%d') >=  DATE_FORMAT('" + dt.format(startFilter) + "','%m-%d') AND " +
							" DATE_FORMAT(xdate,'%m-%d') <=  DATE_FORMAT('" + dt.format(endFilter) + "','%m-%d') ");
				}
			}
			
			value.append("))");
		}
		return value.toString();
	}

	
	public  static  String combineClauses(String ... clauses) {

		StringBuffer sb = new StringBuffer(" e.updated is not null ");

		for ( String clause : clauses ) {
			if (clause != null && clause.trim().length() != 0) {
			sb.append(" AND ");
				sb.append(clause);
		}
		}
		return sb.toString();
	}


	public static Employer convertToEmployerData(Employers employer,Map<String,String> codeMap) {
		
		Employer emp = new Employer();
		emp.setCarrier("Exception occurred");
		
		EmployerKey eKey = new EmployerKey();
		eKey.setFn(employer.getFn());
		eKey.setName(employer.getName());
		eKey.setCounty(employer.getCounty());

		EmployerBase eBase = new EmployerBase();
		eBase.setAddr(employer.getAddr());
		eBase.setCity(employer.getCity());
		eBase.setCounty(employer.getCounty());
		eBase.setState(employer.getState());
		eBase.setZip(employer.getZip()+"");

		EmployerData eData = new EmployerData();
		eData.setCode(employer.getCode());
		eData.setMm(employer.getMm()+"");
		eData.setXdate(employer.getXdate());

		emp.setKey(eKey);
		emp.setBase(eBase);
		emp.setData(eData);
		emp.setCarrier(employer.getCarrier());
		
		
		try {
			if(!org.apache.commons.lang3.StringUtils.isBlank(employer.getCode()) && codeMap.containsKey(employer.getCode())) {
				emp.setDescription(codeMap.get(employer.getCode()));
			} else {
				emp.setDescription(employer.getCode());
			}
		} catch (NumberFormatException e) {
			emp.setDescription(employer.getCode());
			e.printStackTrace();
		}
		
		return emp;
	}
	
}
