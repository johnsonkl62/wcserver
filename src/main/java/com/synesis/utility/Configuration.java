package com.synesis.utility;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
/**
 *  For a full list of configuration parameters refer in wiki page.(https://github.com/paypal/sdk-core-java/blob/master/README.md) 
 */
public class Configuration {

	public static final Float LEAD_COST = 2.5f;
	public static final Float SUBSC_DISCOUNT = 0.9f;
	public static final Float SALES_TAX = 0.06f;

	public static final String TMPBASE = "/tmp/tomcat";
	
	private static final String WEBSITE_IP = "207.244.242.208";

	//  Default to real website 
	private static       String WEBSITE_MODE = "live";
	private static       String WEBSITE_PAYPAL_URL = "https://www.paypal.com/cgi-bin/webscr";
	private static       String WEBSITE_PAYPAL_BUSINESS = "klj1962@gmail.com";
	private static       String WEBSITE_PAYPAL_RETURN = "http://kswcrc.com/WCThanks";
	private static       String WEBSITE_PAYPAL_NOTIFY_URL = "https://kswcrc.com/WCPayPal";
	private static       String WEBSITE_PAYPAL_SUBSCRIBE_NOTIFY_URL = "https://kswcrc.com/WCPayPalSubscribe";
	public static        String MYSQL_URL = WEBSITE_IP;

	private static final String SANDBOX_MODE = "sandbox";
	private static final String SANDBOX_PAYPAL_URL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
	private static final String SANDBOX_PAYPAL_BUSINESS = "klj1962-seller@gmail.com";
	private static       String SANDBOX_PAYPAL_RETURN = "http://xx.xx.xx.xx/wcserver/WCThanks";
	private static       String SANDBOX_PAYPAL_NOTIFY_URL = "http://xx.xx.xx.xx/wcserver/WCPayPal";
	private static       String SANDBOX_PAYPAL_SUBSCRIBE_NOTIFY_URL = "http://xx.xx.xx.xx/wcserver/WCPayPalSubscribe";
	
	private static Map<String,String> configMap = null;
	
	static {
		try {
			String myIp = Executor.newInstance()
					 .execute(Request.Get("https://api.ipify.org/"))
					 .returnContent().asString();

			if ( !WEBSITE_IP.equals(myIp) ) {
				WEBSITE_MODE = SANDBOX_MODE;
				WEBSITE_PAYPAL_URL = SANDBOX_PAYPAL_URL; 
				WEBSITE_PAYPAL_BUSINESS = SANDBOX_PAYPAL_BUSINESS; 
				WEBSITE_PAYPAL_RETURN = SANDBOX_PAYPAL_RETURN.replaceAll("xx.xx.xx.xx", myIp); 
				WEBSITE_PAYPAL_NOTIFY_URL = SANDBOX_PAYPAL_NOTIFY_URL.replaceAll("xx.xx.xx.xx", myIp); 
				WEBSITE_PAYPAL_SUBSCRIBE_NOTIFY_URL = SANDBOX_PAYPAL_SUBSCRIBE_NOTIFY_URL.replaceAll("xx.xx.xx.xx", myIp); 
				MYSQL_URL = "localhost";
			}
			
	        LogUtil.log("External IP: " + myIp + "  Mode set to \"" + WEBSITE_MODE + "\"");
	        
		} catch ( Exception e ) {
			e.printStackTrace();
	    }

		configMap = new HashMap<String,String>();

		configMap.put("mode", WEBSITE_MODE);
		configMap.put("paypalUrl", WEBSITE_PAYPAL_URL);
		configMap.put("paypalBusiness", WEBSITE_PAYPAL_BUSINESS);
		configMap.put("paypalReturn", WEBSITE_PAYPAL_RETURN);
		configMap.put("paypalNotifyUrl", WEBSITE_PAYPAL_NOTIFY_URL);
		configMap.put("paypalSubscribeNotifyUrl", WEBSITE_PAYPAL_SUBSCRIBE_NOTIFY_URL);

	}

	public static final Map<String,String> getConfig(){
		return configMap;
	}

	public static Boolean verifyBaseFolder() {
		Boolean result = true;
		try {
			File tmpTomcat = new File(TMPBASE);
			if (!tmpTomcat.exists()) {
				tmpTomcat.mkdir();
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static void main(String[] args) {
		Map<String,String> cMap = Configuration.getConfig();
		
		for ( String s : cMap.keySet() ) {
			LogUtil.log("Key: " + s + "    Value: " + cMap.get(s));
		}
	}

}
