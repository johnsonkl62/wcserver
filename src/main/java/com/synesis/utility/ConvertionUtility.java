package com.synesis.utility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synesis.beans.CarriersBean;
import com.synesis.beans.ProfileBean;
import com.synesis.beans.RatesBean;
import com.synesis.beans.TemplateBean;
import com.synesis.model.Carriers;
import com.synesis.model.Profile;
import com.synesis.model.Rates;
import com.synesis.model.Template;

public class ConvertionUtility {

	public static CarriersBean converToCarrierBean(Carriers carriers) {
		CarriersBean carriersBean = null;
		if (carriers != null) {
			carriersBean = new CarriersBean();
			carriersBean.setNaic(carriers.getNaic());
			carriersBean.setName(carriers.getName());
		}

		return carriersBean;
	}

	public static Map<String,Double> convertToRatesMap(List<Object[]> rates) {
		Map<String,Double>  map = new HashMap<String, Double>();
		if(rates!=null) {
			for(Object[] rate : rates) {
				map.put(rate[0].toString()+"_"+rate[1].toString(), Double.parseDouble(rate[2].toString()));
			}
		}
		return map;
	}
	
	public static RatesBean convertToRatesBean(Rates rates) {
		RatesBean ratesBean = null;
		if(rates!=null) {
			ratesBean = new RatesBean();
			ratesBean.setNaic(rates.getNaic()+"");
			ratesBean.setCode(rates.getCode().getCode()+"");
			ratesBean.setRate(rates.getRate());
			ratesBean.setDate(rates.getDate());
		}
		return ratesBean;
	}
	
	public static ProfileBean convertToProfileBean(Profile profile,Template template) {

		ProfileBean profileBean = null;
		if (profile != null) {
			profileBean = new ProfileBean(profile.getUsername(),
					profile.getLastName(), profile.getFirstName(),
					profile.getMiddleName(), profile.getSuffix(),
					profile.getAgency(), profile.getEmail(),
					profile.getPhoneNo(), profile.getPassword(),
					profile.getCarrier1(), profile.getCarrier2(),
					profile.getCarrier3());

			if(template!=null) {
				profileBean.setTemplateBean(new TemplateBean(template.getTemplate(), template.getPriority()));
			}

			if(profile.getCarrierModel1()!=null) {
				CarriersBean carriersBean = converToCarrierBean(profile.getCarrierModel1());
				Set<Rates> rates = profile.getCarrierModel1().getRates();
				if(rates!=null) {
					for(Rates rates2 : rates) {
						carriersBean.addRatesBean(convertToRatesBean(rates2));
					}
				}
				profileBean.setCarriersBean1(carriersBean);
			}
			if(profile.getCarrierModel2()!=null) {
				CarriersBean carriersBean = converToCarrierBean(profile.getCarrierModel2());
				Set<Rates> rates = profile.getCarrierModel2().getRates();
				if(rates!=null) {
					for(Rates rates2 : rates) {
						carriersBean.addRatesBean(convertToRatesBean(rates2));
					}
				}
				profileBean.setCarriersBean2(carriersBean);
			}
			if(profile.getCarrierModel3()!=null) {
				CarriersBean carriersBean = converToCarrierBean(profile.getCarrierModel3());
				Set<Rates> rates = profile.getCarrierModel3().getRates();
				if(rates!=null) {
					for(Rates rates2 : rates) {
						carriersBean.addRatesBean(convertToRatesBean(rates2));
					}
				}
				profileBean.setCarriersBean3(carriersBean);
			}
			
		}
		return profileBean;
	}

	public static TemplateBean convertToTemplateBean(Template template) {
		
		if(template!=null) {
			return new TemplateBean(template.getTemplate(), template.getPriority());
		}
		
		return null;
	}

}
