package com.synesis.utility;

import java.io.FileOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.itextpdf.html2pdf.HtmlConverter;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class GeneratePdfFromTemplate {
	
	public static void generatePdf(String path, String filename, List<Map<String, Object>> dataList, String username) throws Exception{
		
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);
        cfg.setClassForTemplateLoading(GeneratePdfFromTemplate.class, "/templates/");
        Template template = cfg.getTemplate("pdf-template.ftl");
        
        StringWriter htmlParent = new StringWriter();
        
        for (Map<String, Object> data : dataList) {
        	data.put("user", username);
            StringWriter html = new StringWriter();

             template.process(data, html);
            
             htmlParent.append(html.toString());

        }
        
        HtmlConverter.convertToPdf(htmlParent.toString(), new FileOutputStream(path + filename+".pdf"));    
		
	}

	public static void main(String[] args) throws Exception {
		
		 List<Map<String, Object>> dataList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
        	Map<String, Object> data = new HashMap<>();
            data.put("user", "John Doe"+i);
            data.put("WC_EMPLOYER", "John Doe"); 
            data.put("WC_STREET", "John Doe");
            data.put("WC_CITY", "John Doe"); 
            data.put("WC_ZIP", "John Doe");
            data.put("WC_RATE", "John Doe"); 
            data.put("WC_C1PCT", "John Doe");
            data.put("WC_CMP1", "John Doe"); 
            data.put("WC_CARRIER", "John Doe");
            data.put("WC_GCC", "John Doe"); 
            data.put("WC_DESC", "John Doe");
            data.put("WC_XDATE", "John Doe");
            data.put("WC_CMP2", "John Doe");
            data.put("WC_C2PCT", 21);
            data.put("WC_MM", 1);
            dataList.add(data);
        }
        
        generatePdf("C:\\yash\\kevin\\", "test.pdf", dataList, "test");
        
		
	}
	
}
