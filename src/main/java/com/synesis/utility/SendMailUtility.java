package com.synesis.utility;

import java.nio.charset.StandardCharsets;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.mail.javamail.MimeMessageHelper;

public class SendMailUtility {

	static final String FROM = "support@kswcrc.com";
	static final String ADMIN = "klj1962@gmail.com";   // BCC'ed on all sent email
	static final String TO = "kapasiyash@gmail.com";   // For testing

	static final String BODY = "This email was sent through the Gmail SMTP interface by using Java.";
	static final String SUBJECT = "Gmail test (SMTP interface accessed using Java)";

	// Supply your SMTP credentials below.
	static final String SMTP_USERNAME = "klj1962@gmail.com";
	static final String SMTP_PASSWORD = "dnglwhrwpybmbquj";

	// Gmail SMTP host name.
	static final String HOST = "smtp.gmail.com";

	// The port you will connect to
	static final int PORT = 587;

	public static void main(String[] args) throws Exception {


		sendMail("kevin@firstaffiliated.com", "Send Mail Utility Test", "main routine standalone test");

		
//		// Create a Properties object to contain connection configuration
//		// information.
//		Properties props = System.getProperties();
//		props.put("mail.transport.protocol", "smtps");
//		props.put("mail.smtp.port", PORT);
//
//		// Set properties indicating that we want to use STARTTLS to encrypt the connection.
//		// The SMTP session will begin on an unencrypted connection, and then the client
//		// will issue a STARTTLS command to upgrade to an encrypted connection.
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.ssl.protocols", "TLSv1.2");
//		props.put("mail.smtp.starttls.required", "true");
//
//		// Create a Session object to represent a mail session with the
//		// specified properties.
//		Session session = Session.getDefaultInstance(props);
//
//		// Create a message with the specified information.
//		MimeMessage msg = new MimeMessage(session);
//		msg.setFrom(new InternetAddress(FROM));
//		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
//		msg.setSubject(SUBJECT);
//		msg.setContent(BODY, "text/plain");
//
//		// Create a transport.
//		Transport transport = session.getTransport();
//
//		// Send the message.
//		try {
//			LogUtil.log("Attempting to send an email through the Gmail SMTP interface...");
//
//			// Connect to Gmail using the SMTP username and password you specified above.
//			transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
//
//			// Send the email.
//			transport.sendMessage(msg, msg.getAllRecipients());
//			LogUtil.log("Email sent!");
//		} catch (Exception ex) {
//			LogUtil.log("The email was not sent.");
//			LogUtil.log("Error message: " + ex.getMessage());
//		} finally {
//			// Close and terminate the connection.
//			transport.close();
//		}
	}

	public static void sendEmail(String url, String token, String toAddress)
			throws Exception {
		url = url + "/resetpassword?emailAddress=" + toAddress + "&token=" + token;

		sendMail(toAddress, "one time password", "Please reset your password by below Link" + " \r\n" + url);
	}

	
	
	public static void sendMail(String toAddress, String subject, String body,String[] fileList) throws Exception {
		Properties props = new Properties();
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.socketFactory.port", PORT);
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.ssl.protocols", "TLSv1.2");
		props.put("mail.smtp.starttls.required", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", PORT);

		Session session = Session.getDefaultInstance(props,
				new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								SMTP_USERNAME, SMTP_PASSWORD);
					}
				});

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress("support@kswcrc.com"));

		message.setSubject(subject);
		message.setText(body);
		message.setRecipient(RecipientType.TO, new InternetAddress(toAddress));
//		message.setRecipient(RecipientType.BCC, new InternetAddress(ADMIN));
		
		 // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();

        // Now set the actual message
        messageBodyPart.setText(body);

		
		  // Create a multipart message
        Multipart multipart = new MimeMultipart();

        // Set text message part
        multipart.addBodyPart(messageBodyPart);

        for ( String fileName : fileList ) {
        	try {
				messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(fileName);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(fileName);
				multipart.addBodyPart(messageBodyPart);
        	} catch ( Exception e ) {
        		e.printStackTrace();
        	}
        }
         // Send the complete message parts
         message.setContent(multipart);
		
		Transport.send(message);

		LogUtil.log("1) Mail Sent to " + toAddress + "  Subject: " + subject);
	}
	
	
	public static void sendMail(String toAddress, String subject, String body,String fileName) throws Exception {
		Properties props = new Properties();
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.socketFactory.port", PORT);
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", PORT);

		Session session = Session.getDefaultInstance(props,
				new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								SMTP_USERNAME,
								SMTP_PASSWORD);
					}
				});

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress("support@kswcrc.com"));

		message.setSubject(subject);
		message.setText(body);
		message.setRecipient(RecipientType.TO, new InternetAddress(toAddress));
//		message.setRecipient(RecipientType.BCC, new InternetAddress(ADMIN));
		
		 // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();

        // Now set the actual message
        messageBodyPart.setText(body);

		
		  // Create a multipar message
        Multipart multipart = new MimeMultipart();

        // Set text message part
        multipart.addBodyPart(messageBodyPart);
		
		 messageBodyPart = new MimeBodyPart();
		 DataSource source = new FileDataSource(fileName);
         messageBodyPart.setDataHandler(new DataHandler(source));
         messageBodyPart.setFileName(fileName);
         multipart.addBodyPart(messageBodyPart);

         // Send the complete message parts
         message.setContent(multipart);
		
		Transport.send(message);

		LogUtil.log("2) Mail Sent to " + toAddress + "  Subject: " + subject);
	}
	
	public static void sendMail(String toAddress, String subject, String body) throws Exception {

		Properties props = new Properties();
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.socketFactory.port", PORT);
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.ssl.protocols", "TLSv1.2");
		props.put("mail.smtp.starttls.required", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", PORT);

		Session session = Session.getDefaultInstance(props,
				new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								SMTP_USERNAME,
								SMTP_PASSWORD);
					}
				});

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress("support@kswcrc.com"));

		message.setSubject(subject);
		message.setText(body);
		message.setRecipient(RecipientType.TO, new InternetAddress(toAddress));
		Transport.send(message);

		LogUtil.log("3) Mail Sent to " + toAddress + "  Subject: " + subject);

	}
	
	
	public static void sendFreemarkerEmail(String to, String html, String subject) {
		Properties props = new Properties();
		props.put("mail.smtp.host", HOST);
//		props.put("mail.smtp.socketFactory.port", PORT);
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", PORT);

		Session session = Session.getDefaultInstance(props,
				new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								SMTP_USERNAME,
								SMTP_PASSWORD);
					}
				});

		try {
			MimeMessage message = new MimeMessage(session);
			MimeMessageHelper helper = new MimeMessageHelper(message,
					MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());

			helper.setFrom(new InternetAddress("support@kswcrc.com"));

			helper.setSubject(subject);
			helper.setText(html,true);
			helper.setTo(to);
			Transport.send(message);
			
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		LogUtil.log("4) Mail Sent to " + to + "  Subject: " + subject);
	}

	public static void emailReport(String text, String subject) {
		String to = "klj1962@gmail.com";// change accordingly
		String from = "kjohnson@synesis-inc.com";

		// Get the session object
		Properties properties = System.getProperties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.ssl.protocols", "TLSv1.2");
		properties.put("mail.smtp.starttls.required", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", PORT);

		Session session = Session.getInstance(properties,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(SMTP_USERNAME, SMTP_PASSWORD);
					}
				});

		// compose the message
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));
			message.setSubject(subject);
			// message.setText(text);
			message.setContent(text, "text/plain; charset=utf-8");

			// Send message
			Transport.send(message);
			LogUtil.log("message sent successfully....");

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

}
