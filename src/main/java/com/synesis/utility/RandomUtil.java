package com.synesis.utility;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Random;

public class RandomUtil {

	private static Random r = new Random(Calendar.getInstance().getTime().getTime());
	
	
	static public Integer getRandom(int minimum, int maximum) {
		return minimum + r.nextInt(maximum - minimum);
	}

	
	static public void main(String[] args) {

//		for ( int i = 0; i < 1000; i++ ) {
//			int result = getRandom(50,75);
//			if ( result < 50 || result > 75 ) {
//				LogUtil.log("Iteration " + i + " -  Random [ 50 - 75 ]: " + result + "   OUT OF RANGE");
//			} 
//			else if ( result == 50 ) {
//				LogUtil.log("Iteration " + i + " -  Random [ 50 - 75 ]: " + result + "   MINIMUM");
//			} 
//			else if ( result == 75 ) {
//				LogUtil.log("Iteration " + i + " -  Random [ 50 - 75 ]: " + result + "   MINIMUM");
//			}
//		}

	LocalDate ld = LocalDate.now(); 
	String month = ld.getMonth().toString().substring(0,3);
	for ( int i = 0; i < 12; i++ ) {
		LogUtil.log("Month: [" + month + "]");
		ld = ld.plusMonths(1);
		month = ld.getMonth().toString().substring(0,3);
	}


		
		
		for ( MonthEnum me : MonthEnum.values() ) {
			LogUtil.log("Previous:  " + me.previous().name() + "      Current:  " + me.name() + "      Next:  " + me.next().name() ); 
		}
	}
	
}
