package com.synesis.utility;

public enum MonthEnum {

	JAN(1), FEB(2), MAR(3), APR(4), MAY(5), JUN(6), JUL(7), AUG(8), SEP(9), OCT(
			10), NOV(11), DEC(12);

	int monthIndex = 0;

	MonthEnum(int monthIndex) {
		this.monthIndex = monthIndex;
	}

	public static MonthEnum byIndex(int index) {
		for (MonthEnum m : MonthEnum.values()) {
			if (m.monthIndex == index) {
				return m;
			}
		}
		return null;
	}

	public int getIndex() {
		return monthIndex;
	}


	public MonthEnum next() {
		return values()[(this.ordinal()+1) % values().length];
	}

	public MonthEnum previous() {
		return values()[(this.ordinal()+11) % values().length];
	}

}
