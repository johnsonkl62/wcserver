package com.synesis.utility;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LogUtil {

	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

	public static void log(String s) {
		System.out.println(LocalDateTime.now().format(dtf) + ": " + s);
	}
}
