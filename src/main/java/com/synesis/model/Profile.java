package com.synesis.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
import com.synesis.model.users.UserRole;

@Entity
@Proxy(lazy=false) 
@Table(name = "profile")
public class Profile implements Serializable {

	private static final long serialVersionUID = 1L;

	private String username;
	private String lastName;
	private String firstName;
	private String middleName;
	private String suffix;
	private String agency;
	private String email;
	private String phoneNo;
	private String password;
	private String carrier1;
	private String carrier2;
	private String carrier3;

	private Carriers carrierModel1;
	private Carriers carrierModel2;
	private Carriers carrierModel3;

	private Set<UserRole> userRole = new HashSet<UserRole>(0);

	public Profile() {
	}

	@Id
	@Column(name = "username", unique = true, nullable = false, length = 50)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "last", nullable = false, length = 24)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "first", nullable = false, length = 16)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "middle", length = 16)
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Column(name = "sfx", length = 3)
	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	@Column(name = "agency", nullable = false, length = 32)
	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	@Column(name = "email", nullable = false, length = 50)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "phone", nullable = false, length = 10)
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Column(name = "password", nullable = false, length = 100)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "carrier1")
	public String getCarrier1() {
		return carrier1;
	}

	public void setCarrier1(String carrier1) {
		this.carrier1 = carrier1;
	}

	@Column(name = "carrier2")
	public String getCarrier2() {
		return carrier2;
	}

	public void setCarrier2(String carrier2) {
		this.carrier2 = carrier2;
	}

	@Column(name = "carrier3")
	public String getCarrier3() {
		return carrier3;
	}

	public void setCarrier3(String carrier3) {
		this.carrier3 = carrier3;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "carrier1", insertable = false, updatable = false)
	public Carriers getCarrierModel1() {
		return carrierModel1;
	}

	public void setCarrierModel1(Carriers carrierModel1) {
		this.carrierModel1 = carrierModel1;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "carrier2", insertable = false, updatable = false)
	public Carriers getCarrierModel2() {
		return carrierModel2;
	}

	public void setCarrierModel2(Carriers carrierModel2) {
		this.carrierModel2 = carrierModel2;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "carrier3", insertable = false, updatable = false)
	public Carriers getCarrierModel3() {
		return carrierModel3;
	}

	public void setCarrierModel3(Carriers carrierModel3) {
		this.carrierModel3 = carrierModel3;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "profile")
	public Set<UserRole> getUserRole() {
		return this.userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}

	@Override
	public String toString() {
		return "Profile [username=" + username + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", middleName=" + middleName
				+ ", suffix=" + suffix + ", agency=" + agency + ", email="
				+ email + ", phoneNo=" + phoneNo + "]";
	}
}
