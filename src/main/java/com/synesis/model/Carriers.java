package com.synesis.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "carriers")
public class Carriers implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String naic;
	private String name;
	
	private Set<Rates> rates = new HashSet<Rates>();

	public Carriers() {
	}

	@Id
	@Column(name = "naic", unique = true, nullable = false, length = 6)
	public String getNaic() {
		return naic;
	}

	public void setNaic(String naic) {
		this.naic = naic;
	}

	@Column(name = "name", nullable = false, length = 64)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy="naic")
	public Set<Rates> getRates() {
		return rates;
	}

	public void setRates(Set<Rates> rates) {
		this.rates = rates;
	}

	@Override
	public String toString() {
		return "Carriers [naic=" + naic + ", name=" + name + "]";
	}

}
