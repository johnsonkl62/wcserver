package com.synesis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zips")
public class Zips  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String zip;
	private String type;
	private String city;
	private String county;

	public Zips() {
	}

	@Id
	@Column(name = "zip", unique = true, nullable = false, length = 5)
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Column(name = "type", unique = true, nullable = false, length = 8)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "city", unique = true, nullable = false, length = 20)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "county", unique = true, nullable = false, length = 14)
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	@Override
	public String toString() {
		return "Zips [zip=" + zip + ", type=" + type + ", city=" + city
				+ ", county=" + county + "]";
	}

}
