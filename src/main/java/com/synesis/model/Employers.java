package com.synesis.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employers")
public class Employers  implements Serializable {

	private static final long serialVersionUID = 1L;
	private String fn;
	private String code;
	private String mm;
	private Date xdate;
	private String name;
	private String addr;
	private String city;
	private String state;
	private String zip;
	private String county;
	private String carrier;
	private Carriers carrierModel;
	private Timestamp updated;

	public Employers() {
	}

	@Id
	@Column(name = "fn", unique = true, nullable = false, length = 10)
	public String getFn() {
		return fn;
	}

	public void setFn(String fn) {
		this.fn = fn;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "code", insertable = false, updatable = false)
	@Column(name="code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "mm")
	public String getMm() {
		return mm;
	}

	public void setMm(String mm) {
		this.mm = mm;
	}

	@Column(name = "xdate")
	public Date getXdate() {
		return xdate;
	}

	public void setXdate(Date xdate) {
		this.xdate = xdate;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "addr")
	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	@Column(name = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "zip")
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Column(name = "county")
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	@Column(name = "carrier")
	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="carrier",referencedColumnName="name" ,insertable=false,updatable=false)
	public Carriers getCarrierModel() {
		return carrierModel;
	}

	public void setCarrierModel(Carriers carrierModel) {
		this.carrierModel = carrierModel;
	}

	@Column(name = "updated")
	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}


	@Override
	public String toString() {
		return "Employers [fn=" + fn + ", code=" + code + ", mm=" + mm
				+ ", xdate=" + xdate + ", name=" + name
				+ ", addr=" + addr + ", city=" + city + ", state="
				+ state + ", zip=" + zip + ", county=" + county
				+ ", carrier=" + carrier + ", updated=" + updated
			    + "]";
	}

}
