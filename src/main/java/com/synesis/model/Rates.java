package com.synesis.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "rates")
public class Rates implements Comparator<Rates>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String naic;
	private Codes code;
	private Double rate;
	private Date date;

	private Carriers carriers;
	
	public Rates() {
	}

	@Id
	@Column(name = "naic", unique = true, nullable = false, length = 5)
	public String getNaic() {
		return naic;
	}

	public void setNaic(String naic) {
		this.naic = naic;
	}
	
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="naic", nullable=false,insertable=false,updatable=false)
	public Carriers getCarriers() {
		return carriers;
	}

	public void setCarriers(Carriers carriers) {
		this.carriers = carriers;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "code",insertable=false,updatable=false)
	public Codes getCode() {
		return code;
	}

	public void setCode(Codes code) {
		this.code = code;
	}

	@Column(name = "rate", nullable = false)
	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Column(name = "date", nullable = false)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Rates [naic=" + naic + ", code=" + code + ", rate=" + rate
				+ ", date=" + date + "]";
	}

	@Override
	public int compare(Rates o1, Rates o2) {
		
		if(o1!=null && o2!=null) {
			return o1.getDate().compareTo(o2.getDate());
		}
		
		return 0;
	}

}
