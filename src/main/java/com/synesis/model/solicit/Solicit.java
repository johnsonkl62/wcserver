package com.synesis.model.solicit;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "solicit")
public class Solicit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private SolicitIdentity solicitIdentity;
	private String codes;
	private String zips;
	private Timestamp startDate;
	private Boolean isActive;


	@EmbeddedId	
	public SolicitIdentity getSolicitIdentity() {
		return solicitIdentity;
	}

	public void setSolicitIdentity(SolicitIdentity solicitIdentity) {
		this.solicitIdentity = solicitIdentity;
	}

	@Column(name = "codes", length=200, nullable = false)
	public String getCodes() {
		return codes;
	}

	public void setCodes(String codes) {
		this.codes = codes;
	}

	@Column(name = "zips", length=100)
	public String getZips() {
		return zips;
	}

	public void setZips(String zips) {
		this.zips = zips;
	}

	@Column(name = "startDate")
	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	@Column(name = "isActive")
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
