package com.synesis.model.solicit;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SolicitIdentity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String agency;
	private String email;
	private String county;
	
	
	@Column(name = "agency", length=32, nullable = false)
	public String getAgency() {
		return agency;
	}
	public void setAgency(String agency) {
		this.agency = agency;
	}
	
	@Column(name = "email", length=50, nullable = false)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "county", length=14, nullable = false)
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

}
