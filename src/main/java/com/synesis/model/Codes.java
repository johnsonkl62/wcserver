package com.synesis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "codes")
public class Codes implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String code;
	private String text;
	
	
	public Codes() {
	}

	@Id
	@Column(name = "code", unique = true, nullable = false, length = 6)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "text", nullable = false, length = 64)
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Codes [code=" + code + ", text=" + text + "]";
	}
	
}
