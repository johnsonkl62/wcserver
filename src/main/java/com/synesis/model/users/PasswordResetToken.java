package com.synesis.model.users;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.synesis.model.Profile;

@Entity
@Table(name = "passwordresettoken")
public class PasswordResetToken  implements Serializable {
  
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
    private String token;
    private Profile profile;
    private Date expiryDate;
    

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "token", unique = true, nullable = false, length = 45)
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@OneToOne(targetEntity = Profile.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "username")
	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	
	

	@Column(name = "expiryDate", nullable = false)
	public Date getExpiryDate() {
		return expiryDate;
	}

	
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
    
    
}

