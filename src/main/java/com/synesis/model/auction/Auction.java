package com.synesis.model.auction;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.synesis.utility.MonthEnum;

@Entity
@Table(name = "auction")
public class Auction  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String area;
	private MonthEnum month;
	private Double currentBid;
	private String currentBidder;
	private Double minimumBid;
	private Date awarded;

	public Auction() {
	}

	@Id
	@Column(name = "area", unique = true, nullable = false, length = 16)
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "month", nullable = false)
	public MonthEnum getMonth() {
		return month;
	}

	public void setMonth(MonthEnum month) {
		this.month = month;
	}

	@Column(name = "currentBid", nullable = false)
	public Double getCurrentBid() {
		return currentBid;
	}

	public void setCurrentBid(Double currentBid) {
		this.currentBid = currentBid;
	}

	@Column(name = "currentBidder", length = 50)
	public String getCurrentBidder() {
		return currentBidder;
	}

	public void setCurrentBidder(String currentBidder) {
		this.currentBidder = currentBidder;
	}

	@Column(name = "minimumBid", nullable = false)
	public Double getMinimumBid() {
		return minimumBid;
	}

	public void setMinimumBid(Double minimumBid) {
		this.minimumBid = minimumBid;
	}

	@Column(name = "awarded")
	public Date getAwarded() {
		return awarded;
	}

	public void setAwarded(Date awarded) {
		this.awarded = awarded;
	}

	@Override
	public String toString() {
		return "Auction [area=" + area + ", month=" + month + ", currentBid="
				+ currentBid + ", currentBidder=" + currentBidder
				+ ", minimumBid=" + minimumBid + ", awarded=" + awarded + "]";
	}

}
