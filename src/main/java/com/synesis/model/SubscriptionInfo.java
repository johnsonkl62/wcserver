package com.synesis.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
@Entity
@Proxy(lazy=false) 
@Table(name = "subscription")
public class SubscriptionInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 48747948057114669L;
	private Long id;
	private String active;
	private String username;
	private String county;
//	private String zip;
	private Date subscriptionDate;
	private String lastPaidMonth;
//	private String purchaseIds;
	private String subscr_id;
	private String item;

	private Profile profile;
	
	public SubscriptionInfo() {
	}

	@Id
	@Column(name = "id", unique = true, nullable = false, length = 50)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	
	/**
	 * @return the active
	 */
	@Column(name = "active")
	public String getActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}
	
	
	
	
	
	@Column(name = "username", nullable = false, length = 50)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "username", insertable = false, updatable = false)
	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}


	
	@Column(name = "county", nullable = false, length = 14)
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}


	
//	@Column(name = "zip")
//	public String getZip() {
//		return zip;
//	}

//	public void setZip(String zip) {
//		this.zip = zip;
//	}

	
	
	
	@Column(name = "subscriptionDate", nullable = false)
	public Date getSubscriptionDate() {
		return subscriptionDate;
	}
	
	public void setSubscriptionDate(Date subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}

	
	

	@Column(name = "lastPaidMonth", nullable = false)
	public String getLastPaidMonth() {
		return lastPaidMonth;
	}
	
	public void setLastPaidMonth(String lastPaidMonth) {
		this.lastPaidMonth = lastPaidMonth;
	}


	
	
//	@Column(name = "purchaseIds")
//	public String getPurchaseIds() {
//		return purchaseIds;
//	}

//	public void setPurchaseIds(String purchaseIds) {
//		this.purchaseIds = purchaseIds;
//	}


	
	
	@Column(name = "subscr_id", nullable = false)
	public String getSubscr_id() {
		return subscr_id;
	}

	public void setSubscr_id(String subscr_id) {
		this.subscr_id = subscr_id;
	}


	
	@Column(name = "item", nullable = false)
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	@Override
	public String toString() {
		return "Subscription [id=" + id + ", active=" + active + ", username=" + username + ", county=" + county + /*", zip=" + zip +*/ ", subscriptionDate=" + subscriptionDate + ", paidTo=" + lastPaidMonth + ", profile=" + profile + ", item=" + item + "]";
	}
	
}
